package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.CategoriaDAO;
import ar.gob.run.spring.model.Categoria;

@Service
@Transactional(readOnly = true)
public class CategoriaService extends GenericService<Categoria> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setCategoriaDAO(CategoriaDAO categoriaDAO){	
		this.setDao(categoriaDAO);
	}
	
	public CategoriaDAO getCategoriaDAO() {
		return (CategoriaDAO) this.getDao();
	}
	
	public List<Categoria> getAll() {
		return super.getAllActives(Categoria.class);
	}
	
	public Categoria get(Integer id){
		return this.get(id, Categoria.class);
	}

	public List<Categoria> getAllActivesWithExtra(Categoria elem) {
		return super.getAllActivesWithExtra(Categoria.class, elem);
	}
}
