package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.CategoriaMotivoIntervencionDAO;
import ar.gob.run.spring.model.CategoriaMotivoIntervencion;

@Service("CategoriaMotivoIntervencionService")
@Transactional(readOnly = true)
public class CategoriaMotivoIntervencionService extends GenericService<CategoriaMotivoIntervencion> {

	@Autowired
	public void setCategoriaMotivoIntervencionDAO(CategoriaMotivoIntervencionDAO categoriaMotivoIntervencionDAO){	
		this.setDao(categoriaMotivoIntervencionDAO);
	}
	
	public CategoriaMotivoIntervencionDAO getCategoriaMotivoIntervencionDAO() {
		return (CategoriaMotivoIntervencionDAO) this.getDao();
	}
	
	public List<CategoriaMotivoIntervencion> getAll() {
		return super.getAllActives(CategoriaMotivoIntervencion.class);
	}
	
	public CategoriaMotivoIntervencion get(Integer id){
		return this.get(id, CategoriaMotivoIntervencion.class);
	}

	public List<CategoriaMotivoIntervencion> getAllActivesWithExtra(CategoriaMotivoIntervencion elem) {
		return super.getAllActivesWithExtra(CategoriaMotivoIntervencion.class, elem);
	}
}
