package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.DerechoAfectado;
import ar.gob.run.spring.service.DerechoAfectadoService;

@ManagedBean(name="derechoAfectadoMB")
@ViewScoped
public class DerechoAfectadoManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{DerechoAfectadoService}")
    DerechoAfectadoService service;

    List<DerechoAfectado> list;

    DerechoAfectado instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(DerechoAfectado ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(DerechoAfectado ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new DerechoAfectado();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new DerechoAfectado();
    	editMode = false;
    }

	public DerechoAfectado getInstance() {
		return instance;
	}

	public void setInstance(DerechoAfectado instance) {
		this.instance = instance;
	}

	public DerechoAfectadoService getService() {
		return service;
	}

	public void setService(DerechoAfectadoService service) {
		this.service = service;
	}

	public List<DerechoAfectado> getList() {
		return getService().getAllActives(DerechoAfectado.class);
	}

	public void setList(List<DerechoAfectado> list) {
		this.list = list;
	}

	public Boolean getEditMode() {
		return editMode;
	}
	
}
