package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.InstitucionDAO;
import ar.gob.run.spring.model.institucion.Institucion;
		  
@Service("InstitucionService")
@Transactional(readOnly = true)
public class InstitucionService extends GenericService<Institucion>{

	
	@Autowired
	public void setInstitucionDAO(InstitucionDAO institucionDAO){
		this.setDao(institucionDAO);
	}

    public InstitucionDAO getInstitucionDAO() {
        return (InstitucionDAO) this.getDao();
    }

	public List<Institucion> getAllActivesWithExtra(Institucion elem) {
		return super.getAllActivesWithExtra(Institucion.class, elem);
	}
	
	public List<Institucion> getAll() {
		return super.getAllActives(Institucion.class);
	}
    
}
