package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Alerta;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.AlertaService;

@ManagedBean(name="alertaMB")
@ViewScoped
public class AlertaManagedBean implements Serializable{

	private static final long serialVersionUID = 3588506260878419736L;

    @ManagedProperty(value="#{AlertaService}")
    AlertaService alertaService;

	public AlertaService getAlertaService() {
		return alertaService;
	}

	public void setAlertaService(AlertaService alertaService) {
		this.alertaService = alertaService;
	}
	
    public List<Alerta> getList(){
    	Usuario usuario = ((MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsuario();
		return alertaService.getAlertas();
    }
    
    
    public String mostrarAlertas(){
    	Usuario usuario = ((MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsuario();
    	if (usuario.hasRole(Rol.ADMIN) || usuario.hasRole(Rol.ZONAL) ||
    			usuario.hasRole(Rol.SUPERVISOR))
    		return "true";
    	return "false";
    }
    
    public String mostrarAlertasPorVencer(){
    	Usuario usuario = ((MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsuario();
    	if ( usuario.hasRole(Rol.ZONAL))
    		return "true";
    	return "false";
    }
}
