package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.CategoriaTipoIntervencion;

@Repository
public class CategoriaTipoIntervencionDAO extends HibernateDAO<CategoriaTipoIntervencion,Integer> {

    public List<CategoriaTipoIntervencion> getCategoriasTipoIntervencion() {
        List<CategoriaTipoIntervencion> list = currentSession().createQuery("from CategoriaTipoIntervencion p order by p.descripcion").list();
        return list;
    }
}
