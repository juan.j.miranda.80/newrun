package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.DestinoEgresoDispositivo;
import ar.gob.run.spring.model.TipoCausaJudicial;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.DestinoEgresoDispositivoService;

@FacesConverter(value = "destinoEgresoDispositivoConverter")
public class DestinoEgresoDispositivoConverter extends GenericConverter<DestinoEgresoDispositivo>{

	public DestinoEgresoDispositivoConverter() {
		super(DestinoEgresoDispositivo.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<DestinoEgresoDispositivo> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{DestinoEgresoDispositivoService}", DestinoEgresoDispositivoService.class);

		DestinoEgresoDispositivoService service = (DestinoEgresoDispositivoService)vex.getValue(context.getELContext());
		return service;
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), DestinoEgresoDispositivo.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((DestinoEgresoDispositivo) value).getId().toString();
	}	


}
