package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_SALUD_FLIAR")
public class SaludFamiliar implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo 	controlPeriodicoTrimestral = SiNo.N;
	private SiNo 	accedeAMetodosAnticonceptivos = SiNo.N;
	private SiNo 	esMadrePadre = SiNo.N;
	private Integer cantidadHijos = 0;
	private String 	edadHijos = "";
	private SiNo 	requiereTratamientoSaludMental = SiNo.N;
	private SiNo 	recibioAtencionSaludMental = SiNo.N;
	private SiNo 	tieneCertificadoDiscapacidad = SiNo.N;
	private SiNo 	accedeATratamientoEspecial = SiNo.N;
	private String 	descripcionTratamientoEspecial = "";
	private String 	principalesDificultades = "";
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_SALUD_FLIAR")
	public Integer getId() {
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="CONTR_PERIOD_TRIM")
	public SiNo getControlPeriodicoTrimestral() {
		return controlPeriodicoTrimestral;
	}

	public void setControlPeriodicoTrimestral(SiNo controlPeriodicoTrimestral) {
		this.controlPeriodicoTrimestral = controlPeriodicoTrimestral;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ACC_MET_ANTICON")
	public SiNo getAccedeAMetodosAnticonceptivos() {
		return accedeAMetodosAnticonceptivos;
	}

	public void setAccedeAMetodosAnticonceptivos(SiNo accedeAMetodosAnticonceptivos) {
		this.accedeAMetodosAnticonceptivos = accedeAMetodosAnticonceptivos;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ES_MADRE_PADRE")
	public SiNo getEsMadrePadre() {
		return esMadrePadre;
	}

	public void setEsMadrePadre(SiNo esMadrePadre) {
		this.esMadrePadre = esMadrePadre;
	}

	@Column(name="CANT_HIJOS")
	public Integer getCantidadHijos() {
		return cantidadHijos;
	}

	public void setCantidadHijos(Integer cantidadHijos) {
		this.cantidadHijos = cantidadHijos;
	}

	@Column(name="EDAD_HIJOS")
	public String getEdadHijos() {
		return edadHijos;
	}

	public void setEdadHijos(String edadHijos) {
		this.edadHijos = edadHijos;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="REQ_TRAT_SALUD_M")
	public SiNo getRequiereTratamientoSaludMental() {
		return requiereTratamientoSaludMental;
	}

	public void setRequiereTratamientoSaludMental(SiNo requiereTratamientoSaludMental) {
		this.requiereTratamientoSaludMental = requiereTratamientoSaludMental;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="REC_AT_SALUD_M")
	public SiNo getRecibioAtencionSaludMental() {
		return recibioAtencionSaludMental;
	}

	public void setRecibioAtencionSaludMental(SiNo recibioAtencionSaludMental) {
		this.recibioAtencionSaludMental = recibioAtencionSaludMental;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_CERT_DISC")
	public SiNo getTieneCertificadoDiscapacidad() {
		return tieneCertificadoDiscapacidad;
	}

	public void setTieneCertificadoDiscapacidad(SiNo tieneCertificadoDiscapacidad) {
		this.tieneCertificadoDiscapacidad = tieneCertificadoDiscapacidad;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ACC_TRAT_ESP")
	public SiNo getAccedeATratamientoEspecial() {
		return accedeATratamientoEspecial;
	}

	public void setAccedeATratamientoEspecial(SiNo accedeATratamientoEspecial) {
		this.accedeATratamientoEspecial = accedeATratamientoEspecial;
	}

	@Column(name="DESC_TRAT_ESP")
	public String getDescripcionTratamientoEspecial() {
		return descripcionTratamientoEspecial;
	}

	public void setDescripcionTratamientoEspecial(String descripcionTratamientoEspecial) {
		this.descripcionTratamientoEspecial = descripcionTratamientoEspecial;
	}

	@Column(name="PPALES_DIFIC")
	public String getPrincipalesDificultades() {
		return principalesDificultades;
	}

	public void setPrincipalesDificultades(String principalesDificultades) {
		this.principalesDificultades = principalesDificultades;
	}
	
}
