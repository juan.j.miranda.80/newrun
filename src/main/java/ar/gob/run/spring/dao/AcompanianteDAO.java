package ar.gob.run.spring.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Acompaniante;
import ar.gob.run.spring.model.Categoria;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.model.TipoIntervencion;

@Repository
public class AcompanianteDAO extends HibernateDAO<Acompaniante,Integer> {

    public List<Acompaniante> getAcompaniantes() {
        List<Acompaniante> list = currentSession().createQuery("from Acompaniante c order by c.apellido").list();
        return list;
    }

	public List<Acompaniante> getAcompaniantesByFilter(String strQuery) {
		Query query = currentSession().createQuery("from Acompaniante z where "
				+ "((upper(z.apellido) like :query) or (upper(z.nombre) like :query) or (upper(z.dni) like :query))"
				+ "and z.activo = TRUE " 
				+ "order by z.apellido asc").
				setParameter("query", "%"+strQuery.toUpperCase()+"%");
		
		List<Acompaniante> list = query.list();
		return list;
	}
}
