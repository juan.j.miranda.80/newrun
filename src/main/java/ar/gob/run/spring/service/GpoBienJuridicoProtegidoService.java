package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.GpoBienJuridicoProtegidoDAO;
import ar.gob.run.spring.model.GpoBienJuridicoProtegido;

@Service("GpoBienJuridicoProtegidoService")
@Transactional(readOnly = true)
public class GpoBienJuridicoProtegidoService extends GenericService<GpoBienJuridicoProtegido> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setGpoBienJuridicoProtegidoDAO(GpoBienJuridicoProtegidoDAO dao){	
		this.setDao(dao);
	}
	
	public GpoBienJuridicoProtegidoDAO getGpoBienJuridicoProtegidoDAO() {
		return (GpoBienJuridicoProtegidoDAO) this.getDao();
	}
	
	public List<GpoBienJuridicoProtegido> getAll() {
		return super.getAllActives(GpoBienJuridicoProtegido.class);
	}
	
	public GpoBienJuridicoProtegido get(Integer id){
		return this.get(id, GpoBienJuridicoProtegido.class);
	}

}
