package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.legajo.CausaJudicial;

@Repository
public class CausaJudicialDAO  extends HibernateDAO<CausaJudicial, Integer>{
	    
}