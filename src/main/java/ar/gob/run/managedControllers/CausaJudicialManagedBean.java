package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.legajo.CausaJudicial;
import ar.gob.run.spring.service.CausaJudicialService;

@ManagedBean(name="causaJudicialMB")
@SessionScoped
public class CausaJudicialManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";

    @ManagedProperty(value="#{CausaJudicialService}")
    CausaJudicialService service;
    
	@ManagedProperty(value="#{legajoMB}")
	LegajoManagedBean legajoMB;

    List<CausaJudicial> list;

    CausaJudicial instance;
    
    public String save() {
        try {
            getService().save(instance);
            return LISTADO;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return ERROR;
    }

	public void edit(CausaJudicial ent){
    	this.instance = ent;
    }
    
    public void delete(CausaJudicial ent){
    	getList().remove(ent);
    }
    
    public String reset() {
    	this.instance = new CausaJudicial();
        return EDITOR;
    }
    
    public String cancel(){
    	this.instance = new CausaJudicial();
    	return LISTADO;
    }

	public CausaJudicial getInstance() {
		if (instance==null)
			instance = new CausaJudicial();
		return instance;
	}

	public void setInstance(CausaJudicial instance) {
		this.instance = instance;
	}

	public CausaJudicialService getService() {
		return service;
	}

	public void setService(CausaJudicialService service) {
		this.service = service;
	}

	public List<CausaJudicial> getList() {
		return legajoMB.getInstance().getCausasJudiciales();
	}

	public void setList(List<CausaJudicial> list) {
		this.list = list;
	}

    public void causaSeleccionada() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Causa Judicial Seleccionada", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
        if (!this.getLegajoMB().getInstance().getCausasJudiciales().contains(getInstance()))
        	this.getLegajoMB().getInstance().getCausasJudiciales().add(getInstance());
        this.instance=new CausaJudicial();
    }
    
	public LegajoManagedBean getLegajoMB() {
		return legajoMB;
	}

	public void setLegajoMB(LegajoManagedBean legajoMB) {
		this.legajoMB = legajoMB;
	}

    
}
