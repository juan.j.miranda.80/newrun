package ar.gob.run.spring.converter;

import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Sexo;

@FacesConverter(value = "sexoConverter")
public class SexoConverter extends EnumConverter {

	public SexoConverter() {
		super(Sexo.class);
	}


}
