package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.service.MunicipioService;

@ManagedBean(name="municipioMB")
@ViewScoped
public class MunicipioManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{municipioService}")
    MunicipioService service;

    List<Municipio> list;

    Municipio instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Municipio ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Municipio ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Municipio();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Municipio();
    	editMode = false;
    }

	public Municipio getInstance() {
		return instance;
	}

	public void setInstance(Municipio instance) {
		this.instance = instance;
	}

	public MunicipioService getService() {
		return service;
	}

	public void setService(MunicipioService service) {
		this.service = service;
	}

	public List<Municipio> getList() {
		return getService().getAllActives(Municipio.class);
	}

	public void setList(List<Municipio> list) {
		this.list = list;
	}


	
}
