package ar.gob.run.spring.model;

public class Alerta {

	private Zonal zonal;

	private Integer pendientes102 = 0;
	private Integer cant45 = 0;
	private Integer cant90 = 0;
	private Integer cant120 = 0;
	private Integer cant150 = 0;
	private Integer cant180 = 0;
	private Integer cantPorVencer45 = 0;
	private Integer cantPorVencer90 = 0;
	private Integer cantPorVencer120 = 0;
	private Integer cantPorVencer150 = 0;
	private Integer vencidas = 0;
	private Integer cantPendientes102 = 0;
	
	private String accion45;
	private String accion90;
	private String accion120;
	private String accion150;
	private String accionPorVencer;
	private String accionPendientes102;
	private String accionVencidas;
	
	public Alerta(Zonal zonal2) {
		this.zonal = zonal2;
	}

	public Zonal getZonal() {
		return zonal;
	}
	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}
	public Integer getCant45() {
		return cant45;
	}
	public void setCant45(Integer cant45) {
		this.cant45 = cant45;
	}
	public Integer getCant90() {
		return cant90;
	}
	public void setCant90(Integer cant90) {
		this.cant90 = cant90;
	}
	public Integer getCant120() {
		return cant120;
	}
	public void setCant120(Integer cant120) {
		this.cant120 = cant120;
	}
	public Integer getCant150() {
		return cant150;
	}
	
	public void setCant150(Integer cant150) {
		this.cant150 = cant150;
	}

	
	
	public Integer getCant180() {
		return cant180;
	}

	public void setCant180(Integer cant180) {
		this.cant180 = cant180;
	}

	public String getAccion45() {
		return accion45;
	}
	public void setAccion45(String accion45) {
		this.accion45 = accion45;
	}
	public String getAccion90() {
		return accion90;
	}
	public void setAccion90(String accion90) {
		this.accion90 = accion90;
	}
	public String getAccion120() {
		return accion120;
	}
	public void setAccion120(String accion120) {
		this.accion120 = accion120;
	}
	public String getAccion150() {
		return accion150;
	}
	public void setAccion150(String accion150) {
		this.accion150 = accion150;
	}
	public String getAccionPorVencer() {
		return accionPorVencer;
	}
	public void setAccionPorVencer(String accionPorVencer) {
		this.accionPorVencer = accionPorVencer;
	}
	public Integer getPendientes102() {
		return pendientes102;
	}
	public void setPendientes102(Integer pendientes102) {
		this.pendientes102 = pendientes102;
	}
	public String getAccionPendientes102() {
		return accionPendientes102;
	}
	public void setAccionPendientes102(String accionPendientes102) {
		this.accionPendientes102 = accionPendientes102;
	}
	public Integer getVencidas() {
		return vencidas;
	}
	public void setVencidas(Integer vencidas) {
		this.vencidas = vencidas;
	}
	public String getAccionVencidas() {
		return accionVencidas;
	}
	public void setAccionVencidas(String accionVencidas) {
		this.accionVencidas = accionVencidas;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getZonal()==null || this.getZonal().getId()==null) return false;
		return this.getZonal().getId().equals(((Alerta)obj).getZonal().getId());
	}

	public Integer getCantPorVencer45() {
		return cantPorVencer45;
	}

	public void setCantPorVencer45(Integer cantPorVencer45) {
		this.cantPorVencer45 = cantPorVencer45;
	}

	public Integer getCantPorVencer90() {
		return cantPorVencer90;
	}

	public void setCantPorVencer90(Integer cantPorVencer90) {
		this.cantPorVencer90 = cantPorVencer90;
	}

	public Integer getCantPorVencer120() {
		return cantPorVencer120;
	}

	public void setCantPorVencer120(Integer cantPorVencer120) {
		this.cantPorVencer120 = cantPorVencer120;
	}

	public Integer getCantPorVencer150() {
		return cantPorVencer150;
	}

	public void setCantPorVencer150(Integer cantPorVencer150) {
		this.cantPorVencer150 = cantPorVencer150;
	}

	public Integer getCantPendientes102() {
		return cantPendientes102;
	}

	public void setCantPendientes102(Integer cantPendientes102) {
		this.cantPendientes102 = cantPendientes102;
	}

	
	
}
