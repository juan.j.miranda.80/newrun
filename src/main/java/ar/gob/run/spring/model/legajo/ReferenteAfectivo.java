package ar.gob.run.spring.model.legajo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.Sexo;

@Entity
@Table(name="T_REFERENTE_AFECTIVO")
public class ReferenteAfectivo implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private TipoVinculoFamiliar tipoVinculoFamiliar;
	private String apellidoYNombre;
	private String documento;
	private Sexo sexo;
	private EstadoCivil estadoCivil;
	private Boolean representanteLegal = false;
	private String telefono;
	private String direccion;
	private EstudioCursado estudioCursado;
	private SituacionLaboral situacionLaboral;

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_REFERENTE_AFECTIVO")
	public Integer getId() {
		return this.id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_VINCULO_FAMILIAR")
	public TipoVinculoFamiliar getTipoVinculoFamiliar() {
		return tipoVinculoFamiliar;
	}

	public void setTipoVinculoFamiliar(TipoVinculoFamiliar tipoVinculoFamiliar) {
		this.tipoVinculoFamiliar = tipoVinculoFamiliar;
	}

	@Column(name="APELLIDO_Y_NOMBRE")
	public String getApellidoYNombre() {
		return apellidoYNombre;
	}

	public void setApellidoYNombre(String apellidoYNombre) {
		this.apellidoYNombre = apellidoYNombre;
	}

	@Column(name="DOCUMENTO")
	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="SEXO")
	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ESTADO_CIVIL")
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	@Type(type= "org.hibernate.type.NumericBooleanType")
	@Column(name="REPRESENTANTE_LEGAL")
	public Boolean getRepresentanteLegal() {
		return representanteLegal;
	}

	public void setRepresentanteLegal(Boolean representanteLegal) {
		this.representanteLegal = representanteLegal;
	}

	@Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name="DIRECCION")
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ESTUDIO_CURSADO")
	public EstudioCursado getEstudioCursado() {
		return estudioCursado;
	}

	public void setEstudioCursado(EstudioCursado estudioCursado) {
		this.estudioCursado = estudioCursado;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_SITUACION_LABORAL")
	public SituacionLaboral getSituacionLaboral() {
		return situacionLaboral;
	}

	public void setSituacionLaboral(SituacionLaboral situacionLaboral) {
		this.situacionLaboral = situacionLaboral;
	}

}
