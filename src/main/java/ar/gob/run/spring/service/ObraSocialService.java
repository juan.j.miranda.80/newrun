package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.ObraSocialDAO;
import ar.gob.run.spring.model.ObraSocial;
		  
@Service("ObraSocialService")
@Transactional(readOnly = true)
public class ObraSocialService extends GenericService<ObraSocial>{

	
	@Autowired
	public void setObraSocialDAO(ObraSocialDAO obraSocialDAO){
		this.setDao(obraSocialDAO);
	}

    public ObraSocialDAO getObraSocialDAO() {
        return (ObraSocialDAO) this.getDao();
    }

	public List<ObraSocial> getAll() {
		return super.getAllActives(ObraSocial.class);
	}

	public List<ObraSocial> getAllActivesWithExtra(ObraSocial elem) {
		return super.getAllActivesWithExtra(ObraSocial.class, elem);
	}

    
}
