package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.IntervencionMEPDAO;
import ar.gob.run.spring.model.IntervencionMEP;
import ar.gob.run.spring.model.Local;
		  
@Service("IntervencionMEPService")
@Transactional(readOnly = true)
public class IntervencionMEPService extends GenericService<IntervencionMEP>{

	
	@Autowired
	public void setIntervencionMEPDAO(IntervencionMEPDAO intervencionDAO){
		this.setDao(intervencionDAO);
	}

    public IntervencionMEPDAO getIntervencionMEPDAO() {
        return (IntervencionMEPDAO) this.getDao();
    }

    public List<IntervencionMEP> getList(Integer legajo){
    	return getIntervencionMEPDAO().getList(legajo);
    }
    
    @Override
    public List<IntervencionMEP> getAllActives(Class type) {
    	return getIntervencionMEPDAO().getList();
    }

	public IntervencionMEP get(Integer id) {
		return super.get(id, IntervencionMEP.class);
	}

}
