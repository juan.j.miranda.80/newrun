package ar.gob.run.spring.model;

public enum TipoCircuito {

	MPI("Medida de protección integral","MPI"),
	MPE("Medida de protección excepcional","MPE"),
	MEP("Medida de ejecución penal","MEP"),
	PAE("Programa de acompañanmiento para el egreso","PAE");
	
	private String descripcion;
	private String code;

	private TipoCircuito(String descripcion, String code) {
		this.descripcion = descripcion;
		this.code = code;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static TipoCircuito fromString(String text) {
		if (text==null) return null;
	    for (TipoCircuito b : TipoCircuito.values()) {
	      if (b.code.equalsIgnoreCase(text)) {
	        return b;
	      }
	    }
	    return null;
	  }
	
}
