/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.gob.run.spring.model.usuario.cidi;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

/**
 *
 * @author 20261513650
 */
public class CalculadorTokken {

    private String TimeStamp;

    public String NuevoTokken(String ClaveAp) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        //genera el token y el timeStamp
        //formato yyyyMMddHHmmssSSS

        DateFormat formato;
        Date fecha;
        byte[] bytTimeSClavAp;
        String timCla;
        MessageDigest digesto;
        byte[] shaDigesto;
        Formatter formateador;
        String resultado;

        formato = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        fecha = new Date();

        setTimeStamp(formato.format(fecha));

        timCla = getTimeStamp() + ClaveAp;

        bytTimeSClavAp = timCla.getBytes("US-ASCII");

        digesto = MessageDigest.getInstance("SHA-1");

        shaDigesto = digesto.digest(bytTimeSClavAp);

        formateador = new Formatter();
        for (byte b : shaDigesto) {
            formateador.format("%02x", b);
        }

        resultado = formateador.toString();

        return resultado.replaceAll("-", "").toUpperCase();

    }

    /**
     * @return the TimeStamp
     */
    public String getTimeStamp() {
        return TimeStamp;
    }

    /**
     * @param TimeStamp the TimeStamp to set
     */
    public void setTimeStamp(String TimeStamp) {
        this.TimeStamp = TimeStamp;
    }

}
