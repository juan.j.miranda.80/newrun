package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.managedControllers.LegajoManagedBean;
import ar.gob.run.spring.model.Persona;
import ar.gob.run.spring.service.LegajoService;
import ar.gob.run.spring.service.PersonaService;

@FacesConverter(value = "personaConverter")
public class PersonaConverter implements Converter {

	@ManagedProperty(value="#{legajoMB}")
    LegajoManagedBean legajoMB;
	
	
	
	public LegajoManagedBean getLegajoMB() {
		return legajoMB;
	}

	public void setLegajoMB(LegajoManagedBean legajoMB) {
		this.legajoMB = legajoMB;
	}

	public PersonaConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Integer idTemp = null;
		Persona pers = new Persona();
		if (value==null) return pers;
		try {
			idTemp = Integer.valueOf(value);
			pers = getService(context).get(idTemp, Persona.class);
		}catch(Exception e){
			//pers.setApellidoYNombre(value);
		}
		return pers;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null || ((Persona) value).getId()==null) return "";
		return ((Persona) value).getId().toString();
	}

	public PersonaService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{PersonaService}", PersonaService.class);

			PersonaService service = (PersonaService)vex.getValue(context.getELContext());
			return service;
	}

}
