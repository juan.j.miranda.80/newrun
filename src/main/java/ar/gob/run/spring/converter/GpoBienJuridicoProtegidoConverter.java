package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.GpoBienJuridicoProtegido;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.GpoBienJuridicoProtegidoService;

@FacesConverter(value = "gpoBienJuridicoProtegidoConverter")
public class GpoBienJuridicoProtegidoConverter extends GenericConverter<GpoBienJuridicoProtegido>{

	public GpoBienJuridicoProtegidoConverter() {
		super(GpoBienJuridicoProtegido.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<GpoBienJuridicoProtegido> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{GpoBienJuridicoProtegidoService}", GpoBienJuridicoProtegidoService.class);

		GpoBienJuridicoProtegidoService service = (GpoBienJuridicoProtegidoService)vex.getValue(context.getELContext());
		return service;
	}

	


}
