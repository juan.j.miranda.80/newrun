package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.DestinoEgresoDispositivo;
import ar.gob.run.spring.service.DestinoEgresoDispositivoService;

@ManagedBean(name="destinoEgresoDispositivoMB")
@ViewScoped
public class DestinoEgresoDispositivoManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{DestinoEgresoDispositivoService}")
    DestinoEgresoDispositivoService service;

    List<DestinoEgresoDispositivo> list;

    DestinoEgresoDispositivo instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(DestinoEgresoDispositivo ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(DestinoEgresoDispositivo ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new DestinoEgresoDispositivo();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new DestinoEgresoDispositivo();
    	editMode = false;
    }

	public DestinoEgresoDispositivo getInstance() {
		return instance;
	}

	public void setInstance(DestinoEgresoDispositivo instance) {
		this.instance = instance;
	}

	public DestinoEgresoDispositivoService getService() {
		return service;
	}

	public void setService(DestinoEgresoDispositivoService service) {
		this.service = service;
	}

	public List<DestinoEgresoDispositivo> getList() {
		return getService().getAllActives(DestinoEgresoDispositivo.class);
	}

	public void setList(List<DestinoEgresoDispositivo> list) {
		this.list = list;
	}


	
}
