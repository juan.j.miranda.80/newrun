package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.service.LegajoService;

@FacesConverter(value = "legajoConverter")
public class LegajoConverter implements Converter{

	public LegajoConverter() {
	}

	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), Legajo.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((Legajo) value).getId().toString();
	}

	public LegajoService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{legajoService}", LegajoService.class);

			LegajoService service = (LegajoService)vex.getValue(context.getELContext());
			return service;
	}

}
