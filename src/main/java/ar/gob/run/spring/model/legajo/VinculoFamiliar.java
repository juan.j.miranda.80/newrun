package ar.gob.run.spring.model.legajo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.Legajo;

@Entity
@Table(name="T_VINCULO_FAMILIAR")
public class VinculoFamiliar implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private TipoVinculoFamiliar tipoVinculoFamiliar;
	private Legajo legajoFamiliar;
	
	
	
	public VinculoFamiliar() {
		super();
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_VINCULO_FAMILIAR")
	public Integer getId() {
		return this.id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_VINCULO_FAMILIAR")
	public TipoVinculoFamiliar getTipoVinculoFamiliar() {
		return tipoVinculoFamiliar;
	}

	public void setTipoVinculoFamiliar(TipoVinculoFamiliar tipoVinculoFamiliar) {
		this.tipoVinculoFamiliar = tipoVinculoFamiliar;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LEGAJO_FAMILIAR")
	public Legajo getLegajoFamiliar() {
		return legajoFamiliar;
	}

	public void setLegajoFamiliar(Legajo legajoFamiliar) {
		this.legajoFamiliar = legajoFamiliar;
	}

	
	
}
