package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.MunicipioDAO;
import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.model.Provincia;

@Service
@Transactional(readOnly = true)
public class MunicipioService extends GenericService<Municipio> {

	@Autowired
	public void setMunicipioDAO(MunicipioDAO MunicipioDAO){	
		this.setDao(MunicipioDAO);
	}
	
	public MunicipioDAO getMunicipioDAO() {
		return (MunicipioDAO) this.getDao();
	}
	
	public List<Municipio> getAll() {
		return super.getAllActives(Municipio.class);
	}
	
	public Municipio get(Integer id){
		return this.get(id, Municipio.class);
	}

	public List<Municipio> getAllActivesWithExtra(Municipio elem) {
		return super.getAllActivesWithExtra(Municipio.class, elem);
	}

	public List<Municipio> getByFilter(Provincia prov) {
		return getMunicipioDAO().getByFilter(prov);
	}
}
