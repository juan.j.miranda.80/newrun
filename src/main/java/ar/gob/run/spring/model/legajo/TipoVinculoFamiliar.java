package ar.gob.run.spring.model.legajo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.DatosAuditoria;
import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;

@Entity
@Table(name="T_TIPO_VINCULO_FAMILIAR")
public class TipoVinculoFamiliar extends ObjetoABM implements  Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String descripcion;
	
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_TIPO_VINCULO_FAMILIAR")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
