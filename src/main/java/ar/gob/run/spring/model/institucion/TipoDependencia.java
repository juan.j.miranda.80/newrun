package ar.gob.run.spring.model.institucion;

public enum TipoDependencia {
	OFICIAL("Programa Oficial","OFI"), ONG("ONG","ONG");
	
	private String descripcion;
	private String code;

	private TipoDependencia(String descripcion, String code) {
		this.descripcion = descripcion;
		this.code = code;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static TipoDependencia fromString(String text) {
		if (text==null) return null;
	    for (TipoDependencia b : TipoDependencia.values()) {
	      if (b.code.equalsIgnoreCase(text)) {
	        return b;
	      }
	    }
	    return null;
	  }
}
