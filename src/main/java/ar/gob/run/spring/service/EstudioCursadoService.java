package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.EstudioCursadoDAO;
import ar.gob.run.spring.model.legajo.EstudioCursado;
		  
@Service("EstudioCursadoService")
@Transactional(readOnly = true)
public class EstudioCursadoService extends GenericService<EstudioCursado>{

	
	@Autowired
	public void setEstudioCursadoDAO(EstudioCursadoDAO estudioCursadoDAO){
		this.setDao(estudioCursadoDAO);
	}

    public EstudioCursadoDAO getEstudioCursadoDAO() {
        return (EstudioCursadoDAO) this.getDao();
    }

	public List<EstudioCursado> getAllActivesWithExtra(EstudioCursado elem) {
		return super.getAllActivesWithExtra(EstudioCursado.class, elem);
	}

    
}
