package ar.gob.run.spring.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.operador102.EstadoLlamada;
import ar.gob.run.spring.model.operador102.Llamada102;

@Repository
public class Llamada102DAO extends HibernateDAO<Llamada102,Integer> {
	
    public List<Llamada102> getLlamadas() {
        List<Llamada102> list = currentSession().createQuery("from Llamada102 l where l.activo=true order by l.id desc").list();
        return list;
    }

	public List<Llamada102> getPendientesByZonal(Zonal zonal) {
		Query query = currentSession().createQuery("from Llamada102 l where l.activo=true "
				+ (zonal!=null?"and l.zonal = :zonal ":"")
				+ "and estado = 0 order by l.id desc");
		if (zonal!=null)
			query.setParameter("zonal", zonal);
		return query.list();
	}
	
	public List<Llamada102> getPendientes() {
		return getPendientesByZonal(null);
	}
	
	public List<Llamada102> getAllByZonal(Zonal zonal) {
		List<Llamada102> list = currentSession().createQuery("from Llamada102 l where l.zonal = :zonal and l.activo=true "
				+ " order by l.id desc")
				.setParameter("zonal", zonal)
				.list();
		return list;
	}

	public List<Llamada102> getRptLlamadas(Date fechaNotifDesde, Date fechaNotifHta, Integer nroNotifDesde,
			Integer nroNotifHta, Zonal zonal, EstadoLlamada estadoLLamada) {
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("from Llamada102 l where l.activo=true "
                		+ (fechaNotifDesde!=null?" and fecha >= :fechaNotifDesde ":"")
                		+ (fechaNotifHta!=null?" and fecha <= :fechaNotifHta ":"")
                		+ (nroNotifDesde!=null?" and id >= :nroNotifDesde ":"")
                		+ (nroNotifHta!=null?" and id <= :nroNotifHta ":"")
                		+ (zonal!=null?" and zonal = :zonal ":"")
                		+ (estadoLLamada!=null?" and estado = :estadoLlamada ":""));
		if (fechaNotifDesde!=null) query.setDate("fechaNotifDesde", fechaNotifDesde);
		if (fechaNotifHta!=null) query.setDate("fechaNotifHta", fechaNotifHta);
		if (nroNotifDesde!=null) query.setInteger("nroNotifDesde", nroNotifDesde);
		if (nroNotifHta!=null) query.setInteger("nroNotifHta", nroNotifHta);
		if (zonal!=null) query.setParameter("zonal", zonal);
		if (estadoLLamada!=null) query.setParameter("estadoLlamada", estadoLLamada);
		return query.list();
	}

	
}