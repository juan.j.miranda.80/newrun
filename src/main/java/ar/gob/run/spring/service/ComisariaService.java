package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.ComisariaDAO;
import ar.gob.run.spring.model.Comisaria;

@Service("ComisariaService")
@Transactional(readOnly = true)
public class ComisariaService extends GenericService<Comisaria> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setComisariaDAO(ComisariaDAO dao){	
		this.setDao(dao);
	}
	
	public ComisariaDAO getComisariaDAO() {
		return (ComisariaDAO) this.getDao();
	}
	
	public List<Comisaria> getAll() {
		return super.getAllActives(Comisaria.class);
	}
	
	public Comisaria get(Integer id){
		return this.get(id, Comisaria.class);
	}

}
