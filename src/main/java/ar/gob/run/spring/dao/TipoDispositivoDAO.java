package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.ExperienciaAcomp;
import ar.gob.run.spring.model.TipoDispositivo;

@Repository
public class TipoDispositivoDAO extends HibernateDAO<TipoDispositivo,Integer> {

    public List<TipoDispositivo> getTiposDispositivos() {
        List<TipoDispositivo> list = currentSession().createQuery("from TipoDispositivo c order by c.order").list();
        return list;
    }
}
