package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Localidad;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.LocalidadService;

@FacesConverter(value = "localidadConverter")
public class LocalidadConverter extends GenericConverter<Localidad>{

	public LocalidadConverter() {
		super(Localidad.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Localidad> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{localidadService}", LocalidadService.class);

		LocalidadService LocalidadService = (LocalidadService)vex.getValue(context.getELContext());
		return LocalidadService;
	}

	


}
