package ar.gob.run.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.CategoriaMotivoIntervencion;
import ar.gob.run.spring.model.MotivoIntervencion;

@Repository
public class MotivoIntervencionDAO  extends HibernateDAO<MotivoIntervencion, Integer>{

	public List<MotivoIntervencion> getAllByCatego(CategoriaMotivoIntervencion categoTpo) {
		Query query = currentSession().createQuery("from MotivoIntervencion z where "
				+ "z.categoriaMotivoIntervencion = :categoTpo "
				+ "order by z.descripcion asc").
				setParameter("categoTpo", categoTpo);
				
		List<MotivoIntervencion> list = query.list();
		return list;
	}
	    
}