package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.legajo.informePAE.InformePAE;
import ar.gob.run.spring.service.CommonService;
import ar.gob.run.spring.service.InformePAEService;

@ManagedBean(name="informePAEMB")
@SessionScoped
public class InformePAEManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";
    
    @ManagedProperty(value="#{InformePAEService}")
    InformePAEService service;

    List<InformePAE> list;

    InformePAE instance;
    
    Legajo legajoActual;
    
    @ManagedProperty(value="#{commonService}")
	CommonService commonService;
    
    public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}
	
    public String save() {
        try {
            getService().save(instance);
            return LISTADO;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return ERROR;
    }

	public String edit(InformePAE ent){
    	this.instance = ent;
    	return EDITOR;
    }
	
	public String addNew(Legajo ent){
    	this.instance = new InformePAE(ent);
    	return EDITOR;
    }
    
    public String delete(InformePAE ent){
    	getService().delete(ent);
    	return LISTADO;
    }
    
    public String reset() {
    	this.instance = new InformePAE(this.legajoActual);
        return EDITOR;
    }
    
    public String cancel(){
    	this.instance = new InformePAE();
    	return LISTADO;
    }

	public InformePAE getInstance() {
		return instance;
	}

	public void setInstance(InformePAE instance) {
		this.instance = instance;
	}

	public InformePAEService getService() {
		return service;
	}

	public void setService(InformePAEService service) {
		this.service = service;
	}

	public List<InformePAE> getList() {
		return getService().getAllActives(InformePAE.class);
	}

	public void setList(List<InformePAE> list) {
		this.list = list;
	}

	public String getList(Legajo legajo){
		this.legajoActual = legajo;
		this.list = getService().getInformesPAEByLegajo(legajo.getId());
		return LISTADO;
	}
}
