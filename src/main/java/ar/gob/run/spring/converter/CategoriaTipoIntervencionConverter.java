package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.CategoriaTipoIntervencion;
import ar.gob.run.spring.service.CategoriaTipoIntervencionService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "categoriaTipoIntervencionConverter")
public class CategoriaTipoIntervencionConverter extends GenericConverter<CategoriaTipoIntervencion>{

	public CategoriaTipoIntervencionConverter() {
		super(CategoriaTipoIntervencion.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<CategoriaTipoIntervencion> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{CategoriaTipoIntervencionService}", CategoriaTipoIntervencionService.class);

		CategoriaTipoIntervencionService categoriaTipoIntervencionService = (CategoriaTipoIntervencionService)vex.getValue(context.getELContext());
		return categoriaTipoIntervencionService;
	}

	


}
