package ar.gob.run.spring.model.legajo;

public enum EstadoCausaJudicial {

	SE_DESCONOCE("Se desconoce"), ABIERTA("Abierta"), CERRADA("Cerrada");
	
	String descripcion;

	private EstadoCausaJudicial(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getDescripcion(){
		return this.descripcion;
	}
	
}
