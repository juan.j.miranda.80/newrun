package ar.gob.run.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Localidad;
import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.model.Provincia;

@Repository
public class LocalidadDAO extends HibernateDAO<Localidad,Integer> {

    public List<Localidad> getLocalidades() {
        List<Localidad> list = currentSession().createQuery("from Localidad p where p.activo = TRUE order by p.nombre").list();
        return list;
    }

	public List<Localidad> getByFilter(Provincia prov, Municipio muni) {
        Query query = currentSession().createQuery("from Localidad p "
        		+ "where p.activo = TRUE "
        		+ (prov!=null?" and p.provincia =  :provincia":"")
        		+ (muni!=null?" and p.municipio =  :municipio":"")
        		+ " order by p.nombre");
        
        if (prov!=null) query.setParameter("provincia", prov);
        if (muni!=null) query.setParameter("municipio", muni);
        
        return query.list();
	}
}
