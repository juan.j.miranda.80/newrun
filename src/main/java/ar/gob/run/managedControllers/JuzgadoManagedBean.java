package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Juzgado;
import ar.gob.run.spring.service.JuzgadoService;

@ManagedBean(name="juzgadoMB")
@ViewScoped
public class JuzgadoManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";

    @ManagedProperty(value="#{JuzgadoService}")
    JuzgadoService service;

    List<Juzgado> list;

    Juzgado instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
            getService().save(instance);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Juzgado ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Juzgado ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Juzgado();
    	editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Juzgado();
    	editMode = false;
    }

	public Juzgado getInstance() {
		return instance;
	}

	public void setInstance(Juzgado instance) {
		this.instance = instance;
	}

	public JuzgadoService getService() {
		return service;
	}

	public void setService(JuzgadoService service) {
		this.service = service;
	}

	public List<Juzgado> getList() {
		return getService().getAllActives(Juzgado.class);
	}

	public void setList(List<Juzgado> list) {
		this.list = list;
	}


	
}
