package ar.gob.run.spring.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.LegajoDAO;
import ar.gob.run.spring.model.Familia;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Persona;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.institucion.Institucion;
import ar.gob.run.spring.model.legajo.RelacionVincular;
import ar.gob.run.spring.model.usuario.Usuario;
		  
@Service
public class LegajoService extends GenericService<Legajo>{

	@Autowired
	Environment environment;
	
	@Value("${tipoIntervencion.idPER45}")
	private String idPER45;
	
	@Value("${tipoIntervencion.idPER90}")
	private String idPER90;
	
	@Value("${tipoIntervencion.idPER120}")
	private String idPER120;
	
	@Value("${tipoIntervencion.idPER150}")
	private String idPER150;
	
	@Autowired
	public void setLegajoDAO(LegajoDAO legajoDAO){
		this.setDao(legajoDAO);
	}

    public LegajoDAO getLegajoDAO() {
        return (LegajoDAO) this.getDao();
    }

    public List<Legajo> getLegajos(){
    	return getLegajoDAO().getLegajos();
    }

    @Transactional(readOnly = true)
	public String getNuevoCodigo(Legajo l) {
		return getLegajoDAO().getNuevoCodigo(l);
	}
	
	//excluye al legajo ingresado y a los vinculos familiares ya agregados
	public List<Legajo> getLegajosByFilterWithoutExtra(String query, Legajo elem){
		List<Legajo> lstExcluir = new ArrayList<Legajo>();
		List<Legajo> listResult = new ArrayList<Legajo>();
		lstExcluir.add(elem);
//		for (VinculoFamiliar vfElem : elem.getVinculosFamiliares()) {
//			lstExcluir.add(vfElem.getLegajoFamiliar());
//		}
		for (RelacionVincular rVElem : elem.getRelacionesVinculares()) {
			if (rVElem.getLegajoFamiliar()!=null)
				lstExcluir.add(rVElem.getLegajoFamiliar());
		}
		List<Legajo> listDB = getLegajoDAO().getLegajosByFilter(query);
		for (Legajo legajo : listDB) {
			if (!lstExcluir.contains(legajo))
				listResult.add(legajo);
		} 
		return listResult;
	}
    
	public List<Legajo> getLegajosByFilter(String query){
		return getLegajoDAO().getLegajosByFilter(query);
	}
	
	public List<Legajo> getLegajosByDoc(String query, String codigo){
		return getLegajoDAO().getLegajosByDoc(query, codigo);
	}
	
	public Boolean existeLegajo(String query, String codigo, Integer idL){
		List<Legajo> list = this.getLegajosByDoc(query, codigo);
		return (list.size()>0 && (idL==null || (idL!=null && !list.get(0).getId().equals(idL))));
	}
    
	public List<Legajo> getLegajosByZonal(Zonal zonal){
		return getLegajoDAO().getLegajosByZonal(zonal);
	}
	
	public List<Legajo> getLegajosByLocal(Local local){
		return getLegajoDAO().getLegajosByLocal(local);
	}
	
	public List<Legajo> getLegajosPorVencer(String idTpoIntervencion, Zonal zonal){
		return getLegajoDAO().getLegajosPorVencer(getIdTipoMPE(idTpoIntervencion), zonal,getDias(idTpoIntervencion));
	}
	
	public List<Legajo> getLegajosVencidos(String idTpoIntervencion, Zonal zonal){
		return getLegajoDAO().getLegajosVencidos(getIdTipoMPE(idTpoIntervencion), zonal, getDias(idTpoIntervencion));
	}
	
	public List<Legajo> getLegajosMayor180Dias(Zonal zonal){
		return getLegajoDAO().getLegajosMayor180Dias(zonal);
	}
	
	private Integer getDias(String idTipoMPE){
		if (idTipoMPE.equals("tipoIntervencion.idPER45"))
			return 45;
		else if (idTipoMPE.equals("tipoIntervencion.idPER90"))
			return 90;
		else if (idTipoMPE.equals("tipoIntervencion.idPER120"))
			return 120;
		else
			return 150;
	}
	
	private Integer getIdTipoMPE(String idTipoMPE){
		if (idTipoMPE==null){
			return null;
		}if (idTipoMPE.equals("tipoIntervencion.idPER45"))
			return Integer.valueOf(this.idPER45);
		else if (idTipoMPE.equals("tipoIntervencion.idPER90"))
			return Integer.valueOf(this.idPER90);
		else if (idTipoMPE.equals("tipoIntervencion.idPER120"))
			return Integer.valueOf(this.idPER120);
		else
			return Integer.valueOf(this.idPER150);
	}

	public List<Legajo> getRptLegajos(Date from, Date to, Sexo sexo, Zonal zonal, Local local, Date nacimientoFrom,
			Date nacimientoTo, Boolean tieneMEP) {
		return getLegajoDAO().getRptLegajos(from,to,sexo,zonal,local,nacimientoFrom,
				nacimientoTo, tieneMEP);
	}

	public List<Legajo> getPendientesAprobados() {
		return getLegajoDAO().getLegajosPendientesAprobados();
	}

	public List<Legajo> getNovedadesMPE(Zonal zonal) {
		return getLegajoDAO().getLegajosNovedadesMPE(zonal);
	}
	
	public List<Legajo> getLegajosAbiertosMPEPorFamilia(Familia familia) {
		return getLegajoDAO().getLegajosAbiertosMPEPorFamilia(familia);
	}
	
	public List<Legajo> getLegajosAbiertosMPEPorInstitucion(Institucion inst) {
		return getLegajoDAO().getLegajosAbiertosMPEPorInstitucion(inst);
	}
	
	public List<Legajo> getLegajosAsignadosReferentePAE(Usuario usuario){
		return getLegajoDAO().getLegajosAsignadosReferentePAE(usuario);
	}

	public Legajo get(Integer id) {
		return getLegajoDAO().get(id);
	}
	
	@Override
	@Transactional
	public void save(Legajo dom) {
		List<RelacionVincular> listRV = dom.getRelacionesVinculares();
		if (listRV!=null){
			for (Iterator<RelacionVincular> iterator = listRV.iterator(); iterator.hasNext();) {
				RelacionVincular relacionVincular = (RelacionVincular) iterator.next();
				if (relacionVincular.getPoseeLegajo()){
					Legajo rV = relacionVincular.getLegajoFamiliar();
					/**/
					List<RelacionVincular> list = rV.getRelacionesVinculares();///.list();
					Iterator<RelacionVincular> it = list.iterator();
					boolean flag = false;
					while (it.hasNext()){
						if (it.next().legajoFamiliar.equals(dom)){
							flag = true;
						}
					}
/**/
					//if (!rV.getRelacionesVinculares().contains(dom)){
					if (!flag){
						RelacionVincular newRV = new RelacionVincular();
						newRV.setFamiliar(null);//el constructor lo crea con instancia en familiar 
						newRV.setPoseeLegajo(true);
						newRV.setLegajoFamiliar(dom);
						newRV.setTipoVinculoFamiliar(relacionVincular.getTipoVinculoFamiliar());
						rV.getRelacionesVinculares().add(newRV);
						getLegajoDAO().update(rV);
					}
				}
			}
		}
		getLegajoDAO().update(dom);
		
	}

	public List<Legajo> getRptLegajosFliar(Persona familiarSeleccionado) {
		return getLegajoDAO().getLegajosFliar(familiarSeleccionado);
	}
}
