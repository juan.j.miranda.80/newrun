package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.CategoriaMotivoIntervencion;
import ar.gob.run.spring.service.CategoriaMotivoIntervencionService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "categoriaMotivoIntervencionConverter")
public class CategoriaMotivoIntervencionConverte extends GenericConverter<CategoriaMotivoIntervencion>{

	public CategoriaMotivoIntervencionConverte() {
		super(CategoriaMotivoIntervencion.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<CategoriaMotivoIntervencion> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{CategoriaMotivoIntervencionService}", CategoriaMotivoIntervencionService.class);

		CategoriaMotivoIntervencionService categoriaMotivoIntervencionService = (CategoriaMotivoIntervencionService)vex.getValue(context.getELContext());
		return categoriaMotivoIntervencionService;
	}

	


}
