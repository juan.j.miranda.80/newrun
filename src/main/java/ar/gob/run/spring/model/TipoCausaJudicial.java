package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.gob.run.spring.model.legajo.CausaJudicial;



@Entity
@Table(name="T_TIPO_CAUSA_JUDICIAL")
public class TipoCausaJudicial implements Serializable, Identificable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String descripcion;
   
    @Id
    @Column(name="ID_TIPO_CAUSA_JUDICIAL")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", desc : ").append(getDescripcion());
        return strBuff.toString();
    }

    

    @Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((TipoCausaJudicial)obj).getId());
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
