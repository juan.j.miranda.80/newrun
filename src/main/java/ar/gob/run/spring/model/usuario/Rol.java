package ar.gob.run.spring.model.usuario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ar.gob.run.spring.model.Identificable;

@Entity
@Table(name="T_ROL")
public class Rol implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static final Integer OPERADOR_102 = 2;
	public static final Integer ZONAL = 4;
	public static final Integer LOCAL = 5;
	public static final Integer ADMIN = 1;
	public static final Integer SUPERVISOR = 7;
	public static final Integer DICTAMEN = 8;
	public static final Integer REFERENTE_PAE = 9;
	
	
	@Id
	@Column(name="ID_ROL")
	private Integer id;
	
	@Column(name="ROL")
	private String nombre;
	
	@Column(name="CODE")
	private String code;
	
	public Rol() {
		super();
	}
	
	public Rol(Integer value){
		super();
		this.id = value;
	}

	public Rol(Integer value, String label) {
		this.id = value;
		this.nombre = label;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Rol)obj).getId());
	}	
}
