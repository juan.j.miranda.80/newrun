package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.ZonalService;

@FacesConverter(value = "zonalConverter")
public class ZonalConverter extends GenericConverter<Zonal>{

	public ZonalConverter() {
		super(Zonal.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Zonal> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{zonalService}", ZonalService.class);

		ZonalService service = (ZonalService)vex.getValue(context.getELContext());
		return service;
	}

	


}
