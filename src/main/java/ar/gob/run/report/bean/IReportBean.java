package ar.gob.run.report.bean;

import java.util.Map;

import ar.gob.run.report.common.ReportService;

public interface IReportBean {
	
	Map<String, Object> getReportParameters();
	
	String getCompileFileName();
	
	ReportService getReportService();
}
