package ar.gob.run.spring.model.legajo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import ar.gob.run.spring.model.Identificable;

@Entity
@Table(name="T_ESTUDIO_CURSADO")
public class EstudioCursado implements Serializable, Identificable{

	private static final long serialVersionUID = 1L;
	private Integer id;
    private String descripcion;
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Id
    @Column(name="ID_ESTUDIO_CURSADO")
	@Override
	public Integer getId() {
		return this.id;
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
    	strBuff.append(", desc : ").append(getDescripcion());
    	return strBuff.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Identificable)obj).getId());
	}
}
