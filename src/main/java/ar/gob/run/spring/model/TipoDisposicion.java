package ar.gob.run.spring.model;

public enum TipoDisposicion {
	
	A("Administrativa"), J("Judicial");
	
	String descripcion;

	private TipoDisposicion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
