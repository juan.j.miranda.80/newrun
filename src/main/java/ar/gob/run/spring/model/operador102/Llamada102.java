package ar.gob.run.spring.model.operador102;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;

@Entity
@Table(name="T_LLAMADA102")
public class Llamada102 extends ObjetoABM implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private Zonal zonal;
	private Local local;
	private Date fecha;
	private EstadoLlamada estado;
	private String referencia;
	private String domicilio;
	private String referenciaGeografica;
	private String apellido;
	private String nombre;
	private Integer edad;
	private TipoVinculoFamiliar tipoVinculo;
	private String observaciones;
	private String detalleSituacion;

	private List<Conviviente> convivientes;
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_LLAMADA102")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}
	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LOCAL")
	public Local getLocal() {
		return local;
	}
	public void setLocal(Local local) {
		this.local = local;
	}
	
	@Column(name="FECHA")
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO")
	public EstadoLlamada getEstado() {
		return estado;
	}
	public void setEstado(EstadoLlamada estado) {
		this.estado = estado;
	}
	
	@Column(name="REFERENCIA")
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	@Column(name="DOMICILIO")
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	@Column(name="REFERENCIA_GEO")
	public String getReferenciaGeografica() {
		return referenciaGeografica;
	}
	public void setReferenciaGeografica(String referenciaGeografica) {
		this.referenciaGeografica = referenciaGeografica;
	}
	
	@Column(name="APELLIDO")
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name="EDAD")
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_VINCULO_FAMILIAR")
	public TipoVinculoFamiliar getTipoVinculo() {
		return tipoVinculo;
	}
	public void setTipoVinculo(TipoVinculoFamiliar tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}
	
	@Column(name="OBSERVACIONES")
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	@Column(name="DETALLE_SITUACION")
	public String getDetalleSituacion() {
		return detalleSituacion;
	}
	public void setDetalleSituacion(String detalleSituacion) {
		this.detalleSituacion = detalleSituacion;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LLAMADA102", referencedColumnName="ID_LLAMADA102")
	@Fetch(value = FetchMode.SUBSELECT)
	public List<Conviviente> getConvivientes() {
		return convivientes;
	}
	public void setConvivientes(List<Conviviente> convivientes) {
		this.convivientes = convivientes;
	}


	
}
