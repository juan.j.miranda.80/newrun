package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.FuerzaSeguridad;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.FuerzaSeguridadService;

@FacesConverter(value = "fuerzaSeguridadConverter")
public class FuerzaSeguridadConverter extends GenericConverter<FuerzaSeguridad>{

	public FuerzaSeguridadConverter() {
		super(FuerzaSeguridad.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<FuerzaSeguridad> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{FuerzaSeguridadService}", FuerzaSeguridadService.class);

		FuerzaSeguridadService service = (FuerzaSeguridadService)vex.getValue(context.getELContext());
		return service;
	}

	


}
