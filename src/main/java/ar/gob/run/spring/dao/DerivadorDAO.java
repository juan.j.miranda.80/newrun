package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Derivador;

@Repository
public class DerivadorDAO extends HibernateDAO<Derivador,Integer> {

    public List<Derivador> getDerivadores() {
        List<Derivador> list = currentSession().createQuery("from Derivador c order by c.descripcion").list();
        return list;
    }
}
