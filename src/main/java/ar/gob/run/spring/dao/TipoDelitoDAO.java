package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.TipoDelito;

@Repository
public class TipoDelitoDAO extends HibernateDAO<TipoDelito,Integer> {

}
