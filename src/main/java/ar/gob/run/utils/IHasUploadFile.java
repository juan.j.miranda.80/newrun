package ar.gob.run.utils;

public interface IHasUploadFile {
	
	void setFileName(String name);
	
	String volver();
	
	String getAllowTypes();

	String getFolder();
	
}
