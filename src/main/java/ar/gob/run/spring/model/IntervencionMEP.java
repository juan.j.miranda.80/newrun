package ar.gob.run.spring.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.legajo.CausaJudicial;
import ar.gob.run.spring.model.usuario.Usuario;

/**
 * 
 * @see Esta es la actual MPJ que se pidio cambiar el nombre
 *
 */
@Entity
@Table(name="T_INTERV_MEP")
public class IntervencionMEP extends ObjetoABM implements Serializable, Intervencion{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private OperadorPenalJuvenil operadorPenalJuvenil;
	private Date fecha;
	private Juzgado juzgado;
	private TipoIntervencion tipoIntervencion;
	private MotivoIntervencion motivoIntervencion;
	private String informe;
	private Legajo legajo;
	private String archivoAdjunto;
	
	private TipoDispositivo tipoDispositivo;
	private TipoDisposicion tipoDisposicion;
	private String horaIngreso;
	private Date fechaIngreso;
	private CausaJudicial causaJudicial;
	private String presuntoDelito;
	private SiNoSinDato pasoPrevioCentroSalud;
	private Date fechaPasoPrevio;
	private MotivoPasoCentroSalud motivoPasoCentroSalud;
	
	private MotivoAprehension motivoAprehension;
	private TipoDelito tipoPesuntoDelito;
	private Procedencia procedencia;
	private String identifcarProcedencia;
	private FuerzaSeguridad fuerzaSeguridad;
	private SiNoSinDato pasoPrevioComisaria = SiNoSinDato.N;
	private Comisaria comisaria;
	private TipoTiempoComisaria tiempoComisaria;
	private SiNoSinDato arrestoDomiciliario;
	private SiNoSinDato mediacion;
	private String observacionMediacion;
	
	private DestinoEgresoDispositivo destinoEgresoDispositivo;
	private TipoDispositivo tipoDispositivoEgreso;
	private String descripcionPersonaDispEgreso;
	private String horaEgreso;
	private Date fechaEgreso;
	private SiNo denunciaPorApremio = SiNo.N;
	private String descripcionDenunciaPorApremio;
	
	
	
	
	@Override
	public void setId(Integer id) {
		this.id=id;
	}

    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_INTERV_MEP")
	@Override
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_OP_PEN_JUV")
	public OperadorPenalJuvenil getOperadorPenalJuvenil() {
		return operadorPenalJuvenil;
	}

	public void setOperadorPenalJuvenil(OperadorPenalJuvenil operadorPenalJuvenil) {
		this.operadorPenalJuvenil = operadorPenalJuvenil;
	}

	@Column(name="FECHA")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	@Column(name="FECHA_INGRESO")
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	
	@Column(name="FECHA_EGRESO")
	public Date getFechaEgreso() {
		return fechaEgreso;
	}
	
	public void setFechaEgreso(Date fechaEgreso) {
		this.fechaEgreso = fechaEgreso;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_JUZGADO")
	public Juzgado getJuzgado() {
		return juzgado;
	}

	public void setJuzgado(Juzgado juzgado) {
		this.juzgado = juzgado;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_INTERVENCION")
	public TipoIntervencion getTipoIntervencion() {
		return tipoIntervencion;
	}

	public void setTipoIntervencion(TipoIntervencion tipoIntervencion) {
		this.tipoIntervencion = tipoIntervencion;
	}

	@Lob
	@Column(columnDefinition="text", name="INFORME")
	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LEGAJO")
	public Legajo getLegajo() {
		return legajo;
	}

	public void setLegajo(Legajo legajo) {
		this.legajo = legajo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOTIVO_INTERVENCION")
	public MotivoIntervencion getMotivoIntervencion() {
		return motivoIntervencion;
	}

	public void setMotivoIntervencion(MotivoIntervencion motivoIntervencion) {
		this.motivoIntervencion = motivoIntervencion;
	}

	@Override
	public int compareTo(Intervencion o) {
		if ( this.fecha.compareTo(o.getFecha()) != 0) return this.fecha.compareTo(o.getFecha());
		if (this.tipoIntervencion.compareTo(o.getTipoIntervencion()) != 0) return this.tipoIntervencion.compareTo(o.getTipoIntervencion()) ;
		return this.legajo.getCodigo().compareTo(o.getLegajo().getCodigo());
	}

	
	//Estos metodos no se usan
	@Transient
	@Override
	public Local getLocal() {
		return null;
	}

	@Override
	@Transient
	public Zonal getZonal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transient
	public Derivador getDerivador() {
		// TODO Auto-generated method stub
		return null;
	}

	@Column(name="ARCHIVO_ADJUNTO")
	public String getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(String archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}

	public IntervencionMEP(){
		super();
	}
	
	public IntervencionMEP(Integer id, Date fecha, Date fechaIngreso, Date fechaEgreso, Integer idLegajo,
			String codigo,
			String nombre, String tpoInterv, 
			String archivo, String usrAlta, Integer idOPJ, String nombreOPJ){
		super();
		this.fecha = fecha;
		this.fechaIngreso = fechaIngreso;
		this.fechaEgreso = fechaEgreso;
		this.id = id;
		this.legajo = new Legajo();
		this.legajo.setCodigo(codigo);
		this.legajo.setApellidoYNombre(nombre);
		this.setTipoIntervencion(new TipoIntervencion());
		this.getTipoIntervencion().setDescripcion(tpoInterv);
		this.archivoAdjunto = archivo;
		this.getDatosAuditoria().setUsuarioAlta(new Usuario());
		this.getDatosAuditoria().getUsuarioAlta().setUserName(usrAlta);
		this.legajo.setId(idLegajo);
		this.setOperadorPenalJuvenil(new OperadorPenalJuvenil());
		this.getOperadorPenalJuvenil().setId(idOPJ);
		this.getOperadorPenalJuvenil().setDescripcion(nombreOPJ);
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_DISPOSITIVO")
	public TipoDispositivo getTipoDispositivo() {
		return tipoDispositivo;
	}

	public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
		this.tipoDispositivo = tipoDispositivo;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ID_TIPO_DISPOSICION")
	public TipoDisposicion getTipoDisposicion() {
		return tipoDisposicion;
	}

	public void setTipoDisposicion(TipoDisposicion tipoDisposicion) {
		this.tipoDisposicion = tipoDisposicion;
	}

	@Column(name="HORA_INGRESO")
	public String getHoraIngreso() {
		return horaIngreso;
	}

	public void setHoraIngreso(String horaIngreso) {
		this.horaIngreso = horaIngreso;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CAUSA_JUDICIAL")
	public CausaJudicial getCausaJudicial() {
		return causaJudicial;
	}

	public void setCausaJudicial(CausaJudicial causaJudicial) {
		this.causaJudicial = causaJudicial;
	}

	@Column(name="PRESUNTO_DELITO")
	public String getPresuntoDelito() {
		return presuntoDelito;
	}

	public void setPresuntoDelito(String presuntoDelito) {
		this.presuntoDelito = presuntoDelito;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="PASO_PREV_CS")
	public SiNoSinDato getPasoPrevioCentroSalud() {
		return pasoPrevioCentroSalud;
	}

	public void setPasoPrevioCentroSalud(SiNoSinDato pasoPrevioCentroSalud) {
		this.pasoPrevioCentroSalud = pasoPrevioCentroSalud;
	}

	@Column(name="FECHA_PASO_PREVIO_CS")
	public Date getFechaPasoPrevio() {
		return fechaPasoPrevio;
	}

	public void setFechaPasoPrevio(Date fechaPasoPrevio) {
		this.fechaPasoPrevio = fechaPasoPrevio;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOTIVO_PASO_CTROSAL")
	public MotivoPasoCentroSalud getMotivoPasoCentroSalud() {
		return motivoPasoCentroSalud;
	}

	public void setMotivoPasoCentroSalud(MotivoPasoCentroSalud motivoPasoCentroSalud) {
		this.motivoPasoCentroSalud = motivoPasoCentroSalud;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MOTIVO_APREHENSION")
	public MotivoAprehension getMotivoAprehension() {
		return motivoAprehension;
	}

	public void setMotivoAprehension(MotivoAprehension motivoAprehension) {
		this.motivoAprehension = motivoAprehension;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_PRESUNTO_DELITO")
	public TipoDelito getTipoPesuntoDelito() {
		return tipoPesuntoDelito;
	}

	public void setTipoPesuntoDelito(TipoDelito tipoPesuntoDelito) {
		this.tipoPesuntoDelito = tipoPesuntoDelito;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PROCEDENCIA")
	public Procedencia getProcedencia() {
		return procedencia;
	}

	public void setProcedencia(Procedencia procendia) {
		this.procedencia = procendia;
	}

	@Column(name="IDENTIFICAR_PROCEDENCIA")
	public String getIdentifcarProcedencia() {
		return identifcarProcedencia;
	}

	public void setIdentifcarProcedencia(String identifcarProcedencia) {
		this.identifcarProcedencia = identifcarProcedencia;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_FUERZA_SEGURIDAD")
	public FuerzaSeguridad getFuerzaSeguridad() {
		return fuerzaSeguridad;
	}

	public void setFuerzaSeguridad(FuerzaSeguridad fuerzaSeguridad) {
		this.fuerzaSeguridad = fuerzaSeguridad;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="PASO_PREV_COMISARIA")
	public SiNoSinDato getPasoPrevioComisaria() {
		return pasoPrevioComisaria;
	}

	public void setPasoPrevioComisaria(SiNoSinDato pasoPrevioComisaria) {
		this.pasoPrevioComisaria = pasoPrevioComisaria;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COMISARIA")
	public Comisaria getComisaria() {
		return comisaria;
	}

	public void setComisaria(Comisaria comisaria) {
		this.comisaria = comisaria;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIEMPO_COMISARIA")
	public TipoTiempoComisaria getTiempoComisaria() {
		return tiempoComisaria;
	}

	public void setTiempoComisaria(TipoTiempoComisaria tiempoComisaria) {
		this.tiempoComisaria = tiempoComisaria;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ARRESTO_DOMICILIARIO")
	public SiNoSinDato getArrestoDomiciliario() {
		return arrestoDomiciliario;
	}

	public void setArrestoDomiciliario(SiNoSinDato arrestoDomiciliario) {
		this.arrestoDomiciliario = arrestoDomiciliario;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="MEDIACION")
	public SiNoSinDato getMediacion() {
		return mediacion;
	}

	public void setMediacion(SiNoSinDato mediacion) {
		this.mediacion = mediacion;
	}

	@Column(name="OBSERVACION_MEDIACION")
	public String getObservacionMediacion() {
		return observacionMediacion;
	}

	public void setObservacionMediacion(String observacionMediacion) {
		this.observacionMediacion = observacionMediacion;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_DESTINO_EGRESO_DISP")
	public DestinoEgresoDispositivo getDestinoEgresoDispositivo() {
		return destinoEgresoDispositivo;
	}

	public void setDestinoEgresoDispositivo(DestinoEgresoDispositivo destinoEgresoDispositivo) {
		this.destinoEgresoDispositivo = destinoEgresoDispositivo;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_DISPOSIT_EGRESO")
	public TipoDispositivo getTipoDispositivoEgreso() {
		return tipoDispositivoEgreso;
	}

	public void setTipoDispositivoEgreso(TipoDispositivo tipoDispositivoEgreso) {
		this.tipoDispositivoEgreso = tipoDispositivoEgreso;
	}

	@Column(name="DESC_PERSON_DISP_EGRESO")
	public String getDescripcionPersonaDispEgreso() {
		return descripcionPersonaDispEgreso;
	}

	public void setDescripcionPersonaDispEgreso(String descripcionPersonaDispEgreso) {
		this.descripcionPersonaDispEgreso = descripcionPersonaDispEgreso;
	}

	@Column(name="HORA_EGRESO")
	public String getHoraEgreso() {
		return horaEgreso;
	}

	public void setHoraEgreso(String horaEgreso) {
		this.horaEgreso = horaEgreso;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="DENUNCIA_APREMIO")
	public SiNo getDenunciaPorApremio() {
		return denunciaPorApremio;
	}

	public void setDenunciaPorApremio(SiNo denunciaPorApremio) {
		this.denunciaPorApremio = denunciaPorApremio;
	}

	@Column(name="DESC_DENUNCIA_APREMIO")
	public String getDescripcionDenunciaPorApremio() {
		return descripcionDenunciaPorApremio;
	}

	public void setDescripcionDenunciaPorApremio(String descripcionDenunciaPorApremio) {
		this.descripcionDenunciaPorApremio = descripcionDenunciaPorApremio;
	}
	
	@Transient
	public Date getFechaListado() {
		if (this.fechaIngreso != null) return this.fechaIngreso;
		if (this.fecha != null) return this.fecha;
		return this.fechaEgreso;
	}
}
