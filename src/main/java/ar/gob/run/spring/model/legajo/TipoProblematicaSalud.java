package ar.gob.run.spring.model.legajo;

public enum TipoProblematicaSalud {

	SIN_VALOR("--Seleccione--","NN"),
	DISCAPACIDAD("Discapacidad","DIS"),
	ENF_CRONICA("Enfermedad crónica","ECR"),
	CONSUMO_SUST("Consumo problematico de sustancias","CPS");
	
	private String descripcion;
	private String code;

	private TipoProblematicaSalud(String descripcion, String code) {
		this.descripcion = descripcion;
		this.code = code;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static TipoProblematicaSalud fromString(String text) {
		if (text==null) return null;
	    for (TipoProblematicaSalud b : TipoProblematicaSalud.values()) {
	      if (b.code.equalsIgnoreCase(text)) {
	        return b;
	      }
	    }
	    return null;
	  }
	
}
