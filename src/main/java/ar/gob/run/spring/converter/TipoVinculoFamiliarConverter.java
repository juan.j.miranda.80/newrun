package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;
import ar.gob.run.spring.service.TipoVinculoFamiliarService;

@FacesConverter(value = "tipoVinculoFamiliarConverter")
public class TipoVinculoFamiliarConverter implements Converter {

	public TipoVinculoFamiliarConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), TipoVinculoFamiliar.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((TipoVinculoFamiliar) value).getId().toString();
	}

	public TipoVinculoFamiliarService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{TipoVinculoFamiliarService}", TipoVinculoFamiliarService.class);

			TipoVinculoFamiliarService service = (TipoVinculoFamiliarService)vex.getValue(context.getELContext());
			return service;
	}

}
