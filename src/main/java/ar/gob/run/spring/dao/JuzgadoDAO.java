package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Juzgado;
import ar.gob.run.spring.model.Legajo;

@Repository
public class JuzgadoDAO  extends HibernateDAO<Juzgado, Integer>{

	public List<Juzgado> getJuzgados(Integer juzgadoId) {
		List<Juzgado> list = currentSession().createQuery(" "
    			+ " from Juzgado z where z.activo = TRUE  and z.tipoOrganoJudicial.id = :juzgadoId order by z.id desc")
				.setInteger("juzgadoId", juzgadoId).list();
        return list;
	}
	
	public List<Juzgado> getNotJuzgados(Integer juzgadoId) {
		List<Juzgado> list = currentSession().createQuery(" "
    			+ " from Juzgado z where z.activo = TRUE  and z.tipoOrganoJudicial.id <> :juzgadoId order by z.id desc")
				.setInteger("juzgadoId", juzgadoId).list();
        return list;
	}
}