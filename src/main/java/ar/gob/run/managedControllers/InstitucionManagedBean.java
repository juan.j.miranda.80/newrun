package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.institucion.Institucion;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.InstitucionService;
import ar.gob.run.spring.service.LegajoService;

@ManagedBean(name="institucionMB")
@ViewScoped
public class InstitucionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{InstitucionService}")
    InstitucionService service;
    
    @ManagedProperty(value="#{legajoService}")
    LegajoService legajoService;

    List<Institucion> list;

    Institucion instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Institucion ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Institucion ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Institucion();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Institucion();
    	editMode = false;
    }

	public Institucion getInstance() {
		return instance;
	}

	public void setInstance(Institucion instance) {
		this.instance = instance;
	}

	public InstitucionService getService() {
		return service;
	}

	public void setService(InstitucionService service) {
		this.service = service;
	}

	public List<Institucion> getList() {
		return getService().getAllActives(Institucion.class);
	}

	public void setList(List<Institucion> list) {
		this.list = list;
	}

	public String puedeConsultar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) || usr.getUsuario().hasRole(Rol.ZONAL)
				|| usr.getUsuario().hasRole(Rol.LOCAL) || usr.getUsuario().hasRole(Rol.SUPERVISOR) || usr.getUsuario().hasRole(Rol.DICTAMEN))
			return "true";
		
		return "false";
	}
	
	public String puedeEditar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) || usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}

	public void cambiaTpoDependencia(){
		this.instance.setConConvenio(null);
		this.instance.setPersoneriaJuridica(null);
	}

	public LegajoService getLegajoService() {
		return legajoService;
	}

	public void setLegajoService(LegajoService legajoService) {
		this.legajoService = legajoService;
	}

	public List<Legajo> getLegajos(Institucion inst){
		return getLegajoService().getLegajosAbiertosMPEPorInstitucion(inst);
	}
}
