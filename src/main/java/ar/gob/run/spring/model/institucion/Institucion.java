package ar.gob.run.spring.model.institucion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.model.SexoAdmision;
import ar.gob.run.spring.model.SiNo;
import ar.gob.run.spring.model.Zonal;

@Entity
@Table(name="T_INSTITUCION")
public class Institucion extends ObjetoABM implements  Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nombre;
	private Zonal zonal;
	private TipoInstitucion tipoInstitucion;
	private String telefono;
	private String institucionResponsable;
	private String responsableLegal;
	private String respEspConvivencial;
	private String email;
	private SexoAdmision sexo;
	private String creenciaReligiosa;
	private Integer edadDesde;
	private Integer edadHasta;
	private Integer plazas;
	private String observaciones;
	
	private String ubicacion;
	private String cp;
	private String coordenadas;
	private TipoDependencia tipoDependencia;
	private SiNo conConvenio;
	private Boolean personeriaJuridica;
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_INSTITUCION")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}
	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_INSTITUCION")
	public TipoInstitucion getTipoInstitucion() {
		return tipoInstitucion;
	}
	public void setTipoInstitucion(TipoInstitucion tipoInstitucion) {
		this.tipoInstitucion = tipoInstitucion;
	}
	
	@Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Column(name="INSTITUCION_RESPONSABLE")
	public String getInstitucionResponsable() {
		return institucionResponsable;
	}
	public void setInstitucionResponsable(String institucionResponsable) {
		this.institucionResponsable = institucionResponsable;
	}
	
	@Column(name="RESPONSABLE_LEGAL")
	public String getResponsableLegal() {
		return responsableLegal;
	}
	public void setResponsableLegal(String responsableLegal) {
		this.responsableLegal = responsableLegal;
	}
	
	@Column(name="RES_ESPACIO_CONVIVENCIAL")
	public String getRespEspConvivencial() {
		return respEspConvivencial;
	}
	public void setRespEspConvivencial(String respEspConvivencial) {
		this.respEspConvivencial = respEspConvivencial;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="SEXO")
	public SexoAdmision getSexo() {
		return sexo;
	}
	public void setSexo(SexoAdmision sexo) {
		this.sexo = sexo;
	}
	
	@Column(name="CREENCIA_RELIGIOSA")
	public String getCreenciaReligiosa() {
		return creenciaReligiosa;
	}
	public void setCreenciaReligiosa(String creenciaReligiosa) {
		this.creenciaReligiosa = creenciaReligiosa;
	}
	
	@Column(name="EDAD_DESDE")
	public Integer getEdadDesde() {
		return edadDesde;
	}
	public void setEdadDesde(Integer edadDesde) {
		this.edadDesde = edadDesde;
	}
	
	@Column(name="EDAD_HASTA")
	public Integer getEdadHasta() {
		return edadHasta;
	}
	public void setEdadHasta(Integer edadHasta) {
		this.edadHasta = edadHasta;
	}
	
	@Column(name="PLAZAS")
	public Integer getPlazas() {
		return plazas;
	}
	public void setPlazas(Integer plazas) {
		this.plazas = plazas;
	}
	
	@Column(name="OBSERVACIONES")
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	@Column(name="UBICACION")
	public String getUbicacion() {
		return ubicacion;
	}
	
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	@Column(name="CP")
	public String getCp() {
		return cp;
	}
	
	public void setCp(String cp) {
		this.cp = cp;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_DEPENDENCIA")
	public TipoDependencia getTipoDependencia() {
		return tipoDependencia;
	}
	
	public void setTipoDependencia(TipoDependencia tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="CON_CONVENIO")
	public SiNo getConConvenio() {
		return conConvenio;
	}
	public void setConConvenio(SiNo conConvenio) {
		this.conConvenio = conConvenio;
	}
	
	@Column(name="PERSONERIA_JURIDICA")
	public Boolean getPersoneriaJuridica() {
		return personeriaJuridica;
	}
	public void setPersoneriaJuridica(Boolean personeriaJuridica) {
		this.personeriaJuridica = personeriaJuridica;
	}
	@Column(name="COORDENADAS")
	public String getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	
}
