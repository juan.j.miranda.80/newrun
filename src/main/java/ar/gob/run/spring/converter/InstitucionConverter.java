package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.institucion.Institucion;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.InstitucionService;

@FacesConverter(value = "institucionConverter")
public class InstitucionConverter extends GenericConverter<Institucion>{

	public InstitucionConverter() {
		super(Institucion.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Institucion> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{InstitucionService}", InstitucionService.class);

		InstitucionService service = (InstitucionService)vex.getValue(context.getELContext());
		return service;
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), Institucion.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((Institucion) value).getId().toString();
	}


}
