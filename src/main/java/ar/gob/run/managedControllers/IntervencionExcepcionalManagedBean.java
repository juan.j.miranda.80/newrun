package ar.gob.run.managedControllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.report.bean.ImprimirInformeBean;
import ar.gob.run.spring.model.CategoriaMotivoIntervencion;
import ar.gob.run.spring.model.EstadoSupervInterv;
import ar.gob.run.spring.model.IntervencionExcepcional;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.TipoAlertaMPE;
import ar.gob.run.spring.model.TipoAmbitoCumplimiento;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.CommonService;
import ar.gob.run.spring.service.IntervencionExcepcionalService;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.MotivoIntervencionService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="intervencionExcepcionalMB")
@SessionScoped
public class IntervencionExcepcionalManagedBean extends GenericIntervencionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";
    private static final String VOLVER_A_LEGAJO = "volverALegajo";
    private static final String VOLVER_A_PENDIENTES = "listadoPendientesAprob";
	private static final String VOLVER_FROM_UPLOAD = "/legajo/intervencionExcepcional/editor.xhtml";;

    List<IntervencionExcepcional> list;
    List<IntervencionExcepcional> filteredList;

    IntervencionExcepcional instance;
    
    Boolean legajoActivo = false;
    
    Boolean fromPendiente = false;
    
    @ManagedProperty(value="#{IntervencionExcepcionalService}")
    IntervencionExcepcionalService service;
    
    @ManagedProperty(value="#{commonService}")
	CommonService commonService;
    
    public String editPendiente(IntervencionExcepcional ent){
    	this.instance = service.get(ent.getId());
    	if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
    		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
    	
    	this.fromPendiente = true;
    	this.legajoActivo = false;
    	return EDITOR;
    }
    
    public String edit(IntervencionExcepcional ent){
    	this.instance = service.get(ent.getId());
    	if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
    		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
    	this.legajoActivo = false;
    	return EDITOR;
    }
	
	public String edit(IntervencionExcepcional ent, Boolean legajoActivo){
		this.instance = service.get(ent.getId());
    	if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
    		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
    	this.legajoActivo = legajoActivo;
    	this.fromPendiente = false;
    	return EDITOR;
    }
	
	public String nueva(Legajo legajo, Boolean legajoActivo){
    	this.instance = new IntervencionExcepcional();
    	this.instance.setLegajo(legajo);
    	this.instance.setZonal(legajo.getZonal());
    	this.legajoActivo = legajoActivo;
    	List<IntervencionExcepcional> mpes = legajo.getIntervencionesMPE();
    	if (mpes!=null && !mpes.isEmpty()){
    		IntervencionExcepcional mpe = mpes.get(mpes.size()-1);
    		if (commonService.isSolicitaMPE(mpe.getId())){
    			this.instance.setMotivoIntervencion(mpe.getMotivoIntervencion());
    			this.instance.setDerivador(mpe.getDerivador());
    		}
    		this.instance.setDerivador(mpe.getDerivador());
    		this.instance.setMotivoIntervencion(mpe.getMotivoIntervencion());
        	if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
        		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
    	}
    	return EDITOR;
    }
    
    public void delete(IntervencionExcepcional ent, Boolean legajoActivo){
    	this.legajoActivo = legajoActivo;
    	getService().delete(ent);
    }
    
    public String delete(IntervencionExcepcional ent){
    	getService().delete(ent);
    	return LISTADO;
    }
    
    
    public String reset() {
    	this.instance = new IntervencionExcepcional();
        return EDITOR;
    }
    
    public String cancel(){
    	legajoParam.setId(null);
    	//Guardo el id del legajo antes de refrescar la instancia
    	Integer idLegajo = (this.instance!=null && this.instance.getLegajo()!=null)?this.instance.getLegajo().getId():null;
    	if (this.instance==null || this.instance.getId()==null)
    		this.instance = new IntervencionExcepcional();
    	if (legajoActivo){
    		legajoParam.setId(idLegajo);
        	return VOLVER_A_LEGAJO;
    	}
    	return LISTADO;
    }

	public IntervencionExcepcional getInstance() {
		if (instance==null)
			instance = new IntervencionExcepcional();
		return instance;
	}

	public void setInstance(IntervencionExcepcional instance) {
		this.instance = instance;
	}

	public List<IntervencionExcepcional> getList() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.LOCAL))
			return service.getIntervencionesByZonal(usr.getUsuario().getZonal());
		this.legajoActivo=false;
		return service.getAllActives(IntervencionExcepcional.class);
	}
	
	public List<IntervencionExcepcional> getList(Integer legajo) {
		this.legajoActivo=false;
		return service.getList(legajo);
	}

	public void setList(List<IntervencionExcepcional> list) {
		this.list = list;
	}

    public String save() {
        try {
            getService().save(instance);            
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Intervencion salvada con exito","");
            FacesContext.getCurrentInstance().addMessage(null, message);
            legajoParam.setId(null);
            if (legajoActivo){
            	legajoParam.setId(this.instance.getLegajo().getId());            	
            	return VOLVER_A_LEGAJO;
            }
            if (fromPendiente){
            	fromPendiente=false;
            	return VOLVER_A_PENDIENTES;
            }
            return LISTADO;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return ERROR;
    }

	public IntervencionExcepcionalService getService() {
		return service;
	}

	public void setService(IntervencionExcepcionalService service) {
		this.service = service;
	}

	public List<IntervencionExcepcional> getFilteredList() {
		return filteredList;
	}

	public void setFilteredList(List<IntervencionExcepcional> filteredList) {
		this.filteredList = filteredList;
	}

    public boolean filterByTpoIntervencion(Object value, Object filter, Locale locale) {
        if (value == null || filter == null) {
            return false;
        }

        return ((Comparable) value).compareTo(filter) > 0;
    }

	public Boolean getLegajoActivo() {
		return legajoActivo;
	}
    
    //logica zonal-local dependiente
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(){
		return zonalService.getZonales(this.instance.getZonal());
	}
	
	public String puedeEditar(IntervencionExcepcional intInt){
		if (intInt==null) return "true";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				//(usr.getUsuario().hasRole(Rol.ZONAL) && intInt!=null && 
				//		intInt.getZonal()!=null && intInt.getZonal().equals(usr.getUsuario().getZonal())) ||
				this.esUsuarioCreadorInt(intInt.getDatosAuditoria(), usr.getUsuario()) 
				|| usr.getUsuario().hasRole(Rol.SUPERVISOR)
				|| usr.getUsuario().hasRole(Rol.DICTAMEN))
				return "true";
		else 
			return "false";
				
	}
	
	public String verObservacion(){
		IntervencionExcepcional intInt = this.instance;
		if (intInt==null) return "false";
		if (StringUtils.isEmpty(intInt.getObservacionMedida()))
				return "false";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				(usr.getUsuario().hasRole(Rol.ZONAL) && intInt!=null && 
						intInt.getZonal()!=null && intInt.getZonal().equals(usr.getUsuario().getZonal())) 
				|| usr.getUsuario().hasRole(Rol.SUPERVISOR)
				|| usr.getUsuario().hasRole(Rol.DICTAMEN))
				return "true";
		else 
			return "false";
	}
	
	public String puedeEditar(){
		if (this.getInstance().getId()==null) return "true";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN)  ||
				(usr.getUsuario().hasRole(Rol.ZONAL) && this.getInstance().getZonal().equals(usr.getUsuario().getZonal())) 
				|| usr.getUsuario().hasRole(Rol.SUPERVISOR) ||
				this.esUsuarioCreadorInt(this.getInstance().getDatosAuditoria(), usr.getUsuario()))
			return "true";
		return "false";
	}
	
	
	public String puedeImprimir(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (!usr.getUsuario().hasRole(Rol.OPERADOR_102))
			return "true";
		return "false";
	}
	
	public void onTipoIntervencionChange(){
		if (this.getInstance().getTipoIntervencion()!=null &&
				this.getInstance().getTipoIntervencion().getTemplate()!=null)
			this.getInstance().setInforme(this.getInstance().getTipoIntervencion().getTemplate());
		else 
			this.getInstance().setInforme(null);
	}

	public String aprobarMPE(){
		this.instance.setEstadoSuperv(EstadoSupervInterv.ACEPTADO);
		return this.save();
	}
	
	public String rechazarMPE(){
		this.instance.setEstadoSuperv(EstadoSupervInterv.RECHAZADO);
		return this.save();
	}
	
	public String observarMPE(){
		if (StringUtils.isEmpty(this.instance.getObservacionMedida())){
			FacesMessage message = new FacesMessage("Para observar la medida debe completar la observacion");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return "";
		}
		this.instance.setEstadoSuperv(EstadoSupervInterv.OBSERVADA);
		return this.save();
	}
	
	public String puedeSuperv(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (( (usr.getUsuario().hasRole(Rol.SUPERVISOR) || usr.getUsuario().hasRole(Rol.ADMIN) ) 
				&& this.instance.getEstadoSuperv()!=null && this.instance.getEstadoSuperv().equals(EstadoSupervInterv.PENDIENTE)))
			return "true";
		return "false";
	}

	public String puedeObservar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}
	
	public String mostrarCorregir(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) && this.instance.getEstadoSuperv()!=null && this.instance.getEstadoSuperv().equals(EstadoSupervInterv.OBSERVADA))
			return "true";
		return "false";
	}
	
	public String corregir(){
		this.instance.setEstadoSuperv(EstadoSupervInterv.PENDIENTE);
		return this.save();
	}
	
	public String isSupervisada(){
		if (this.instance!=null && instance.getEstadoSuperv()!=null && !instance.getEstadoSuperv().equals(EstadoSupervInterv.NULL))
			return "true";
		return "false";
	}

	public String cargarSoloCese(){
		String soloTipoCese="false";
		if (this.getInstance().getLegajo().getIntervencionesMPE()==null || this.getInstance().getLegajo().getIntervencionesMPE().size()==0)
			return soloTipoCese;
		Iterator<IntervencionExcepcional> it = this.getInstance().getLegajo().getIntervencionesMPE().iterator();
		Integer idSolicita = commonService.getIdSolicitaMPE();
		while (it.hasNext() && soloTipoCese.equals("false")){
			IntervencionExcepcional elem = it.next();
			Date fecha = elem.getFecha();
			if (elem.getTipoIntervencion().getId().equals(idSolicita)){
				if (fecha != null && TimeUnit.DAYS.convert((new Date().getTime() - fecha.getTime()), 
						TimeUnit.MILLISECONDS)>=Long.valueOf(TipoAlertaMPE.CESE180.getDias())){
					soloTipoCese = "true";
				}
			}
		}
		return soloTipoCese;
	}
	
	public String cargarSoloIniciaCirc(){
		String soloIniciaCirc="false";
		if (this.getInstance().getLegajo().getIntervencionesMPE().size()==0)
			return "true";
		IntervencionExcepcional ultima = Collections.max(this.getInstance().getLegajo().getIntervencionesMPE());
		
		if (ultima!=null && ultima.getTipoIntervencion()!=null &&
				ultima.getTipoIntervencion().getId().equals(commonService.getIdSolicitaMPE()) &&
				ultima.getEstadoSuperv()!=null && 
				!ultima.getEstadoSuperv().equals(EstadoSupervInterv.PENDIENTE))
			return "false";
		
		if ((ultima.getTipoIntervencion().getIntervencionCese()!=null &&
				ultima.getTipoIntervencion().getIntervencionCese()) ||
				(ultima.getEstadoSuperv()!= null && ultima.getEstadoSuperv().equals(EstadoSupervInterv.RECHAZADO)))
			soloIniciaCirc =  "true";
		return soloIniciaCirc;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}
	
	public String esPERInicial(TipoIntervencion tpoI){
		if (tpoI!=null && tpoI.getId().equals(getCommonService().getIdSolicitaMPE())){
			return "true";
		}
		return "false";
	}
	
	public void cambiaAmbito(){
		if (instance.getAmbitoCumplimiento().equals(TipoAmbitoCumplimiento.AMPLIADA)){
			instance.setFamilia(null);
			instance.setInstitucion(null);
		}else if (instance.getAmbitoCumplimiento().equals(TipoAmbitoCumplimiento.FORMAL)){
			instance.setTxtFamiliaAmpliada(null);
		}
	}
	
	public void cambiaInstitucion(){
		instance.setFamilia(null);
	}
	
	public void cambiaFamilia(){
		instance.setInstitucion(null);
	}
	
	public String getExpandAlojamiento(){
		if (instance!=null &&
				instance.getTipoIntervencion()!=null &&
				Boolean.parseBoolean(esPERInicial(instance.getTipoIntervencion()))){
			return "0";
		}
		return "";
	}
	
	//Es necesario para la propiedad que expande o retrae la solapa de ambito de cumplimiento. Pero no hace nada este setter
	public void setExpandAlojamiento(String ex){
	}

	
	public void handleFileUpload(FileUploadEvent event) {
    	try {
            byte[] bytes=null;
     
                if (null!=event.getFile()) {
                    bytes = event.getFile().getContents();
                    String filename = FilenameUtils.getName(event.getFile().getFileName() );
        			File file = new File(getCommonService().getFileUploaderPath() + "/MPE/" + filename);
        			file.getParentFile().mkdirs();	
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();
                    this.getInstance().setArchivoAdjunto(filename);
                    FacesMessage message = new FacesMessage("El informe  " + event.getFile().getFileName() + " fue subido con éxito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    return;
                }
    		}catch(IOException e){
    			FacesMessage message = new FacesMessage("Error al subir informe ", event.getFile().getFileName());
                FacesContext.getCurrentInstance().addMessage(null, message);
    		}
    	FacesMessage message = new FacesMessage("Error al intentar subir el informe  ", event.getFile().getFileName());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
	
    public StreamedContent downloadFile(String name) {
    	StreamedContent fileDownload = null;
    	String path = getCommonService().getFileUploaderPath() + "/MPE/" + name;
        InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
		}
        return fileDownload;
    }

	@Override
	public String getNombreArchivo() {
		return this.getInstance().getLegajo().getApellidoYNombre() + "-MPE-";
	}

	@Override
	public String getNombreLegajo() {
		Legajo leg = this.getInstance().getLegajo();
		return leg.getCodigo() + " - " + leg.getApellidoYNombre();
	}

	@Override
	public String getInforme() {
		return getInstance().getInforme();
	}	
    
}
