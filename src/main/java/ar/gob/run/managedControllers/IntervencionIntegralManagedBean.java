package ar.gob.run.managedControllers;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import ar.gob.run.report.bean.ImprimirInformeBean;
import ar.gob.run.spring.model.IntervencionIntegral;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.CommonService;
import ar.gob.run.spring.service.IntervencionIntegralService;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name = "intervencionIntegralMB")
@SessionScoped
public class IntervencionIntegralManagedBean extends GenericIntervencionManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String LISTADO = "listado";
	private static final String EDITOR = "editor";
	private static final String ERROR = "error";
	private static final String VOLVER_A_LEGAJO = "volverALegajo";

	List<IntervencionIntegral> list;
	List<IntervencionIntegral> filteredList;

	IntervencionIntegral instance;

	Boolean legajoActivo = false;

	@ManagedProperty(value = "#{IntervencionIntegralService}")
	IntervencionIntegralService service;

	@ManagedProperty(value = "#{commonService}")
	CommonService commonService;

	public String edit(IntervencionIntegral ent) {
		this.instance = service.get(ent.getId());
		if (this.instance.getMotivoIntervencion() != null
				&& this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion() != null)
			this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
		return EDITOR;
	}

	public String edit(IntervencionIntegral ent, Boolean legajoActivo) {
		this.legajoActivo = legajoActivo;
		return this.edit(ent);
	}

	public String nueva(Legajo legajo, Boolean legajoActivo) {
		this.instance = new IntervencionIntegral();
		this.instance.setLegajo(legajo);
		this.instance.setZonal(legajo.getZonal());
		this.instance.setLocal(legajo.getLocal());
		this.onZonalChange();
		this.legajoActivo = legajoActivo;

		List<IntervencionIntegral> mpis = legajo.getIntervencionesMPI();
		if (mpis.size() > 0) {
			IntervencionIntegral mpi = mpis.get(mpis.size() - 1);
			this.instance.setMotivoIntervencion(mpi.getMotivoIntervencion());
			this.instance.setDerivador(mpi.getDerivador());
			if (this.instance.getMotivoIntervencion() != null
					&& this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion() != null)
				this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
		}
		return EDITOR;
	}

	public void delete(IntervencionIntegral ent, Boolean legajoActivo) {
		this.legajoActivo = legajoActivo;
		getService().delete(ent);
	}

	public String delete(IntervencionIntegral ent) {
		getService().delete(ent);
		return LISTADO;
	}

	public String reset() {
		this.instance = new IntervencionIntegral();
		return EDITOR;
	}

	public String cancel() {
		//Guardo el id del legajo antes de refrescar la instancia
    	Integer idLegajo = (this.instance!=null && this.instance.getLegajo()!=null)?this.instance.getLegajo().getId():null;
		if (this.instance == null || this.instance.getId() == null)
			this.instance = new IntervencionIntegral();
		legajoParam.setId(null);
		if (legajoActivo) {
			legajoParam.setId(idLegajo);
			return VOLVER_A_LEGAJO;
		}
		return LISTADO;
	}

	public IntervencionIntegral getInstance() {
		if (instance == null)
			instance = new IntervencionIntegral();
		return instance;
	}

	public void setInstance(IntervencionIntegral instance) {
		this.instance = instance;
	}

	public List<IntervencionIntegral> getList() {
		MySecUser usr = (MySecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		// if (usr.getUsuario().hasRole(Rol.ZONAL))
		// return
		// service.getIntervencionesIntByZonal(usr.getUsuario().getZonal());
		legajoActivo = false;
		if (usr.getUsuario().hasRole(Rol.LOCAL))
			return service.getIntervencionesIntByLocal(usr.getUsuario().getLocal());
		return service.getAllActives(IntervencionIntegral.class);
	}

	public List<IntervencionIntegral> getList(Integer legajo) {
		legajoActivo = false;
		return service.getList(legajo);
	}

	public void setList(List<IntervencionIntegral> list) {
		this.list = list;
	}

	public String save() {
		try {
			getService().save(instance);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Intervencion salvada con exito", "");
			FacesContext.getCurrentInstance().addMessage(null, message);
			legajoParam.setId(null);
			if (legajoActivo) {
				legajoParam.setId(this.instance.getLegajo().getId());
				return VOLVER_A_LEGAJO;
			}
			return LISTADO;
		} catch (DataAccessException e) {
			e.printStackTrace();
		}
		return ERROR;
	}

	public IntervencionIntegralService getService() {
		return service;
	}

	public void setService(IntervencionIntegralService service) {
		this.service = service;
	}

	public List<IntervencionIntegral> getFilteredList() {
		return filteredList;
	}

	public void setFilteredList(List<IntervencionIntegral> filteredList) {
		this.filteredList = filteredList;
	}

	public boolean filterByTpoIntervencion(Object value, Object filter, Locale locale) {
		if (value == null || filter == null) {
			return false;
		}

		return ((Comparable) value).compareTo(filter) > 0;
	}

	public Boolean getLegajoActivo() {
		return legajoActivo;
	}

	// logica zonal-local dependiente
	@ManagedProperty(value = "#{zonalService}")
	ZonalService zonalService;

	@ManagedProperty(value = "#{localService}")
	LocalService localService;
	List<Local> locales;

	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}

	public List<Zonal> getZonales() {
		return zonalService.getZonales(this.instance.getZonal());
	}

	public void onZonalChange() {
		if (this.instance.getZonal() != null && this.instance.getZonal().getId() != null)
			locales = localService.getLocales(this.instance.getZonal(), this.instance.getLocal());
		else
			locales = new ArrayList<Local>();
	}

	public void setLocalService(LocalService localService) {
		this.localService = localService;
	}

	public List<Local> getLocales() {
		if (locales == null)
			locales = new ArrayList<Local>();
		if (instance.getLocal() != null)
			onZonalChange();
		return locales;
	}

	public String puedeEditar(IntervencionIntegral intInt) {
		if (intInt == null)
			return "true";
		if (this.getInstance().getId() == null)
			return "true";
		MySecUser usr = (MySecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN)
				|| (intInt != null && intInt.getDatosAuditoria() != null
						&& usr.getUsuario().equals(intInt.getDatosAuditoria().getUsuarioAlta()))
				|| this.esUsuarioCreadorInt(intInt.getDatosAuditoria(), usr.getUsuario()) ||
				// (usr.getUsuario().hasRole(Rol.ZONAL) && intInt!=null &&
				// intInt.getZonal()!=null &&
				// intInt.getZonal().equals(usr.getUsuario().getZonal())) ||
				(usr.getUsuario().hasRole(Rol.LOCAL) && intInt != null && intInt.getLocal() != null
						&& intInt.getLocal().equals(usr.getUsuario().getLocal())))
			return "true";
		else
			return "false";

	}

	public String puedeEditar() {
		return this.puedeEditar(this.getInstance());
	}

	public String puedeImprimir() {
		MySecUser usr = (MySecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (!usr.getUsuario().hasRole(Rol.OPERADOR_102))
			return "true";
		return "false";
	}

	public void onTipoIntervencionChange() {
		if (this.instance.getTipoIntervencion() != null && this.instance.getTipoIntervencion().getTemplate() != null)
			this.instance.setInforme(this.instance.getTipoIntervencion().getTemplate());
		else
			this.instance.setInforme(null);
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {
			byte[] bytes = null;

			if (null != event.getFile()) {
				bytes = event.getFile().getContents();
				String filename = FilenameUtils.getName(event.getFile().getFileName());
				File file = new File(getCommonService().getFileUploaderPath() + "/MPI/" + filename);
				file.getParentFile().mkdirs();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
				stream.write(bytes);
				stream.close();
				this.getInstance().setArchivoAdjunto(filename);
				FacesMessage message = new FacesMessage(
						"El informe  " + event.getFile().getFileName() + " fue subido con éxito.");
				FacesContext.getCurrentInstance().addMessage(null, message);
				return;
			}
		} catch (IOException e) {
			FacesMessage message = new FacesMessage("Error al subir informe ", event.getFile().getFileName());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		FacesMessage message = new FacesMessage("Error al intentar subir el informe ", event.getFile().getFileName());
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public StreamedContent downloadFile(String name) {
		StreamedContent fileDownload = null;
		String path = getCommonService().getFileUploaderPath() + "/MPI/" + name;
		InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
		}
		return fileDownload;
	}

	@Override
	public String getNombreArchivo() {
		return this.getInstance().getLegajo().getCodigo() + "-MPI-";
	}

	@Override
	public String getNombreLegajo() {
		Legajo leg = this.getInstance().getLegajo();
		return leg.getCodigo() + " - " + leg.getApellidoYNombre();
	}

	@Override
	public String getInforme() {
		return getInstance().getInforme();
	}	
}
