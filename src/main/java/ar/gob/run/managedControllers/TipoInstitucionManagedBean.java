package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.institucion.TipoInstitucion;
import ar.gob.run.spring.service.TipoInstitucionService;

@ManagedBean(name="tipoInstitucionMB")
@ViewScoped
public class TipoInstitucionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";

    @ManagedProperty(value="#{TipoInstitucionService}")
    TipoInstitucionService service;

    List<TipoInstitucion> list;

    TipoInstitucion instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(TipoInstitucion ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(TipoInstitucion ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new TipoInstitucion();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new TipoInstitucion();
    	editMode = false;
    }

	public TipoInstitucion getInstance() {
		return instance;
	}

	public void setInstance(TipoInstitucion instance) {
		this.instance = instance;
	}

	public TipoInstitucionService getService() {
		return service;
	}

	public void setService(TipoInstitucionService service) {
		this.service = service;
	}

	public List<TipoInstitucion> getList() {
		return getService().getAllActives(TipoInstitucion.class);
	}

	public void setList(List<TipoInstitucion> list) {
		this.list = list;
	}

	public Boolean getEditMode() {
		return editMode;
	}
	
}
