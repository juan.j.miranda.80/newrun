package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.MotivoPasoCentroSalud;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.MotivoPasoCentroSaludService;

@FacesConverter(value = "motivoPasoCentroSaludConverter")
public class MotivoPasoCentroSaludConverter extends GenericConverter<MotivoPasoCentroSalud>{

	public MotivoPasoCentroSaludConverter() {
		super(MotivoPasoCentroSalud.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<MotivoPasoCentroSalud> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{MotivoPasoCentroSaludService}", MotivoPasoCentroSaludService.class);

		MotivoPasoCentroSaludService service = (MotivoPasoCentroSaludService)vex.getValue(context.getELContext());
		return service;
	}

	


}
