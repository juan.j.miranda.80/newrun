package ar.gob.run.managedControllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.report.bean.ImprimirInformeBean;
import ar.gob.run.spring.model.EstadoSupervInterv;
import ar.gob.run.spring.model.IntervencionIntegral;
import ar.gob.run.spring.model.IntervencionPAE;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.TipoAmbitoCumplimiento;
import ar.gob.run.spring.model.TipoDomicilioJoven;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.CommonService;
import ar.gob.run.spring.service.IntervencionPAEService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="intervencionPAEMB")
@SessionScoped
public class IntervencionPAEManagedBean extends GenericIntervencionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";
    private static final String VOLVER_A_LEGAJO = "volverALegajo";
    private static final String VOLVER_A_PENDIENTES = "listadoPendientesAprob";

    List<IntervencionPAE> list;
    List<IntervencionPAE> filteredList;

    IntervencionPAE instance;
    
    Boolean legajoActivo = false;
    
    Boolean fromPendiente = false;
    
    @ManagedProperty(value="#{IntervencionPAEService}")
    IntervencionPAEService service;
    
    @ManagedProperty(value="#{commonService}")
	CommonService commonService;
    
    public String editPendiente(IntervencionPAE ent){
    	this.edit(ent);
    	this.fromPendiente = true;
    	return EDITOR;
    }
    
    public String edit(IntervencionPAE ent){
    	this.edit(ent,false);
    	return EDITOR;
    }
	
	public String edit(IntervencionPAE ent, Boolean legajoActivo){
		this.instance = service.get(ent.getId());
		if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
    		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
    	this.legajoActivo = legajoActivo;
    	this.fromPendiente = false;
    	return EDITOR;
    }
	
	public String nueva(Legajo legajo, Boolean legajoActivo){
    	this.instance = new IntervencionPAE();
    	this.instance.setLegajo(legajo);
    	this.instance.setZonal(legajo.getZonal());
    	this.legajoActivo = legajoActivo;

    	List<IntervencionPAE> mpaes = legajo.getIntervencionesPAE();
    	if (mpaes!=null && !mpaes.isEmpty()){
	    	IntervencionPAE mpae = mpaes.get(mpaes.size()-1);
	    	this.instance.setMotivoIntervencion(mpae.getMotivoIntervencion());
	    	if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
	    		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
    	}
    	return EDITOR;
    }
    
    public void delete(IntervencionPAE ent, Boolean legajoActivo){
    	this.legajoActivo = legajoActivo;
    	getService().delete(ent);
    }
    
    public String delete(IntervencionPAE ent){
    	getService().delete(ent);
    	return LISTADO;
    }
    
    
    public String reset() {
    	this.instance = new IntervencionPAE();
        return EDITOR;
    }
    
    public String cancel(){
    	//Guardo el id del legajo antes de refrescar la instancia
    	Integer idLegajo = (this.instance!=null && this.instance.getLegajo()!=null)?this.instance.getLegajo().getId():null;
    	if (this.instance==null || this.instance.getId()==null)
    		this.instance = new IntervencionPAE();
    	this.legajoParam.setId(null);
    	if (legajoActivo){
    		legajoParam.setId(idLegajo);
        	return VOLVER_A_LEGAJO;
    	}
    	return LISTADO;
    }

	public IntervencionPAE getInstance() {
		if (instance==null)
			instance = new IntervencionPAE();
		return instance;
	}

	public void setInstance(IntervencionPAE instance) {
		this.instance = instance;
	}

	public List<IntervencionPAE> getList() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		this.legajoActivo=false;
		if (usr.getUsuario().hasRole(Rol.LOCAL))
			return service.getIntervencionesByZonal(usr.getUsuario().getZonal());
		return service.getAllActives(IntervencionPAE.class);
	}
	
	public List<IntervencionPAE> getList(Integer legajo) {
		this.legajoActivo=false;
		return service.getList(legajo);
	}

	public void setList(List<IntervencionPAE> list) {
		this.list = list;
	}

    public String save() {
        try {
            getService().save(instance);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Intervencion salvada con exito","");
            FacesContext.getCurrentInstance().addMessage(null, message);
            this.legajoParam.setId(null);
            if (legajoActivo){
            	legajoParam.setId(this.instance.getLegajo().getId());
            	return VOLVER_A_LEGAJO;
            }
            if (fromPendiente){
            	fromPendiente=false;
            	return VOLVER_A_PENDIENTES;
            }
            return LISTADO;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return ERROR;
    }

	public IntervencionPAEService getService() {
		return service;
	}

	public void setService(IntervencionPAEService service) {
		this.service = service;
	}

	public List<IntervencionPAE> getFilteredList() {
		return filteredList;
	}

	public void setFilteredList(List<IntervencionPAE> filteredList) {
		this.filteredList = filteredList;
	}

    public boolean filterByTpoIntervencion(Object value, Object filter, Locale locale) {
        if (value == null || filter == null) {
            return false;
        }

        return ((Comparable) value).compareTo(filter) > 0;
    }

	public Boolean getLegajoActivo() {
		return legajoActivo;
	}
    
    //logica zonal-local dependiente
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(){
		return zonalService.getZonales(this.instance.getZonal());
	}
	
	public String puedeEditar(IntervencionPAE intInt){
		if (intInt==null) return "true";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				this.esUsuarioCreadorInt(intInt.getDatosAuditoria(), usr.getUsuario()) ||
				//(usr.getUsuario().hasRole(Rol.ZONAL) && intInt!=null && 
				//		intInt.getZonal()!=null && intInt.getZonal().equals(usr.getUsuario().getZonal())) 
				usr.getUsuario().hasRole(Rol.SUPERVISOR)
				|| usr.getUsuario().hasRole(Rol.DICTAMEN))
				return "true";
		else 
			return "false";
				
	}
	
	public String puedeEditar(){
		if (this.getInstance().getId()==null) return "true";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN)  ||
				this.esUsuarioCreadorInt(this.getInstance().getDatosAuditoria(), usr.getUsuario()) ||
				//(usr.getUsuario().hasRole(Rol.ZONAL) && this.getInstance().getZonal().equals(usr.getUsuario().getZonal())) 
				 usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}
	
	
	public String puedeImprimir(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (!usr.getUsuario().hasRole(Rol.OPERADOR_102))
			return "true";
		return "false";
	}
	
	@Override
	public String getNombreArchivo() {
		return this.getInstance().getLegajo().getApellidoYNombre() + "-PAE-";
	}

	@Override
	public String getNombreLegajo() {
		Legajo leg = this.getInstance().getLegajo();
		return leg.getCodigo() + " - " + leg.getApellidoYNombre();
	}

	@Override
	public String getInforme() {
		return getInstance().getInforme();
	}	
	
	public String aprobarPAE(){
		this.instance.setEstadoSuperv(EstadoSupervInterv.ACEPTADO);
		return this.save();
	}
	
	public String rechazarPAE(){
		this.instance.setEstadoSuperv(EstadoSupervInterv.RECHAZADO);
		return this.save();
	}
	
	public String puedeSuperv(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((usr.getUsuario().hasRole(Rol.SUPERVISOR) && this.instance.getEstadoSuperv()!=null && this.instance.getEstadoSuperv().equals(EstadoSupervInterv.PENDIENTE)))
			return "true";
		return "false";
	}

	public String mostrarCorregir(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) && this.instance.getEstadoSuperv()!=null && this.instance.getEstadoSuperv().equals(EstadoSupervInterv.OBSERVADA))
			return "true";
		return "false";
	}
	
	public String corregir(){
		this.instance.setEstadoSuperv(EstadoSupervInterv.PENDIENTE);
		return this.save();
	}
	
	public String isSupervisada(){
		if (this.instance!=null && instance.getEstadoSuperv()!=null && !instance.getEstadoSuperv().equals(EstadoSupervInterv.NULL))
			return "true";
		return "false";
	}

	public String cargarSoloPAE(){
		if (this.getInstance().getLegajo().getIntervencionesPAE()==null || this.getInstance().getLegajo().getIntervencionesPAE().size()==0)
			return "true";
		return "false";
	}
	
	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}
	
	public String esPAEInicial(TipoIntervencion tpoI){
		if (tpoI!=null && getCommonService().isPAEInicial(tpoI.getId())){
			return "true";
		}
		return "false";
	}
	
	public void cambiaDomicilio(){
		if (instance.getDomicilioJoven().equals(TipoDomicilioJoven.VIVIENDA)){
			instance.setFamilia(null);
			instance.setInstitucion(null);
		}else if (instance.getDomicilioJoven().equals(TipoDomicilioJoven.FORMAL)){
			instance.setTxtDomicilioJoven(null);
		}
	}
	
	public void cambiaInstitucion(){
		instance.setFamilia(null);
	}
	
	public void cambiaFamilia(){
		instance.setInstitucion(null);
	}
	
	public String getExpandAlojamiento(){
		if (instance!=null &&
				instance.getTipoIntervencion()!=null &&
				getCommonService().isPAEInicial(instance.getTipoIntervencion().getId())){
			return "0";
		}
		return "";
	}
	
	//Es necesario para la propiedad que expande o retrae la solapa de ambito de cumplimiento. Pero no hace nada este setter
	public void setExpandAlojamiento(String ex){
	}

	public void onTipoIntervencionChange(){
		if (this.getInstance().getTipoIntervencion()!=null &&
				this.getInstance().getTipoIntervencion().getTemplate()!=null)
			this.getInstance().setInforme(this.getInstance().getTipoIntervencion().getTemplate());
		else 
			this.getInstance().setInforme(null);
	}
	
	public void handleFileUpload(FileUploadEvent event) {
    	try {
            byte[] bytes=null;
     
                if (null!=event.getFile()) {
                    bytes = event.getFile().getContents();
                    String filename = FilenameUtils.getName(event.getFile().getFileName() );
        			File file = new File(getCommonService().getFileUploaderPath() + "/PAE/" + filename);
        			file.getParentFile().mkdirs();	
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();
                    this.getInstance().setArchivoAdjunto(filename);
                    FacesMessage message = new FacesMessage("El informe  " + event.getFile().getFileName() + " fue subido con éxito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    return;
                }
    		}catch(IOException e){
    			FacesMessage message = new FacesMessage("Error al subir informe ", event.getFile().getFileName());
                FacesContext.getCurrentInstance().addMessage(null, message);
    		}
    	FacesMessage message = new FacesMessage("Error al intentar subir el informe  ", event.getFile().getFileName());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
	
    public StreamedContent downloadFile(String name) {
    	StreamedContent fileDownload = null;
    	String path = getCommonService().getFileUploaderPath() + "/PAE/" + name;
        InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
		}
        return fileDownload;
    }	
}
