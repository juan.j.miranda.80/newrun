package ar.gob.run.managedControllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.GpoBienJuridicoProtegido;
import ar.gob.run.spring.model.IntervencionMEP;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.MotivoAprehension;
import ar.gob.run.spring.model.TipoDelito;
import ar.gob.run.spring.model.TipoDispositivo;
import ar.gob.run.spring.model.TipoDispositivoEgreso;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.legajo.CausaJudicial;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.CommonService;
import ar.gob.run.spring.service.IntervencionMEPService;
import ar.gob.run.spring.service.JuzgadoService;
import ar.gob.run.spring.service.TipoDelitoService;
import ar.gob.run.spring.service.TipoIntervencionService;
import ar.gob.run.utils.IHasUploadFile;

@ManagedBean(name="intervencionMEPMB")
@SessionScoped
public class IntervencionMEPManagedBean extends GenericIntervencionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";
    private static final String VOLVER_A_LEGAJO = "volverALegajo";

    List<IntervencionMEP> list;
    List<IntervencionMEP> filteredList;

    List<TipoIntervencion> listTipoIntervenciones = new ArrayList<TipoIntervencion>();
    
    List<TipoDelito> tiposDelito = new ArrayList<TipoDelito>();
    
    IntervencionMEP instance;
    
    Boolean legajoActivo = false;
    
    TipoDispositivoEgreso tipoDispositivoEgreso = TipoDispositivoEgreso.PERS;
    
    @ManagedProperty(value="#{IntervencionMEPService}")
    IntervencionMEPService service;
    
    @ManagedProperty(value="#{commonService}")
	CommonService commonService;
    
    @ManagedProperty(value="#{JuzgadoService}")
	JuzgadoService juzgadoService;
    
    @ManagedProperty(value="#{TipoIntervencionService}")
	TipoIntervencionService tipoIntervencionService;
    
    @ManagedProperty(value="#{TipoDelitoService}")
	TipoDelitoService tipoDelitoService;
    
    GpoBienJuridicoProtegido gpoBienJuridicoProtegido;
    
    public String editPendiente(IntervencionMEP ent){
    	return this.edit(ent);
    }
    
    public String edit(IntervencionMEP ent){
    	return edit(ent, false);
    }
	
	public String edit(IntervencionMEP ent, Boolean legajoActivo){
		this.instance = service.get(ent.getId());
		if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
    		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
		this.listTipoIntervenciones = tipoIntervencionService.getTipoIntervencionMEP();//this.instance.getLegajo().getIntervencionesMEP().size()==1 ||
				//tipoIntervencionService.isIntervencionMEPInicial(this.instance.getTipoIntervencion()));
		this.legajoActivo = legajoActivo;
    	return EDITOR;
    }
	
	public String nueva(Legajo legajo, Boolean legajoActivo){
    	this.instance = new IntervencionMEP();
    	this.instance.setLegajo(legajo);
    	this.legajoActivo = legajoActivo;
    	
    	List<IntervencionMEP> meps = legajo.getIntervencionesMEP();
    	if (meps!=null && !meps.isEmpty()){
	    	IntervencionMEP mep = meps.get(meps.size()-1);
	    	this.instance.setMotivoIntervencion(mep.getMotivoIntervencion());
	    	if (this.instance.getMotivoIntervencion()!=null && this.instance.getMotivoIntervencion().getCategoriaMotivoIntervencion()!=null)
	    		this.setCatMotIntSelected(this.getInstance().getMotivoIntervencion().getCategoriaMotivoIntervencion());
	    	this.listTipoIntervenciones = tipoIntervencionService.getTipoIntervencionMEP(false);
    	}else{
    		this.listTipoIntervenciones = tipoIntervencionService.getTipoIntervencionMEP(true);
    	}
    	return EDITOR;
    }
	
	public String isIntermedia(){
		if (this.instance.getTipoIntervencion()==null) return "false";
		return tipoIntervencionService.isIntervencionMEPIntermedia(this.instance.getTipoIntervencion()).toString();
	}
	
	public String isInicial(){
		if (this.instance.getTipoIntervencion()==null) return "false";
		return tipoIntervencionService.isIntervencionMEPInicial(this.instance.getTipoIntervencion()).toString();
	}
    
	public String isCierre(){
		if (this.instance.getTipoIntervencion()==null) return "false";
		return this.instance.getTipoIntervencion().getIntervencionCese().toString();
	}
	
    public void delete(IntervencionMEP ent, Boolean legajoActivo){
    	this.legajoActivo = legajoActivo;
    	getService().delete(ent);
    }
    
    public String delete(IntervencionMEP ent){
    	getService().delete(ent);
    	return LISTADO;
    }
    
    
    public String reset() {
    	this.instance = new IntervencionMEP();
        return EDITOR;
    }
    
    public String cancel(){
    	this.legajoParam.setId(null);
    	//Guardo el id del legajo antes de refrescar la instancia
    	Integer idLegajo = (this.instance!=null && this.instance.getLegajo()!=null)?this.instance.getLegajo().getId():null;
    	if (this.instance==null || this.instance.getId()==null)
    		this.instance = new IntervencionMEP();
    	if (legajoActivo){
    		legajoParam.setId(idLegajo);
        	return VOLVER_A_LEGAJO;
    	}
    	return LISTADO;
    }

	public IntervencionMEP getInstance() {
		if (instance==null)
			instance = new IntervencionMEP();
		return instance;
	}

	public void setInstance(IntervencionMEP instance) {
		this.instance = instance;
	}

	public List<IntervencionMEP> getList() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		this.legajoActivo = false;
		return service.getAllActives(IntervencionMEP.class);
	}
	
	public List<IntervencionMEP> getList(Integer legajo) {
		this.legajoActivo = false;
		return service.getList(legajo);
	}

	public void setList(List<IntervencionMEP> list) {
		this.list = list;
	}

    public String save() {
        try {
    		if (getInstance().getFechaEgreso()!=null &&
    				getInstance().getFechaEgreso().after(new Date())){
    			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
    					"La fecha de egreso no puede ser posterior a la actual", "");
    			FacesContext.getCurrentInstance().addMessage(null, message);
    			return "";
    		}
            getService().save(instance);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Intervencion salvada con exito","");
            FacesContext.getCurrentInstance().addMessage(null, message);
            this.legajoParam.setId(null);
            if (legajoActivo){
            	legajoParam.setId(this.instance.getLegajo().getId());
            	return VOLVER_A_LEGAJO;
            }
            return LISTADO;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return ERROR;
    }

	public IntervencionMEPService getService() {
		return service;
	}

	public void setService(IntervencionMEPService service) {
		this.service = service;
	}

	public List<IntervencionMEP> getFilteredList() {
		return filteredList;
	}

	public void setFilteredList(List<IntervencionMEP> filteredList) {
		this.filteredList = filteredList;
	}

    public boolean filterByTpoIntervencion(Object value, Object filter, Locale locale) {
        if (value == null || filter == null) {
            return false;
        }

        return ((Comparable) value).compareTo(filter) > 0;
    }

	public Boolean getLegajoActivo() {
		return legajoActivo;
	}
    
	public String puedeEditar(IntervencionMEP intInt){
		if (intInt==null) return "true";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				this.esUsuarioCreadorInt(intInt.getDatosAuditoria(), usr.getUsuario())
				//(usr.getUsuario().hasRole(Rol.ZONAL) && intInt!=null && 
				//		intInt.getZonal()!=null && intInt.getZonal().equals(usr.getUsuario().getZonal())) 
				|| usr.getUsuario().hasRole(Rol.SUPERVISOR)
				|| usr.getUsuario().hasRole(Rol.DICTAMEN))
				return "true";
		else 
			return "false";
				
	}
	
	public String puedeEditar(){
		if (this.getInstance().getId()==null) return "true";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN)  ||
				this.esUsuarioCreadorInt(this.instance.getDatosAuditoria(), usr.getUsuario()) ||
				//(usr.getUsuario().hasRole(Rol.ZONAL) && this.getInstance().getZonal().equals(usr.getUsuario().getZonal())) 
				 usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}
	
	
	public String puedeImprimir(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (!usr.getUsuario().hasRole(Rol.OPERADOR_102))
			return "true";
		return "false";
	}
	
	public int getSortList(Object val1, Object val2) {
		return ((Timestamp)val1).compareTo((Timestamp)val2);
	}
	
	public void onTipoIntervencionChange(){
		if (this.getInstance().getTipoIntervencion()!=null &&
				this.getInstance().getTipoIntervencion().getTemplate()!=null)
			this.getInstance().setInforme(this.getInstance().getTipoIntervencion().getTemplate());
		else 
			this.getInstance().setInforme(null);
	}

	IHasUploadFile uploadFile = new IHasUploadFile() {
		

		@Override
		public void setFileName(String name) {
			getInstance().setArchivoAdjunto(name);
		}

		@Override
		public String volver() {
			return "/legajo/intervencionMEP/editor.xhtml";
		}
		
		@Override
		public String getAllowTypes() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getFolder() {
			return "MEP";
		}
	};

	public void handleFileUpload(FileUploadEvent event) {
    	try {
            byte[] bytes=null;
     
                if (null!=event.getFile()) {
                    bytes = event.getFile().getContents();
                    String filename = FilenameUtils.getName(event.getFile().getFileName() );
        			File file = new File(getCommonService().getFileUploaderPath() + "/MEP/" + filename);
        			file.getParentFile().mkdirs();	
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();
                    this.getInstance().setArchivoAdjunto(filename);
                    FacesMessage message = new FacesMessage("El informe  " + event.getFile().getFileName() + " fue subido con éxito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    return;
                }
    		}catch(IOException e){
    			FacesMessage message = new FacesMessage("Error al subir informe ", event.getFile().getFileName());
                FacesContext.getCurrentInstance().addMessage(null, message);
    		}
    	FacesMessage message = new FacesMessage("Error al intentar subir el informe  ", event.getFile().getFileName());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public StreamedContent downloadFile(String name) {
    	StreamedContent fileDownload = null;
    	String path = getCommonService().getFileUploaderPath() + "/MEP/" + name;
        InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
		}
        return fileDownload;
    }	
	
    public String getJuzgadoInterviniente(CausaJudicial causaSel){
    	if (causaSel == null ) return "";
    	return causaSel.getJuzgadoInterviniente().getDescripcion();
    }
    
	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public List<TipoIntervencion> getListTipoIntervenciones() {
		return listTipoIntervenciones;
	}

	public void setListTipoIntervenciones(List<TipoIntervencion> listTipoIntervenciones) {
		this.listTipoIntervenciones = listTipoIntervenciones;
	}

	public TipoIntervencionService getTipoIntervencionService() {
		return tipoIntervencionService;
	}

	public void setTipoIntervencionService(TipoIntervencionService tipoIntervencionService) {
		this.tipoIntervencionService = tipoIntervencionService;
	}
	
	public TipoDispositivoEgreso[] getValoresTipoDispositivoEgreso() {
	    return TipoDispositivoEgreso.values();
	  }

	public TipoDispositivoEgreso getTipoDispositivoEgreso() {
		return tipoDispositivoEgreso;
	}

	public void setTipoDispositivoEgreso(TipoDispositivoEgreso tipoDispositivoEgreso) {
		this.tipoDispositivoEgreso = tipoDispositivoEgreso;
	} 
	
	public void setActiveIndex(String ind) {
		
	}
	
	public String getActiveIndex(){
		if (Boolean.valueOf(this.isInicial() ) ) return "0";
		else if (Boolean.valueOf(this.isIntermedia())) return "1";
		else if (Boolean.valueOf(this.isCierre())) return "2";
		else return "";
	}
	
	public String getDisabledMotApreh(){
		if (this.instance.getTipoDispositivo()!=null && 
				(this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EST_ESP_APREH) 
				|| this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EQ_ESP_GUARD_POL))){
			return "false";
		}
		return "true";
	}
	
	public String getRequiredTpoPresDelito(){
		if ((this.instance.getMotivoAprehension()!=null 
				&& this.instance.getMotivoAprehension().getId().equals(MotivoAprehension.PRESUNTO_DELITO))||
				
			(this.instance.getTipoDispositivo() != null && 
				(this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.DISP_MED_PEN_TERR) ||
					this.getInstance().getTipoDispositivo().getId().equals(TipoDispositivo.EST_PRIV_LIB) ||
					this.getInstance().getTipoDispositivo().getId().equals(TipoDispositivo.EST_RESTR_LIB)) &&
					(this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EST_ESP_APREH) 
				&& this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EQ_ESP_GUARD_POL)))){
			return "true";
		}
		return "false";
	}
	
	public String getRequiredFzSeguridad(){
		if (this.instance.getTipoDispositivo() != null && (this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EST_ESP_APREH) 
				|| this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EQ_ESP_GUARD_POL))){
			return "true";
		}
		return "false";
	}
	
	public String getRequiredHora(){
		if (this.instance.getTipoDispositivo() != null && !this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.DISP_MED_PEN_TERR) ){
			return "true";
		}
		return "false";
	}
	
	public String getRequiredArrestoyMediacion() {
		if (this.instance.getTipoDispositivo() != null && !this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EST_RESTR_LIB) ){
			return "true";
		}
		return "false";
	}
	
	public String getRequiredObsApremio() {
		if (this.instance.getTipoDispositivo() != null && 
				(this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EST_ESP_APREH) ||
						this.instance.getTipoDispositivo().getId().equals(TipoDispositivo.EQ_ESP_GUARD_POL) )){
			return "true";
		}
		return "false";
	}

	public JuzgadoService getJuzgadoService() {
		return juzgadoService;
	}

	public void setJuzgadoService(JuzgadoService juzgadoService) {
		this.juzgadoService = juzgadoService;
	}
	
	public List<CausaJudicial> getCausasJudiciales(){
		return this.instance.getLegajo().getCausasJudiciales();
	}
	
	@Override
	public String getNombreArchivo() {
		return this.getInstance().getLegajo().getApellidoYNombre() + "-MEP-";
	}

	@Override
	public String getNombreLegajo() {
		Legajo leg = this.getInstance().getLegajo();
		return leg.getCodigo() + " - " + leg.getApellidoYNombre();
	}

	@Override
	public String getInforme() {
		return getInstance().getInforme();
	}

	public GpoBienJuridicoProtegido getGpoBienJuridicoProtegido() {
		if (this.instance.getTipoPesuntoDelito()!=null && 
				this.instance.getTipoPesuntoDelito().getId()!=null){
			this.gpoBienJuridicoProtegido = this.instance.getTipoPesuntoDelito().getGpoBienJuridico();
			this.onChangeBJP();
		}
		return gpoBienJuridicoProtegido;
	}

	public void setGpoBienJuridicoProtegido(GpoBienJuridicoProtegido gpoBienJuridicoProtegido) {
		this.gpoBienJuridicoProtegido = gpoBienJuridicoProtegido;
	}	

	public void onChangeBJP(){
		this.tiposDelito = this.gpoBienJuridicoProtegido.getTpoDelitos();
	}

	public List<TipoDelito> getTiposDelito() {
		if (this.gpoBienJuridicoProtegido==null) {
			tiposDelito = tipoDelitoService.getAll();
		}
		return tiposDelito;
	}

	public void setTiposDelito(List<TipoDelito> tiposDelito) {
		this.tiposDelito = tiposDelito;
	}

	public TipoDelitoService getTipoDelitoService() {
		return tipoDelitoService;
	}

	public void setTipoDelitoService(TipoDelitoService tipoDelitoService) {
		this.tipoDelitoService = tipoDelitoService;
	}
	
}
