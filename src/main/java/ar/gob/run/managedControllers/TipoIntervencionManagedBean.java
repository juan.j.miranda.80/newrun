package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.service.TipoIntervencionService;

@ManagedBean(name="tipoIntervencionMB")
@ViewScoped
public class TipoIntervencionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{TipoIntervencionService}")
    TipoIntervencionService service;

    List<TipoIntervencion> list;

    TipoIntervencion instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(TipoIntervencion ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(TipoIntervencion ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new TipoIntervencion();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new TipoIntervencion();
    	editMode = false;
    }

	public TipoIntervencion getInstance() {
		return instance;
	}

	public void setInstance(TipoIntervencion instance) {
		this.instance = instance;
	}

	public TipoIntervencionService getService() {
		return service;
	}

	public void setService(TipoIntervencionService service) {
		this.service = service;
	}

	public List<TipoIntervencion> getList() {
		return getService().getList();
	}

	public void setList(List<TipoIntervencion> list) {
		this.list = list;
	}

	public String tieneCese(){
		if (this.getInstance().getTipoCircuito()!=null &&
				(this.getInstance().getTipoCircuito().equals(TipoCircuito.MPE) ||
						this.getInstance().getTipoCircuito().equals(TipoCircuito.MEP)))
			return "true";
		this.getInstance().setIntervencionCese(false);
		this.getInstance().setHabilitaInclusionPE(false);
		return "false";
	}
	
	public String circuitoMPE(){
		if (this.getInstance().getTipoCircuito()!=null &&
				(this.getInstance().getTipoCircuito().equals(TipoCircuito.MPE) ))
			return "true";
		this.getInstance().setIntervencionCese(false);
		this.getInstance().setHabilitaInclusionPE(false);
		return "false";
	}
	
}
