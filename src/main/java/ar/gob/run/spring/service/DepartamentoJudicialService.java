package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.DepartamentoJudicialDAO;
import ar.gob.run.spring.model.DepartamentoJudicial;
		  
@Service("DepartamentoJudicialService")
@Transactional(readOnly = true)
public class DepartamentoJudicialService extends GenericService<DepartamentoJudicial>{

	
	@Autowired
	public void setVinculoFamiliarDAO(DepartamentoJudicialDAO departamentoJudicialDAO){
		this.setDao(departamentoJudicialDAO);
	}

    public DepartamentoJudicialDAO getDepartamentoJudicialDAO() {
        return (DepartamentoJudicialDAO) this.getDao();
    }

	public List<DepartamentoJudicial> getAllActivesWithExtra(DepartamentoJudicial elem) {
		return this.getAllActivesWithExtra(DepartamentoJudicial.class, elem);
	}

    
}
