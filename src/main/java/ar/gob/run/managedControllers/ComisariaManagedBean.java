package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Comisaria;
import ar.gob.run.spring.service.ComisariaService;

@ManagedBean(name="comisariaMB")
@ViewScoped
public class ComisariaManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{ComisariaService}")
    ComisariaService service;

    List<Comisaria> list;

    Comisaria instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Comisaria ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Comisaria ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Comisaria();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Comisaria();
    	editMode = false;
    }

	public Comisaria getInstance() {
		return instance;
	}

	public void setInstance(Comisaria instance) {
		this.instance = instance;
	}

	public ComisariaService getService() {
		return service;
	}

	public void setService(ComisariaService service) {
		this.service = service;
	}

	public List<Comisaria> getList() {
		return getService().getAllActives(Comisaria.class);
	}

	public void setList(List<Comisaria> list) {
		this.list = list;
	}


	
}
