package ar.gob.run.spring.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.model.Intervencion;
import ar.gob.run.spring.model.IntervencionIntegral;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;

@Repository
public class IntervencionIntegralDAO  extends HibernateDAO<IntervencionIntegral, Integer>{

	public List<IntervencionIntegral> getList(Integer legajo) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionIntegral(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ " from IntervencionIntegral e where e.legajo.id=? and e.activo=true order by e.fecha desc")
                .setParameter(0, legajo).list();
        return list;
	}
	
	public List<IntervencionIntegral> getList() {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionIntegral(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ " from IntervencionIntegral e where e.activo=true order by e.fecha desc")
                .list();
        return list;
	}
	
	public List<IntervencionIntegral> getListByZonal(Zonal zonal){
		List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionIntegral(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ "  from IntervencionIntegral e where e.activo=true and e.zonal = :zonal order by e.fecha desc")
                .setParameter("zonal", zonal)
                .list();
        return list;
	}
	
	public List<IntervencionIntegral> getListByLocal(Local local){
		List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionIntegral(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ " from IntervencionIntegral e where e.activo=true and e.local = :local order by e.fecha desc")
                .setParameter("local", local)
                .list();
        return list;
	}

	public List<Intervencion> getRptIntervenciones(String codigoLegajo, Sexo sexo, Zonal zonalLegajo, Local localLegajo,
			Zonal zonalIntervencion, Local localIntervencion, Date fechaIntervencionDde, Date fechaIntervencionHta,
			Derivador derivador, TipoIntervencion tipoIntervencion, MotivoIntervencion motivoIntervencion,
			TipoCircuito circuito) {
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("from IntervencionIntegral i where"
                		+ " activo=true "
                		+ (codigoLegajo!=null?" and i.legajo.codigo = :codigo ":"")
                		+ (sexo != null?" and i.legajo.sexo = :sexo" :"")
                		+ (zonalLegajo!=null ? " and i.legajo.zonal = :zonalL " : "")
                		+ (localLegajo!=null ? " and i.legajo.local = :localL " : "")
                		+ (zonalIntervencion!=null ? " and i.zonal = :zonalI " : "")
                		+ (localIntervencion!=null ? " and i.local = :localI " : "")
                		+ (fechaIntervencionDde!=null ? " and i.fecha >= :fechaIDde ":"")
                		+ (fechaIntervencionHta!=null ? " and i.fecha <= :fechaIHta " :"")
                		+ (derivador!=null?" and i.derivador = :derivador " :"")
                		+ (tipoIntervencion!=null? " and i.tipoIntervencion = :tipoIntervencion " : "")
                		+ (motivoIntervencion!=null ? " and i.motivoIntervencion = :motivoIntervencion " :"")
                		+ (circuito != null ? " and i.tipoIntervencion.tipoCircuito = :circuito " :""));
		if (codigoLegajo !=null) query.setString("codigo", codigoLegajo);
		if (sexo != null) query.setParameter("sexo", sexo.name());
		if (zonalLegajo!=null) query.setParameter("zonalL", zonalLegajo);
		if (localLegajo!=null) query.setParameter("zonalL", zonalLegajo);
		if (zonalIntervencion!=null) query.setParameter("zonalI", zonalIntervencion);
		if (localIntervencion!=null) query.setParameter("localI", localIntervencion);
		if (fechaIntervencionDde!=null) query.setDate("fechaIDde", fechaIntervencionDde);
		if (fechaIntervencionHta!=null) query.setDate("fechaIHta", fechaIntervencionHta);
		if (derivador!=null) query.setParameter("derivador", derivador);
		if (tipoIntervencion!=null) query.setParameter("tipoIntervencion", tipoIntervencion);
		if (motivoIntervencion!=null) query.setParameter("motivoIntervencion", motivoIntervencion);
		if (circuito!=null) query.setParameter("circuito", circuito	);
		return query.list();
	}
	    
}