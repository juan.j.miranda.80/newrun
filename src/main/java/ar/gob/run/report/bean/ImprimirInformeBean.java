package ar.gob.run.report.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import ar.gob.run.report.common.ReportService;
import ar.gob.run.spring.model.Legajo;

@ManagedBean(name="imprimirInformeB")
@SessionScoped
public class ImprimirInformeBean implements IReportBean , Serializable{
 
	private static final long serialVersionUID = 1L;

	private String tipo;
	
	private final String COMPILE_FILE_NAME_INTEGRAL = "informeIntervencionIntegral";
	private final String COMPILE_FILE_NAME_EXCEPCIONAL = "informeIntervencionExcepcional";
	private final String COMPILE_FILE_NAME_MEP = "informeIntervencionMEP";
	private final String COMPILE_FILE_NAME_PAE = "informeIntervencionPAE";

    
    private Integer id;
    
    @ManagedProperty(value="#{ReportService}")
    ReportService reportService;
    
    
    @Override
    public Map<String, Object> getReportParameters() {
        Map<String, Object> reportParameters = new HashMap<String, Object>();
 
        if (tipo.equals("integral"))
        	reportParameters.put("ID_INTERV_PROT_INTEGRAL", id);
        else if (tipo.equals("excepcional"))
        	reportParameters.put("ID_INTERV_PROT_EXCEP", id);
        else if (tipo.equals("PAE"))
        	reportParameters.put("ID_INTERV_PAE", id);
        else
        	reportParameters.put("ID_INTERV_MEP", id);
        
 
        return reportParameters;
    }
 
    public String execute(Integer id, String tipo){
    	try {
    		this.id = id;
    		this.tipo = tipo;
            getReportService().prepareReport(getCompileFileName(), getReportParameters());
        } catch (Exception e) {
            // make your own exception handling
            e.printStackTrace();
        }
    	return null;
    }

    public String getCompileFileName() {
    	if (tipo==null) return null;
    	if (tipo.equals("integral"))
    		return COMPILE_FILE_NAME_INTEGRAL;
    	else if (tipo.equals("excepcional"))
    		return COMPILE_FILE_NAME_EXCEPCIONAL;
    	else if (tipo.equals("PAE"))
    		return COMPILE_FILE_NAME_PAE;
    	return COMPILE_FILE_NAME_MEP;
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

}