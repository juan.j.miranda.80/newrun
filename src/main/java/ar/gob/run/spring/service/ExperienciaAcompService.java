package ar.gob.run.spring.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.ExperienciaAcompDAO;
import ar.gob.run.spring.model.ExperienciaAcomp;

@Service("ExperienciaAcompService")
@Transactional(readOnly = true)
public class ExperienciaAcompService extends GenericService<ExperienciaAcomp> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setExperienciaAcompDAO(ExperienciaAcompDAO experienciaAcompDAO){	
		this.setDao(experienciaAcompDAO);
	}
	
	public ExperienciaAcompDAO getExperienciaAcompDAO() {
		return (ExperienciaAcompDAO) this.getDao();
	}
	
	public List<ExperienciaAcomp> getAll() {
		return super.getAllActives(ExperienciaAcomp.class);
	}
	
	public ExperienciaAcomp get(Integer id){
		return this.get(id, ExperienciaAcomp.class);
	}

	public List<ExperienciaAcomp> getAllActivesWithExtra(ExperienciaAcomp elem) {
		return super.getAllActivesWithExtra(ExperienciaAcomp.class, elem);
	}
}
