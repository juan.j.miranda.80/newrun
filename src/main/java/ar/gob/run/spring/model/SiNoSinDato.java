package ar.gob.run.spring.model;

public enum SiNoSinDato {
	
	S("Si"), N("No"), SD("Sin Dato");
	
	String descripcion;

	private SiNoSinDato(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
}
