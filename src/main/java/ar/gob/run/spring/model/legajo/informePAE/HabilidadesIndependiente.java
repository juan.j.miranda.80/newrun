package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_HABILIDADES_INDEPTE")
public class HabilidadesIndependiente implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo 	cocina = SiNo.N;
	private SiNo	viajaTranspPub = SiNo.N;
	private SiNo	tramites = SiNo.N;
	private SiNo	ordenarLimpiarLugares = SiNo.N;
	private SiNo	higienePersonal = SiNo.N;
	private String 	ppalesDificultades 	= "";
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_HABILIDADES_INDEPTE")
	public Integer getId() {
		return this.id;
	}

	@Column(name="PPALES_DIFICULTADES")
	public String getPpalesDificultades() {
		return ppalesDificultades;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="COCINA")
	public SiNo getCocina() {
		return cocina;
	}

	public void setCocina(SiNo cocina) {
		this.cocina = cocina;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TRANSPORTE_PUBLICO")
	public SiNo getViajaTranspPub() {
		return viajaTranspPub;
	}

	public void setViajaTranspPub(SiNo viajaTranspPub) {
		this.viajaTranspPub = viajaTranspPub;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TRAMITES")
	public SiNo getTramites() {
		return tramites;
	}

	public void setTramites(SiNo tramites) {
		this.tramites = tramites;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ORDEN_LIMPIEZA")
	public SiNo getOrdenarLimpiarLugares() {
		return ordenarLimpiarLugares;
	}

	public void setOrdenarLimpiarLugares(SiNo ordenarLimpiarLugares) {
		this.ordenarLimpiarLugares = ordenarLimpiarLugares;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="HIGIENE_PERSONAL")
	public SiNo getHigienePersonal() {
		return higienePersonal;
	}

	public void setHigienePersonal(SiNo higienePersonal) {
		this.higienePersonal = higienePersonal;
	}

	public void setPpalesDificultades(String ppalesDificultades) {
		this.ppalesDificultades = ppalesDificultades;
	}
	
	
}
