package ar.gob.run.managedControllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Acompaniante;
import ar.gob.run.spring.service.AcompanianteService;
import ar.gob.run.spring.service.CommonService;

@ManagedBean(name="acompanianteMB")
@ViewScoped
public class AcompanianteManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{AcompanianteService}")
    AcompanianteService service;

    List<Acompaniante> list;

    Acompaniante instance;
    
    @ManagedProperty(value="#{commonService}")
	CommonService commonService;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}
	
    public void handleFileAdmisionUpload(FileUploadEvent event) {
    	try {
            byte[] bytes=null;
     
                if (null!=event.getFile()) {
                    bytes = event.getFile().getContents();
                    String filename = FilenameUtils.getName(event.getFile().getFileName() );
        			File file = new File(getCommonService().getFileUploaderPath() + "/ACOMP/" + filename);
        			file.getParentFile().mkdirs();	
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();
                    this.getInstance().setFileInformeAdmision(filename);
                    FacesMessage message = new FacesMessage("El informe de admision " + event.getFile().getFileName() + " fue subido con éxito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    return;
                }
    		}catch(IOException e){
    			FacesMessage message = new FacesMessage("Error al subir informe de admision ", event.getFile().getFileName());
                FacesContext.getCurrentInstance().addMessage(null, message);
    		}
    	FacesMessage message = new FacesMessage("Error al intentar subir el informe de admision ", event.getFile().getFileName());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public StreamedContent downloadFile(String name) {
    	StreamedContent fileDownload = null;
    	String path = getCommonService().getFileUploaderPath() + "/ACOMP/" + name;
        InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
		}
        return fileDownload;
    }
    
    public void handleFileBajaUpload(FileUploadEvent event) {
    	try {
            byte[] bytes=null;
     
                if (null!=event.getFile()) {
                    bytes = event.getFile().getContents();
                    String filename = FilenameUtils.getName(event.getFile().getFileName() );
        			File file = new File(getCommonService().getFileUploaderPath() + "/ACOMP/" + filename);
        			file.getParentFile().mkdirs();	
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();
                    this.getInstance().setFileInformeBaja(filename);
                    FacesMessage message = new FacesMessage("El informe de baja " + event.getFile().getFileName() + " fue subido con éxito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    return;
                }
    		}catch(IOException e){
    			FacesMessage message = new FacesMessage("Error al subir informe de baja ", event.getFile().getFileName());
                FacesContext.getCurrentInstance().addMessage(null, message);
    		}
    	FacesMessage message = new FacesMessage("Error al subir informe de baja ", event.getFile().getFileName());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void save() {
        try {        	
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Acompaniante ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Acompaniante ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Acompaniante();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Acompaniante();
    	editMode = false;
    }

	public Acompaniante getInstance() {
		return instance;
	}

	public void setInstance(Acompaniante instance) {
		this.instance = instance;
	}

	public AcompanianteService getService() {
		return service;
	}

	public void setService(AcompanianteService service) {
		this.service = service;
	}

	public List<Acompaniante> getList() {
		return getService().getAllActives(Acompaniante.class);
	}

	public void setList(List<Acompaniante> list) {
		this.list = list;
	}


}
