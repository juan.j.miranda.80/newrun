package ar.gob.run.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.TipoIntervencionDAO;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.legajo.RelacionVincular;
		  
@Service("TipoIntervencionService")
@Transactional(readOnly = true)
public class TipoIntervencionService extends GenericService<TipoIntervencion>{

	@Autowired
	Environment environment;
	
	@Value("${tipoIntervencion.idIngresoMEP}")
	private String idIngresoMEP;
	
	@Autowired
	public void setTipoIntervencionDAO(TipoIntervencionDAO tipoIntervencionDAO){
		this.setDao(tipoIntervencionDAO);
	}

    public TipoIntervencionDAO getTipoIntervencionDAO() {
        return (TipoIntervencionDAO) this.getDao();
    }

	public List<TipoIntervencion> getAllActivesWithExtra(TipoIntervencion elem, TipoCircuito tipoCircuito) {
		return getTipoIntervencionDAO().getByTipoCircuito(elem, tipoCircuito);
	}
	
	public List<TipoIntervencion> getAll() {
		return super.getAllActives(TipoIntervencion.class);
	}
	
	public List<TipoIntervencion> getTipoIntervencionByFilterWithoutExtra(String query, TipoIntervencion elem){
		List<TipoIntervencion> lstExcluir = new ArrayList<TipoIntervencion>();
		List<TipoIntervencion> listResult = new ArrayList<TipoIntervencion>();
		lstExcluir.add(elem);
		List<TipoIntervencion> listDB = getTipoIntervencionDAO().getTipoIntervencionByFilter(query);
		for (TipoIntervencion elem2 : listDB) {
			if (!lstExcluir.contains(elem2))
				listResult.add(elem2);
		} 
		return listResult;
	}
    
	public List<TipoIntervencion> getTipoIntervencionByFilter(String query){
		return getTipoIntervencionDAO().getTipoIntervencionByFilter(query);
	}

	public List<TipoIntervencion> getTipoIntervencionByFilterWithoutExtra(String query, TipoIntervencion o,
			TipoCircuito tipoCirc, Boolean soloCese, Boolean soloInicia, Boolean soloIncPAE) {
		return getTipoIntervencionDAO().getTipoIntervencionByFilter(query, tipoCirc, soloCese, soloInicia, soloIncPAE);
	}

	public List<TipoIntervencion> getTipoIntervencionByFilter(String query, TipoCircuito tipoCirc, 
			Boolean soloCese, Boolean soloInicia, Boolean soloIncPAE) {
		return getTipoIntervencionDAO().getTipoIntervencionByFilter(query, tipoCirc, soloCese, soloInicia, soloIncPAE);
	}

	public List<TipoIntervencion> getList() {
		return getTipoIntervencionDAO().getList();
	}

	public List<TipoIntervencion> getTipoIntervencionMEP(boolean soloInicia) {
		return getTipoIntervencionDAO().getTipoIntervencionMEP(soloInicia);
	}

	public List<TipoIntervencion> getTipoIntervencionMEP() {
		return getTipoIntervencionDAO().getTipoIntervencionMEP();
	}
	
	public Boolean isIntervencionMEPIntermedia(TipoIntervencion tipoInt){
		return !tipoInt.getId().toString().equals(this.idIngresoMEP) && !tipoInt.getIntervencionCese();
	}

	public Boolean isIntervencionMEPInicial(TipoIntervencion tipoIntervencion) {
		return tipoIntervencion!=null && tipoIntervencion.getId().toString().equals(this.idIngresoMEP);
	}
}
