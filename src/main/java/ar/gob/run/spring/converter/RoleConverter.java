package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.service.RoleService;

@FacesConverter(value = "roleConverter")
public class RoleConverter implements Converter {

	public RoleConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null ) return "";
		return getService(context).get(Integer.valueOf(value), Rol.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null || ((Rol) value).getId()==null) return "";
		return ((Rol) value).getId().toString();
	}

	public RoleService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{RoleService}", RoleService.class);

			RoleService service = (RoleService)vex.getValue(context.getELContext());
			return service;
	}

}
