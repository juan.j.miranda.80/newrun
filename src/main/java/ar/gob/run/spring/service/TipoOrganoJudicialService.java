package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.TipoOrganoJudicialDAO;
import ar.gob.run.spring.model.legajo.TipoOrganoJudicial;
		  
@Service("TipoOrganoJudicialService")
@Transactional(readOnly = true)
public class TipoOrganoJudicialService extends GenericService<TipoOrganoJudicial>{

	
	@Autowired
	public void setTipoOrganoJudicialDAO(TipoOrganoJudicialDAO tipoOrganoJudicialDAO){
		this.setDao(tipoOrganoJudicialDAO);
	}

    public TipoOrganoJudicialDAO getTipoOrganoJudicialDAO() {
        return (TipoOrganoJudicialDAO) this.getDao();
    }

	public List<TipoOrganoJudicial> getAllActivesWithExtra(TipoOrganoJudicial elem) {
		return this.getAllActivesWithExtra(TipoOrganoJudicial.class, elem);
	}

    
}
