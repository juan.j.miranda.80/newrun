package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Localidad;
import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.model.OperadorPenalJuvenil;
import ar.gob.run.spring.service.LocalidadService;
import ar.gob.run.spring.service.MunicipioService;
import ar.gob.run.spring.service.OperadorPenalJuvenilService;

@ManagedBean(name="operadorPenalJuvenilMB")
@SessionScoped
public class OperadorPenalJuvenilManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";

    @ManagedProperty(value="#{OperadorPenalJuvenilService}")
    OperadorPenalJuvenilService service;

    List<OperadorPenalJuvenil> list;

    OperadorPenalJuvenil instance;
    
    Boolean editMode = false;
    
  	List<Localidad> localidades;
  	
  	@ManagedProperty(value="#{localidadService}")
  	LocalidadService localidadService;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
            getService().save(instance);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        this.editMode=false;
    }

	public void edit(OperadorPenalJuvenil ent){
    	this.instance = ent;
    	this.editMode = true;
    	this.onMunicipioChange();
    }
    
    public void delete(OperadorPenalJuvenil ent){
    	getService().delete(ent);
    	this.editMode = false;
    }
    
    public void reset() {
    	this.instance = new OperadorPenalJuvenil();
    	this.editMode = true;
    }
    
    public void cancel(){
    	this.instance = new OperadorPenalJuvenil();
    	this.editMode = false;
    }

	public OperadorPenalJuvenil getInstance() {
		return instance;
	}

	public void setInstance(OperadorPenalJuvenil instance) {
		this.instance = instance;
	}

	public OperadorPenalJuvenilService getService() {
		return service;
	}

	public void setService(OperadorPenalJuvenilService service) {
		this.service = service;
	}

	public List<OperadorPenalJuvenil> getList() {
		return getService().getAllActives(OperadorPenalJuvenil.class);
	}

	public void setList(List<OperadorPenalJuvenil> list) {
		this.list = list;
	}

	public void onMunicipioChange(){
		localidades = localidadService.getByFilter(null, this.getInstance().getDepartamento());		
	}

	public void setLocalidadService(LocalidadService localidadService) {
		this.localidadService = localidadService;
	}

	public List<Localidad> getLocalidades() {
		return localidades;
	}

	
	
}
