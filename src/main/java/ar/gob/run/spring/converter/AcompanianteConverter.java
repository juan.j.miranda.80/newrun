package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Acompaniante;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.service.AcompanianteService;

@FacesConverter(value = "acompanianteConverter")
public class AcompanianteConverter implements Converter{

	public AcompanianteConverter() {
	}

	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), Acompaniante.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((Acompaniante) value).getId().toString();
	}

	public AcompanianteService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{AcompanianteService}", AcompanianteService.class);

			AcompanianteService service = (AcompanianteService)vex.getValue(context.getELContext());
			return service;
	}

}
