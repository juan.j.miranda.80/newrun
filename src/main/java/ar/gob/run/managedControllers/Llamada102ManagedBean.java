package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.operador102.Conviviente;
import ar.gob.run.spring.model.operador102.EstadoLlamada;
import ar.gob.run.spring.model.operador102.Llamada102;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.Llamada102Service;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="llamada102MB")
@ViewScoped
public class Llamada102ManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{Llamada102Service}")
    Llamada102Service service;
    
    List<Llamada102> list;

    Llamada102 instance;
    
    Conviviente conviviente;
    
    Boolean verHistoricas;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
        	this.instance.setFecha(new Date());
            getService().save(instance);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Llamada 102 guardada con éxito", "");
            FacesContext.getCurrentInstance().addMessage(null, message);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Llamada102 ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Llamada102 ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Llamada102();
    	this.instance.setEstado(EstadoLlamada.RECIBIDA_POR_102);
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Llamada102();
    	this.instance.setEstado(EstadoLlamada.RECIBIDA_POR_102);
    	editMode = false;
    }

	public Llamada102 getInstance() {
		return instance;
	}

	public void setInstance(Llamada102 instance) {
		this.instance = instance;
	}

	public Llamada102Service getService() {
		return service;
	}

	public void setService(Llamada102Service service) {
		this.service = service;
	}
	
	public void setZonal(ActionEvent event){
		zonalParam = (Zonal)event.getComponent().getAttributes().get("zonal");
	}
	Zonal zonalParam;

	public List<Llamada102> getList() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		Map<String,String> params =
//				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//		String paramZonal = params.get("zonal");
		Zonal zonal = usr.getUsuario().hasRole(Rol.ZONAL)?usr.getUsuario().getZonal():zonalParam;//new Zonal(Integer.valueOf(zonalParam));
		if (usr.getUsuario().hasRole(Rol.ZONAL) || zonalParam!=null){
			zonalParam = null;
			if (!getVerHistoricas())
				return getService().getPendientesByZonal(zonal);
			return getService().getAllByZonal(zonal);
		}
		return getService().getAll();
	}
	
	public void setList(List<Llamada102> list) {
		this.list = list;
	}

    //logica zonal-local dependiente
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	@ManagedProperty(value="#{localService}")
	LocalService localService;
	List<Local> locales;
	
	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(){
		return zonalService.getZonales(this.instance.getZonal());
	}
	
    public void onZonalChange() {
        if(this.instance.getZonal() !=null && this.instance.getZonal().getId()!=null)
        	locales = localService.getLocales(this.instance.getZonal(),this.instance.getLocal());
        else
        	locales = new ArrayList<Local>();
    }
	
	public void setLocalService(LocalService localService) {
		this.localService = localService;
	}
	
	public List<Local> getLocales(){
		if (locales == null) locales = new ArrayList<Local>();
		if (this.instance.getLocal()!=null)
			locales.add(this.instance.getLocal());
		return locales;
	}
	// -- fin logica zonal-local dependiente --
	
	public Conviviente getConviviente() {
		if (this.conviviente==null)
			this.conviviente = new Conviviente();
		return conviviente;
	}

	public void setConviviente(Conviviente conviviente) {
		this.conviviente = conviviente;
	}

	
	public void editConviviente(Conviviente conv){
		this.conviviente = conv;
	}
	
	public void nuevoConviviente(){
		this.conviviente= new Conviviente();
	}
	
	public void deleteConviviente(Conviviente ent){
		this.instance.getConvivientes().remove(ent);
	}
	
	public void convivienteSeleccionado(){
		if (!this.instance.getConvivientes().contains(this.conviviente))
			this.instance.getConvivientes().add(this.conviviente);
		this.conviviente = new Conviviente();
	}

	public List<Conviviente> getConvivientes(){
		if (this.instance.getConvivientes()==null)
			this.instance.setConvivientes(new ArrayList<Conviviente>());
		return this.instance.getConvivientes();
	}
	
	public void aceptarLlamada(){
		this.instance.setEstado(EstadoLlamada.ACEPTADA_POR_ZONAL);
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "La llamada ha sido aceptada por el zonal","");
        FacesContext.getCurrentInstance().addMessage(null, message);
		this.save();
	}
	
	public String usuarioZonal(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) || 
				usr.getUsuario().hasRole(Rol.ADMIN))
			return "true";
		return "false";
	}
	
	public String puedeEditar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.OPERADOR_102) )
			return "true";
		return "false";
	}
	
	public String puedeBorrar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) )
			return "true";
		return "false";
	}

	public String puedeVerListado(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.OPERADOR_102) || usr.getUsuario().hasRole(Rol.ADMIN) || 
				usr.getUsuario().hasRole(Rol.ZONAL) || usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}
	
	public String puedeAceptarLlamada(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (this.instance!=null && this.instance.getZonal()!=null &&
				usr.getUsuario().hasRole(Rol.ZONAL) &&
				this.instance.getZonal().equals(usr.getUsuario().getZonal()))
			return "true";
		return "false";
		
	}

	public Boolean getVerHistoricas() {
		if (verHistoricas==null)
			this.verHistoricas=false;
		return verHistoricas;
	}

	public void setVerHistoricas(Boolean verHistoricas) {
		this.verHistoricas = verHistoricas;
	}
	
	
}
