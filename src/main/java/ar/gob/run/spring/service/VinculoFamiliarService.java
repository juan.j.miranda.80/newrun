package ar.gob.run.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.VinculoFamiliarDAO;
import ar.gob.run.spring.model.legajo.VinculoFamiliar;
		  
@Service("VinculoFamiliarService")
@Transactional(readOnly = true)
public class VinculoFamiliarService extends GenericService<VinculoFamiliar>{

	
	@Autowired
	public void setVinculoFamiliarDAO(VinculoFamiliarDAO vinculoFamiliarDAO){
		this.setDao(vinculoFamiliarDAO);
	}

    public VinculoFamiliarDAO getVinculoFamiliarDAO() {
        return (VinculoFamiliarDAO) this.getDao();
    }

    
}
