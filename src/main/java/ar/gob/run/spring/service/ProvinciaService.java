package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.ProvinciaDAO;
import ar.gob.run.spring.model.Provincia;

@Service
@Transactional(readOnly = true)
public class ProvinciaService extends GenericService<Provincia> {

	@Autowired
	public void setProvinciaDAO(ProvinciaDAO provinciaDAO){	
		this.setDao(provinciaDAO);
	}
	
	public ProvinciaDAO getProvinciaDAO() {
		return (ProvinciaDAO) this.getDao();
	}
	
	public List<Provincia> getAll() {
		return super.getAllActives(Provincia.class);
	}
	
	public Provincia get(Integer id){
		return this.get(id, Provincia.class);
	}

	public List<Provincia> getAllActivesWithExtra(Provincia elem) {
		return super.getAllActivesWithExtra(Provincia.class, elem);
	}
}
