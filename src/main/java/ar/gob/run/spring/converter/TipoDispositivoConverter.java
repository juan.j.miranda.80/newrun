package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.TipoDispositivo;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.TipoDispositivoService;

@FacesConverter(value = "tipoDispositivoConverter")
public class TipoDispositivoConverter extends GenericConverter<TipoDispositivo>{

	public TipoDispositivoConverter() {
		super(TipoDispositivo.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<TipoDispositivo> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{TipoDispositivoService}", TipoDispositivoService.class);

		TipoDispositivoService service = (TipoDispositivoService)vex.getValue(context.getELContext());
		return service;
	}

	


}
