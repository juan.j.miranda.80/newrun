package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;

@Repository
public class LocalDAO extends HibernateDAO<Local,Integer> {
	
    public List<Local> getLocales() {
        List<Local> list = currentSession().createQuery("from Local l "
        		+ "where l.activo = TRUE order by l.zonal.nombre , l.nombre").list();
        return list;
    }

	public List<Local> getLocalesByZonal(Zonal zonal) {
		List<Local> list = currentSession().createQuery("from Local l where l.zonal = :zonal "
				+ " and l.activo = TRUE order by l.nombre")
				.setParameter("zonal", zonal)
				.list();
		return list;
	}
	
}