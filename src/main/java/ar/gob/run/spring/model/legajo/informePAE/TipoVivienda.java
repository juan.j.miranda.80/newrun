package ar.gob.run.spring.model.legajo.informePAE;

public enum TipoVivienda {
	
	P("Se la presta un conocido/amigo"),
	A("Alquilado"),
	D("Es su propiedad"),
	C("Accedió a la vivienda mediante un crédito"),
	O("Otra situación"),
	S("Se encuentra en situación de calle");
	
	private String descripcion;
	
	private TipoVivienda(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
