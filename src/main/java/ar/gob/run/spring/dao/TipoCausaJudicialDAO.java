package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.TipoCausaJudicial;

@Repository
public class TipoCausaJudicialDAO extends HibernateDAO<TipoCausaJudicial,Integer> {

}
