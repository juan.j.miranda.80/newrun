/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.gob.run.spring.model.usuario.cidi;

/**
 *
 * @author 20261513650
 */
public class Pedido {

    private int IdAplicacion;
    private String Contrasenia;
    private String HashCookie;
    private String TokenValue;
    private String TimeStamp;

    /**
     * @return the IdAplicacion
     */
    public int getIdAplicacion() {
        return IdAplicacion;
    }

    /**
     * @param IdAplicacion the IdAplicacion to set
     */
    public void setIdAplicacion(int IdAplicacion) {
        this.IdAplicacion = IdAplicacion;
    }

    /**
     * @return the Contrasenia
     */
    public String getContrasenia() {
        return Contrasenia;
    }

    /**
     * @param Contrasenia the Contrasenia to set
     */
    public void setContrasenia(String Contrasenia) {
        this.Contrasenia = Contrasenia;
    }

    /**
     * @return the HashCookie
     */
    public String getHashCookie() {
        return HashCookie;
    }

    /**
     * @param HashCookie the HashCookie to set
     */
    public void setHashCookie(String HashCookie) {
        this.HashCookie = HashCookie;
    }

    /**
     * @return the TokenValue
     */
    public String getTokenValue() {
        return TokenValue;
    }

    /**
     * @param TokenValue the TokenValue to set
     */
    public void setTokenValue(String TokenValue) {
        this.TokenValue = TokenValue;
    }

    /**
     * @return the TimeStamp
     */
    public String getTimeStamp() {
        return TimeStamp;
    }

    /**
     * @param TimeStamp the TimeStamp to set
     */
    public void setTimeStamp(String TimeStamp) {
        this.TimeStamp = TimeStamp;
    }

}
