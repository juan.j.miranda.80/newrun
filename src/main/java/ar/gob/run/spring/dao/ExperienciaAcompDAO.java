package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.ExperienciaAcomp;

@Repository
public class ExperienciaAcompDAO extends HibernateDAO<ExperienciaAcomp,Integer> {

    public List<ExperienciaAcomp> getCategorias() {
        List<ExperienciaAcomp> list = currentSession().createQuery("from ExperienciaAcomp c order by c.descripcion").list();
        return list;
    }
}
