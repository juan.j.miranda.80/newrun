package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_IDENTIDAD")
public class Identidad implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo 	tieneDNI = SiNo.N;
	private SiNo 	tieneCertifEscolaridad = SiNo.N;
	private SiNo 	generoAutopercibidoDistinto = SiNo.N;
	private String 	ppalesDificultades 	= "";
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_IDENTIDAD")
	public Integer getId() {
		return this.id;
	}

	@Column(name="PPALES_DIFICULTADES")
	public String getPpalesDificultades() {
		return ppalesDificultades;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_DNI")
	public SiNo getTieneDNI() {
		return tieneDNI;
	}

	public void setTieneDNI(SiNo tieneDNI) {
		this.tieneDNI = tieneDNI;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_CERTIF_ESCOLARIDAD")
	public SiNo getTieneCertifEscolaridad() {
		return tieneCertifEscolaridad;
	}

	public void setTieneCertifEscolaridad(SiNo tieneCertifEscolaridad) {
		this.tieneCertifEscolaridad = tieneCertifEscolaridad;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="GENERO_AUTOP_DISTINTO")
	public SiNo getGeneroAutopercibidoDistinto() {
		return generoAutopercibidoDistinto;
	}

	public void setGeneroAutopercibidoDistinto(SiNo generoAutopercibidoDistinto) {
		this.generoAutopercibidoDistinto = generoAutopercibidoDistinto;
	}

	public void setPpalesDificultades(String ppalesDificultades) {
		this.ppalesDificultades = ppalesDificultades;
	}
	
	
}
