package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_PLANFIN_Y_DINERO")
public class PlanifFinYManejoDinero implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo 	recibePrestaciones = SiNo.N;
	private SiNo 	comproBienes = SiNo.N;
	private SiNo 	pudoAhorrar = SiNo.N;
	private String 	ppalesDificultades 	= "";
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_PLANFIN_Y_DINERO")
	public Integer getId() {
		return this.id;
	}

	@Column(name="PPALES_DIFICULTADES")
	public String getPpalesDificultades() {
		return ppalesDificultades;
	}

	public void setPpalesDificultades(String ppalesDificultades) {
		this.ppalesDificultades = ppalesDificultades;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="RECIBE_PRESTACIONES")
	public SiNo getRecibePrestaciones() {
		return recibePrestaciones;
	}

	public void setRecibePrestaciones(SiNo recibePrestaciones) {
		this.recibePrestaciones = recibePrestaciones;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="COMPRO_BIENES")
	public SiNo getComproBienes() {
		return comproBienes;
	}

	public void setComproBienes(SiNo comproBienes) {
		this.comproBienes = comproBienes;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="PUDO_AHORRAR")
	public SiNo getPudoAhorrar() {
		return pudoAhorrar;
	}

	public void setPudoAhorrar(SiNo pudoAhorrar) {
		this.pudoAhorrar = pudoAhorrar;
	}
	
	
}
