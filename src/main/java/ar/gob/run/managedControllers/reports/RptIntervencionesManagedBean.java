package ar.gob.run.managedControllers.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.model.Intervencion;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.IntervencionService;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="rptIntervencionesMB")
@SessionScoped
public class RptIntervencionesManagedBean implements Serializable{

	String codigoLegajo;
	Sexo sexo;
	Zonal zonalLegajo;
	Local localLegajo;
	Zonal zonalIntervencion;
	Local localIntervencion;
	Date fechaIntervencionDde;
	Date fechaIntervencionHta;
	Derivador derivador;
	TipoIntervencion tipoIntervencion;
	MotivoIntervencion motivoIntervencion;
	TipoCircuito circuito;
	
	List<Intervencion> list;
	
	@ManagedProperty(value="#{IntervencionService}")
	IntervencionService intervencionService;
	
	private static final long serialVersionUID = 1L;

	public String puedeEjecutar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) || usr.getUsuario().hasRole(Rol.ADMIN) ||
				usr.getUsuario().hasRole(Rol.SUPERVISOR) ||
				usr.getUsuario().hasRole(Rol.DICTAMEN))
			return "true";
		return "false";
	}
	
	public String usuarioZonal(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) ) return "true";
		return "false";
	}
	
	
	public void buscarIntervenciones(){
		this.list = getIntervencionService().getRptIntervenciones(getCodigoLegajo(), getSexo(), getZonalLegajo(), 
				getLocalLegajo(), getZonalIntervencion(), getLocalIntervencion(), getFechaIntervencionDde(), getFechaIntervencionHta(),
				getDerivador(), getTipoIntervencion(), getMotivoIntervencion(), getCircuito());
	}
	
    //logica zonal-local dependiente
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	@ManagedProperty(value="#{localService}")
	LocalService localService;
	List<Local> localesLegajo;
	
	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonalesLegajo(){
		return zonalService.getZonales(this.getZonalLegajo());
	}
	
    public void onZonalLegajoChange() {
        if(this.getZonalesLegajo() !=null && this.getZonalLegajo().getId()!=null)
        	localesLegajo = localService.getLocales(this.getZonalLegajo(),this.getLocalLegajo());
        else
        	localesLegajo = new ArrayList<Local>();
    }
	
	public void setLocalService(LocalService localService) {
		this.localService = localService;
	}
	
	public List<Local> getLocalesLegajo(){
		if (localesLegajo == null) localesLegajo = new ArrayList<Local>();
		return localesLegajo;
	}

    //logica zonal-local dependiente
	List<Local> localesIntervencion;
	
	public List<Zonal> getZonalesIntervencion(){
		return zonalService.getZonales(this.getZonalIntervencion());
	}
	
    public void onZonalIntervencionChange() {
        if(this.getZonalesIntervencion() !=null && this.getZonalIntervencion().getId()!=null)
        	localesIntervencion = localService.getLocales(this.getZonalIntervencion(),this.getLocalIntervencion());
        else
        	localesIntervencion = new ArrayList<Local>();
    }
	
	public List<Local> getLocalesIntervencion(){
		if (localesIntervencion == null) localesIntervencion = new ArrayList<Local>();
		return localesIntervencion;
	}
	
	public String getCodigoLegajo() {
		return codigoLegajo;
	}

	public void setCodigoLegajo(String codigoLegajo) {
		this.codigoLegajo = codigoLegajo;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Zonal getZonalLegajo() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL))
			this.zonalLegajo = usr.getUsuario().getZonal();
		return zonalLegajo;
	}

	public void setZonalLegajo(Zonal zonalLegajo) {
		this.zonalLegajo = zonalLegajo;
	}

	public Local getLocalLegajo() {
		return localLegajo;
	}

	public void setLocalLegajo(Local localLegajo) {
		this.localLegajo = localLegajo;
	}

	public Zonal getZonalIntervencion() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL))
			this.zonalIntervencion = usr.getUsuario().getZonal();
		return zonalIntervencion;
	}

	public void setZonalIntervencion(Zonal zonalIntervencion) {
		this.zonalIntervencion = zonalIntervencion;
	}

	public Local getLocalIntervencion() {
		return localIntervencion;
	}

	public void setLocalIntervencion(Local localIntervencion) {
		this.localIntervencion = localIntervencion;
	}

	public Date getFechaIntervencionDde() {
		return fechaIntervencionDde;
	}

	public void setFechaIntervencionDde(Date fechaIntervencionDde) {
		this.fechaIntervencionDde = fechaIntervencionDde;
	}

	public Date getFechaIntervencionHta() {
		return fechaIntervencionHta;
	}

	public void setFechaIntervencionHta(Date fechaIntervencionHta) {
		this.fechaIntervencionHta = fechaIntervencionHta;
	}

	public Derivador getDerivador() {
		return derivador;
	}

	public void setDerivador(Derivador derivador) {
		this.derivador = derivador;
	}

	public TipoIntervencion getTipoIntervencion() {
		return tipoIntervencion;
	}

	public void setTipoIntervencion(TipoIntervencion tipoIntervencion) {
		this.tipoIntervencion = tipoIntervencion;
	}

	public MotivoIntervencion getMotivoIntervencion() {
		return motivoIntervencion;
	}

	public void setMotivoIntervencion(MotivoIntervencion motivoIntervencion) {
		this.motivoIntervencion = motivoIntervencion;
	}

	public TipoCircuito getCircuito() {
		return circuito;
	}

	public void setCircuito(TipoCircuito circuito) {
		this.circuito = circuito;
	}

	public List<Intervencion> getList() {
		return list;
	}

	public void setList(List<Intervencion> list) {
		this.list = list;
	}

	public IntervencionService getIntervencionService() {
		return intervencionService;
	}

	public void setIntervencionService(IntervencionService intervencionService) {
		this.intervencionService = intervencionService;
	}	
	
	
}
