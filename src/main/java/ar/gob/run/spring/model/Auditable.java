package ar.gob.run.spring.model;

public interface Auditable {
	
	public DatosAuditoria getDatosAuditoria() ;
}
