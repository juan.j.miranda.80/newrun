package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.institucion.TipoInstitucion;
import ar.gob.run.spring.service.TipoInstitucionService;

@FacesConverter(value = "tipoInstitucionConverter")
public class TipoInstitucionConverter implements Converter {

	public TipoInstitucionConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), TipoInstitucion.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((TipoInstitucion) value).getId().toString();
	}

	public TipoInstitucionService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{TipoInstitucionService}", TipoInstitucionService.class);

			TipoInstitucionService service = (TipoInstitucionService)vex.getValue(context.getELContext());
			return service;
	}

}
