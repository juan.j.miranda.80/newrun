package ar.gob.run.spring.model;

public enum TipoAmbitoCumplimiento {
	FORMAL("Alojamiento formal","AF"), AMPLIADA("Familia ampliada","FA");
	
	private String descripcion;
	private String code;

	private TipoAmbitoCumplimiento(String descripcion, String code) {
		this.descripcion = descripcion;
		this.code = code;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static TipoAmbitoCumplimiento fromString(String text) {
		if (text==null) return null;
	    for (TipoAmbitoCumplimiento b : TipoAmbitoCumplimiento.values()) {
	      if (b.code.equalsIgnoreCase(text)) {
	        return b;
	      }
	    }
	    return null;
	  }
}
