package ar.gob.run.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.model.Provincia;

@Repository
public class MunicipioDAO extends HibernateDAO<Municipio,Integer> {

    public List<Municipio> getMunicipios() {
        List<Municipio> list = currentSession().createQuery("from Municipio p where p.activo = TRUE order by p.nombre").list();
        return list;
    }

    public List<Municipio> getByFilter(Provincia prov) {
        Query query = currentSession().createQuery("from Municipio p "
        		+ "where p.activo = TRUE "
        		+ (prov!=null?"and p.provincia =  :provincia":"")
        		+ " order by p.nombre");
        
        if (prov!=null) query.setParameter("provincia", prov);
        
        return query.list();
	}
}
