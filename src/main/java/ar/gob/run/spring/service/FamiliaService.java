package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.FamiliaDAO;
import ar.gob.run.spring.dao.InstitucionDAO;
import ar.gob.run.spring.model.Familia;
import ar.gob.run.spring.model.institucion.Institucion;
		  
@Service("FamiliaService")
@Transactional(readOnly = true)
public class FamiliaService extends GenericService<Familia>{

	
	@Autowired
	public void setFamiliaDAO(FamiliaDAO familiaDAO){
		this.setDao(familiaDAO);
	}

    public FamiliaDAO getFamiliaDAO() {
        return (FamiliaDAO) this.getDao();
    }

	public List<Familia> getAllActivesWithExtra(Familia elem) {
		return super.getAllActivesWithExtra(Familia.class, elem);
	}
	
	public List<Familia> getAll() {
		return super.getAllActives(Familia.class);
	}
    
}
