package ar.gob.run.spring.model;

import java.io.Serializable;

public enum TipoDispositivoEgreso implements Serializable{
	
	PERS("Persona"), DISP("Dispositivo");

	private String descripcion;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private TipoDispositivoEgreso(String st) {
		this.descripcion = st;
	}
	
}
