package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.FuerzaSeguridad;
import ar.gob.run.spring.service.FuerzaSeguridadService;

@ManagedBean(name="fuerzaSeguridadMB")
@ViewScoped
public class FuerzaSeguridadManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{FuerzaSeguridadService}")
    FuerzaSeguridadService service;

    List<FuerzaSeguridad> list;

    FuerzaSeguridad instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(FuerzaSeguridad ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(FuerzaSeguridad ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new FuerzaSeguridad();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new FuerzaSeguridad();
    	editMode = false;
    }

	public FuerzaSeguridad getInstance() {
		return instance;
	}

	public void setInstance(FuerzaSeguridad instance) {
		this.instance = instance;
	}

	public FuerzaSeguridadService getService() {
		return service;
	}

	public void setService(FuerzaSeguridadService service) {
		this.service = service;
	}

	public List<FuerzaSeguridad> getList() {
		return getService().getAllActives(FuerzaSeguridad.class);
	}

	public void setList(List<FuerzaSeguridad> list) {
		this.list = list;
	}


	
}
