package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.MotivoAprehensionDAO;
import ar.gob.run.spring.model.MotivoAprehension;

@Service("MotivoAprehensionService")
@Transactional(readOnly = true)
public class MotivoAprehensionService extends GenericService<MotivoAprehension> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setMotivoAprehensionDAO(MotivoAprehensionDAO dao){	
		this.setDao(dao);
	}
	
	public MotivoAprehensionDAO getMotivoAprehensionDAO() {
		return (MotivoAprehensionDAO) this.getDao();
	}
	
	public List<MotivoAprehension> getAll() {
		return super.getAllActives(MotivoAprehension.class);
	}
	
	public MotivoAprehension get(Integer id){
		return this.get(id, MotivoAprehension.class);
	}

}
