package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Familia;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.FamiliaService;
import ar.gob.run.spring.service.LegajoService;

@ManagedBean(name="familiaMB")
@ViewScoped
public class FamiliaManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{FamiliaService}")
    FamiliaService service;

    List<Familia> list;

    Familia instance;
    
    @ManagedProperty(value="#{legajoService}")
    LegajoService legajoService;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Familia ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Familia ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Familia();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Familia();
    	editMode = false;
    }

	public Familia getInstance() {
		return instance;
	}

	public void setInstance(Familia instance) {
		this.instance = instance;
	}

	public FamiliaService getService() {
		return service;
	}

	public void setService(FamiliaService service) {
		this.service = service;
	}

	public List<Familia> getList() {
		return getService().getAllActives(Familia.class);
	}

	public void setList(List<Familia> list) {
		this.list = list;
	}

	public String puedeConsultar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) || usr.getUsuario().hasRole(Rol.ZONAL)
				|| usr.getUsuario().hasRole(Rol.LOCAL) || usr.getUsuario().hasRole(Rol.SUPERVISOR) || usr.getUsuario().hasRole(Rol.DICTAMEN))
			return "true";
		
		return "false";
	}
	
	public String puedeEditar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) || usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}

	public LegajoService getLegajoService() {
		return legajoService;
	}

	public void setLegajoService(LegajoService legajoService) {
		this.legajoService = legajoService;
	}

	public List<Legajo> getLegajos(Familia flia){
		return getLegajoService().getLegajosAbiertosMPEPorFamilia(flia);
	}
	
	public Boolean getEditMode() {
		return editMode;
	}
}
