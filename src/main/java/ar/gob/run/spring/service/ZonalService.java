package ar.gob.run.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.ZonalDAO;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
		  
@Service
@Transactional(readOnly = true)
public class ZonalService extends GenericService<Zonal>{

	
	@Autowired
	public void setZonalDAO(ZonalDAO zonalDAO){
		this.setDao(zonalDAO);
	}

    public ZonalDAO getZonalDAO() {
        return (ZonalDAO) this.getDao();
    }

    public List<Zonal> getZonales(Zonal ent){
		List<Zonal> list = getZonalDAO().getZonales();
		if (ent!=null && ent.getId()!=null && !ent.getActivo() && !list.contains(ent)){
			List<Zonal> newList = new ArrayList<Zonal>();
			newList.add(ent);
			newList.addAll(list);
			list = newList;
		}
		return list;
    }
    
}
