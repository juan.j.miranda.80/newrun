package ar.gob.run.report.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ar.gob.run.report.common.ReportService;
import ar.gob.run.spring.model.Legajo;

@ManagedBean(name="imprimirLegajoB")
@SessionScoped
public class ImprimirLegajoBean implements IReportBean {
 
    private final String COMPILE_FILE_NAME = "detalleLegajo";
    
    private Legajo legajoParam;
    
    @ManagedProperty(value="#{ReportService}")
    ReportService reportService;
    
    
    @Override
    public Map<String, Object> getReportParameters() {
        Map<String, Object> reportParameters = new HashMap<String, Object>();
 
        reportParameters.put("ID_LEGAJO", legajoParam.getId());
 
        return reportParameters;
    }
 
    public String execute(Legajo legajo){
    	try {
    		this.legajoParam = legajo;
    		List<String> fileNames = new ArrayList<String>();	
    		fileNames.add(getCompileFileName());
    		fileNames.add("detalleLegajo_intervenciones");
            getReportService().prepareReports(fileNames, getReportParameters());
        } catch (Exception e) {
            // make your own exception handling
            e.printStackTrace();
        }
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Imprimiendo...", "");
		FacesContext.getCurrentInstance().addMessage(null, message);
    	return null;
    }

    public String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

}