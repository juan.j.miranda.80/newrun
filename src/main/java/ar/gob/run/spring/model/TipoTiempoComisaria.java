package ar.gob.run.spring.model;

public enum TipoTiempoComisaria {

	MENOS12("Hasta 12hrs"), 
	MENOS24("Entre 12 y 24 hrs"),
	DE1A3("Entre 1 y 3 días"),
	DE3A7("Entre 3 y 7 días"),
	DE7A15("Entre 7 y 15 días"),
	DE15A30("Entre 15 y 30 días"),
	DE1A2M("Entre 1 y 2 meses"),
	MASDE2M("Más de 2 meses");
	
	String descripcion;

	private TipoTiempoComisaria(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
}
