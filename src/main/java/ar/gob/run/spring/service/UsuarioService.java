package ar.gob.run.spring.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.UsuarioDAO;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;
		  
@Service("UsuarioService")
@Transactional(readOnly = true)
public class UsuarioService extends GenericService<Usuario>{

	
	private static final long serialVersionUID = 1L;
	
	@Autowired
    UsuarioDAO usuarioDAO;

    @Transactional(readOnly = false)
    public void addUsuario(Usuario usuario) {
    	if(usuario.getId()!=null && usuario.getId()>0)
    		getUsuarioDAO().updateUsuario(usuario);
    	else {
    		usuario.setFechaAlta(new Date());
    		getUsuarioDAO().addUsuario(usuario);
    	}
    }

    @Transactional(readOnly = false)
    public void deleteUsuario(Usuario usuario) {
        getUsuarioDAO().deleteUsuario(usuario);
    }

    @Transactional(readOnly = false)
    public void updateUsuario(Usuario usuario) {
        getUsuarioDAO().updateUsuario(usuario);
    }


    public Usuario getUsuarioById(int id) {
        return getUsuarioDAO().getUsuarioById(id);
    }
    
    @Override
    public Usuario get(Integer id, Class type) {
    	return this.getUsuarioById(id);
    }
    
    public Usuario getUsuarioByUserName(String userName) {
        return getUsuarioDAO().getUsuarioByUserName(userName);
    }

    public List<Usuario> getUsuarios() {
        return getUsuarioDAO().getUsuarios();
    }

    public UsuarioDAO getUsuarioDAO() {    	
        return usuarioDAO;
    }

    public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
        this.usuarioDAO = usuarioDAO;
    }
    
    public List<Rol> getRoles(){
    	return usuarioDAO.getRoles();
    }

    public Rol getRolById(Integer rol){
    	return usuarioDAO.getRolById(rol);
    }

	public List<Usuario> getUsuariosByFilter(String query, Integer referentePae) {
		return usuarioDAO.getUuariosByFilter(query, referentePae);
	}
}
