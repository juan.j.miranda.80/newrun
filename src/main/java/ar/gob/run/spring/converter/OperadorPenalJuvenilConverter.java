package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.OperadorPenalJuvenil;
import ar.gob.run.spring.service.OperadorPenalJuvenilService;

@FacesConverter(value = "operadorPenalJuvenilConverter")
public class OperadorPenalJuvenilConverter implements Converter {

	public OperadorPenalJuvenilConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), OperadorPenalJuvenil.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null ) return "";
		return ((OperadorPenalJuvenil) value).getId().toString();
	}

	public OperadorPenalJuvenilService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{OperadorPenalJuvenilService}", OperadorPenalJuvenilService.class);

			OperadorPenalJuvenilService service = (OperadorPenalJuvenilService)vex.getValue(context.getELContext());
			return service;
	}

}
