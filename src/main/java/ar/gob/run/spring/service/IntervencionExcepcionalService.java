package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.IntervencionExcepcionalDAO;
import ar.gob.run.spring.model.EstadoSupervInterv;
import ar.gob.run.spring.model.IntervencionExcepcional;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
		  
@Service("IntervencionExcepcionalService")
@Transactional(readOnly = true)
public class IntervencionExcepcionalService extends GenericService<IntervencionExcepcional>{

	@Autowired
	public void setIntervencionExcepcionalDAO(IntervencionExcepcionalDAO intervencionDAO){
		this.setDao(intervencionDAO);
	}

    public IntervencionExcepcionalDAO getIntervencionExcepcionalDAO() {
        return (IntervencionExcepcionalDAO) this.getDao();
    }

    public List<IntervencionExcepcional> getList(Integer legajo){
    	return getIntervencionExcepcionalDAO().getList(legajo);
    }
    
    @Override
    public List<IntervencionExcepcional> getAllActives(Class type) {
    	return getIntervencionExcepcionalDAO().getList();
    }
    
    public List<IntervencionExcepcional> getIntervencionesByZonal(Zonal zonal){
    	return getIntervencionExcepcionalDAO().getListByZonal(zonal);
    }
    
	public List<IntervencionExcepcional> getPendientesSupervisar() {
		return getIntervencionExcepcionalDAO().getPendientesSupervisar();
	}

	public IntervencionExcepcional get(Integer id) {
		return super.get(id, IntervencionExcepcional.class);
	}
    
}
