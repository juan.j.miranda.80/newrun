package ar.gob.run.managedControllers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.RoleService;
import ar.gob.run.spring.service.UsuarioService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="usuarioMB")
@ViewScoped
public class UsuarioManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{UsuarioService}")
    UsuarioService usuarioService;

    @ManagedProperty(value="#{RoleService}")
    RoleService roleService;
    
    List<Usuario> usuarioList;

    private List<Rol> roles;

    Usuario instance;
    
    public void addUsuario() {
        try {
//        	Rol rol = getRolById(this.rol);
//            instance.setRol(rol);
            getUsuarioService().addUsuario(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        editMode = false;
    }
    
    public String cambiarContrasenia(){
    	 try {
             getUsuarioService().addUsuario(instance);
             FacesMessage message = new FacesMessage("Se han actualizado los datos ");
             FacesContext.getCurrentInstance().addMessage(null, message);
             editMode = true;
             //FacesContext.getCurrentInstance().getExternalContext().redirect("/home");
         } catch (DataAccessException e) {
             FacesMessage message = new FacesMessage("Error inesperado, consulte al administrador");
             FacesContext.getCurrentInstance().addMessage(null, message);
             e.printStackTrace();
         } 
    	 return "home";
    }
    
	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public void editUsuario(Integer idUsuario){
    	this.instance = getUsuarioService().getUsuarioById(idUsuario);
    	editMode = true;
    }
    
    public void deleteUsuario(Integer idUsuario){
    	getUsuarioService().deleteUsuario(getUsuarioService().getUsuarioById(idUsuario));
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Usuario();
        editMode = true;
    }
    
    public void cancel(){
    	editMode = false;
    }

    public List<Usuario> getUsuarioList() {
        usuarioList = new ArrayList<Usuario>();
        usuarioList.addAll(getUsuarioService().getUsuarios());
        return usuarioList;
    }

    public UsuarioService getUsuarioService() {
        return usuarioService;
    }

    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }
    
    public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

	public List<Rol> getRoles() {
		if (roles !=null) return roles;
		roles = getRoleService().getAllActivesWithExtra(this.getInstance().getRol());
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	public Usuario getInstance() {
		HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		if (this.instance == null && origRequest!=null && origRequest.getRequestURI().contains("cambiarContrasenia"))
			this.instance = ((MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsuario();
		return instance;
	}

	public void setInstance(Usuario instance) {
		this.instance = instance;
	}
	
    //logica zonal-local dependiente
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	@ManagedProperty(value="#{localService}")
	LocalService localService;
	List<Local> locales;
	
	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(){
		return zonalService.getZonales(this.getInstance().getZonal());
	}
	
    public void onZonalChange() {
        if(this.getInstance().getZonal() !=null && this.getInstance().getZonal().getId()!=null)
        	locales = localService.getLocales(this.getInstance().getZonal(),this.getInstance().getLocal());
        else
        	locales = new ArrayList<Local>();
    }
	
	public void setLocalService(LocalService localService) {
		this.localService = localService;
	}
	
	public List<Local> getLocales(){
		if (locales == null) locales = new ArrayList<Local>();
		return locales;
	}

	public void actualizarDatos(){
		if (!instance.hasRole(Rol.LOCAL)){
			instance.setLocal(null);
			instance.setZonal(null);
		}
		if (!instance.hasRole(Rol.ZONAL)){
			instance.setZonal(null);
			instance.setLocal(null);
		}
	}
	
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
}
