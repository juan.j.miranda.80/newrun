package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="T_TIPO_INTERVENCION")
public class TipoIntervencion extends ObjetoABM implements  Identificable, Serializable, Comparable<TipoIntervencion>{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String descripcion;
	private CategoriaTipoIntervencion categoriaTipoIntervencion;
	
	private TipoCircuito tipoCircuito;
	
	private String template;

	private Boolean puedeAdjuntarDoc = false;
	
	private Boolean intervencionCese = false;
	private Boolean habilitaInclusionPE = false;
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_TIPO_INTERVENCION")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_CATEGO_TIPO_INTERV")
	public CategoriaTipoIntervencion getCategoriaTipoIntervencion() {
		return categoriaTipoIntervencion;
	}
	public void setCategoriaTipoIntervencion(CategoriaTipoIntervencion categoriaTipoIntervencion) {
		this.categoriaTipoIntervencion = categoriaTipoIntervencion;
	}
	
	@Transient
	public String getFullName(){
		return (getCategoriaTipoIntervencion()!=null?getCategoriaTipoIntervencion().getDescripcion():"") + " - " + getDescripcion();
	}
	
	
	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_CIRCUITO")
	public TipoCircuito getTipoCircuito() {
		return tipoCircuito;
	}
	
	public void setTipoCircuito(TipoCircuito tipoCircuito) {
		this.tipoCircuito = tipoCircuito;
	}
	
	@Lob
	@Column(columnDefinition="text", name="TEMPLATE")
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	@Override
	public int compareTo(TipoIntervencion o) {
		if (this.id == null) return -1;
		if (o.id == null) return 1;
		return (this.id.compareTo(o.id));
	}
	
	@Column(name="PUEDE_ADJUNTAR_DOC")
	public Boolean getPuedeAdjuntarDoc() {
		return puedeAdjuntarDoc;
	}
	public void setPuedeAdjuntarDoc(Boolean puedeAdjuntarDoc) {
		this.puedeAdjuntarDoc = puedeAdjuntarDoc;
	}
	
	@Column(name="INTERVENCION_CESE")
	public Boolean getIntervencionCese() {
		if (intervencionCese == null ) return false;
		return intervencionCese;
	}
	public void setIntervencionCese(Boolean intervencionCese) {
		this.intervencionCese = intervencionCese;
	}

	@Column(name="HABILITA_INCLUSION_PE")
	public Boolean getHabilitaInclusionPE() {
		return habilitaInclusionPE;
	}
	public void setHabilitaInclusionPE(Boolean habilitaInclusionPE) {
		this.habilitaInclusionPE = habilitaInclusionPE;
	}

}
