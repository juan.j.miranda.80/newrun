package ar.gob.run.spring.model;

public enum EstadoSupervInterv {

	NULL("No aplica"),PENDIENTE("Pendiente"),ACEPTADO("Aceptado"),RECHAZADO("Rechazado"),OBSERVADA("Observada");
	
	String name;

	private EstadoSupervInterv(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
