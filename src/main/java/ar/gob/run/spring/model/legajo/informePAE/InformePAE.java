package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.model.usuario.Usuario;

@Entity
@Table(name="T_INFORME_PAE")
public class InformePAE extends ObjetoABM  implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;

	private Legajo legajo;
	private SaludFamiliar saludFamiliar;
	private EducFormEmpleo formYEmpleo;
	private Vivienda vivienda;
	private DDHHYFormCiud ddhhYCiud;
	private FliaYContencion fliaYContencion;
	private RecreacionTpoLibre recreacion;
	private HabilidadesIndependiente habilidades;
	private Identidad identidad;
	private PlanifFinYManejoDinero planificacionYManejo;
	
    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_INFORME_PAE")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LEGAJO")
	public Legajo getLegajo() {
		return legajo;
	}

	public void setLegajo(Legajo legajo) {
		this.legajo = legajo;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_SALUD_FLIAR", referencedColumnName="ID_IPAE_SALUD_FLIAR")
	public SaludFamiliar getSaludFamiliar() {
		return saludFamiliar;
	}

	public void setSaludFamiliar(SaludFamiliar saludFamiliar) {
		this.saludFamiliar = saludFamiliar;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_EDUC_FORM_EMPLEO", referencedColumnName="ID_IPAE_EDUC_FORM_EMPLEO")
	public EducFormEmpleo getFormYEmpleo() {
		return formYEmpleo;
	}

	public void setFormYEmpleo(EducFormEmpleo formYEmpleo) {
		this.formYEmpleo = formYEmpleo;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_VIVIENDA", referencedColumnName="ID_IPAE_VIVIENDA")
	public Vivienda getVivienda() {
		return vivienda;
	}

	public void setVivienda(Vivienda vivienda) {
		this.vivienda = vivienda;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_DDHH_FORM_CIUD", referencedColumnName="ID_IPAE_DDHH_FORM_CIUD")
	public DDHHYFormCiud getDdhhYCiud() {
		return ddhhYCiud;
	}

	public void setDdhhYCiud(DDHHYFormCiud ddhhYCiud) {
		this.ddhhYCiud = ddhhYCiud;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_FLIA_Y_CONT", referencedColumnName="ID_IPAE_FLIA_Y_CONT")
	public FliaYContencion getFliaYContencion() {
		return fliaYContencion;
	}

	public void setFliaYContencion(FliaYContencion fliaYContencion) {
		this.fliaYContencion = fliaYContencion;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_REC_TPO_LIB", referencedColumnName="ID_IPAE_REC_TPO_LIB")
	public RecreacionTpoLibre getRecreacion() {
		return recreacion;
	}

	public void setRecreacion(RecreacionTpoLibre recreacion) {
		this.recreacion = recreacion;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_HABILIDADES_INDEPTE", referencedColumnName="ID_IPAE_HABILIDADES_INDEPTE")
	public HabilidadesIndependiente getHabilidades() {
		return habilidades;
	}

	public void setHabilidades(HabilidadesIndependiente habilidades) {
		this.habilidades = habilidades;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_IDENTIDAD", referencedColumnName="ID_IPAE_IDENTIDAD")
	public Identidad getIdentidad() {
		return identidad;
	}

	public void setIdentidad(Identidad identidad) {
		this.identidad = identidad;
	}

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_IPAE_PLANFIN_Y_DINERO", referencedColumnName="ID_IPAE_PLANFIN_Y_DINERO")
	public PlanifFinYManejoDinero getPlanificacionYManejo() {
		return planificacionYManejo;
	}

	public void setPlanificacionYManejo(PlanifFinYManejoDinero planificacionYManejo) {
		this.planificacionYManejo = planificacionYManejo;
	}

	public InformePAE() {
		super();
		this.saludFamiliar = new SaludFamiliar();
		this.ddhhYCiud = new DDHHYFormCiud();
		this.fliaYContencion = new FliaYContencion();
		this.formYEmpleo = new EducFormEmpleo();
		this.habilidades = new HabilidadesIndependiente();
		this.identidad = new Identidad();
		this.legajo = new Legajo();
		this.planificacionYManejo = new PlanifFinYManejoDinero();
		this.recreacion = new RecreacionTpoLibre();
		this.vivienda = new Vivienda();
	}

    public InformePAE(Integer id, Integer idLegajo, String codigo, Date fechaAlta, String userName){
    	super();
    	this.id = id;
    	this.legajo = new Legajo();
    	this.legajo.setId(idLegajo);
    	this.legajo.setCodigo(codigo);
    	this.getDatosAuditoria().setFechaAlta(fechaAlta);
    	Usuario usrAlta = new Usuario();
    	usrAlta.setUserName(userName);
    	this.getDatosAuditoria().setUsuarioAlta(usrAlta);
    }

	public InformePAE(Legajo legajo2) {
		this();
		this.setLegajo(legajo2);
		
	}
    
}
