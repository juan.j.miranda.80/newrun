package ar.gob.run.spring.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AuthenticationInterceptor  {
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String uri = request.getRequestURI();
		if (!uri.contains("login") && !uri.contains("logout")) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if ((auth instanceof AnonymousAuthenticationToken)) {
				response.sendRedirect("/j_spring_security_logout");
				return false;
			}
		}
		return true;
	}
}