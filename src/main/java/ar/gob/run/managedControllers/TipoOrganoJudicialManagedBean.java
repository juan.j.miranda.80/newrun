package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.legajo.TipoOrganoJudicial;
import ar.gob.run.spring.service.TipoOrganoJudicialService;

@ManagedBean(name="tipoOrganoJudicialMB")
@ViewScoped
public class TipoOrganoJudicialManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{TipoOrganoJudicialService}")
    TipoOrganoJudicialService service;

    List<TipoOrganoJudicial> list;

    TipoOrganoJudicial instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(TipoOrganoJudicial ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(TipoOrganoJudicial ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new TipoOrganoJudicial();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new TipoOrganoJudicial();
    	editMode = false;
    }

	public TipoOrganoJudicial getInstance() {
		return instance;
	}

	public void setInstance(TipoOrganoJudicial instance) {
		this.instance = instance;
	}

	public TipoOrganoJudicialService getService() {
		return service;
	}

	public void setService(TipoOrganoJudicialService service) {
		this.service = service;
	}

	public List<TipoOrganoJudicial> getList() {
		return getService().getAllActives(TipoOrganoJudicial.class);
	}

	public void setList(List<TipoOrganoJudicial> list) {
		this.list = list;
	}


	
}
