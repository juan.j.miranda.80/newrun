package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Familia;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Persona;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.FamiliaService;
import ar.gob.run.spring.service.LegajoService;
import ar.gob.run.spring.service.PersonaService;

@ManagedBean(name="personaMB")
@ViewScoped
public class PersonaManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{PersonaService}")
    PersonaService service;

    List<Persona> list;

    Persona instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Persona ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Persona ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Persona();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Persona();
    	editMode = false;
    }

	public Persona getInstance() {
		return instance;
	}

	public void setInstance(Persona instance) {
		this.instance = instance;
	}

	public PersonaService getService() {
		return service;
	}

	public void setService(PersonaService service) {
		this.service = service;
	}

	public List<Persona> getList() {
		return getService().getAllActives(Persona.class);
	}

	public void setList(List<Persona> list) {
		this.list = list;
	}

	public String puedeConsultar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) || usr.getUsuario().hasRole(Rol.ZONAL)
				|| usr.getUsuario().hasRole(Rol.LOCAL) || usr.getUsuario().hasRole(Rol.SUPERVISOR) || usr.getUsuario().hasRole(Rol.DICTAMEN))
			return "true";
		
		return "false";
	}
	
	public String puedeEditar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) || usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}

	public Boolean getEditMode() {
		return editMode;
	}
}
