package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.service.TipoIntervencionService;

@FacesConverter(value = "tipoIntervencionConverter")
public class TipoIntervencionConverter implements Converter {

	public TipoIntervencionConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), TipoIntervencion.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null || ((TipoIntervencion) value).getId()==null) return "";
		return ((TipoIntervencion) value).getId().toString();
	}

	public TipoIntervencionService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{TipoIntervencionService}", TipoIntervencionService.class);

			TipoIntervencionService service = (TipoIntervencionService)vex.getValue(context.getELContext());
			return service;
	}

}
