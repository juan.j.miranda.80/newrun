package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.TipoInstitucionDAO;
import ar.gob.run.spring.model.institucion.TipoInstitucion;
		  
@Service("TipoInstitucionService")
@Transactional(readOnly = true)
public class TipoInstitucionService extends GenericService<TipoInstitucion>{

	
	@Autowired
	public void setTipoInstitucionDAO(TipoInstitucionDAO tipoInstitucionDAO){
		this.setDao(tipoInstitucionDAO);
	}

    public TipoInstitucionDAO getTipoInstitucionDAO() {
        return (TipoInstitucionDAO) this.getDao();
    }

	public List<TipoInstitucion> getAllActivesWithExtra(TipoInstitucion elem) {
		return super.getAllActivesWithExtra(TipoInstitucion.class, elem);
	}
	
	public List<TipoInstitucion> getAll() {
		return super.getAllActives(TipoInstitucion.class);
	}
    
}
