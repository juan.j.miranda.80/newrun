package ar.gob.run.spring.model.legajo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.Juzgado;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.TipoCausaJudicial;

@Entity
@Table(name="T_CAUSA_JUDICIAL")
public class CausaJudicial implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String nombre;
	private String causa;
	private String presuntoDelito;
	private Boolean trasgredioLaLey = false;
	private EstadoCausaJudicial estado;
	private Juzgado juzgadoInterviniente;
	private Juzgado defensoriaInterviniente;
	private TipoDefensaTecnica defensaTecnica;
	private String apellidoNombreDefensor;
	private TipoCausaJudicial tipoCausaJudicial;
	private String observacion;
	
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_CAUSA_JUDICIAL")
	public Integer getId() {
		return this.id;
	}


	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name="CAUSA")
	public String getCausa() {
		return causa;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO")
	public EstadoCausaJudicial getEstado() {
		return estado;
	}

	public void setEstado(EstadoCausaJudicial estado) {
		this.estado = estado;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_JUZGADO_INTERV")
	public Juzgado getJuzgadoInterviniente() {
		return juzgadoInterviniente;
	}

	public void setJuzgadoInterviniente(Juzgado juzgadoInterviniente) {
		this.juzgadoInterviniente = juzgadoInterviniente;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_DEFENSORIA_INTERV")
	public Juzgado getDefensoriaInterviniente() {
		return defensoriaInterviniente;
	}

	public void setDefensoriaInterviniente(Juzgado defensoriaInterviniente) {
		this.defensoriaInterviniente = defensoriaInterviniente;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="DEFENSA_TECNICA")
	public TipoDefensaTecnica getDefensaTecnica() {
		return defensaTecnica;
	}

	public void setDefensaTecnica(TipoDefensaTecnica defensaTecnica) {
		this.defensaTecnica = defensaTecnica;
	}

	@Column(name="APEL_NOM_DEFENSOR")
	public String getApellidoNombreDefensor() {
		return apellidoNombreDefensor;
	}

	public void setApellidoNombreDefensor(String apellidoNombreDefensor) {
		this.apellidoNombreDefensor = apellidoNombreDefensor;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TIPO_CAUSA_JUDICIAL")
	public TipoCausaJudicial getTipoCausaJudicial() {
		return tipoCausaJudicial;
	}

	public void setTipoCausaJudicial(TipoCausaJudicial tipoCausaJudicial) {
		this.tipoCausaJudicial = tipoCausaJudicial;
	}

	@Column(name="OBSERVACION")
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	@Type(type= "org.hibernate.type.NumericBooleanType")
	@Column(name="TRASGREDIO_LEY", nullable=true)
	public Boolean getTrasgredioLaLey() {
		return trasgredioLaLey;
	}

	public void setTrasgredioLaLey(Boolean trasgredioLaLey) {
		this.trasgredioLaLey = trasgredioLaLey;
	}

	@Column(name="PRESUNTO_DELITO")
	public String getPresuntoDelito() {
		return presuntoDelito;
	}

	public void setPresuntoDelito(String presuntoDelito) {
		this.presuntoDelito = presuntoDelito;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((CausaJudicial)obj).getId());
	}
}
