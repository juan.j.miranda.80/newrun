package ar.gob.run.spring.model.legajo;

public enum TipoDefensaTecnica {
	
	PARTICULAR("Particular"), OFICIAL("Oficial");

	private String descripcion;
	
	
	private TipoDefensaTecnica(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getDescripcion(){
		return this.descripcion;
	}
}
