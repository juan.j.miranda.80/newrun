package ar.gob.run.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.security.MySecUser;

@MappedSuperclass
public abstract class ObjetoABM implements Identificable, Auditable{
	
	private DatosAuditoria datosAuditoria;
	private Boolean activo = true;
	
	@Embedded
	public DatosAuditoria getDatosAuditoria() {
		if (datosAuditoria==null)
			datosAuditoria = new DatosAuditoria();
		return datosAuditoria;
	}
	public void setDatosAuditoria(DatosAuditoria datosAuditoria) {
		this.datosAuditoria = datosAuditoria;
	}

	@Column(name="ACTIVO")
	public Boolean getActivo() {
		if (activo==null)this.activo = false;
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Identificable)obj).getId());
	}

}
