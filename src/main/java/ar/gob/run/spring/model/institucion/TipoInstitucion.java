package ar.gob.run.spring.model.institucion;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.CategoriaTipoIntervencion;
import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;

@Entity
@Table(name="T_TIPO_INSTITUCION")
public class TipoInstitucion extends ObjetoABM implements  Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nombre;
	
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_TIPO_INSTITUCION")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
