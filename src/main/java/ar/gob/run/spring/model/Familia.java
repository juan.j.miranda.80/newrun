package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.institucion.TipoDependencia;

@Entity
@Table(name="T_FAMILIA")
public class Familia extends ObjetoABM implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String identificacion;
	private String ubicacion;
	private String cp;
	private String telefono;
	private String email;
	private String coordenadas;
	private TipoDependencia tipoDependencia;
	private SexoAdmision sexo;
	private Integer edadAdmisionDesde;
	private Integer edadAdmisionHasta;
	private Integer cantidadPlazas;
	private String referente;
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_FAMILIA")
	@Override
	public Integer getId() {
		return id;
	}

    @Column(name="IDENTIFICACION")
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identifiacion) {
		this.identificacion = identifiacion;
	}

	@Column(name="UBICACION")
	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	@Column(name="CP")
	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	@Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="COORDENADAS")
	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_DEPENDENCIA")
	public TipoDependencia getTipoDependencia() {
		return tipoDependencia;
	}

	public void setTipoDependencia(TipoDependencia tipoDependencia) {
		this.tipoDependencia = tipoDependencia;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="SEXO")
	public SexoAdmision getSexo() {
		return sexo;
	}

	public void setSexo(SexoAdmision sexo) {
		this.sexo = sexo;
	}

	@Column(name="EDAD_DESDE")
	public Integer getEdadAdmisionDesde() {
		return edadAdmisionDesde;
	}

	public void setEdadAdmisionDesde(Integer edadAdmisionDesde) {
		this.edadAdmisionDesde = edadAdmisionDesde;
	}

	@Column(name="EDAD_HASTA")
	public Integer getEdadAdmisionHasta() {
		return edadAdmisionHasta;
	}

	public void setEdadAdmisionHasta(Integer edadAdmisionHasta) {
		this.edadAdmisionHasta = edadAdmisionHasta;
	}

	@Column(name="CANTIDAD_PLAZAS")
	public Integer getCantidadPlazas() {
		return cantidadPlazas;
	}

	public void setCantidadPlazas(Integer catntidadPlazas) {
		this.cantidadPlazas = catntidadPlazas;
	}

	@Column(name="REFERENTE")
	public String getReferente() {
		return referente;
	}

	public void setReferente(String referente) {
		this.referente = referente;
	}

	
}
