package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Zonal;

@Repository
public class ZonalDAO  extends HibernateDAO<Zonal, Integer>{
	    
    public List<Zonal> getZonales(){
    	List<Zonal> list = currentSession().createQuery("from Zonal z where z.activo = TRUE order by z.nombre").list();
        return list;
    }


}