package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.MotivoPasoCentroSaludDAO;
import ar.gob.run.spring.model.MotivoPasoCentroSalud;

@Service("MotivoPasoCentroSaludService")
@Transactional(readOnly = true)
public class MotivoPasoCentroSaludService extends GenericService<MotivoPasoCentroSalud> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setMotivoPasoCentroSaludDAO(MotivoPasoCentroSaludDAO dao){	
		this.setDao(dao);
	}
	
	public MotivoPasoCentroSaludDAO getMotivoPasoCentroSaludDAO() {
		return (MotivoPasoCentroSaludDAO) this.getDao();
	}
	
	public List<MotivoPasoCentroSalud> getAll() {
		return super.getAllActives(MotivoPasoCentroSalud.class);
	}
	
	public MotivoPasoCentroSalud get(Integer id){
		return this.get(id, MotivoPasoCentroSalud.class);
	}

}
