package ar.gob.run.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.LocalDAO;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
		  
@Service
@Transactional(readOnly = true)
public class LocalService extends GenericService<Local> {

	@Autowired
	public void setLocalDAO(LocalDAO localDAO){	
		this.setDao(localDAO);
	}
	
	public LocalDAO getLocalDAO() {
		return (LocalDAO) this.getDao();
	}
	
	public List<Local> getLocales(Local ent){
		List<Local> list = getLocalDAO().getLocales();
		if (ent!=null && ent.getId()!=null && !list.contains(ent)){
			List<Local> newList = new ArrayList<Local>();
			newList.add(ent);
			newList.addAll(list);
			list = newList;
		}
		return list;
	}

	public List<Local> getLocales(Zonal zonal, Local local) {
		List<Local> list = null;
		if (zonal!=null)
			list = getLocalDAO().getLocalesByZonal(zonal);
		else 
			list = getLocalDAO().getLocales();
		
		if (local!=null && local.getId()!=null && !local.getActivo() && !list.contains(local)){
			List<Local> newList = new ArrayList<Local>();
			newList.add(local);
			newList.addAll(list);
			list = newList;
		}
		return list;
	}

}
