package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.service.MotivoIntervencionService;

@ManagedBean(name="motivoIntervencionMB")
@ViewScoped
public class MotivoIntervencionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{MotivoIntervencionService}")
    MotivoIntervencionService service;

    List<MotivoIntervencion> list;

    MotivoIntervencion instance;
    
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(MotivoIntervencion ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(MotivoIntervencion ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new MotivoIntervencion();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new MotivoIntervencion();
    	editMode = false;
    }

	public MotivoIntervencion getInstance() {
		return instance;
	}

	public void setInstance(MotivoIntervencion instance) {
		this.instance = instance;
	}

	public MotivoIntervencionService getService() {
		return service;
	}

	public void setService(MotivoIntervencionService service) {
		this.service = service;
	}

	public List<MotivoIntervencion> getList() {
		return getService().getAllActives(MotivoIntervencion.class);
	}

	public void setList(List<MotivoIntervencion> list) {
		this.list = list;
	}


	
}
