package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_REC_TPO_LIB")
public class RecreacionTpoLibre implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo 	participoCulturales = SiNo.N;
	private String 	ppalesDificultades 	= "";
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_REC_TPO_LIB")
	public Integer getId() {
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="PARTICIPO_CULTURALES")
	public SiNo getParticipoCulturales() {
		return participoCulturales;
	}

	public void setParticipoCulturales(SiNo participoCulturales) {
		this.participoCulturales = participoCulturales;
	}

	@Column(name="PPALES_DIFICULTADES")
	public String getPpalesDificultades() {
		return ppalesDificultades;
	}

	public void setPpalesDificultades(String ppalesDificultades) {
		this.ppalesDificultades = ppalesDificultades;
	}
	
	
}
