package ar.gob.run.managedControllers;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import com.ibm.as400.util.commtrace.Message;

import ar.gob.run.spring.model.EstadoDesconocePoseeONo;
import ar.gob.run.spring.model.EstadoDesconoceSIoNO;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.legajo.EstadoCivil;
import ar.gob.run.spring.model.legajo.EstadoDocumento;
import ar.gob.run.spring.model.legajo.EstudioCursado;
import ar.gob.run.spring.model.legajo.RelacionVincular;
import ar.gob.run.spring.model.legajo.SituacionLaboral;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.LegajoService;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="legajoInformePAEMB")
@SessionScoped
public class LegajoInformePAEManagedBean extends LegajoManagedBean {

	private static final long serialVersionUID = 1L;

	@Override
	public List<Legajo> getList() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.REFERENTE_PAE)|| usr.getUsuario().hasRole(Rol.ADMIN)){
			return legajoService.getLegajosAsignadosReferentePAE(usr.getUsuario());
		}
		return new ArrayList<Legajo>();
	}
}
