package ar.gob.run.report.bean;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import ar.gob.run.report.common.ReportService;
import ar.gob.run.spring.model.Legajo;

@ManagedBean(name="imprimirCaratulaB")
@SessionScoped
public class ImprimirCaratulaBean implements IReportBean {
 
    private final String COMPILE_FILE_NAME = "caratula";
    
    private Legajo legajoParam;
    
    @ManagedProperty(value="#{ReportService}")
    ReportService reportService;
    
    
    @Override
    public Map<String, Object> getReportParameters() {
        Map<String, Object> reportParameters = new HashMap<String, Object>();
 
        reportParameters.put("ID_LEGAJO", legajoParam.getId());
 
        return reportParameters;
    }
 
    public String execute(Legajo legajo){
    	try {
    		this.legajoParam = legajo;
            getReportService().prepareReport(getCompileFileName(), getReportParameters());
        } catch (Exception e) {
            // make your own exception handling
            e.printStackTrace();
        }
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Imprimiendo...", "");
		FacesContext.getCurrentInstance().addMessage(null, message);
    	return null;
    }

    public String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

}