package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="T_TIPO_DELITO")
public class TipoDelito implements Serializable, Identificable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String descripcion;
   
    private GpoBienJuridicoProtegido gpoBienJuridico;
    
    @Id
    @Column(name="ID_TIPO_DELITO")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", desc : ").append(getDescripcion());
        return strBuff.toString();
    }

    

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Identificable)obj).getId());
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ID_GPO_BIEN_JURIDICO", referencedColumnName="ID_GPO_BIEN_JURIDICO_PROTEGIDO")
	public GpoBienJuridicoProtegido getGpoBienJuridico() {
		return gpoBienJuridico;
	}

	public void setGpoBienJuridico(GpoBienJuridicoProtegido gpoBienJuridico) {
		this.gpoBienJuridico = gpoBienJuridico;
	}
	
	
}
