package ar.gob.run.spring.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.institucion.Institucion;
import ar.gob.run.spring.model.usuario.Usuario;

@Entity
@Table(name="T_INTERV_PAE")
public class IntervencionPAE extends ObjetoABM implements Serializable, Intervencion{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Zonal zonal;
	private Date fecha;
	private TipoIntervencion tipoIntervencion;
	private MotivoIntervencion motivoIntervencion;
	private Legajo legajo;
	private EstadoSupervInterv estadoSuperv;
	private Date fechaUltCambioEstado;
	private String archivoAdjunto;
	
	private Dimension dimension;
	
	private Familia familia;
	private Institucion institucion;
	private String txtDomicilioJoven;
	private TipoDomicilioJoven domicilioJoven;
	
	private SiNo conformidad;
	
	private String observacionMedida;
	
	private String informe;
	
	@Override
	public void setId(Integer id) {
		this.id=id;
	}

    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_INTERV_PROT_EXCEP")
	@Override
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}

	@Column(name="FECHA")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_INTERVENCION")
	public TipoIntervencion getTipoIntervencion() {
		return tipoIntervencion;
	}

	public void setTipoIntervencion(TipoIntervencion tipoIntervencion) {
		this.tipoIntervencion = tipoIntervencion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LEGAJO")
	public Legajo getLegajo() {
		return legajo;
	}

	public void setLegajo(Legajo legajo) {
		this.legajo = legajo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MOTIVO_INTERVENCION")
	public MotivoIntervencion getMotivoIntervencion() {
		return motivoIntervencion;
	}

	public void setMotivoIntervencion(MotivoIntervencion motivoIntervencion) {
		this.motivoIntervencion = motivoIntervencion;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO_SUPERV", nullable = true)
	public EstadoSupervInterv getEstadoSuperv() {
		return estadoSuperv;
	}

	public void setEstadoSuperv(EstadoSupervInterv estadoSuperv) {
		this.fechaUltCambioEstado = new Date();
		this.estadoSuperv = estadoSuperv;
	}

	@Override
	public int compareTo(Intervencion o) {
		if ((this.fecha != null && o.getFecha()!=null) && 
				(this.fecha.compareTo(o.getFecha()) != 0)) 
			return this.fecha.compareTo(o.getFecha());
		if (this.tipoIntervencion.compareTo(o.getTipoIntervencion()) != 0) return this.tipoIntervencion.compareTo(o.getTipoIntervencion()) ;
		return this.legajo.getCodigo().compareTo(o.getLegajo().getCodigo());
	}

	@Transient
	@Override
	public Local getLocal() {
		return null;
	}

	@Column(name="ARCHIVO_ADJUNTO")
	public String getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(String archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_FAMILIA")
	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_INSTITUCION")
	public Institucion getInstitucion() {
		return institucion;
	}

	public void setInstitucion(Institucion institucion) {
		this.institucion = institucion;
	}

	@Column(name="TXT_DOMICILIO_JOVEN")
	public String getTxtDomicilioJoven() {
		return txtDomicilioJoven;
	}

	public void setTxtDomicilioJoven(String txtFamiliaAmpliada) {
		this.txtDomicilioJoven = txtFamiliaAmpliada;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="DOMICILIO_JOVEN", nullable = true)
	public TipoDomicilioJoven getDomicilioJoven() {
		return domicilioJoven;
	}

	public void setDomicilioJoven(TipoDomicilioJoven tipoAmbitoCumplimiento) {
		this.domicilioJoven = tipoAmbitoCumplimiento;
	}

	@Column(name="OBSERVACION_MEDIDA")
	public String getObservacionMedida() {
		return observacionMedida;
	}

	public void setObservacionMedida(String observacionMedida) {
		this.observacionMedida = observacionMedida;
	}

	@Column(name="FECHA_ULT_CMBIO_EST")
	public Date getFechaUltCambioEstado() {
		return fechaUltCambioEstado;
	}

	public void setFechaUltCambioEstado(Date fechaUltCambioEstado) {
		this.fechaUltCambioEstado = fechaUltCambioEstado;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_DIMENSION")
	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="CONFORMIDAD")
	public SiNo getConformidad() {
		return conformidad;
	}

	public void setConformidad(SiNo conformidad) {
		this.conformidad = conformidad;
	}

	@Transient
	@Override
	public Derivador getDerivador() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Lob
	@Column(columnDefinition="text", name="INFORME")
	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	
	public IntervencionPAE(){
		super();
	}
	
	public IntervencionPAE(Integer id, Date fecha, Integer idLegajo,
			String codigo,
			String nombre, String tpoInterv, EstadoSupervInterv estadoSup,
			String archivo, String usrAlta){
		super();
		this.fecha = fecha;
		this.id = id;
		this.legajo = new Legajo();
		this.legajo.setCodigo(codigo);
		this.legajo.setApellidoYNombre(nombre);
		this.setTipoIntervencion(new TipoIntervencion());
		this.getTipoIntervencion().setDescripcion(tpoInterv);
		this.setEstadoSuperv(estadoSup);
		this.archivoAdjunto = archivo;
		this.getDatosAuditoria().setUsuarioAlta(new Usuario());
		this.getDatosAuditoria().getUsuarioAlta().setUserName(usrAlta);
		this.legajo.setId(idLegajo);
		
	}
	
}
