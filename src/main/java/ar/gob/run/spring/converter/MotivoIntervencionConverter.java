package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.service.MotivoIntervencionService;
import ar.gob.run.spring.service.TipoInstitucionService;

@FacesConverter(value = "motivoIntervencionConverter")
public class MotivoIntervencionConverter implements Converter {

	public MotivoIntervencionConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), MotivoIntervencion.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null || ((MotivoIntervencion) value).getId()==null) return "";
		return ((MotivoIntervencion) value).getId().toString();
	}

	public MotivoIntervencionService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{MotivoIntervencionService}", MotivoIntervencionService.class);

			MotivoIntervencionService service = (MotivoIntervencionService)vex.getValue(context.getELContext());
			return service;
	}

}
