package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.legajo.TipoOrganoJudicial;

@Entity
@Table(name="T_JUZGADO")
public class Juzgado extends ObjetoABM implements  Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String descripcion;
	
	private TipoOrganoJudicial tipoOrganoJudicial;
	private DepartamentoJudicial departamentoJudicial;
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_JUZGADO")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_DEPARTAMENTO_JUDICIAL")
	public DepartamentoJudicial getDepartamentoJudicial() {
		return departamentoJudicial;
	}

	public void setDepartamentoJudicial(DepartamentoJudicial departamentoJudicial) {
		this.departamentoJudicial = departamentoJudicial;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TPO_ORG_JUDICIAL")
	public TipoOrganoJudicial getTipoOrganoJudicial() {
		return tipoOrganoJudicial;
	}

	public void setTipoOrganoJudicial(TipoOrganoJudicial tipoOrganoJudicial) {
		this.tipoOrganoJudicial = tipoOrganoJudicial;
	}

}
