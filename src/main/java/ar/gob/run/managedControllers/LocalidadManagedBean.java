package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Localidad;
import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.service.LocalidadService;
import ar.gob.run.spring.service.MunicipioService;

@ManagedBean(name="localidadMB")
@ViewScoped
public class LocalidadManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{localidadService}")
    LocalidadService service;

    List<Localidad> list;

    Localidad instance;
    
    List<Municipio> municipios;
    
    @ManagedProperty(value="#{municipioService}")
    MunicipioService municipioService;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Localidad ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Localidad ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Localidad();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Localidad();
    	editMode = false;
    }

	public Localidad getInstance() {
		return instance;
	}

	public void setInstance(Localidad instance) {
		this.instance = instance;
	}

	public LocalidadService getService() {
		return service;
	}

	public void setService(LocalidadService service) {
		this.service = service;
	}

	public List<Localidad> getList() {
		return getService().getAllActives(Localidad.class);
	}

	public void setList(List<Localidad> list) {
		this.list = list;
	}

	public List<Municipio> getMunicipios() {
		if (municipios == null || this.instance.getProvincia()==null ||
				this.instance.getProvincia().getId()==null) municipios = municipioService.getAll();
		return municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}

	public MunicipioService getMunicipioService() {
		return municipioService;
	}

	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}

	public void onProvinciaChange(){
		this.municipios = this.getMunicipioService().getByFilter(this.getInstance().getProvincia());
		String codigo ="";
		if (this.instance.getProvincia()!=null)
			codigo = this.instance.getProvincia().getId().toString();
		if (this.instance.getMunicipio()!=null)
			codigo += StringUtils.leftPad(this.instance.getMunicipio().getId().toString(), 
					3, "0");
		this.instance.setCodigoBahra(codigo);	
	}
	
	public void onMuniChange(){
		String codigo ="";
		if (this.instance.getProvincia()!=null)
			codigo = this.instance.getProvincia().getId().toString();
		if (this.instance.getMunicipio()!=null){			
			codigo += StringUtils.leftPad(this.instance.getMunicipio().getId().toString(), 
					3, "0");
		}
		this.instance.setCodigoBahra(codigo);
	}
}
