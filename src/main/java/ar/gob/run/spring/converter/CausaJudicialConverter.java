package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.legajo.CausaJudicial;
import ar.gob.run.spring.service.CausaJudicialService;

@FacesConverter(value = "causaJudicialConverter")
public class CausaJudicialConverter implements Converter{

	public CausaJudicialConverter() {
	}

	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), CausaJudicial.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((CausaJudicial) value).getId().toString();
	}

	public CausaJudicialService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{CausaJudicialService}", CausaJudicialService.class);

			CausaJudicialService service = (CausaJudicialService)vex.getValue(context.getELContext());
			return service;
	}

}
