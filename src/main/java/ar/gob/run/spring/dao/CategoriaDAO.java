package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Categoria;

@Repository
public class CategoriaDAO extends HibernateDAO<Categoria,Integer> {

    public List<Categoria> getCategorias() {
        List<Categoria> list = currentSession().createQuery("from Categoria c order by c.descripcion").list();
        return list;
    }
}
