package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Procedencia;
import ar.gob.run.spring.service.ProcedenciaService;

@ManagedBean(name="procedenciaMB")
@ViewScoped
public class ProcedenciaManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{ProcedenciaService}")
    ProcedenciaService service;

    List<Procedencia> list;

    Procedencia instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Procedencia ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Procedencia ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Procedencia();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Procedencia();
    	editMode = false;
    }

	public Procedencia getInstance() {
		return instance;
	}

	public void setInstance(Procedencia instance) {
		this.instance = instance;
	}

	public ProcedenciaService getService() {
		return service;
	}

	public void setService(ProcedenciaService service) {
		this.service = service;
	}

	public List<Procedencia> getList() {
		return getService().getAllActives(Procedencia.class);
	}

	public void setList(List<Procedencia> list) {
		this.list = list;
	}


	
}
