package ar.gob.run.spring.model.legajo;

public enum EstadoDocumento {
	
	POSEE_DOCUMENTO("Posee documento"), POSEE_DOC_EXTRANJERO("Posee documento extranjero"),
	NO_POSEE_DOCUMENTO("No posee documento"), SE_DESCONOCE("Se desconoce"),EN_TRAMITE("En trámite"), PERDIDO("Perdido");

	private String descripcion;
	
	
	private EstadoDocumento(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getDescripcion(){
		return this.descripcion;
	}
}
