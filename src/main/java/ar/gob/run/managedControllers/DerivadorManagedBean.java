package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.service.DerivadorService;

@ManagedBean(name="derivadorMB")
@ViewScoped
public class DerivadorManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{DerivadorService}")
    DerivadorService service;

    List<Derivador> list;

    Derivador instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Derivador ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Derivador ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Derivador();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Derivador();
    	editMode = false;
    }

	public Derivador getInstance() {
		return instance;
	}

	public void setInstance(Derivador instance) {
		this.instance = instance;
	}

	public DerivadorService getService() {
		return service;
	}

	public void setService(DerivadorService service) {
		this.service = service;
	}

	public List<Derivador> getList() {
		return getService().getAllActives(Derivador.class);
	}

	public void setList(List<Derivador> list) {
		this.list = list;
	}

	public Boolean getEditMode() {
		return editMode;
	}
	
}
