package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.LocalidadDAO;
import ar.gob.run.spring.model.Localidad;
import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.model.Provincia;

@Service
@Transactional(readOnly = true)
public class LocalidadService extends GenericService<Localidad> {

	@Autowired
	public void setLocalidadDAO(LocalidadDAO LocalidadDAO){	
		this.setDao(LocalidadDAO);
	}
	
	public LocalidadDAO getLocalidadDAO() {
		return (LocalidadDAO) this.getDao();
	}
	
	public List<Localidad> getAll() {
		return super.getAllActives(Localidad.class);
	}
	
	public Localidad get(Integer id){
		return this.get(id, Localidad.class);
	}

	public List<Localidad> getAllActivesWithExtra(Localidad elem) {
		return super.getAllActivesWithExtra(Localidad.class, elem);
	}

	public List<Localidad> getByFilter(Provincia prov, Municipio muni) {
		return getLocalidadDAO().getByFilter(prov, muni);
	}
}
