package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.TipoDelitoDAO;
import ar.gob.run.spring.model.TipoDelito;

@Service("TipoDelitoService")
@Transactional(readOnly = true)
public class TipoDelitoService extends GenericService<TipoDelito> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setTipoDelitoDAO(TipoDelitoDAO dao){	
		this.setDao(dao);
	}
	
	public TipoDelitoDAO getTipoDelitoDAO() {
		return (TipoDelitoDAO) this.getDao();
	}
	
	public List<TipoDelito> getAll() {
		return super.getAllActives(TipoDelito.class);
	}
	
	public TipoDelito get(Integer id){
		return this.get(id, TipoDelito.class);
	}

}
