package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.legajo.EstadoCivil;
import ar.gob.run.spring.service.EstadoCivilService;

@FacesConverter(value = "estadoCivilConverter")
public class EstadoCivilConverter implements Converter {

	public EstadoCivilConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), EstadoCivil.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null ) return "";
		return ((EstadoCivil) value).getId().toString();
	}

	public EstadoCivilService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{EstadoCivilService}", EstadoCivilService.class);

			EstadoCivilService service = (EstadoCivilService)vex.getValue(context.getELContext());
			return service;
	}

}
