package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Provincia;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.ProvinciaService;

@FacesConverter(value = "provinciaConverter")
public class ProvinciaConverter extends GenericConverter<Provincia>{

	public ProvinciaConverter() {
		super(Provincia.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Provincia> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{provinciaService}", ProvinciaService.class);

		ProvinciaService provinciaService = (ProvinciaService)vex.getValue(context.getELContext());
		return provinciaService;
	}

	


}
