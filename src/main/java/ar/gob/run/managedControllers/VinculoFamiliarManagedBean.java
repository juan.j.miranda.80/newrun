package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.legajo.VinculoFamiliar;
import ar.gob.run.spring.service.VinculoFamiliarService;

@ManagedBean(name="vinculoFamiliarMB")
@SessionScoped
public class VinculoFamiliarManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";

    @ManagedProperty(value="#{VinculoFamiliarService}")
    VinculoFamiliarService service;
    
	@ManagedProperty(value="#{legajoMB}")
	LegajoManagedBean legajoMB;

    List<VinculoFamiliar> list;

    VinculoFamiliar instance;
	private String nombre;
    
    public String save() {
        try {
            getService().save(instance);
            return LISTADO;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return ERROR;
    }

	public String edit(VinculoFamiliar ent){
    	this.instance = ent;
    	return EDITOR;
    }
    
//    public void delete(VinculoFamiliar ent){
//    	getList().remove(ent);
//    }
    
    public String reset() {
    	this.instance = new VinculoFamiliar();
        return EDITOR;
    }
    
    public String cancel(){
    	this.instance = new VinculoFamiliar();
    	return LISTADO;
    }

	public VinculoFamiliar getInstance() {
		if (instance==null)
			instance = new VinculoFamiliar();
		return instance;
	}

	public void setInstance(VinculoFamiliar instance) {
		this.instance = instance;
	}

	public VinculoFamiliarService getService() {
		return service;
	}

	public void setService(VinculoFamiliarService service) {
		this.service = service;
	}

//	public List<VinculoFamiliar> getList() {
//		return legajoMB.getInstance().getVinculosFamiliares();
//	}

	public void setList(List<VinculoFamiliar> list) {
		this.list = list;
	}

//    public void vinculoSeleccionado() {
//        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Vinculo Familiar Seleccionado", "");
//        FacesContext.getCurrentInstance().addMessage(null, message);
//        this.getLegajoMB().getInstance().getVinculosFamiliares().add(getInstance());
//        this.instance=new VinculoFamiliar();
//    }
//    
    public String getNombre(){
    	return this.nombre;
    }
    
    public void setNombre(String nombre){
    	this.nombre=nombre;
    }

	public LegajoManagedBean getLegajoMB() {
		return legajoMB;
	}

	public void setLegajoMB(LegajoManagedBean legajoMB) {
		this.legajoMB = legajoMB;
	}

    
}
