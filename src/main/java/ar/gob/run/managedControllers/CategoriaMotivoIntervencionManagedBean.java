package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.CategoriaMotivoIntervencion;
import ar.gob.run.spring.service.CategoriaMotivoIntervencionService;

@ManagedBean(name="categoriaMotivoIntervencionMB")
@ViewScoped
public class CategoriaMotivoIntervencionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{CategoriaMotivoIntervencionService}")
    CategoriaMotivoIntervencionService service;

    List<CategoriaMotivoIntervencion> list;

    CategoriaMotivoIntervencion instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

	public void edit(CategoriaMotivoIntervencion ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(CategoriaMotivoIntervencion ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new CategoriaMotivoIntervencion();
    	editMode = true;
    }
    
    public void cancel(){
    	this.instance = new CategoriaMotivoIntervencion();
    	editMode = false;
    }

	public CategoriaMotivoIntervencion getInstance() {
		return instance;
	}

	public void setInstance(CategoriaMotivoIntervencion instance) {
		this.instance = instance;
	}

	public CategoriaMotivoIntervencionService getService() {
		return service;
	}

	public void setService(CategoriaMotivoIntervencionService service) {
		this.service = service;
	}

	public List<CategoriaMotivoIntervencion> getList() {
		return getService().getAllActives(CategoriaMotivoIntervencion.class);
	}

	public void setList(List<CategoriaMotivoIntervencion> list) {
		this.list = list;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	
	
}
