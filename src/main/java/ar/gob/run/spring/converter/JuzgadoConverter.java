package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Juzgado;
import ar.gob.run.spring.service.JuzgadoService;

@FacesConverter(value = "juzgadoConverter")
public class JuzgadoConverter implements Converter {

	public JuzgadoConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), Juzgado.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null ) return "";
		return ((Juzgado) value).getId().toString();
	}

	public JuzgadoService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{JuzgadoService}", JuzgadoService.class);

			JuzgadoService service = (JuzgadoService)vex.getValue(context.getELContext());
			return service;
	}

}
