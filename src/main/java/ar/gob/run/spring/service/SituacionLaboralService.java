package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.SituacionLaboralDAO;
import ar.gob.run.spring.model.legajo.SituacionLaboral;
		  
@Service("SituacionLaboralService")
@Transactional(readOnly = true)
public class SituacionLaboralService extends GenericService<SituacionLaboral>{

	
	@Autowired
	public void setSituacionLaboralDAO(SituacionLaboralDAO situacionLaboralDAO){
		this.setDao(situacionLaboralDAO);
	}

    public SituacionLaboralDAO getSituacionLaboralDAO() {
        return (SituacionLaboralDAO) this.getDao();
    }

	public List<SituacionLaboral> getAllActivesWithExtra(SituacionLaboral elem) {
		return super.getAllActivesWithExtra(SituacionLaboral.class, elem);
	}

    
}
