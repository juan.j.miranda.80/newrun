package ar.gob.run.spring.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "upperCaseConverter")
public class UpperCaseConverter implements Converter {

	public UpperCaseConverter() {
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return value.toUpperCase();
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return value.toString();
	}

}
