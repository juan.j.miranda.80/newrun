package ar.gob.run.spring.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.model.Intervencion;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;

@Repository
public class IntervencionDAO  extends HibernateDAO<Intervencion, Integer>{

	    
}