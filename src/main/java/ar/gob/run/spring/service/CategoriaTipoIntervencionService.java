package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.CategoriaTipoIntervencionDAO;
import ar.gob.run.spring.model.CategoriaTipoIntervencion;

@Service("CategoriaTipoIntervencionService")
@Transactional(readOnly = true)
public class CategoriaTipoIntervencionService extends GenericService<CategoriaTipoIntervencion> {

	@Autowired
	public void setCategoriaTipoIntervencionDAO(CategoriaTipoIntervencionDAO categoriaTipoIntervencionDAO){	
		this.setDao(categoriaTipoIntervencionDAO);
	}
	
	public CategoriaTipoIntervencionDAO getCategoriaTipoIntervencionDAO() {
		return (CategoriaTipoIntervencionDAO) this.getDao();
	}
	
	public List<CategoriaTipoIntervencion> getAll() {
		return super.getAllActives(CategoriaTipoIntervencion.class);
	}
	
	public CategoriaTipoIntervencion get(Integer id){
		return this.get(id, CategoriaTipoIntervencion.class);
	}

	public List<CategoriaTipoIntervencion> getAllActivesWithExtra(CategoriaTipoIntervencion elem) {
		return super.getAllActivesWithExtra(CategoriaTipoIntervencion.class, elem);
	}
}
