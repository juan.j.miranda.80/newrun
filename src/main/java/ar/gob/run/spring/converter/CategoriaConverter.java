package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Categoria;
import ar.gob.run.spring.service.CategoriaService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "categoriaConverter")
public class CategoriaConverter extends GenericConverter<Categoria>{

	public CategoriaConverter() {
		super(Categoria.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Categoria> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{categoriaService}", CategoriaService.class);

		CategoriaService categoriaService = (CategoriaService)vex.getValue(context.getELContext());
		return categoriaService;
	}

	


}
