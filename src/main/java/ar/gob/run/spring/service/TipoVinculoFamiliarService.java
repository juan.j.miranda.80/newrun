package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.TipoVinculoFamiliarDAO;
import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;
		  
@Service("TipoVinculoFamiliarService")
@Transactional(readOnly = true)
public class TipoVinculoFamiliarService extends GenericService<TipoVinculoFamiliar>{

	
	@Autowired
	public void setTipoVinculoFamiliarDAO(TipoVinculoFamiliarDAO vinculoFamiliarDAO){
		this.setDao(vinculoFamiliarDAO);
	}

    public TipoVinculoFamiliarDAO getTipoVinculoFamiliarDAO() {
        return (TipoVinculoFamiliarDAO) this.getDao();
    }

	public List<TipoVinculoFamiliar> getAllActivesWithExtra(TipoVinculoFamiliar elem) {
		return super.getAllActivesWithExtra(TipoVinculoFamiliar.class, elem);
	}

    
}
