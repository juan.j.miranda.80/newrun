package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Dimension;
import ar.gob.run.spring.service.DimensionService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "dimensionConverter")
public class DimensionConverter extends GenericConverter<Dimension>{

	public DimensionConverter() {
		super(Dimension.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Dimension> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{DimensionService}", DimensionService.class);

		DimensionService dimensionService = (DimensionService)vex.getValue(context.getELContext());
		return dimensionService;
	}

	


}
