package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name="T_LOCALIDAD")
public class Localidad extends ObjetoABM implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String nombre;
    
    private Municipio municipio;
    private Provincia provincia;
    
    private String codigoBahra;
   
    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_LOCALIDAD")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", nombre : ").append(getNombre());
        return strBuff.toString();
    }

    @Column(name="NOMBRE")
    public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Localidad)obj).getId());
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MUNICIPIO")
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PROVINCIA")
	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	@Column(name="CODIGO_BAHRA" )
	public String getCodigoBahra() {
		return codigoBahra;
	}

	public void setCodigoBahra(String codigoBahra) {
		this.codigoBahra = codigoBahra;
	}
	
	
}
