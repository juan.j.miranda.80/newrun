package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="T_MOTIVO_INTERVENCION")
public class MotivoIntervencion extends ObjetoABM implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String descripcion;

	private DerechoAfectado derechoAfectado = new DerechoAfectado();
	private CategoriaMotivoIntervencion categoriaMotivoIntervencion = new CategoriaMotivoIntervencion();
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_MOTIVO_INTERVENCION")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_DERECHO_AFECTADO")
	public DerechoAfectado getDerechoAfectado() {
		return derechoAfectado;
	}

	public void setDerechoAfectado(DerechoAfectado derechoAfectado) {
		this.derechoAfectado = derechoAfectado;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_CATEGO_MOTIVO_INTERV")
	public CategoriaMotivoIntervencion getCategoriaMotivoIntervencion() {
		return categoriaMotivoIntervencion;
	}

	public void setCategoriaMotivoIntervencion(CategoriaMotivoIntervencion categoriaMotivoIntervencion) {
		this.categoriaMotivoIntervencion = categoriaMotivoIntervencion;
	}
}
