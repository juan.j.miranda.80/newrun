package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.DerechoAfectadoDAO;
import ar.gob.run.spring.model.DerechoAfectado;

@Service("DerechoAfectadoService")
@Transactional(readOnly = true)
public class DerechoAfectadoService extends GenericService<DerechoAfectado> {

	@Autowired
	public void setDerechoAfectadoDAO(DerechoAfectadoDAO derechoAfectadoDAO){	
		this.setDao(derechoAfectadoDAO);
	}
	
	public DerechoAfectadoDAO getDerechoAfectadoDAO() {
		return (DerechoAfectadoDAO) this.getDao();
	}
	
	public List<DerechoAfectado> getAll() {
		return super.getAllActives(DerechoAfectado.class);
	}
	
	public DerechoAfectado get(Integer id){
		return this.get(id, DerechoAfectado.class);
	}

	public List<DerechoAfectado> getAllActivesWithExtra(DerechoAfectado elem) {
		return super.getAllActivesWithExtra(DerechoAfectado.class, elem);
	}
}
