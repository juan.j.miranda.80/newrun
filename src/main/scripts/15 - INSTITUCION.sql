CREATE TABLE "T_TIPO_INSTITUCION" 
   (	"ID_TIPO_INSTITUCION" NUMBER(10,0) NOT NULL ENABLE,    
	"NOMBRE" VARCHAR2(500),  
  "FECHA_ALTA" DATE,
  "ID_USUARIO_ALTA" NUMBER(10,0),
  "FECHA_MODIFICACION" DATE,
  "ID_USUARIO_MODIF" NUMBER(10,0),
  "ACTIVO" NUMBER(1,0),
	 CONSTRAINT "PK_TPO_INST" PRIMARY KEY ("ID_TIPO_INSTITUCION"),   
   CONSTRAINT "FK_TPO_INST_USU_ALTA" FOREIGN KEY ("ID_USUARIO_ALTA") REFERENCES "T_USUARIO" ("ID_USUARIO"),
   CONSTRAINT "FK_TPO_INST_USU_MODIF" FOREIGN KEY ("ID_USUARIO_MODIF") REFERENCES "T_USUARIO" ("ID_USUARIO")
   )  ;
   
CREATE TABLE "T_INSTITUCION" 
   (	"ID_INSTITUCION" NUMBER(10,0) NOT NULL ENABLE,    
    	"ID_ZONAL" NUMBER(10,0) ,       
      "ID_TIPO_INSTITUCION" NUMBER(10,0) , 
      "TELEFONO" VARCHAR2(30),
      "NOMBRE" VARCHAR2(200),
      "INSTITUCION_RESPONSABLE" VARCHAR2(200),
      "RESPONSABLE_LEGAL" VARCHAR2(200),
      "RES_ESPACIO_CONVIVENCIAL" VARCHAR2(200),
      "EMAIL" VARCHAR2(100),
      "SEXO" VARCHAR2(1),
      "CREENCIA_RELIGIOSA" VARCHAR2(200),
      "EDAD_DESDE" NUMBER(3),
      "EDAD_HASTA" NUMBER(3),
      "PLAZAS" NUMBER(4),
      "OBSERVACIONES" VARCHAR2(1000),
      "FECHA_ALTA" DATE,
      "ID_USUARIO_ALTA" NUMBER(10,0),
      "FECHA_MODIFICACION" DATE,
      "ID_USUARIO_MODIF" NUMBER(10,0),
      "ACTIVO" NUMBER(1,0),
	 CONSTRAINT "PK_INST" PRIMARY KEY ("ID_INSTITUCION"), 
   CONSTRAINT "FK_INST_ZONAL" FOREIGN KEY ("ID_ZONAL") REFERENCES "T_ZONAL" ("ID_ZONAL"),
   CONSTRAINT "FK_INST_TPOINST" FOREIGN KEY ("ID_TIPO_INSTITUCION") REFERENCES "T_TIPO_INSTITUCION" ("ID_TIPO_INSTITUCION"),
   CONSTRAINT "FK_INST_USU_ALTA" FOREIGN KEY ("ID_USUARIO_ALTA") REFERENCES "T_USUARIO" ("ID_USUARIO"),
   CONSTRAINT "FK_INST_USU_MODIF" FOREIGN KEY ("ID_USUARIO_MODIF") REFERENCES "T_USUARIO" ("ID_USUARIO")
   )  ;   