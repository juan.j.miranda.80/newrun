package ar.gob.run.spring.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;

@Repository
public class UsuarioDAO  extends HibernateDAO<Usuario, Integer>{
	
    @Autowired
    private SessionFactory sessionFactory;

    static final Logger logger = Logger.getLogger(UsuarioDAO.class);
    
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public Serializable addUsuario(Usuario usuario) {
        return getSessionFactory().getCurrentSession().save(usuario);
    }

    public void deleteUsuario(Usuario usuario) {
        getSessionFactory().getCurrentSession().delete(usuario);
    }

    public void updateUsuario(Usuario usuario) {
        getSessionFactory().getCurrentSession().update(usuario);
    }


    public Usuario getUsuarioById(int id) {
    	logger.info("Buscando usuario: " + id);
        List list = getSessionFactory().getCurrentSession()
                                            .createQuery("from Usuario  where id=?")
                                            .setParameter(0, id).list();
        return (Usuario)list.get(0);
    }

    public Usuario getUsuarioByUserName(String userName) {
        List list = getSessionFactory().getCurrentSession()
                                            .createQuery("from Usuario  where userName=?")
                                            .setParameter(0, userName).list();
        logger.info("id: || usuario: " + userName + " || accion: ingresando a sistema");
        return (Usuario)list.get(0);
    }

    public List<Usuario> getUsuarios() {
        List<Usuario> list = getSessionFactory().getCurrentSession().createQuery("from Usuario").list();
        return list;
    }
    
    public List<Rol> getRoles(){
    	List<Rol> list = getSessionFactory().getCurrentSession().createQuery("from Rol").list();
        return list;
    }

    public Rol getRolById(Integer id){
        List list = getSessionFactory().getCurrentSession()
                .createQuery("from Rol  where id=?")
                .setParameter(0, id).list();
        return (Rol)list.get(0);
    }

	public List<Usuario> getUuariosByFilter(String queryStr, Integer referentePae) {
		Query query = getSessionFactory().getCurrentSession().createQuery("from Usuario z where "
				+ "upper(z.userName) like :query"
				+ " and (:referentePae is null or z.rol.id = :referentePae) "
				+ " and z.activo = TRUE "
				+ " order by z.userName asc").
				setParameter("query", "%"+queryStr.toUpperCase()+"%")
				.setParameter("referentePae", referentePae);
				
		List<Usuario> list = query.list();
		return list;
	}
}