package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.legajo.EstudioCursado;
import ar.gob.run.spring.service.EstudioCursadoService;

@FacesConverter(value = "estudioCursadoConverter")
public class EstudioCursadoConverter implements Converter {

	public EstudioCursadoConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), EstudioCursado.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null ) return "";
		return ((EstudioCursado) value).getId().toString();
	}

	public EstudioCursadoService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{EstudioCursadoService}", EstudioCursadoService.class);

			EstudioCursadoService service = (EstudioCursadoService)vex.getValue(context.getELContext());
			return service;
	}

}
