package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Nacionalidad;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.NacionalidadService;

@FacesConverter(value = "nacionalidadConverter")
public class NacionalidadConverter extends GenericConverter<Nacionalidad>{

	public NacionalidadConverter() {
		super(Nacionalidad.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Nacionalidad> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{nacionalidadService}", NacionalidadService.class);

		NacionalidadService nacionalidadService = (NacionalidadService)vex.getValue(context.getELContext());
		return nacionalidadService;
	}

	


}
