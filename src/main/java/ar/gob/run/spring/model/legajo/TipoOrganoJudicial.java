package ar.gob.run.spring.model.legajo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;

@Entity
@Table(name="T_TPO_ORG_JUDICIAL")
public class TipoOrganoJudicial extends ObjetoABM implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String descripcion;
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_TPO_ORG_JUDICIAL")
	public Integer getId() {
		return this.id;
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Identificable)obj).getId());
	}
}
