package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.DimensionDAO;
import ar.gob.run.spring.model.Dimension;

@Service("DimensionService")
@Transactional(readOnly = true)
public class DimensionService extends GenericService<Dimension> {

	@Autowired
	public void setDimensionDAO(DimensionDAO dimensionDAO){	
		this.setDao(dimensionDAO);
	}
	
	public DimensionDAO getDimensionDAO() {
		return (DimensionDAO) this.getDao();
	}
	
	public List<Dimension> getAll() {
		return super.getAllActives(Dimension.class);
	}
	
	public Dimension get(Integer id){
		return this.get(id, Dimension.class);
	}

	public List<Dimension> getAllActivesWithExtra(Dimension elem) {
		return super.getAllActivesWithExtra(Dimension.class, elem);
	}
}
