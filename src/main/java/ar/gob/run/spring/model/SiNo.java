package ar.gob.run.spring.model;

public enum SiNo {
	
	S("Si"), N("No");
	
	String descripcion;

	private SiNo(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
}
