package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.DepartamentoJudicial;

@Repository
public class DepartamentoJudicialDAO  extends HibernateDAO<DepartamentoJudicial, Integer>{
	    
}