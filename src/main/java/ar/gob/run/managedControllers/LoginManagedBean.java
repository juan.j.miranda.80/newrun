package ar.gob.run.managedControllers;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.model.usuario.cidi.Sesion;
import ar.gob.run.spring.service.LoginService;
import ar.gob.run.spring.service.UsuarioService;

@ManagedBean(name = "loginMB")
@SessionScoped
public class LoginManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String password;
	private String message, userName;

	@ManagedProperty(value="#{UsuarioService}")
    UsuarioService usuarioService;
    
    @ManagedProperty("#{loginService}")
    LoginService loginService;
    
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String login() {
		Usuario usuario = usuarioService.getUsuarioByUserName(userName);
		boolean result = usuario!=null && getLoginService().loginUser(userName, password);
		if (result) {
			// get Http Session and store username
//			session.setAttribute("username", userName);
//			session.setAttribute("usuario", usuario);
			
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Login valido", "Bienvenido a RUN");
	        FacesContext.getCurrentInstance().addMessage(null, message);
	        
			return "home";
		} else {

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Login invalido", "Usuario o password incorrecto"));

			// invalidate session, and redirect to other pages

			// message = "Invalid Login. Please Try Again!";
			return "login";
		}
	}

	public String logout() {
//		HttpSession session = getAppLoginUtils().getSession();
//		session.invalidate();
		return "login";
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

//	public AppLoginUtils getAppLoginUtils() {
//		return appLoginUtils;
//	}
//
//	public void setAppLoginUtils(AppLoginUtils appLoginUtils) {
//		this.appLoginUtils = appLoginUtils;
//	}
	
	
}
