package ar.gob.run.report.common;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ar.gob.run.report.util.ReportConfigUtil;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.j2ee.servlets.BaseHttpServlet;

@Service("ReportService")
public class ReportService {
	 
    public enum ExportOption {
 
        PDF, HTML, EXCEL, RTF
    }
    private ExportOption exportOption;
    
	@Value("${reports.folder}")
	private String reportsFolder;
	
    private String message;
    
    private SessionFactory sessionFactory;
 
	@Autowired
    private Environment environment;
    
    public ReportService() {
        super();
        setExportOption(ExportOption.PDF);
    }
 
    private JasperPrint getJasperPrint(String compileFileName, Map<String, Object> parameteres) throws JRException, IOException {

    	ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
    	 
        ServletContext context = (ServletContext) externalContext.getContext();
 
        ReportConfigUtil.compileReport(context, getCompileDir(), compileFileName);
 
        File reportFile = new File(getClass().getClassLoader().getResource(compileFileName + ".jasper").getFile());
 
        ///////////////////
        Connection conn = null;
        try {
            conn = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        SessionImplementor sessionImplementation = (SessionImplementor) getSessionFactory().openSession();
        //ConnectionProvider connectionProvider = sessionImplementation.getJdbcConnectionAccess().obtainConnection();
        try {
            conn = sessionImplementation.getJdbcConnectionAccess().obtainConnection();
            // do your work using connection
        } catch (SQLException e) {
            e.printStackTrace();
        }
 
        /////////////////////
 
        JasperPrint jasperPrint = ReportConfigUtil.fillReport(reportFile, parameteres, conn);
        try {
			sessionImplementation.getJdbcConnectionAccess().releaseConnection(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return jasperPrint;
    }
    
    public void prepareReport(String compileFileName, Map<String, Object> parameteres) throws JRException, IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
 
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
 
        removeSessionAttributes(request.getSession());
        /////////////////////
 
        JasperPrint jasperPrint = getJasperPrint(compileFileName, parameteres);

        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        // Set to expire far in the past.
        response.setHeader("Expires", "-1");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        if (getExportOption().equals(ExportOption.HTML)) {
            ReportConfigUtil.exportReportAsHtml(jasperPrint, response.getWriter());
        } else if (getExportOption().equals(ExportOption.EXCEL)) {
            ReportConfigUtil.exportReportAsExcel(jasperPrint, response.getWriter());
        } else if (getExportOption().equals(ExportOption.PDF)) {
        	request.getSession().setAttribute(net.sf.jasperreports.j2ee.servlets.PdfServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
            response.sendRedirect(request.getContextPath() + "/servlets/report/" + getExportOption()
            	+ "?" + net.sf.jasperreports.j2ee.servlets.PdfServlet.JASPER_PRINT_REQUEST_PARAMETER + "=" + net.sf.jasperreports.j2ee.servlets.PdfServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE);
        } else {
            request.getSession().setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
            response.sendRedirect(request.getContextPath() + "/servlets/report/" + getExportOption());
        }
 
        FacesContext.getCurrentInstance().responseComplete();
    }
    
    public void prepareReports(List<String> compileFileName, Map<String, Object> parameteres) throws JRException, IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
 
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
 
        removeSessionAttributes(request.getSession());
        
        List<JasperPrint> jasperPrints = new ArrayList<JasperPrint>();
        Iterator<String> fileName = compileFileName.iterator();
        while (fileName.hasNext())
        	jasperPrints.add(getJasperPrint(fileName.next(), parameteres));
        
        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        // Set to expire far in the past.
        response.setHeader("Expires", "-1");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
 
    	request.getSession().setAttribute(net.sf.jasperreports.j2ee.servlets.PdfServlet.DEFAULT_JASPER_PRINT_LIST_SESSION_ATTRIBUTE, jasperPrints);
        response.sendRedirect(request.getContextPath() + "/servlets/report/" + getExportOption());
 
        FacesContext.getCurrentInstance().responseComplete();
    }    
 
    public ExportOption getExportOption() {
        return exportOption;
    }
 
    public void setExportOption(ExportOption exportOption) {
        this.exportOption = exportOption;
    }
 
    protected String getCompileDir() {
        return reportsFolder;
    }
    
 
    public String getMessage() {
        return message;
    }
 
    public void setMessage(String message) {
        this.message = message;
    }

    public SessionFactory getSessionFactory(){
    	return this.sessionFactory;
    }
    
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory){
    	this.sessionFactory = sessionFactory;
    }

    private void removeSessionAttributes(HttpSession session){
    	session.removeAttribute(net.sf.jasperreports.j2ee.servlets.PdfServlet.DEFAULT_JASPER_PRINT_LIST_SESSION_ATTRIBUTE);
    	session.removeAttribute(net.sf.jasperreports.j2ee.servlets.PdfServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE);
    	
    }
}