package ar.gob.run.spring.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.legajo.EstadoCivil;
import ar.gob.run.spring.model.legajo.EstudioCursado;
import ar.gob.run.spring.model.legajo.SituacionLaboral;

@Entity
@Table(name="T_PERSONA")
public class Persona implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private String apellidoYNombre;
	private String documento;
	private Sexo sexo;
	private EstadoCivil estadoCivil;
	private String telefono;
	private String direccion;
	private EstudioCursado estudioCursado;
	private SituacionLaboral situacionLaboral;
	private Date fechaNacimiento;
	
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_PERSONA")
	public Integer getId() {
		return this.id;
	}
	
	@Column(name="APELLIDO_Y_NOMBRE")
	public String getApellidoYNombre() {
		if (this.apellidoYNombre!=null) this.apellidoYNombre = this.apellidoYNombre.toUpperCase();
		return apellidoYNombre;
	}

	public void setApellidoYNombre(String apellidoYNombre) {
		if (apellidoYNombre!=null) apellidoYNombre = apellidoYNombre.toUpperCase();
		this.apellidoYNombre = apellidoYNombre;
	}

	@Column(name="DOCUMENTO")
	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="SEXO")
	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ESTADO_CIVIL")
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	@Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name="DIRECCION")
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ESTUDIO_CURSADO")
	public EstudioCursado getEstudioCursado() {
		return estudioCursado;
	}

	public void setEstudioCursado(EstudioCursado estudioCursado) {
		this.estudioCursado = estudioCursado;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_SITUACION_LABORAL")
	public SituacionLaboral getSituacionLaboral() {
		return situacionLaboral;
	}

	public void setSituacionLaboral(SituacionLaboral situacionLaboral) {
		this.situacionLaboral = situacionLaboral;
	}

	@Column(name="FECHA_NACIMIENTO")	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
}
