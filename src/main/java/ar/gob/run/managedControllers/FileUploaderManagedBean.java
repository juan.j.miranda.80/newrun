package ar.gob.run.managedControllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import ar.gob.run.spring.service.CommonService;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.utils.IHasUploadFile;

@ManagedBean(name="fileUploaderMB")
@SessionScoped
public class FileUploaderManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private UploadedFile file;
	
	static final Logger logger = Logger.getLogger(GenericService.class);
	
    @ManagedProperty(value="#{commonService}")
	CommonService commonService;
	
    public void fileUpload(FileUploadEvent event) {
    	try {
            byte[] bytes=null;
     
                if (null!=event.getFile()) {
                    bytes = event.getFile().getContents();
                    String filename = FilenameUtils.getName(event.getFile().getFileName() );
        			File file = new File(getCommonService().getFileUploaderPath()+ iMB.getFolder() + filename);
        			file.getParentFile().mkdirs();	
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();
                    iMB.setFileName(filename);
                    FacesMessage message = new FacesMessage("El archivo " + event.getFile().getFileName() + " fue subido con éxito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    FacesContext.getCurrentInstance().getExternalContext().redirect(
                    		FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()  + iMB.volver());
                    return;
                }
    		}catch(IOException e){
    			FacesMessage message = new FacesMessage("Error al subir archivo ", event.getFile().getFileName());
                FacesContext.getCurrentInstance().addMessage(null, message);
                logger.error(e);
    		}
    	FacesMessage message = new FacesMessage("Error al intentar subir un archivo ", event.getFile().getFileName());
        FacesContext.getCurrentInstance().addMessage(null, message);
        logger.error(message.getDetail());
    }
    
    IHasUploadFile iMB;
    
    public String uploadFile(IHasUploadFile iMB){
    	this.iMB = iMB;
    	return "editor";
    }

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}
    
	private StreamedContent fileDownload;
    
    public StreamedContent downloadFile(String name) {
    	String path = getCommonService().getFileUploaderPath() + iMB.getFolder() + name;
        InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
			logger.error(e);
		}
        return fileDownload;
    }
    
    public StreamedContent downloadFile(String name, String folder) {
    	StreamedContent fileDownload = null;
    	String path = getCommonService().getFileUploaderPath() + folder + name;
        InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
			logger.error(e);
		}
        return fileDownload;
    }

    public String getAllowTypes(){
    	String defaultTypes = "gif|jpe?g|pdf";
    	if (iMB!=null && iMB.getAllowTypes()!=null)
    		defaultTypes = iMB.getAllowTypes();
    	
    	return "/(\\.|\\/)(" + defaultTypes + ")$/";
    }
    
}
