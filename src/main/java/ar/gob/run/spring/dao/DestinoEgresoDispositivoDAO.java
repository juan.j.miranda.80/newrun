package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.DestinoEgresoDispositivo;

@Repository
public class DestinoEgresoDispositivoDAO extends HibernateDAO<DestinoEgresoDispositivo,Integer> {

}
