package ar.gob.run.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Persona;

@Repository
public class PersonaDAO  extends HibernateDAO<Persona, Integer>{

	public List<Persona> getPersonaByFilter(String apYN, String dni) {
		Query query = currentSession().createQuery("from Persona z where  "
				+ (apYN!=null?"upper(z.apellidoYNombre) like :apYN ":"")
				+ (dni!=null?"upper(z.documento) like :dni ":"")
				+ "order by z.apellidoYNombre asc");
		if (apYN!=null)
			query.setParameter("apYN", "%"+apYN.toUpperCase()+"%");
		if (dni!=null)
			query.setParameter("dni", "%"+dni.toUpperCase()+"%");
		
		List<Persona> list = query.list();
		return list;
	}

}