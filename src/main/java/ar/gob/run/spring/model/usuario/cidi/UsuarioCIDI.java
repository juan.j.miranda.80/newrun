	/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.gob.run.spring.model.usuario.cidi;

public class UsuarioCIDI {

    private String CUIL;
    private String CuilFormateado;
    private String NroDocumento;
    private String Apellido;
    private String Nombre;
    private String NombreFormateado;
    private String FechaNacimiento;
    private String Id_Sexo;
    private String PaiCodPais;
    private int Id_Numero;
    private int Id_Estado;
    private String Estado;
    private String Email;
    private String TelArea;
    private String TelNro;
    private String TelFormateado;
    private String CelArea;
    private String CelNro;
    private String CelFormateado;
    private String Empleado;
    private String Id_Empleado;
    private String FechaRegistro;
    private String FechaBloqueo;
    private String IdAplicacionOrigen;
    private String TieneRepresentados;
    private String CodigoIngresado;
    private String Constatado;
//    private Domicilio Domicilio;
//    private Representado Representado;
//    private Respuesta Respuesta;
//    private RespuestaModificacion RespuestaModificacion;

    public UsuarioCIDI() {
//        Domicilio = new Domicilio();
//        Representado = new Representado();
//        Respuesta = new Respuesta();
//        RespuestaModificacion = new RespuestaModificacion();
    }

    /**
     * @return the CUIL
     */
    public String getCUIL() {
        return CUIL;
    }

    /**
     * @param CUIL the CUIL to set
     */
    public void setCUIL(String CUIL) {
        this.CUIL = CUIL;
    }

    /**
     * @return the CuilFormateado
     */
    public String getCuilFormateado() {
        return CuilFormateado;
    }

    /**
     * @param CuilFormateado the CuilFormateado to set
     */
    public void setCuilFormateado(String CuilFormateado) {
        this.CuilFormateado = CuilFormateado;
    }

    /**
     * @return the NroDocumento
     */
    public String getNroDocumento() {
        return NroDocumento;
    }

    /**
     * @param NroDocumento the NroDocumento to set
     */
    public void setNroDocumento(String NroDocumento) {
        this.NroDocumento = NroDocumento;
    }

    /**
     * @return the Apellido
     */
    public String getApellido() {
        return Apellido;
    }

    /**
     * @param Apellido the Apellido to set
     */
    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the NombreFormateado
     */
    public String getNombreFormateado() {
        return NombreFormateado;
    }

    /**
     * @param NombreFormateado the NombreFormateado to set
     */
    public void setNombreFormateado(String NombreFormateado) {
        this.NombreFormateado = NombreFormateado;
    }

    /**
     * @return the FechaNacimiento
     */
    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    /**
     * @param FechaNacimiento the FechaNacimiento to set
     */
    public void setFechaNacimiento(String FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    /**
     * @return the Id_Sexo
     */
    public String getId_Sexo() {
        return Id_Sexo;
    }

    /**
     * @param Id_Sexo the Id_Sexo to set
     */
    public void setId_Sexo(String Id_Sexo) {
        this.Id_Sexo = Id_Sexo;
    }

    /**
     * @return the PaiCodPais
     */
    public String getPaiCodPais() {
        return PaiCodPais;
    }

    /**
     * @param PaiCodPais the PaiCodPais to set
     */
    public void setPaiCodPais(String PaiCodPais) {
        this.PaiCodPais = PaiCodPais;
    }

    /**
     * @return the Id_Numero
     */
    public int getId_Numero() {
        return Id_Numero;
    }

    /**
     * @param Id_Numero the Id_Numero to set
     */
    public void setId_Numero(int Id_Numero) {
        this.Id_Numero = Id_Numero;
    }

    /**
     * @return the Id_Estado
     */
    public int getId_Estado() {
        return Id_Estado;
    }

    /**
     * @param Id_Estado the Id_Estado to set
     */
    public void setId_Estado(int Id_Estado) {
        this.Id_Estado = Id_Estado;
    }

    /**
     * @return the Estado
     */
    public String getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    /**
     * @return the Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email the Email to set
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return the TelArea
     */
    public String getTelArea() {
        return TelArea;
    }

    /**
     * @param TelArea the TelArea to set
     */
    public void setTelArea(String TelArea) {
        this.TelArea = TelArea;
    }

    /**
     * @return the TelNro
     */
    public String getTelNro() {
        return TelNro;
    }

    /**
     * @param TelNro the TelNro to set
     */
    public void setTelNro(String TelNro) {
        this.TelNro = TelNro;
    }

    /**
     * @return the TelFormateado
     */
    public String getTelFormateado() {
        return TelFormateado;
    }

    /**
     * @param TelFormateado the TelFormateado to set
     */
    public void setTelFormateado(String TelFormateado) {
        this.TelFormateado = TelFormateado;
    }

    /**
     * @return the CelArea
     */
    public String getCelArea() {
        return CelArea;
    }

    /**
     * @param CelArea the CelArea to set
     */
    public void setCelArea(String CelArea) {
        this.CelArea = CelArea;
    }

    /**
     * @return the CelNro
     */
    public String getCelNro() {
        return CelNro;
    }

    /**
     * @param CelNro the CelNro to set
     */
    public void setCelNro(String CelNro) {
        this.CelNro = CelNro;
    }

    /**
     * @return the CelFormateado
     */
    public String getCelFormateado() {
        return CelFormateado;
    }

    /**
     * @param CelFormateado the CelFormateado to set
     */
    public void setCelFormateado(String CelFormateado) {
        this.CelFormateado = CelFormateado;
    }

    /**
     * @return the Empleado
     */
    public String getEmpleado() {
        return Empleado;
    }

    /**
     * @param Empleado the Empleado to set
     */
    public void setEmpleado(String Empleado) {
        this.Empleado = Empleado;
    }

    /**
     * @return the Id_Empleado
     */
    public String getId_Empleado() {
        return Id_Empleado;
    }

    /**
     * @param Id_Empleado the Id_Empleado to set
     */
    public void setId_Empleado(String Id_Empleado) {
        this.Id_Empleado = Id_Empleado;
    }

    /**
     * @return the FechaRegistro
     */
    public String getFechaRegistro() {
        return FechaRegistro;
    }

    /**
     * @param FechaRegistro the FechaRegistro to set
     */
    public void setFechaRegistro(String FechaRegistro) {
        this.FechaRegistro = FechaRegistro;
    }

    /**
     * @return the FechaBloqueo
     */
    public String getFechaBloqueo() {
        return FechaBloqueo;
    }

    /**
     * @param FechaBloqueo the FechaBloqueo to set
     */
    public void setFechaBloqueo(String FechaBloqueo) {
        this.FechaBloqueo = FechaBloqueo;
    }

    /**
     * @return the IdAplicacionOrigen
     */
    public String getIdAplicacionOrigen() {
        return IdAplicacionOrigen;
    }

    /**
     * @param IdAplicacionOrigen the IdAplicacionOrigen to set
     */
    public void setIdAplicacionOrigen(String IdAplicacionOrigen) {
        this.IdAplicacionOrigen = IdAplicacionOrigen;
    }

    /**
     * @return the TieneRepresentados
     */
    public String getTieneRepresentados() {
        return TieneRepresentados;
    }

    /**
     * @param TieneRepresentados the TieneRepresentados to set
     */
    public void setTieneRepresentados(String TieneRepresentados) {
        this.TieneRepresentados = TieneRepresentados;
    }

    /**
     * @return the CodigoIngresado
     */
    public String getCodigoIngresado() {
        return CodigoIngresado;
    }

    /**
     * @param CodigoIngresado the CodigoIngresado to set
     */
    public void setCodigoIngresado(String CodigoIngresado) {
        this.CodigoIngresado = CodigoIngresado;
    }

    /**
     * @return the Constatado
     */
    public String getConstatado() {
        return Constatado;
    }

    /**
     * @param Constatado the Constatado to set
     */
    public void setConstatado(String Constatado) {
        this.Constatado = Constatado;
    }

    /**
     * @return the Domicilio
     */
//    public Domicilio getDomicilio() {
//        return Domicilio;
//    }
//
//    /**
//     * @param Domicilio the Domicilio to set
//     */
//    public void setDomicilio(Domicilio Domicilio) {
//        this.Domicilio = Domicilio;
//    }
//
//    /**
//     * @return the Representado
//     */
//    public Representado getRepresentado() {
//        return Representado;
//    }
//
//    /**
//     * @param Representado the Representado to set
//     */
//    public void setRepresentado(Representado Representado) {
//        this.Representado = Representado;
//    }
//
//    /**
//     * @return the Respuesta
//     */
//    public Respuesta getRespuesta() {
//        return Respuesta;
//    }
//
//    /**
//     * @param Respuesta the Respuesta to set
//     */
//    public void setRespuesta(Respuesta Respuesta) {
//        this.Respuesta = Respuesta;
//    }
//
//    /**
//     * @return the RespuestaModificacion
//     */
//    public RespuestaModificacion getRespuestaModificacion() {
//        return RespuestaModificacion;
//    }
//
//    /**
//     * @param RespuestaModificacion the RespuestaModificacion to set
//     */
//    public void setRespuestaModificacion(RespuestaModificacion RespuestaModificacion) {
//        this.RespuestaModificacion = RespuestaModificacion;
//    }

}
