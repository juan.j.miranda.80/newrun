package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.ExperienciaAcomp;
import ar.gob.run.spring.service.ExperienciaAcompService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "experienciaAcompConverter")
public class ExperienciaAcompConverter extends GenericConverter<ExperienciaAcomp>{

	public ExperienciaAcompConverter() {
		super(ExperienciaAcomp.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<ExperienciaAcomp> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{ExperienciaAcompService}", ExperienciaAcompService.class);

		ExperienciaAcompService expAcompService = (ExperienciaAcompService)vex.getValue(context.getELContext());
		return expAcompService;
	}

	


}
