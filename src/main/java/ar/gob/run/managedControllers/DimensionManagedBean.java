package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Dimension;
import ar.gob.run.spring.service.DimensionService;

@ManagedBean(name="dimensionMB")
@ViewScoped
public class DimensionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";
    private static final String ERROR   = "error";
    
    @ManagedProperty(value="#{DimensionService}")
    DimensionService service;

    List<Dimension> list;

    Dimension instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Dimension ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Dimension ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Dimension();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Dimension();
    	editMode = false;
    }

	public Dimension getInstance() {
		return instance;
	}

	public void setInstance(Dimension instance) {
		this.instance = instance;
	}

	public DimensionService getService() {
		return service;
	}

	public void setService(DimensionService service) {
		this.service = service;
	}

	public List<Dimension> getList() {
		return getService().getAllActives(Dimension.class);
	}

	public void setList(List<Dimension> list) {
		this.list = list;
	}

	public Boolean getEditMode() {
		return editMode;
	}
	
}
