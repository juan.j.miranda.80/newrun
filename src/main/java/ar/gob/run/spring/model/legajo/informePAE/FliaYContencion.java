package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_FLIA_Y_CONT")
public class FliaYContencion implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo		tieneVinculoFliaOrig = SiNo.N;
	private SiNo		tieneVincAmigosPareja = SiNo.N;
	private SiNo		tieneVincRedComun = SiNo.N;
	private SiNo		tieneOtrosReferentes = SiNo.N;
	private SiNo		tieneHijos = SiNo.N;
	private SiNo		tieneVinculoHijos = SiNo.N;
	private SiNo		tieneVincFrecHijos = SiNo.N;
	private String		ppalesDificultades = "";

	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_FLIA_Y_CONT")
	public Integer getId() {
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_VIN_FLIA_O")
	public SiNo getTieneVinculoFliaOrig() {
		return tieneVinculoFliaOrig;
	}

	public void setTieneVinculoFliaOrig(SiNo tieneVinculoFliaOrig) {
		this.tieneVinculoFliaOrig = tieneVinculoFliaOrig;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_VIN_AMIG_PAREJA")
	public SiNo getTieneVincAmigosPareja() {
		return tieneVincAmigosPareja;
	}

	public void setTieneVincAmigosPareja(SiNo tieneVincAmigosPareja) {
		this.tieneVincAmigosPareja = tieneVincAmigosPareja;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_VIN_RED_COMUN")
	public SiNo getTieneVincRedComun() {
		return tieneVincRedComun;
	}

	public void setTieneVincRedComun(SiNo tieneVincRedComun) {
		this.tieneVincRedComun = tieneVincRedComun;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_OTROS_REF")
	public SiNo getTieneOtrosReferentes() {
		return tieneOtrosReferentes;
	}

	public void setTieneOtrosReferentes(SiNo tieneOtrosReferentes) {
		this.tieneOtrosReferentes = tieneOtrosReferentes;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_HIJOS")
	public SiNo getTieneHijos() {
		return tieneHijos;
	}

	public void setTieneHijos(SiNo tieneHijos) {
		this.tieneHijos = tieneHijos;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_VINC_HIJOS")
	public SiNo getTieneVinculoHijos() {
		return tieneVinculoHijos;
	}

	public void setTieneVinculoHijos(SiNo tieneVinculoHijos) {
		this.tieneVinculoHijos = tieneVinculoHijos;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_VINC_FREC_HIJOS")
	public SiNo getTieneVincFrecHijos() {
		return tieneVincFrecHijos;
	}

	public void setTieneVincFrecHijos(SiNo tieneVincFrecHijos) {
		this.tieneVincFrecHijos = tieneVincFrecHijos;
	}

	@Column(name="PPALES_DIFICULT")
	public String getPpalesDificultades() {
		return ppalesDificultades;
	}

	public void setPpalesDificultades(String ppalesDificultades) {
		this.ppalesDificultades = ppalesDificultades;
	}

}
