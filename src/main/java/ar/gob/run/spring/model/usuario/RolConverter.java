package ar.gob.run.spring.model.usuario;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.service.UsuarioService;

@FacesConverter(value = "rolConverter")
public class RolConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value != null && value.trim().length() > 0) {
            try {
                UsuarioService service = (UsuarioService) context.getApplication().getExpressionFactory()
                        .createValueExpression(context.getELContext(), "#{UsuarioService}", UsuarioService.class)
                        .getValue(context.getELContext());
                return service.getUsuarioById(Integer.parseInt(value));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        else {
            return null;
        }
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
            return String.valueOf(((Rol) value).getId());
        }
        else {
            return null;
        }
	}

}
