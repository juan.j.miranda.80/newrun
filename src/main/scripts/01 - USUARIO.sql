  CREATE TABLE "RUNNA"."T_USUARIO" 
   (	"ID_USUARIO" NUMBER(10,0) NOT NULL ENABLE, 
	"USERNAME" VARCHAR2(50) NOT NULL ENABLE, 
  "ACTIVO" NUMBER(1,0) DEFAULT 1 NOT NULL ,
	"PASSWORD" VARCHAR2(50 BYTE), 
  	"ID_LOCAL" NUMBER(10,0), 
	"ID_ZONAL" NUMBER(10,0),
	 CONSTRAINT "PK_USUARIO" PRIMARY KEY ("ID_USUARIO")
   )  ;
   
CREATE TABLE "T_ROL" (
  "ID_ROL" NUMBER(10,0) NOT NULL ENABLE,
  "ROL" varchar(50) NOT NULL,
  "CODE" VARCHAR2(50 BYTE), 		
  CONSTRAINT "PK_ROL" PRIMARY KEY ("ID_ROL")
);   
  
CREATE TABLE "T_USUARIO_ROL" (  
  "ID_USUARIO" NUMBER(10) NOT NULL,  
  "ID_ROL" NUMBER(10) NOT NULL, 
  CONSTRAINT "PK_USUARIO_ROL" PRIMARY KEY ("ID_USUARIO","ID_ROL"),
  CONSTRAINT "FK_USUARIO_ROL" FOREIGN KEY ("ID_USUARIO") REFERENCES "T_USUARIO" ("ID_USUARIO"),
  CONSTRAINT "FK_USUARIO_ROL2" FOREIGN KEY ("ID_ROL") REFERENCES "T_ROL" ("ID_ROL")
); 

/

INSERT INTO t_rol VALUES (1,'Administrador','ADMIN');
INSERT INTO t_rol VALUES (2,'Operador 102','OP102');
INSERT INTO t_rol VALUES (3,'Funcionario Provincial','FCP');
INSERT INTO t_rol VALUES (4,'Funcionario del Servicio Zonal','FCSZ');
INSERT INTO t_rol VALUES (5,'Funcionario del Servicio Local','FCSL');
INSERT INTO t_rol VALUES (6,'Usuario','USR');
INSERT INTO t_usuario VALUES (1,'admin', 1,'admin',null,null);
INSERT INTO t_usuario VALUES (2,'oper', 1,'oper',null,null);
INSERT INTO t_usuario_rol values (1,1);
INSERT INTO t_usuario_rol values (2,2);
commit;

--delete t_usuario_rol where 1=1;
--delete t_rol where 1=1;
--delete t_usuario where 1=1;
/

select * from t_rol;