package ar.gob.run.managedControllers.reports;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Persona;
import ar.gob.run.spring.service.LegajoService;

@ManagedBean(name="rptLegajosFliarMB")
@ViewScoped
public class RptLegajosFliarManagedBean implements Serializable{

	Persona familiarSeleccionado;
	
	List<Legajo> list;
	
	@ManagedProperty(value="#{legajoService}")
	LegajoService legajoService;
	
	private static final long serialVersionUID = 1L;


	public Persona getFamiliarSeleccionado() {
		return familiarSeleccionado;
	}

	public void setFamiliarSeleccionado(Persona familiarSeleccionado) {
		this.familiarSeleccionado = familiarSeleccionado;
	}

	public LegajoService getLegajoService() {
		return legajoService;
	}

	public void setLegajoService(LegajoService legajoService) {
		this.legajoService = legajoService;
	}

	public List<Legajo> getList() {
		return list;
	}

	public void setList(List<Legajo> list) {
		this.list = list;
	}

	public void buscarLegajos(){
		this.list = getLegajoService().getRptLegajosFliar(getFamiliarSeleccionado());
	}
	
}
