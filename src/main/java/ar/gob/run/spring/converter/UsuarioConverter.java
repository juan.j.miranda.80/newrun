package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.service.UsuarioService;

@FacesConverter(value = "usuarioConverter")
public class UsuarioConverter implements Converter{

	public UsuarioConverter() {
	}

	
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), Usuario.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((Usuario) value).getId().toString();
	}

	public UsuarioService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{UsuarioService}", UsuarioService.class);

			UsuarioService service = (UsuarioService)vex.getValue(context.getELContext());
			return service;
	}

}
