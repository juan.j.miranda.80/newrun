package ar.gob.run.spring.model;

public enum TipoDomicilioJoven {
	FORMAL("Alojamiento formal","AF"), VIVIENDA("Vivienda","VI");
	
	private String descripcion;
	private String code;

	private TipoDomicilioJoven(String descripcion, String code) {
		this.descripcion = descripcion;
		this.code = code;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static TipoDomicilioJoven fromString(String text) {
		if (text==null) return null;
	    for (TipoDomicilioJoven b : TipoDomicilioJoven.values()) {
	      if (b.code.equalsIgnoreCase(text)) {
	        return b;
	      }
	    }
	    return null;
	  }
}
