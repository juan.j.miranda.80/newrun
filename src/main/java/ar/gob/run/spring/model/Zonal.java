package ar.gob.run.spring.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name="T_ZONAL")
public class Zonal extends ObjetoABM implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
    private Provincia provincia;
    private String nombre;
    private String codigo;
    private String responsable;
    private String telefono;
    private String fax;
    private String celGuardia;
    private String email;
    private String direccion;
    private String codigoPostal;
    private String observacion;
   
    private Set<Local> locales = new HashSet<Local>();
    
//    @Embedded
//    private DatosAuditoria datosAuditoria = new DatosAuditoria();

    
    @Id 
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_ZONAL")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", nombre : ").append(getNombre());
        return strBuff.toString();
    }

    @Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PROVINCIA")
	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	@Column(name="CODIGO")
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name="RESPONSABLE")
	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	@Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name="FAX")
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name="CEL_GUARDIA")
	public String getCelGuardia() {
		return celGuardia;
	}

	public void setCelGuardia(String celGuardia) {
		this.celGuardia = celGuardia;
	}

	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="DIRECCION")
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Column(name="CODIGO_POSTAL")
	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Column(name="OBSERVACION")
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@OneToMany(cascade = {CascadeType.ALL}, mappedBy = "zonal", targetEntity=Local.class)
	public Set<Local> getLocales() {
		return locales;
	}

	public void setLocales(Set<Local> locales) {
		this.locales = locales;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Zonal)obj).getId());
	}

	public Zonal(Integer id) {
		super();
		this.id = id;
	}

	public Zonal() {
		super();
	}
	
}
