package ar.gob.run.spring.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.model.EstadoSupervInterv;
import ar.gob.run.spring.model.Intervencion;
import ar.gob.run.spring.model.IntervencionPAE;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;

@Repository
public class IntervencionPAEDAO  extends HibernateDAO<IntervencionPAE, Integer>{

	@Autowired
	Environment environment;
	
	@Value("${tipoIntervencion.idPAEInicial}")
	private String idPAEInicial;
	
	public List<IntervencionPAE> getList(Integer legajo) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionPAE(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.estadoSuperv, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ " from IntervencionPAE e where e.legajo.id=? and e.activo=true order by e.fecha desc")
                .setParameter(0, legajo).list();
        return list;
	}
	
	public List<IntervencionPAE> getList() {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionPAE(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.estadoSuperv, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ " from IntervencionPAE e where e.activo=true order by e.fecha desc")
                .list();
        return list;
	}
	
	public List<IntervencionPAE> getListByZonal(Zonal zonal){
		List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionPAE(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.estadoSuperv, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ " from IntervencionPAE e where e.activo=true and e.zonal = :zonal order by e.fecha desc")
                .setParameter("zonal", zonal)
                .list();
        return list;
	}
	
	public List<Intervencion> getRptIntervenciones(String codigoLegajo, Sexo sexo, Zonal zonalLegajo, Local localLegajo,
			Zonal zonalIntervencion, Local localIntervencion, Date fechaIntervencionDde, Date fechaIntervencionHta,
			Derivador derivador, TipoIntervencion tipoIntervencion, MotivoIntervencion motivoIntervencion,
			TipoCircuito circuito) {
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("from IntervencionPAE i where"
                		+ " activo=true "
                		+ (codigoLegajo!=null?" and i.legajo.codigo = :codigo ":"")
                		+ (sexo != null?" and i.legajo.sexo = :sexo" :"")
                		+ (zonalLegajo!=null ? " and i.legajo.zonal = :zonalL " : "")
                		+ (localLegajo!=null ? " and i.legajo.local = :localL " : "")
                		+ (zonalIntervencion!=null ? " and i.zonal = :zonalI " : "")
                		+ (fechaIntervencionDde!=null ? " and i.fecha >= :fechaIDde ":"")
                		+ (fechaIntervencionHta!=null ? " and i.fecha <= :fechaIHta " :"")
                		+ (derivador!=null?" and i.derivador = :derivador " :"")
                		+ (tipoIntervencion!=null? " and i.tipoIntervencion = :tipoIntervencion " : "")
                		+ (motivoIntervencion!=null ? " and i.motivoIntervencion = :motivoIntervencion " :"")
                		+ (circuito != null ? " and i.tipoIntervencion.tipoCircuito = :circuito " :""));
		if (codigoLegajo !=null) query.setString("codigo", codigoLegajo);
		if (sexo != null) query.setParameter("sexo", sexo.name());
		if (zonalLegajo!=null) query.setParameter("zonalL", zonalLegajo);
		if (localLegajo!=null) query.setParameter("zonalL", zonalLegajo);
		if (zonalIntervencion!=null) query.setParameter("zonalI", zonalIntervencion);
		if (fechaIntervencionDde!=null) query.setDate("fechaIDde", fechaIntervencionDde);
		if (fechaIntervencionHta!=null) query.setDate("fechaIHta", fechaIntervencionHta);
		if (derivador!=null) query.setParameter("derivador", derivador);
		if (tipoIntervencion!=null) query.setParameter("tipoIntervencion", tipoIntervencion);
		if (motivoIntervencion!=null) query.setParameter("motivoIntervencion", motivoIntervencion);
		if (circuito!=null) query.setParameter("circuito", circuito	);
		return query.list();
	}

	public List<IntervencionPAE> getPendientesSupervisar() {
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionPAE(e.id, e.fecha, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.estadoSuperv, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName) "
                		+ "  from IntervencionPAE e where e.estadoSuperv = :estadoPend "
                		+ " and e.activo=true order by e.fecha desc");
		query.setParameter("estadoPend", EstadoSupervInterv.PENDIENTE);
        return query.list();
	}
	
	@Override
	public void update(IntervencionPAE entity) {
		//si es la primer MPE (id 55) le pongo el estado supervision pendiente
      	if (entity.getTipoIntervencion()!=null && entity.getTipoIntervencion().getId()!=null &&
      			entity.getTipoIntervencion().getId().equals(Integer.valueOf(idPAEInicial)) &&
      			(entity.getEstadoSuperv()==null || entity.getEstadoSuperv().equals(EstadoSupervInterv.NULL))){
      		entity.setEstadoSuperv(EstadoSupervInterv.PENDIENTE);
      	}
		super.update(entity);
	}
	

	    
}