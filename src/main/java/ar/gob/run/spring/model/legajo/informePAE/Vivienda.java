package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_VIVIENDA")
public class Vivienda implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo			seEncuentraEnHogOAcogFliar = SiNo.N;
	private TipoVivienda	tipoVivienda = null;
	private String			ppalesDificultades = "";

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_VIVIENDA")
	public Integer getId() {
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="HOG_O_ACOG_FLIAR")
	public SiNo getSeEncuentraEnHogOAcogFliar() {
		return seEncuentraEnHogOAcogFliar;
	}

	public void setSeEncuentraEnHogOAcogFliar(SiNo seEncuentraEnHogOAcogFliar) {
		this.seEncuentraEnHogOAcogFliar = seEncuentraEnHogOAcogFliar;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_VIVIENDA")
	public TipoVivienda getTipoVivienda() {
		return tipoVivienda;
	}

	public void setTipoVivienda(TipoVivienda tipoVivienda) {
		this.tipoVivienda = tipoVivienda;
	}

	@Column(name="PPALES_DIFICULT")
	public String getPpalesDificultades() {
		return ppalesDificultades;
	}

	public void setPpalesDificultades(String ppalesDificultades) {
		this.ppalesDificultades = ppalesDificultades;
	}
	
}
