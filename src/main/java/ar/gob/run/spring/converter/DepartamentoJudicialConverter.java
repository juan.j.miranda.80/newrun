package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.DepartamentoJudicial;
import ar.gob.run.spring.service.DepartamentoJudicialService;

@FacesConverter(value = "departamentoJudicialConverter")
public class DepartamentoJudicialConverter implements Converter {

	public DepartamentoJudicialConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), DepartamentoJudicial.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((DepartamentoJudicial) value).getId().toString();
	}

	public DepartamentoJudicialService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{DepartamentoJudicialService}", DepartamentoJudicialService.class);

			DepartamentoJudicialService service = (DepartamentoJudicialService)vex.getValue(context.getELContext());
			return service;
	}

}
