package ar.gob.run.spring.service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.IntervencionExcepcionalDAO;
import ar.gob.run.spring.dao.IntervencionIntegralDAO;
import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.model.Intervencion;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;
		  
@Service("IntervencionService")
@Transactional(readOnly = true)
public class IntervencionService extends GenericService<Intervencion>{

	IntervencionIntegralDAO mpiDAO;
	IntervencionExcepcionalDAO mpeDAO;
	
	@Autowired
	public void setIntervencionIntegralDAO(IntervencionIntegralDAO intervencionDAO){
		this.mpiDAO = intervencionDAO;
	}
	
	@Autowired
	public void setIntervencionExcepcionalDAO(IntervencionExcepcionalDAO intervencionDAO){
		this.mpeDAO = intervencionDAO;
	}

	public List<Intervencion> getRptIntervenciones(String codigoLegajo, Sexo sexo, Zonal zonalLegajo, Local localLegajo,
			Zonal zonalIntervencion, Local localIntervencion, Date fechaIntervencionDde, Date fechaIntervencionHta,
			Derivador derivador, TipoIntervencion tipoIntervencion, MotivoIntervencion motivoIntervencion,
			TipoCircuito circuito) {
		if (sexo.equals(Sexo.N)) sexo = null;
		List<Intervencion> list =  this.mpiDAO.getRptIntervenciones(codigoLegajo, sexo, zonalLegajo,
				localLegajo, zonalIntervencion, localIntervencion, fechaIntervencionDde, fechaIntervencionHta,
				derivador, tipoIntervencion, motivoIntervencion, circuito);
		List<Intervencion> listMPE =  this.mpeDAO.getRptIntervenciones(codigoLegajo, sexo, zonalLegajo,
				localLegajo, zonalIntervencion, localIntervencion, fechaIntervencionDde, fechaIntervencionHta,
				derivador, tipoIntervencion, motivoIntervencion, circuito);
		list.addAll(listMPE);
		
		if (list!=null && list.size()>0)
			Collections.sort(list);
		return list;
	}
    
	
	
}
