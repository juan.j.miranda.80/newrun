package ar.gob.run.spring.model;

public enum Sexo {
	N("Otro género"), M("Masculino"), F("Femenino"), TM("Trans Masculino"), TF("Trans Femenino");
	
	String descripcion;

	private Sexo(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	

}
