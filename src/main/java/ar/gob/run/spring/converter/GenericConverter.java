package ar.gob.run.spring.converter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.ProvinciaService;

public abstract class GenericConverter<T extends Identificable> implements Converter,
Serializable {

	private static final Long SIGNAL_PERSISTIDO = 12380489012390l;

	private static final Long SIGNAL_SERIALIZADO = 2348900898904l;

	
	private final Class<T> type;
	
	public GenericConverter(Class<T> type) {
		super();
        this.type = type;
	}

	private void escribirObjeto(ObjectOutputStream oos, Object objeto)
			throws IOException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		if (objeto instanceof Identificable) {
			// es un objeto de este converter
			Integer id = ((Identificable) objeto).getId();
			if (id == null || id.longValue() == 0l) {
				oos.writeObject(SIGNAL_SERIALIZADO);
				oos.writeObject(objeto);
			} else {
				oos.writeObject(SIGNAL_PERSISTIDO);
				oos.writeObject(id);
			}
		}
	}

	public Object getAsObject(FacesContext facesContext,
			UIComponent uiComponent, String valor) throws ConverterException {
		if (valor==null) return "";
		ByteArrayInputStream bais = null;
		try {
			byte[] b = Base64.decodeBase64(valor.getBytes());
			bais = new ByteArrayInputStream(b);
			ObjectInputStream ois = new ObjectInputStream(bais);
			Object obj = leerObjeto(ois, facesContext);
			return obj;
		} catch (Exception e) {
			throw new ConverterException(e);
		} finally {
			try {
				bais.close();
			} catch (IOException e) {
				throw new ConverterException(e);
			}
		}
	}

	public String getAsString(FacesContext facesContext,
			UIComponent uiComponent, Object object) throws ConverterException {
		if (object==null) return "";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			escribirObjeto(oos, object);
			byte[] b = Base64.encodeBase64(baos.toByteArray());
			oos.close();
			return new String(b);
		} catch (Exception e) {
			throw new ConverterException(e);
		} finally {
			try {
				baos.close();
			} catch (IOException e) {
				throw new ConverterException(e);
			}
		}
	}

	private Identificable leerEnSession(Integer id, FacesContext context) {
		try {
			return getService(context).get(id, this.type );
		} catch (NumberFormatException e) {
			throw new ConverterException("Id de "
					+ " Provincia inválido: " + id);
		} catch (Exception e) {
			throw new ConverterException(
					"No se pudo instanciar el DTO para la clase  : "
							+ e.getMessage(), e);
		}
	}

	private Object leerObjeto(ObjectInputStream ois, FacesContext context) throws Exception {
		Object objeto = null;
		Object signal = ois.readObject();
		if (SIGNAL_PERSISTIDO.equals(signal)) {
			Object valorActual = ois.readObject();
			objeto = leerEnSession((Integer) valorActual, context);
		} else if (SIGNAL_SERIALIZADO.equals(signal)) {
			objeto = ois.readObject();
		}
		return objeto;
	}

	protected abstract GenericService<T> getService(FacesContext context);
}
