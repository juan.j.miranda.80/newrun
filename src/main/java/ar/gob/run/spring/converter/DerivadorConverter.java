package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.service.DerivadorService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "derivadorConverter")
public class DerivadorConverter extends GenericConverter<Derivador>{

	public DerivadorConverter() {
		super(Derivador.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Derivador> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{DerivadorService}", DerivadorService.class);

		DerivadorService derivadorService = (DerivadorService)vex.getValue(context.getELContext());
		return derivadorService;
	}

	


}
