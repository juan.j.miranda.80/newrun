package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.report.bean.ImprimirInformeBean;
import ar.gob.run.spring.model.EstadoSupervInterv;
import ar.gob.run.spring.model.IntervencionExcepcional;
import ar.gob.run.spring.model.IntervencionPAE;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.IntervencionExcepcionalService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="intervencionPAEPendAprobMB")
@SessionScoped
public class IntervencionPAEPendAprobManagedBean extends IntervencionPAEManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<IntervencionPAE> getList() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.SUPERVISOR)|| usr.getUsuario().hasRole(Rol.ADMIN)){
			return service.getPendientesSupervisar();
		}
		return super.getList();
	}
	
	
}
