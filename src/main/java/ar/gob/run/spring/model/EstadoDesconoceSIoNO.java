package ar.gob.run.spring.model;

public enum EstadoDesconoceSIoNO {
	
	SE_DESCONOCE("Se desconoce"), SI("Si"), NO("No");
	
	String descripcion;

	private EstadoDesconoceSIoNO(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	

}
