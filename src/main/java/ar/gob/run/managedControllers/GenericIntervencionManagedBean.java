package ar.gob.run.managedControllers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import ar.gob.run.spring.model.CategoriaMotivoIntervencion;
import ar.gob.run.spring.model.DatosAuditoria;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.service.MotivoIntervencionService;

public abstract class GenericIntervencionManagedBean implements Serializable {

    
	private static final long serialVersionUID = 1L;

	List<MotivoIntervencion> motivosInt;
    
    @ManagedProperty(value="#{MotivoIntervencionService}")
    MotivoIntervencionService motivoIntervencionService;
    
    CategoriaMotivoIntervencion catMotIntSelected;
    
    Legajo legajoParam = new Legajo();
    
    public void onCatMotivoIntChange(ValueChangeEvent event) {
    	if (event.getNewValue()!=null) catMotIntSelected = (CategoriaMotivoIntervencion) event.getNewValue();
        if(catMotIntSelected !=null &&
        		catMotIntSelected.getId()!=null)
        	motivosInt = motivoIntervencionService.getAllByCatego(catMotIntSelected);
        else
        	motivosInt = new ArrayList<MotivoIntervencion>();
    }
	
	public void setMotivoIntervencionService(MotivoIntervencionService motivoIntervencionService) {
		this.motivoIntervencionService = motivoIntervencionService;
	}
	
	public List<MotivoIntervencion> getMotivosIntervencion(){
		if (motivosInt == null) motivosInt= motivoIntervencionService.getAllByCatego(catMotIntSelected);
		return motivosInt;
	}

	public CategoriaMotivoIntervencion getCatMotIntSelected() {
		return catMotIntSelected;
	}

	public void setCatMotIntSelected(CategoriaMotivoIntervencion catMotIntSelected) {
		this.catMotIntSelected = catMotIntSelected;
	}
	
	protected Boolean esUsuarioCreadorInt(DatosAuditoria datAu, Usuario usr){
		return (datAu!=null &&
				usr.getId().equals(datAu.getUsuarioAlta().getId()));
	}
	
	public Legajo getLegajoParam() {
		return legajoParam;
	}

	public void setLegajoParam(Legajo legajoParam) {
		this.legajoParam = legajoParam;
	}
	
	
	public abstract String getNombreArchivo();
	public abstract String getNombreLegajo();
	public abstract String getInforme();
	
	public void imprimir() {
		Document document = new Document(PageSize.A4);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, baos);
			document.open();

			PdfPTable table = new PdfPTable(2);

			Image imagen = Image.getInstance(getClass().getClassLoader().getResource("unicef.png"));
			Image image2 = Image.getInstance(getClass().getClassLoader().getResource("run.png"));
			PdfPCell cell = new PdfPCell(imagen);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			cell = new PdfPCell(image2);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorder(Rectangle.NO_BORDER);
			table.addCell(cell);
			Font font = new Font(FontFamily.HELVETICA, 12, Font.BOLD);
			Phrase ph = new Phrase("Legajo: " + getNombreLegajo(), font);
			cell = new PdfPCell(ph);
			cell.setBorder(Rectangle.BOTTOM);
			cell.setPaddingTop(5);
			cell.setColspan(2);
			table.addCell(cell);
			document.add(table);

			String k;

			k = "<html><body>" + getInforme() + "</body></html>";
			InputStream is = new ByteArrayInputStream(k.getBytes());
			XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);

		} catch (Exception ex) {
			System.out.println("Error " + ex.getMessage());
		}
		document.close();
		FacesContext context = FacesContext.getCurrentInstance();
		Object response = context.getExternalContext().getResponse();
		if (response instanceof HttpServletResponse) {
			HttpServletResponse hsr = (HttpServletResponse) response;
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
			String nombrePdf = getNombreArchivo() + "-" + dateFormat.format(new Date()) + ".pdf";
			hsr.setContentType("application/pdf");
			hsr.addHeader("Content-Disposition", "attachment; filename=\"" + nombrePdf + "\"");
			hsr.setContentLength(baos.size());
			try {
				ServletOutputStream out = hsr.getOutputStream();
				baos.writeTo(out);
				out.flush();
			} catch (IOException ex) {
				System.out.println("Error:  " + ex.getMessage());
			}
			context.responseComplete();
		}
	}
	
}
