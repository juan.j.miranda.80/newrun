  CREATE TABLE "T_ESTADO_CIVIL" 
   (	"ID_ESTADO_CIVIL" NUMBER(10,0) NOT NULL ENABLE,    
	"DESCRIPCION" VARCHAR2(50) NOT NULL ENABLE,
	 CONSTRAINT "PK_ESTADO_CIVIL" PRIMARY KEY ("ID_ESTADO_CIVIL")
   )  ;  
   
insert into t_estado_civil values (1,'Casado');
insert into t_estado_civil values (2,'Soltero');
commit;