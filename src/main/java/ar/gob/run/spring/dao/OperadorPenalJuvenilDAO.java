package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Juzgado;
import ar.gob.run.spring.model.OperadorPenalJuvenil;
import ar.gob.run.spring.model.TipoDispositivo;

@Repository
public class OperadorPenalJuvenilDAO  extends HibernateDAO<OperadorPenalJuvenil, Integer>{

	public List<OperadorPenalJuvenil> getByTipoDispositivo(TipoDispositivo tpoD) {
		List<OperadorPenalJuvenil> list = currentSession().createQuery(" "
    			+ " from OperadorPenalJuvenil z where z.activo = TRUE  "
    			+ " and z.tipoDispositivo.id = :tpoDId order by z.id desc")
				.setInteger("tpoDId", tpoD.getId()).list();
        return list;
	}
	    
}