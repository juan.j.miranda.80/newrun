package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Dimension;

@Repository
public class DimensionDAO extends HibernateDAO<Dimension,Integer> {

    public List<Dimension> getDimensiones() {
        List<Dimension> list = currentSession().createQuery("from Dimension c order by c.descripcion").list();
        return list;
    }
}
