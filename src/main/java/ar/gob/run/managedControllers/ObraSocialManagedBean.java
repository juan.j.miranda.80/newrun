package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.ObraSocial;
import ar.gob.run.spring.service.ObraSocialService;

@ManagedBean(name="obraSocialMB")
@ViewScoped
public class ObraSocialManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{ObraSocialService}")
    ObraSocialService service;

    List<ObraSocial> list;

    ObraSocial instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(ObraSocial ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(ObraSocial ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new ObraSocial();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new ObraSocial();
    	editMode = false;
    }

	public ObraSocial getInstance() {
		return instance;
	}

	public void setInstance(ObraSocial instance) {
		this.instance = instance;
	}

	public ObraSocialService getService() {
		return service;
	}

	public void setService(ObraSocialService service) {
		this.service = service;
	}

	public List<ObraSocial> getList() {
		return getService().getAll();
	}

	public void setList(List<ObraSocial> list) {
		this.list = list;
	}


	
}
