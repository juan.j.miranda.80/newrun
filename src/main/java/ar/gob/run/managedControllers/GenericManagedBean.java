package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.ZonalService;

public abstract class GenericManagedBean<T> implements Serializable {

    protected static final long serialVersionUID = 1L;
    protected static final String LISTADO = "listado";
    protected static final String EDITOR = "editor";
    protected static final String ERROR   = "error";

    GenericService<T> service;

    List<T> list;

    T instance;
    
    String name;
    
    public GenericManagedBean(T instance) {
		super();
		this.instance = instance;
	}

	public String save() {
        try {
            getService().save(instance);
            return LISTADO;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return ERROR;
    }

	public String edit(T ent){
    	this.instance = ent;
    	return EDITOR;
    }
    
    public String delete(T ent){
    	getService().delete(ent);
    	return LISTADO;
    }
    
    public String reset() {
    	this.instance = getNewInstance();
        return EDITOR;
    }
    
    public String cancel(){
    	this.instance = getNewInstance();
    	return LISTADO;
    }

	public abstract T getInstance();

	public void setInstance(T instance) {
		this.instance = instance;
	}

	public GenericService getService() {
		return service;
	}

	public void setService(GenericService service) {
		this.service = service;
	}

	public List<T> getList() {
		return getService().getAllActives(this.getNewInstance().getClass());
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public T getNewInstance(){
		try {
			return (T) this.instance.getClass().newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public abstract void setName(String name) ;

	
}
