package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.NacionalidadDAO;
import ar.gob.run.spring.model.Nacionalidad;

@Service
@Transactional(readOnly = true)
public class NacionalidadService extends GenericService<Nacionalidad> {

	@Autowired
	public void setNacionalidadDAO(NacionalidadDAO nacionalidadDAO){	
		this.setDao(nacionalidadDAO);
	}
	
	public NacionalidadDAO getNacionalidadDAO() {
		return (NacionalidadDAO) this.getDao();
	}
	
	public List<Nacionalidad> getAll() {
		return super.getAllActives(Nacionalidad.class);
	}
	
	public Nacionalidad get(Integer id){
		return this.get(id, Nacionalidad.class);
	}

	public List<Nacionalidad> getAllActivesWithExtra(Nacionalidad elem) {
		return super.getAllActivesWithExtra(Nacionalidad.class, elem);
	}
}
