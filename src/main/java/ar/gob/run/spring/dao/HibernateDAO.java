package ar.gob.run.spring.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.model.Auditable;
import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.security.MySecUser;

@Transactional(propagation= Propagation.REQUIRED, readOnly=false)
public class HibernateDAO<E, K extends Serializable>  {
    
	private SessionFactory sessionFactory;
	
    public HibernateDAO() {
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    

    protected Session currentSession() {
        return sessionFactory.getCurrentSession();
    }
 
    public void add(E entity) {
    	if (entity instanceof Auditable)
    		auditEntity((Auditable) entity);
        currentSession().save(entity);
    }
 
    public void update(E entity) {    	
    	if (entity instanceof Auditable){
    		auditEntity((Auditable) entity);
    	}
    	if (entity instanceof Identificable){
    		if (((Identificable)entity).getId()==null){
    			currentSession().persist(entity);
    		}else{
    			currentSession().merge(entity);
    		}
    	}
    }
 
    public void remove(E entity) {
    	if (entity instanceof ObjetoABM){
    		((ObjetoABM)entity).setActivo(false);
    		currentSession().update(entity);
    	}else    		
    		currentSession().delete(entity);
    }

    public E find(K key, Class<Identificable> type) {
        return (E) currentSession().get(type, key);
    }
 
    
    public List<E> list(Class<Identificable> type) {
    	Criteria criteria = currentSession().createCriteria(type);
    	if (ObjetoABM.class.isAssignableFrom(type) ){
    		criteria.add(Restrictions.eq("activo", Boolean.TRUE));
    		
    	}
        return criteria.list();
    }
    
    private void auditEntity(Auditable entity){
    	MySecUser usr = (MySecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (entity.getDatosAuditoria().getUsuarioAlta()==null || entity.getDatosAuditoria().getUsuarioAlta().getId()==null 
    			|| entity.getDatosAuditoria().getFechaAlta()==null ){
    		entity.getDatosAuditoria().setUsuarioAlta(usr.getUsuario());
    		if (entity.getDatosAuditoria().getFechaAlta()==null)
    			entity.getDatosAuditoria().setFechaAlta(new Date());
    	}else{
    		entity.getDatosAuditoria().setUsuarioModif(usr.getUsuario());
    		entity.getDatosAuditoria().setFechaModificacion(new Date());
    	}
    }
    
    protected SessionFactory getSessionFactory(){
    	return this.sessionFactory;
    }
}
