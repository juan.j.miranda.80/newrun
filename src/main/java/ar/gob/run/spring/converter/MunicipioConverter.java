package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.MunicipioService;

@FacesConverter(value = "municipioConverter")
public class MunicipioConverter extends GenericConverter<Municipio>{

	public MunicipioConverter() {
		super(Municipio.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Municipio> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{municipioService}", MunicipioService.class);

		MunicipioService municipioService = (MunicipioService)vex.getValue(context.getELContext());
		return municipioService;
	}

	


}
