package ar.gob.run.spring.model.operador102;

public enum TipoConviviente {
	NINIO("Niño"), ADOLESCENTE("Adolescente"), ADULTO("Adulto");
	
	String descripcion;

	private TipoConviviente(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	
}
