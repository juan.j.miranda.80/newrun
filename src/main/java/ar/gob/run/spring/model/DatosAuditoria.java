package ar.gob.run.spring.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import ar.gob.run.spring.model.usuario.Usuario;

@Embeddable
public class DatosAuditoria implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public Usuario usuarioAlta;
	public Date fechaAlta;
	public Usuario usuarioModif;
	public Date fechaModificacion;
	
	@Column(name="FECHA_ALTA")
	public Date getFechaAlta() {
		return fechaAlta;
	}
	
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	@Column(name="FECHA_MODIFICACION")
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_USUARIO_ALTA")
	public Usuario getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(Usuario usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_USUARIO_MODIF")
	public Usuario getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(Usuario usuarioModif) {
		this.usuarioModif = usuarioModif;
	}
	
	
	
}
