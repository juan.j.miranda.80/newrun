package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.CategoriaTipoIntervencion;
import ar.gob.run.spring.service.CategoriaTipoIntervencionService;

@ManagedBean(name="categoriaTipoIntervencionMB")
@ViewScoped
public class CategoriaTipoIntervencionManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{CategoriaTipoIntervencionService}")
    CategoriaTipoIntervencionService service;

    List<CategoriaTipoIntervencion> list;

    CategoriaTipoIntervencion instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

	public void edit(CategoriaTipoIntervencion ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(CategoriaTipoIntervencion ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new CategoriaTipoIntervencion();
    	editMode = true;
    }
    
    public void cancel(){
    	this.instance = new CategoriaTipoIntervencion();
    	editMode = false;
    }

	public CategoriaTipoIntervencion getInstance() {
		return instance;
	}

	public void setInstance(CategoriaTipoIntervencion instance) {
		this.instance = instance;
	}

	public CategoriaTipoIntervencionService getService() {
		return service;
	}

	public void setService(CategoriaTipoIntervencionService service) {
		this.service = service;
	}

	public List<CategoriaTipoIntervencion> getList() {
		return getService().getAllActives(CategoriaTipoIntervencion.class);
	}

	public void setList(List<CategoriaTipoIntervencion> list) {
		this.list = list;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	
	
}
