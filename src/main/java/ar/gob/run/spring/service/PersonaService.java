package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.PersonaDAO;
import ar.gob.run.spring.model.Persona;
		  
@Service("PersonaService")
@Transactional(readOnly = true)
public class PersonaService extends GenericService<Persona>{

	
	@Autowired
	public void setPersonaDAO(PersonaDAO personaDAO){
		this.setDao(personaDAO);
	}

    public PersonaDAO getPersonaDAO() {
        return (PersonaDAO) this.getDao();
    }

	public List<Persona> getPersonaByApYNom(String query){
		return getPersonaDAO().getPersonaByFilter(query, null);
	}
	
	public List<Persona> getPersonaByDNI(String query){
		return getPersonaDAO().getPersonaByFilter(null, query);
	}

}
