package ar.gob.run.spring.service;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;

/**
 * Este servicio queda disponible para provincias que posean un servicio de login
 * propio o necesario contra el cual autenticar el acceso a run.
 * El ejemplo Cordoba y CIDI es uno de ellos
 * 
 * @author Pepo
 *
 */
@Service
@SessionScoped
public class LoginService implements Serializable{

	private static final long serialVersionUID = -1410251162798775105L;

	public Boolean loginUser(String userName, String password){		
		return true;
	}
}
