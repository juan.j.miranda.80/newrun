package ar.gob.run.managedControllers.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.LegajoService;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="rptLegajosMB")
@SessionScoped
public class RptLegajosManagedBean implements Serializable{

	Date from;
	Date to;
	Sexo sexo;
	Zonal zonal;
	Local local;
	Date nacimientoFrom;
	Date nacimientoTo;
	Boolean tieneMEP;
	
	List<Legajo> list;
	
	@ManagedProperty(value="#{legajoService}")
	LegajoService legajoService;
	
	private static final long serialVersionUID = 1L;

	public String puedeEjecutar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (!usr.getUsuario().hasRole(Rol.OPERADOR_102))
			return "true";
		return "false";
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public Zonal getZonal() {
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public Date getNacimientoFrom() {
		return nacimientoFrom;
	}

	public void setNacimientoFrom(Date nacimientoFrom) {
		this.nacimientoFrom = nacimientoFrom;
	}

	public Date getNacimientoTo() {
		return nacimientoTo;
	}

	public void setNacimientoTo(Date nacimientoTo) {
		this.nacimientoTo = nacimientoTo;
	}

	public LegajoService getLegajoService() {
		return legajoService;
	}

	public void setLegajoService(LegajoService legajoService) {
		this.legajoService = legajoService;
	}

	public List<Legajo> getList() {
		return list;
	}

	public void setList(List<Legajo> list) {
		this.list = list;
	}

	public void buscarLegajos(){
		this.list = getLegajoService().getRptLegajos(getFrom(), getTo(), getSexo(), 
				getZonal(), getLocal(), getNacimientoFrom(), getNacimientoTo(), getTieneMEP());
	}
	
    //logica zonal-local dependiente
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	@ManagedProperty(value="#{localService}")
	LocalService localService;
	List<Local> locales;
	
	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(){
		return zonalService.getZonales(this.getZonal());
	}
	
    public void onZonalChange() {
        if(this.getZonal() !=null && this.getZonal().getId()!=null)
        	locales = localService.getLocales(this.getZonal(),this.getLocal());
        else
        	locales = new ArrayList<Local>();
    }
	
	public void setLocalService(LocalService localService) {
		this.localService = localService;
	}
	
	public List<Local> getLocales(){
		if (locales == null) locales = new ArrayList<Local>();
		return locales;
	}

	public Boolean getTieneMEP() {
		return tieneMEP;
	}

	public void setTieneMEP(Boolean tieneMEP) {
		this.tieneMEP = tieneMEP;
	}	
	
	
}
