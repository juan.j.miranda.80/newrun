package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.institucion.Institucion;

@Repository
public class InstitucionDAO  extends HibernateDAO<Institucion, Integer>{
	    
}