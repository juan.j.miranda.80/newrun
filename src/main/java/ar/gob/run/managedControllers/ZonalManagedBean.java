package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="zonalMB")
@ViewScoped
public class ZonalManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{zonalService}")
    ZonalService zonalService;

    List<Zonal> list;

    Zonal instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getZonalService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Zonal ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Zonal ent){
    	getZonalService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Zonal();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Zonal();
    	editMode = false;
    }

	public Zonal getInstance() {
		return instance;
	}

	public void setInstance(Zonal instance) {
		this.instance = instance;
	}

	public ZonalService getZonalService() {
		return zonalService;
	}

	public void setZonalService(ZonalService service) {
		this.zonalService = service;
	}

	public List<Zonal> getList() {
		return getZonalService().getAllActives(Zonal.class);
	}

	public void setList(List<Zonal> list) {
		this.list = list;
	}


	
}
