package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Procedencia;

@Repository
public class ProcedenciaDAO extends HibernateDAO<Procedencia,Integer> {

}
