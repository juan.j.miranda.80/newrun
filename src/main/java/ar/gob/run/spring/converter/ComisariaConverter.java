package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Comisaria;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.ComisariaService;

@FacesConverter(value = "comisariaConverter")
public class ComisariaConverter extends GenericConverter<Comisaria>{

	public ComisariaConverter() {
		super(Comisaria.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Comisaria> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{ComisariaService}", ComisariaService.class);

		ComisariaService service = (ComisariaService)vex.getValue(context.getELContext());
		return service;
	}

	


}
