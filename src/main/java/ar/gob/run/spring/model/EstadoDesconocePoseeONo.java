package ar.gob.run.spring.model;

public enum EstadoDesconocePoseeONo {
	
	SE_DESCONOCE("Se desconoce"), NO_POSEE("No posee"), POSEE("Posee");
	
	String descripcion;

	private EstadoDesconocePoseeONo(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
