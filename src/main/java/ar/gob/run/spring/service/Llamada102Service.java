package ar.gob.run.spring.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.Llamada102DAO;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.operador102.EstadoLlamada;
import ar.gob.run.spring.model.operador102.Llamada102;
		  
@Service("Llamada102Service")
@Transactional(readOnly = true)
public class Llamada102Service extends GenericService<Llamada102>{

	
	@Autowired
	public void setLlamada102DAO(Llamada102DAO llamada102DAO){
		this.setDao(llamada102DAO);
	}

    public Llamada102DAO getLlamada102DAO() {
        return (Llamada102DAO) this.getDao();
    }

//	public List<Llamada102> getAllActivesWithExtra(Llamada102 elem) {
//		return super.getAllActivesWithExtra(Llamada102.class, elem);
//	}
	
	public List<Llamada102> getAll() {
		return getLlamada102DAO().getLlamadas();
	}
	
	public List<Llamada102> getPendientesByZonal(Zonal zonal) {
		return getLlamada102DAO().getPendientesByZonal(zonal);
	}

	public List<Llamada102> getAllByZonal(Zonal zonal) {
		return getLlamada102DAO().getAllByZonal(zonal);
	}

	public List<Llamada102> getRptLlamadas(Date fechaNotifDesde, Date fechaNotifHta, Integer nroNotifDesde,
			Integer nroNotifHta, Zonal zonal, EstadoLlamada estadoLLamada) {
		return getLlamada102DAO().getRptLlamadas(fechaNotifDesde, fechaNotifHta, nroNotifDesde,
				nroNotifHta, zonal, estadoLLamada);
	}
}
