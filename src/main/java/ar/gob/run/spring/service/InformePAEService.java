package ar.gob.run.spring.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.InformePAEDAO;
import ar.gob.run.spring.model.Alerta;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.legajo.informePAE.InformePAE;
import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.security.MySecUser;

@Service("InformePAEService")
@Transactional(readOnly = true)
public class InformePAEService extends GenericService<InformePAE> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setInformePAEDAO(InformePAEDAO informePAEDAO){	
		this.setDao(informePAEDAO);
	}
	
	public InformePAEDAO getInformePAEDAO() {
		return (InformePAEDAO) this.getDao();
	}
	
	public List<InformePAE> getAll() {
		return super.getAllActives(InformePAE.class);
	}
	
	public InformePAE get(Integer id){
		return this.get(id, InformePAE.class);
	}

	public List<InformePAE> getInformesPAEByLegajo(Integer idLegajo) {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Usuario usuario = usr.getUsuario();
		return getInformePAEDAO().getListInformePAE(idLegajo, usuario);
	}
}
