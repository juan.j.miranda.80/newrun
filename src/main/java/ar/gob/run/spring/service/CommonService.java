package ar.gob.run.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class CommonService{
	
	@Autowired
    private Environment environment;

	private static final long serialVersionUID = 8810832343472247722L;

	@Value("${fileUploader.path}")
	private String fileUploaderPath;
	
	public String getFileUploaderPath(){
		return fileUploaderPath;
	}
	
	@Value("${tipoIntervencion.idSolicitaMPE}")
	private String idSolicitaMPE;

	public Integer getIdSolicitaMPE() {
		return Integer.valueOf(idSolicitaMPE);
	}
	
	public Boolean isSolicitaMPE(Integer id){
		return (id!=null && id.equals(Integer.valueOf(idSolicitaMPE)));
	}
	
	@Value("${tipoIntervencion.idPAEInicial}")
	private String idPAEInicial;
	
	public Boolean isPAEInicial(Integer id){
		return (id!=null && id.equals(Integer.valueOf(idPAEInicial)));
	}
}