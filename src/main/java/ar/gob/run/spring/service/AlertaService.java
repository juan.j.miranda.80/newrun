package ar.gob.run.spring.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.LegajoDAO;
import ar.gob.run.spring.dao.Llamada102DAO;
import ar.gob.run.spring.model.Alerta;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.operador102.Llamada102;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.security.MySecUser;

@Service("AlertaService")
@Transactional(readOnly = true)
public class AlertaService {
	
	@Autowired
    private Environment environment;

	private static final long serialVersionUID = 8810832343472247722L;

	private Llamada102DAO llamada102DAO;
	
	private LegajoDAO legajoDAO;
	
	@Value("${tipoIntervencion.idPER45}")
	private String idPER45;
	
	@Value("${tipoIntervencion.idPER90}")
	private String idPER90;
	
	@Value("${tipoIntervencion.idPER120}")
	private String idPER120;
	
	@Value("${tipoIntervencion.idPER150}")
	private String idPER150;
	

	public Llamada102DAO getLlamada102DAO() {
		return llamada102DAO;
	}

	@Autowired
	public void setLlamada102DAO(Llamada102DAO llamada102dao) {
		llamada102DAO = llamada102dao;
	}

	public LegajoDAO getLegajoDAO() {
		return legajoDAO;
	}

	@Autowired
	public void setLegajoDAO(LegajoDAO legajoDAO) {
		this.legajoDAO = legajoDAO;
	}

	public List<Alerta> getAlertas(){
		Map<Zonal,Alerta> alertasByZonales =  getAlertaLegajoVtoByZonal();
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Usuario usuario = usr.getUsuario();
		
		List<Llamada102> list = getLlamada102DAO().getPendientesByZonal(usuario.getZonal());
		for (Llamada102 llamada102 : list) {
			Alerta alerta = alertasByZonales.get(llamada102.getZonal());
			if (alerta==null) {
				alerta = new Alerta(llamada102.getZonal());
				//alertasByZonales.put(llamada102.getZonal(), alerta);
			}
			alerta.setCantPendientes102(alerta.getCantPendientes102()+1);
			alertasByZonales.put(llamada102.getZonal(), alerta);
		}
		ArrayList<Alerta> result = new ArrayList<Alerta>(alertasByZonales.values());
		Collections.sort(result, new Comparator<Alerta>() {
		    @Override
		    public int compare(Alerta o1, Alerta o2) {
		        return o1.getZonal().getId().compareTo(o2.getZonal().getId());
		    }
		});
		return result;
	}

	public Map<Zonal,Alerta> getAlertaLegajoVtoByZonal(){
		Map<Zonal,Alerta> alertasByZonales = new HashMap<Zonal,Alerta>();
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Usuario usuario = usr.getUsuario();
		
		
		List<Legajo> legajos = getLegajoDAO().getLegajosVencidos(
					Integer.valueOf(idPER45),
					usuario.getZonal(),45, true);
		for (Legajo legajo : legajos) {
			if (alertasByZonales.get(legajo.getZonal())==null)
				alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
			alertasByZonales.get(legajo.getZonal()).setCant45(alertasByZonales.get(legajo.getZonal()).getCant45()+1);
		}
		
		legajos = getLegajoDAO().getLegajosVencidos(
				Integer.valueOf(idPER90),
				usuario.getZonal(),90, true );
		for (Legajo legajo : legajos) {
			if (alertasByZonales.get(legajo.getZonal())==null)
				alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
			alertasByZonales.get(legajo.getZonal()).setCant90(alertasByZonales.get(legajo.getZonal()).getCant90()+1);
		}
		
		legajos = getLegajoDAO().getLegajosVencidos(
				Integer.valueOf(idPER120),
				usuario.getZonal(),120, true );
		for (Legajo legajo : legajos) {
			if (alertasByZonales.get(legajo.getZonal())==null)
				alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
			alertasByZonales.get(legajo.getZonal()).setCant120(alertasByZonales.get(legajo.getZonal()).getCant120()+1);
		}
		
		legajos = getLegajoDAO().getLegajosVencidos(
				Integer.valueOf(idPER150),
				usuario.getZonal(),150, true );
		for (Legajo legajo : legajos) {
			if (alertasByZonales.get(legajo.getZonal())==null)
				alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
			alertasByZonales.get(legajo.getZonal()).setCant150(alertasByZonales.get(legajo.getZonal()).getCant150()+1);
		}
		
		//por vencer
		if (usuario.hasRole(Rol.ZONAL)){
			legajos = getLegajoDAO().getLegajosPorVencer(
					Integer.valueOf(idPER45),
					usuario.getZonal(),45, true );
			for (Legajo legajo : legajos) {
				if (alertasByZonales.get(legajo.getZonal())==null)
					alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
				alertasByZonales.get(legajo.getZonal()).setCantPorVencer45(alertasByZonales.get(legajo.getZonal()).getCantPorVencer45()+1);
			}
			
			legajos = getLegajoDAO().getLegajosPorVencer(
					Integer.valueOf(idPER90),
					usuario.getZonal(),90, true );
			for (Legajo legajo : legajos) {
				if (alertasByZonales.get(legajo.getZonal())==null)
					alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
				alertasByZonales.get(legajo.getZonal()).setCantPorVencer90(alertasByZonales.get(legajo.getZonal()).getCantPorVencer90()+1);
			}
			
			legajos = getLegajoDAO().getLegajosPorVencer(
					Integer.valueOf(idPER120),
					usuario.getZonal(),120, true );
			for (Legajo legajo : legajos) {
				if (alertasByZonales.get(legajo.getZonal())==null)
					alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
				alertasByZonales.get(legajo.getZonal()).setCantPorVencer120(alertasByZonales.get(legajo.getZonal()).getCantPorVencer120()+1);
			}
			
			legajos = getLegajoDAO().getLegajosPorVencer(
					Integer.valueOf(idPER150),
					usuario.getZonal(),150, true );
			for (Legajo legajo : legajos) {
				if (alertasByZonales.get(legajo.getZonal())==null)
					alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
				alertasByZonales.get(legajo.getZonal()).setCantPorVencer150(alertasByZonales.get(legajo.getZonal()).getCantPorVencer150()+1);
			}
		}
		
		legajos = getLegajoDAO().getLegajosMayor180Dias(
				usuario.getZonal(), true);
		for (Legajo legajo : legajos) {
			if (alertasByZonales.get(legajo.getZonal())==null)
				alertasByZonales.put(legajo.getZonal(), new Alerta(legajo.getZonal()));
			alertasByZonales.get(legajo.getZonal()).setCant180(alertasByZonales.get(legajo.getZonal()).getCant180()+1);
		}
		
		return alertasByZonales;
	}
	
}
