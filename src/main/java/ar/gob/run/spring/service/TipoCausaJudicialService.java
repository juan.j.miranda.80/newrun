package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.TipoCausaJudicialDAO;
import ar.gob.run.spring.model.TipoCausaJudicial;

@Service("TipoCausaJudicialService")
@Transactional(readOnly = true)
public class TipoCausaJudicialService extends GenericService<TipoCausaJudicial> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setTipoCausaJudicialDAO(TipoCausaJudicialDAO dao){	
		this.setDao(dao);
	}
	
	public TipoCausaJudicialDAO getTipoCausaJudicialDAO() {
		return (TipoCausaJudicialDAO) this.getDao();
	}
	
	public List<TipoCausaJudicial> getAll() {
		return super.getAllActives(TipoCausaJudicial.class);
	}
	
	public TipoCausaJudicial get(Integer id){
		return this.get(id, TipoCausaJudicial.class);
	}

}
