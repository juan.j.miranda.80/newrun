package ar.gob.run.spring.model;

public enum TipoDisponibilidad {
	
	A("Activa"), F("Finalizada"),
	ACTIVA("Activa"), FINALIZADA("Finalizada");
	
	String descripcion;

	private TipoDisponibilidad(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
}
