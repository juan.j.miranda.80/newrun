package ar.gob.run.managedControllers.reports;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.security.core.context.SecurityContextHolder;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.operador102.EstadoLlamada;
import ar.gob.run.spring.model.operador102.Llamada102;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.LegajoService;
import ar.gob.run.spring.service.Llamada102Service;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="rptLlamadas102MB")
@SessionScoped
public class RptLlamadas102ManagedBean implements Serializable{

	Integer nroNotifDesde;
	Integer nroNotifHta;
	Date fechaNotifDesde;
	Date fechaNotifHta;
	Zonal zonal;
	EstadoLlamada estadoLLamada;
	
	List<Llamada102> list;
	
	@ManagedProperty(value="#{Llamada102Service}")
	Llamada102Service llamada102Service;
	
	private static final long serialVersionUID = 1L;

	public String puedeEjecutar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) || usr.getUsuario().hasRole(Rol.ADMIN) ||
				usr.getUsuario().hasRole(Rol.OPERADOR_102) ||
				usr.getUsuario().hasRole(Rol.SUPERVISOR))
			return "true";
		return "false";
	}

	public String usuarioZonal(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) ) return "true";
		return "false";
	}
	
	public Zonal getZonal() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ZONAL) )
			zonal = usr.getUsuario().getZonal();
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}


	public List<Llamada102> getList() {
		return list;
	}

	public void setList(List<Llamada102> list) {
		this.list = list;
	}

	public void buscarLlamadas(){
		this.list = getLlamada102Service().getRptLlamadas(getFechaNotifDesde(),
				getFechaNotifHta(),getNroNotifDesde(), getNroNotifHta(),
				getZonal(),getEstadoLLamada());
	}
	
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(){
		return zonalService.getZonales(this.getZonal());
	}

	public Integer getNroNotifDesde() {
		return nroNotifDesde;
	}

	public void setNroNotifDesde(Integer nroNotifDesde) {
		this.nroNotifDesde = nroNotifDesde;
	}

	public Integer getNroNotifHta() {
		return nroNotifHta;
	}

	public void setNroNotifHta(Integer nroNotifHta) {
		this.nroNotifHta = nroNotifHta;
	}

	public Date getFechaNotifDesde() {
		return fechaNotifDesde;
	}

	public void setFechaNotifDesde(Date fechaNotifDesde) {
		this.fechaNotifDesde = fechaNotifDesde;
	}

	public Date getFechaNotifHta() {
		return fechaNotifHta;
	}

	public void setFechaNotifHta(Date fechaNotifHta) {
		this.fechaNotifHta = fechaNotifHta;
	}

	public EstadoLlamada getEstadoLLamada() {
		return estadoLLamada;
	}

	public void setEstadoLLamada(EstadoLlamada estadoLLamada) {
		this.estadoLLamada = estadoLLamada;
	}

	public Llamada102Service getLlamada102Service() {
		return llamada102Service;
	}

	public void setLlamada102Service(Llamada102Service llamada102Service) {
		this.llamada102Service = llamada102Service;
	}

	public ZonalService getZonalService() {
		return zonalService;
	}
	
	
}
