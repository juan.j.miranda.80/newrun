package ar.gob.run.spring.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.DatosAuditoria;
import ar.gob.run.spring.model.EstadoSupervInterv;
import ar.gob.run.spring.model.Familia;
import ar.gob.run.spring.model.IntervencionExcepcional;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Persona;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.TipoAlertaMPE;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.institucion.Institucion;
import ar.gob.run.spring.model.usuario.Usuario;

@Repository
public class LegajoDAO  extends HibernateDAO<Legajo, Integer>{
	    
	@Autowired
	Environment environment;
	
	@Value("${tipoIntervencion.diasAvisoPrevio}")
	private String diasAvisoPrevio;
	
	@Value("${tipoIntervencion.idComunicaMPE}")
	private String idComunicaMPE;

	@Value("${tipoIntervencion.idSolicitaMPE}")
	private String idSolicitaMPE;
	
    public List<Legajo> getLegajos(){
    	List<Legajo> list = currentSession().createQuery("select new Legajo(z.id, z.codigo, z.apellidoYNombre, z.nroDocumento, z.telefono) "
    			+ " from Legajo z where z.activo = TRUE order by z.id desc").list();
        return list;
    }

	public String getNuevoCodigo(Legajo l) {
		List<Legajo> result = currentSession().createQuery("select new Legajo(l.id, l.codigo, l.apellidoYNombre, l.nroDocumento, l.telefono) from Legajo l where l.zonal = :zonal and l.local = :local and extract(year from l.datosAuditoria.fechaAlta) = extract(year from sysdate()) order by l.id desc")
				.setParameter("zonal", l.getZonal())
				.setParameter("local", l.getLocal())
				.list();
		String numberCode = "00000001";
		if (result.size()>0){
			String codigo = result.get(0).getCodigo();
			Integer primerCero = codigo.indexOf("0");
			numberCode = agregarCeros(String.valueOf(Integer.valueOf(codigo.substring(primerCero, primerCero+8))+1),8);
		}
		String codigo = l.getZonal().getCodigo() + l.getLocal().getCodigo() + numberCode + "/" + String.valueOf(Calendar.getInstance().get(Calendar.YEAR)).substring(2);
		return  codigo;	
	}

	private String agregarCeros(String string, int largo){
		String ceros = "";
		int cantidad = largo - string.length();
		if (cantidad >= 1){
			for(int i=0;i<cantidad;i++)
				ceros += "0";
			return (ceros + string);
		} else
			return string;
	}

	public List<Legajo> getLegajosByFilter(String strQuery) {
		Query query = currentSession().createQuery("select new Legajo(z.id, z.codigo, z.apellidoYNombre, "
				+ "	z.nroDocumento, z.telefono) "
				+ " from Legajo z where "
				+ "(z.codigo like :query or upper(z.apellidoYNombre) like :query"
				+ " or z.nroDocumento like :query) "
				+ "and z.activo = TRUE "
				+ "order by z.codigo asc").
				setParameter("query", "%"+strQuery.toUpperCase()+"%");
				
		List<Legajo> list = query.list();
		return list;
	}
	
	public List<Legajo> getLegajosByDoc(String doc, String codigo) {
		Query query = currentSession().createQuery("select new Legajo(z.id, z.codigo, z.apellidoYNombre, "
				+ " z.nroDocumento, z.telefono)  "
				+ " from Legajo z where "
				+ "(upper(z.nroDocumento) like :doc) "
				+ "and z.activo = TRUE "
				+ (!StringUtils.isEmpty(codigo)?" and codigo <> :codigo ":"")
				+ "order by z.codigo asc").
				setParameter("doc", "%"+doc.toUpperCase()+"%");
		
		if (!StringUtils.isEmpty(codigo))
			query.setString("codigo", codigo);
				
		List<Legajo> list = query.list();
		return list;
	}
	
	public List<Legajo> getLegajosByZonal(Zonal zonal){
		Query query = currentSession().createQuery("select new Legajo(l.id, l.codigo, l.apellidoYNombre, l.nroDocumento, l.telefono)  "
				+ " from Legajo z where "
				+ "z.zonal = :zonal  "
				+ "and z.activo = TRUE "
				+ "order by z.codigo asc").
				setParameter("zonal", zonal);
		List<Legajo> list = query.list();
		return list;
	}
	
	public List<Legajo> getLegajosByLocal(Local local){
		Query query = currentSession().createQuery("select new Legajo(z.id, z.codigo, z.apellidoYNombre, z.nroDocumento, z.telefono)  "
				+ " from Legajo z where "
				+ "z.local = :local  "
				+ "and z.activo = TRUE "
				+ "order by z.id desc").
				setParameter("local", local);
		List<Legajo> list = query.list();
		return list;
	}
	
	public List<Legajo> getLegajosVencidos(Integer idTpoIntervencion, Zonal zonal, Integer dias){
		return getLegajosVencidos(idTpoIntervencion, zonal, dias, false);
	}
	
	public List<Legajo> getLegajosVencidos(Integer idTpoIntervencion, Zonal zonal, Integer dias, Boolean compacto){
		
		String beginQuery ="select l ";
		if (compacto)
			beginQuery = "select new Legajo(l.id, l.zonal) ";
		
		Query query = getSessionFactory().getCurrentSession()
                .createQuery(beginQuery + "from Legajo l "
                		+ " where l.activo=true "
                		+ " and exists (from IntervencionExcepcional ie1 where ie1.activo=true and ie1.legajo = l "
                		+ "					and  ie1.tipoIntervencion.id = :circuitoMPE "
                		+ "					and ( datediff(sysdate(),ie1.fecha) >= :dias )"
                		+ "					and ie1.id > ( "
                		+ " 					select coalesce(max(i3.id),-1 ) as id3 from IntervencionExcepcional i3 where i3.legajo = l and i3.tipoIntervencion.intervencionCese = true and i3.activo = true "
                		+ " 					) "
                		+ " 			) "
                		+ "	and not exists (from IntervencionExcepcional ie2 where ie2.activo = true and ie2.legajo = l "
                		+ "					 	and ie2.tipoIntervencion.id = :idTipoIntervencion "
                		+ " 					and ie2.id > ( "
                        						+ " select coalesce(max(i3.id),-1 ) as id3 from IntervencionExcepcional i3 where i3.legajo = l and i3.tipoIntervencion.intervencionCese = true and i3.activo = true "
                        		+ " ) "
                		+ "						 ) "
                		+ (zonal!=null?" and l.zonal = :zonal ":"")
                		+ " order by l.id desc ");
		
		query.setParameter("circuitoMPE", Integer.valueOf(idComunicaMPE));
		query.setParameter("idTipoIntervencion", idTpoIntervencion);
		query.setParameter("dias", dias);
		if (zonal!=null) query.setParameter("zonal", zonal);
		
		return query.list();
	}
	
	public List<Legajo> getLegajosPorVencer(Integer idTpoIntervencion, Zonal zonal, Integer dias){
		return getLegajosPorVencer(idTpoIntervencion, zonal, dias, false); 
	}
	
	public List<Legajo> getLegajosPorVencer(Integer idTpoIntervencion, Zonal zonal, Integer dias, Boolean compacto){
		//si soy usuario zonal, voy a querer que me envien el aviso por vencer
		Integer diasPorVencer = dias-Integer.valueOf(diasAvisoPrevio);
		String beginQuery ="";
		if (compacto)
			beginQuery = "select new Legajo(l.id, l.zonal) ";
		Query query = getSessionFactory().getCurrentSession()
                .createQuery(beginQuery + "from Legajo l where l.activo=true "
                		+ " and exists (from IntervencionExcepcional ie1 where ie1.activo=true and ie1.legajo = l "
                		+ "					and  ie1.tipoIntervencion.id = :circuitoMPE "
                		+ "					and (datediff(sysdate(),ie1.fecha) BETWEEN :diasPorVencer  and :dias) "
                		+ "					and ie1.id > ( "
                		+ " 					select coalesce(max(i3.id),-1 ) as id3 from IntervencionExcepcional i3 where i3.legajo = l and i3.tipoIntervencion.intervencionCese = true and i3.activo = true "
                		+ " 					) "
                		+ " 			) "
                		+ "	and not exists (from IntervencionExcepcional ie2 where ie2.activo = true and ie2.legajo = l "
                		+ "					 	and ie2.tipoIntervencion.id = :idTipoIntervencion "
                		+ "					and ie2.id > ( "
                		+ " 					select coalesce(max(i3.id),-1 ) as id3 from IntervencionExcepcional i3 where i3.legajo = l and i3.tipoIntervencion.intervencionCese = true and i3.activo = true "
                		+ " 					) ) "
                		+ " and l.zonal = :zonal "
                		+ " order by l.id desc ");
		
		query.setParameter("circuitoMPE", Integer.valueOf(idComunicaMPE));
		query.setParameter("idTipoIntervencion", idTpoIntervencion);
		query.setParameter("dias", dias);
		query.setParameter("diasPorVencer", diasPorVencer);
		query.setParameter("zonal", zonal);
		
		return query.list();
		
	}

	public List<Legajo> getLegajosMayor180Dias(Zonal zonal){
		return getLegajosMayor180Dias(zonal, false);
	}
	
	public List<Legajo> getLegajosMayor180Dias(Zonal zonal, Boolean compacto){
		
		String beginQuery ="";
		if (compacto)
			beginQuery = "select new Legajo(l.id, l.zonal) ";
		
		Query query = getSessionFactory().getCurrentSession()
                .createQuery(beginQuery + "from Legajo l where l.activo=true "
                		+ " and exists (from IntervencionExcepcional ie1 where ie1.activo=true and ie1.legajo = l "
                		+ "					and  (ie1.tipoIntervencion.id = :solicitaMPE or ie1.tipoIntervencion.intervencionCese = TRUE) "
                		+ " 			) "
                		+ (zonal!=null?" and l.zonal = :zonal ":"")
                		);
		
		query.setParameter("solicitaMPE", Integer.valueOf(idSolicitaMPE));
		if (zonal!=null) query.setParameter("zonal", zonal);
		
		List<Legajo> list = query.list();
		List<Legajo> result = new ArrayList<Legajo>();
		
		Iterator<Legajo> it = list.iterator();
		while (it.hasNext()){
			Legajo legajo = it.next();
			List<IntervencionExcepcional> mpes = legajo.getIntervencionesMPE();
			if (mpes!=null && mpes.size()>0){
				Collections.sort(mpes, Collections.reverseOrder());
				IntervencionExcepcional ultimaAgregada = mpes.get(0);
				if (ultimaAgregada.getFecha()!=null && ultimaAgregada.getTipoIntervencion().getId().equals(Integer.valueOf(idSolicitaMPE)) &&
						(TimeUnit.DAYS.convert((new Date().getTime() - ultimaAgregada.getFecha().getTime()), 
								TimeUnit.MILLISECONDS)>=Long.valueOf(TipoAlertaMPE.CESE180.getDias()))){
					result.add(legajo);
				}
			}
		}
		
		return result;
	}

	public List<Legajo> getRptLegajos(Date from, Date to, Sexo sexo, Zonal zonal, 
			Local local, Date nacimientoFrom,
			Date nacimientoTo, Boolean tieneMEP) {
		
//		String queryStr ="select new Legajo(l.id, l.codigo, l.apellidoYNombre, l.datosAuditoria,"
//				+ " l.sexo, l.fechaNacimiento, l.tipoDocumento, l.nroDocumento, l.zonal, l.local,"
//				+ " count(iMPE), count(l.intervencionesMPI) )  "
		String queryStr ="select l.id, l.codigo, l.apellidoYNombre, l.datosAuditoria,"
				+ " l.sexo, l.fechaNacimiento, l.tipoDocumento, l.nroDocumento, l.zonal, l.local , count(iMPE), count(iMPI) "
				+ " from Legajo l "
				+ " left outer join l.intervencionesMPE iMPE "
				+ " left outer join l.intervencionesMPI iMPI where l.activo=true "
				+ " and iMPE.activo=true and iMPI.activo=true and iMPE.tipoIntervencion.id = :idSolicitaMPE "
				+ " and iMPE.estadoSuperv = :aprobado "
        		+ (from!=null?" and datosAuditoria.fechaAlta >= :from ":"")
        		+ (to!=null?" and datosAuditoria.fechaAlta <= :to ":"")
        		+ ((sexo!=null && sexo.ordinal()!=0)?" and sexo = :sexo ":"")
        		+ (local!=null?" and local = :local ":"")
        		+ (zonal!=null?" and zonal = :zonal ":"")
        		+ (nacimientoFrom!=null?" and fechaNacimiento >= :nacimientoFrom ":"")
        		+ (nacimientoTo!=null?" and fechaNacimiento <= :nacimientoTo ":""); 
		
		if (tieneMEP != null && tieneMEP)   
			queryStr += " and exists (from IntervencionMEP ie1 where ie1.activo=true and ie1.legajo = l )";
		
		queryStr += " group by l.id, l.codigo, l.apellidoYNombre, l.datosAuditoria,"
				+ " l.sexo, l.fechaNacimiento, l.tipoDocumento, l.nroDocumento, l.zonal, l.local ";
		Query query = getSessionFactory().getCurrentSession()
                .createQuery(queryStr);
		
		if (from!=null) query.setParameter("from", from);
		if (to!=null) query.setParameter("to", to);
		if (sexo!=null && sexo.ordinal()!=0) query.setParameter("sexo", sexo.name());
		if (zonal!=null) query.setParameter("zonal", zonal);
		if (local!=null) query.setParameter("local", local);
		if (nacimientoFrom!=null) query.setParameter("nacimientoFrom", nacimientoFrom);
		if (nacimientoTo!=null) query.setParameter("nacimientoTo", nacimientoTo);
     
		query.setParameter("idSolicitaMPE", Integer.valueOf(idSolicitaMPE));
		query.setParameter("aprobado", EstadoSupervInterv.ACEPTADO);
		
		query.setResultTransformer(new ResultTransformer() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public Object transformTuple(Object[] arg0, String[] arg1) {
				Legajo leg = new Legajo((Integer)arg0[0],(String)arg0[1],(String)arg0[2],(DatosAuditoria)arg0[3],
						(Sexo)arg0[4],(Date)arg0[5],(String)arg0[6],(String)arg0[7],
						(Zonal)arg0[8],(Local)arg0[9],(Long)arg0[10],(Long)arg0[11]);
				return leg;
			}

			@Override
			public List transformList(List arg0) {
				return arg0;
			}
			
		});
		
		return query.list();
	}

	public List<Legajo> getLegajosPendientesAprobados() {
		Integer intSolicitaMPE = Integer.valueOf(this.idSolicitaMPE);
		
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("select new Legajo(l.id, l.codigo, l.apellidoYNombre, l.nroDocumento, l.telefono) "
                		+ " from Legajo l where l.activo=true "
                		+ " and exists (from IntervencionExcepcional ie1 where ie1.activo=true and ie1.legajo = l "
                		+ "					and  ie1.tipoIntervencion.id = :idSolicitaMPE and ie1.estadoSuperv = :aprobado "
                		+ " 			) "
                		+ "	and not exists (from IntervencionExcepcional ie2 where ie2.activo = true and ie2.legajo = l "
                		+ "					 	and ie2.tipoIntervencion.id = :idComunicaMPE ) "
                		+ " order by l.id desc ");
		
		query.setParameter("idSolicitaMPE", intSolicitaMPE);
		query.setParameter("idComunicaMPE", Integer.valueOf(idComunicaMPE));
		query.setParameter("aprobado", EstadoSupervInterv.ACEPTADO);
		
		return query.list();
	}

	public List<Legajo> getLegajosNovedadesMPE(Zonal zonal) {
		//si soy usuario zonal, voy a querer que me envien el aviso por vencer
		Integer intSolicitaMPE = Integer.valueOf(this.idSolicitaMPE);
		Integer intComunicaMPE = Integer.valueOf(this.idComunicaMPE);
		
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("select new Legajo(l.id, l.codigo, l.apellidoYNombre, l.nroDocumento, l.telefono, iMPE.estadoSuperv)"
                		+ "  from Legajo l "
                		+ " join l.intervencionesMPE iMPE where l.activo=true "
                		+ (zonal!=null?" and iMPE.zonal = :zonal ":"")
                		+ " and iMPE.activo=true "
                		+ "	and (iMPE.tipoIntervencion.id = :idSolicitaMPE or iMPE.tipoIntervencion.id = :idComunicaMPE) "
                		+ " and iMPE.fechaUltCambioEstado >= :ultimoMes "
                		+ " and iMPE.id = (select max(e.id) from IntervencionExcepcional e where e.legajo = l group by e.legajo) "
                		+ " order by iMPE.estadoSuperv, iMPE.fechaUltCambioEstado desc "
                		);
		
		query.setParameter("idSolicitaMPE", intSolicitaMPE);
		query.setParameter("idComunicaMPE", intComunicaMPE);
		if (zonal!=null) query.setParameter("zonal", zonal);
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, -30);
		query.setParameter("ultimoMes", cal.getTime());
		
		return query.list();
	}
	
	
	public List<Legajo> getLegajosAbiertosMPEPorInstitucion(Institucion inst) {
		Integer intSolicitaMPE = Integer.valueOf(this.idSolicitaMPE);
		
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("select l from Legajo l "
                		+ " join l.intervencionesMPE iMPE where l.activo=true "
                		+ (inst!=null?" and iMPE.institucion = :institucion ":"")
                		+ " and iMPE.activo=true "
                		//+ "	and iMPE.tipoIntervencion.id = :idSolicitaMPE "
                		);
		
		//query.setParameter("idSolicitaMPE", intSolicitaMPE);
		if (inst!=null) query.setParameter("institucion", inst);
		
		List<Legajo> result =  new ArrayList<Legajo>();
		List<Legajo> queryRslt = query.list();
		if (queryRslt.size()>0){
			Iterator<Legajo> legs = queryRslt.iterator();
			while (legs.hasNext()){
				Legajo leg = legs.next();
				if (tieneMPEActiva(leg))
					result.add(leg);
			}
		}
		return result;
	}
	
	public List<Legajo> getLegajosAbiertosMPEPorFamilia(Familia flia) {
		Integer intSolicitaMPE = Integer.valueOf(this.idSolicitaMPE);
		
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("select l from Legajo l "
                		+ " join l.intervencionesMPE iMPE where l.activo=true "
                		+ (flia!=null?" and iMPE.familia = :familia ":"")
                		+ " and iMPE.activo=true "
                		//+ "	and iMPE.tipoIntervencion.id = :idSolicitaMPE "
                		);
		
		//query.setParameter("idSolicitaMPE", intSolicitaMPE);
		if (flia!=null) query.setParameter("familia", flia);
		
		List<Legajo> result =  new ArrayList<Legajo>();
		Iterator<Legajo> legs = query.list().iterator();
		while (legs.hasNext()){
			Legajo leg = legs.next();
			if (tieneMPEActiva(leg))
				result.add(leg);
		}
		return result;
	}
	
	public Boolean tieneMPEActiva(Legajo leg){
		List<IntervencionExcepcional> listMPE = leg.getIntervencionesMPE();
		Boolean tieneActiva = false;
		Integer idActiva = null;
		if (listMPE!=null){
			Collections.sort(listMPE);
			Iterator<IntervencionExcepcional> it = listMPE.iterator();
			while (it.hasNext()){
				IntervencionExcepcional mpe = it.next();
				if (mpe.getTipoIntervencion().getId().equals(Integer.valueOf(idSolicitaMPE))){
					tieneActiva = true;
					idActiva = mpe.getId();
				}else if (idActiva != null && mpe.getId().compareTo(idActiva)==-1){
					if (mpe.getTipoIntervencion().getIntervencionCese()){
						tieneActiva = false;
						idActiva = null;
					}
				}
			}
		}
		return tieneActiva;
	}
	
	public List<Legajo> getLegajosAsignadosReferentePAE(Usuario usuario) {
		
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("select new Legajo(l.id, l.codigo, l.apellidoYNombre, l.nroDocumento, l.telefono) from Legajo l "
                		+ " where l.activo=true "
                		+ "	and (l.referentePAE is not null and l.referentePAE.usuario is not null and l.referentePAE.usuario = :usuario) "
                		+ " order by 1 desc"
                		).setParameter("usuario", usuario);
		
		return query.list();
	}

	public Legajo get(Integer id) {
		return (Legajo) getSessionFactory().getCurrentSession().get(Legajo.class,id);
	}

	public List<Legajo> getLegajosFliar(Persona familiarSeleccionado) {
		Query query = getSessionFactory().getCurrentSession()
                .createQuery("select new Legajo(l.id, l.codigo, l.apellidoYNombre, l.nroDocumento, relV.tipoVinculoFamiliar) from Legajo l "
                		+ " join l.relacionesVinculares relV "
                		+ " where l.activo=true "
                		+ "	and (relV.familiar in :familiarSeleccionado ) "
                		+ " order by l.id desc "
                		).setParameter("familiarSeleccionado", familiarSeleccionado);
		
		return query.list();
	}

}