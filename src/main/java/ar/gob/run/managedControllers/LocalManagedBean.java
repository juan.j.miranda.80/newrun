package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.service.LocalService;

@ManagedBean(name="localMB")
@ViewScoped
public class LocalManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{localService}")
    LocalService localService;

    List<Local> list = new ArrayList<Local>();

    Local instance = new Local();
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
    
    public void save() {
        try {
            getLocalService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(Local ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(Local ent){
    	getLocalService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new Local();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new Local();
    	editMode = false;
    }

	public Local getInstance() {
		return instance;
	}

	public void setInstance(Local instance) {
		this.instance = instance;
	}

	public LocalService getLocalService() {
		return localService;
	}

	public void setLocalService(LocalService service) {
		this.localService = service;
	}

	public List<Local> getList() {
		return getLocalService().getAllActives(Local.class);
	}

	public void setList(List<Local> list) {
		this.list = list;
	}
}
