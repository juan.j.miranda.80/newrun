package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;
import ar.gob.run.spring.service.TipoVinculoFamiliarService;

@ManagedBean(name="tipoVinculoFamiliarMB")
@ViewScoped
public class TipoVinculoFamiliarManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{TipoVinculoFamiliarService}")
    TipoVinculoFamiliarService service;

    List<TipoVinculoFamiliar> list;

    TipoVinculoFamiliar instance;
    
    Boolean editMode = false;
    
	public Boolean getEditMode() {
		return editMode;
	}
	
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(TipoVinculoFamiliar ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(TipoVinculoFamiliar ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new TipoVinculoFamiliar();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new TipoVinculoFamiliar();
    	editMode = false;
    }

	public TipoVinculoFamiliar getInstance() {
		return instance;
	}

	public void setInstance(TipoVinculoFamiliar instance) {
		this.instance = instance;
	}

	public TipoVinculoFamiliarService getService() {
		return service;
	}

	public void setService(TipoVinculoFamiliarService service) {
		this.service = service;
	}

	public List<TipoVinculoFamiliar> getList() {
		return getService().getAllActives(TipoVinculoFamiliar.class);
	}

	public void setList(List<TipoVinculoFamiliar> list) {
		this.list = list;
	}


	
}
