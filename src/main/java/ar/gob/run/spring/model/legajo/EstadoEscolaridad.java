package ar.gob.run.spring.model.legajo;

public enum EstadoEscolaridad {

	ES("Escolarizado"), NC("No concurre"),
	NE("No escolarizado"), ND("No corresponde por edad"),
	SD("Se desconoce");
	
	private String descripcion;

	private EstadoEscolaridad(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	
	
}
