package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.ObraSocial;
import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;
import ar.gob.run.spring.service.ObraSocialService;
import ar.gob.run.spring.service.TipoVinculoFamiliarService;

@FacesConverter(value = "obraSocialConverter")
public class ObraSocialConverter implements Converter {

	public ObraSocialConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), ObraSocial.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((ObraSocial) value).getId().toString();
	}

	public ObraSocialService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{ObraSocialService}", ObraSocialService.class);

			ObraSocialService service = (ObraSocialService)vex.getValue(context.getELContext());
			return service;
	}

}
