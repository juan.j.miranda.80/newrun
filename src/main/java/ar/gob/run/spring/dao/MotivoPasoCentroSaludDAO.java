package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.MotivoPasoCentroSalud;

@Repository
public class MotivoPasoCentroSaludDAO extends HibernateDAO<MotivoPasoCentroSalud,Integer> {

}
