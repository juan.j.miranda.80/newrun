package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.JuzgadoDAO;
import ar.gob.run.spring.model.Juzgado;
import ar.gob.run.spring.model.OperadorPenalJuvenil;
import ar.gob.run.spring.model.legajo.CausaJudicial;
		  
@Service("JuzgadoService")
@Transactional(readOnly = true)
public class JuzgadoService extends GenericService<Juzgado>{

	@Autowired
	Environment environment;
	
	@Value("${tipoOrganoJudicial.tipoJuzgadoId}")
	private String tipoJuzgadoId;
	
	@Value("${tipoOrganoJudicial.tipoDefensoriaId}")
	private String tipoDefensoriaId;
	
	@Autowired
	public void setJuzgadoDAO(JuzgadoDAO juzgadoDAO){
		this.setDao(juzgadoDAO);
	}

    public JuzgadoDAO getJuzgadoDAO() {
        return (JuzgadoDAO) this.getDao();
    }

	public List<Juzgado> getAllActivesWithExtra(Juzgado elem) {
		return super.getAllActivesWithExtra(Juzgado.class, elem);
	}

	public List<Juzgado> getAll() {
		return super.getAllActives(Juzgado.class);
	}

	public List<Juzgado> getJuzgadosIntervinientes() {
		Integer juzgadoId=Integer.valueOf(tipoJuzgadoId);
		return getJuzgadoDAO().getJuzgados(juzgadoId);
	}
	
	public List<Juzgado> getJuzgadosDefensoria() {
		Integer juzgadoId=Integer.valueOf(tipoJuzgadoId);
		return getJuzgadoDAO().getNotJuzgados(juzgadoId);
	}

	public Juzgado getJuzgadoByCausa(CausaJudicial causaSel) {
		return null;
	}
    
}
