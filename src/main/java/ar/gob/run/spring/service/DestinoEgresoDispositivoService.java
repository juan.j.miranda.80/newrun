package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.DestinoEgresoDispositivoDAO;
import ar.gob.run.spring.model.DestinoEgresoDispositivo;

@Service("DestinoEgresoDispositivoService")
@Transactional(readOnly = true)
public class DestinoEgresoDispositivoService extends GenericService<DestinoEgresoDispositivo> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setDestinoEgresoDispositivoDAO(DestinoEgresoDispositivoDAO dao){	
		this.setDao(dao);
	}
	
	public DestinoEgresoDispositivoDAO getDestinoEgresoDispositivoDAO() {
		return (DestinoEgresoDispositivoDAO) this.getDao();
	}
	
	public List<DestinoEgresoDispositivo> getAll() {
		return super.getAllActives(DestinoEgresoDispositivo.class);
	}
	
	public DestinoEgresoDispositivo get(Integer id){
		return this.get(id, DestinoEgresoDispositivo.class);
	}

}
