package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import ar.gob.run.spring.model.Acompaniante;
import ar.gob.run.spring.model.Categoria;
import ar.gob.run.spring.model.CategoriaMotivoIntervencion;
import ar.gob.run.spring.model.CategoriaTipoIntervencion;
import ar.gob.run.spring.model.Comisaria;
import ar.gob.run.spring.model.DepartamentoJudicial;
import ar.gob.run.spring.model.DerechoAfectado;
import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.model.DestinoEgresoDispositivo;
import ar.gob.run.spring.model.Dimension;
import ar.gob.run.spring.model.EstadoDesconocePoseeONo;
import ar.gob.run.spring.model.EstadoDesconoceSIoNO;
import ar.gob.run.spring.model.ExperienciaAcomp;
import ar.gob.run.spring.model.Familia;
import ar.gob.run.spring.model.FuerzaSeguridad;
import ar.gob.run.spring.model.GpoBienJuridicoProtegido;
import ar.gob.run.spring.model.Juzgado;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Localidad;
import ar.gob.run.spring.model.MotivoAprehension;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.MotivoPasoCentroSalud;
import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.model.Nacionalidad;
import ar.gob.run.spring.model.NivelEducacion;
import ar.gob.run.spring.model.ObraSocial;
import ar.gob.run.spring.model.OperadorPenalJuvenil;
import ar.gob.run.spring.model.Persona;
import ar.gob.run.spring.model.Procedencia;
import ar.gob.run.spring.model.Provincia;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.SexoAdmision;
import ar.gob.run.spring.model.SiNo;
import ar.gob.run.spring.model.SiNoSinDato;
import ar.gob.run.spring.model.TipoAmbitoCumplimiento;
import ar.gob.run.spring.model.TipoCausaJudicial;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoDelito;
import ar.gob.run.spring.model.TipoDisponibilidad;
import ar.gob.run.spring.model.TipoDisposicion;
import ar.gob.run.spring.model.TipoDispositivo;
import ar.gob.run.spring.model.TipoDomicilioJoven;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.TipoTiempoComisaria;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.institucion.Institucion;
import ar.gob.run.spring.model.institucion.TipoDependencia;
import ar.gob.run.spring.model.institucion.TipoInstitucion;
import ar.gob.run.spring.model.legajo.CausaJudicial;
import ar.gob.run.spring.model.legajo.EstadoCausaJudicial;
import ar.gob.run.spring.model.legajo.EstadoCivil;
import ar.gob.run.spring.model.legajo.EstadoDocumento;
import ar.gob.run.spring.model.legajo.EstadoEscolaridad;
import ar.gob.run.spring.model.legajo.EstudioCursado;
import ar.gob.run.spring.model.legajo.SituacionLaboral;
import ar.gob.run.spring.model.legajo.TipoDefensaTecnica;
import ar.gob.run.spring.model.legajo.TipoOrganoJudicial;
import ar.gob.run.spring.model.legajo.TipoProblematicaSalud;
import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;
import ar.gob.run.spring.model.legajo.informePAE.TipoVivienda;
import ar.gob.run.spring.model.operador102.EstadoLlamada;
import ar.gob.run.spring.model.operador102.TipoConviviente;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.service.AcompanianteService;
import ar.gob.run.spring.service.CategoriaMotivoIntervencionService;
import ar.gob.run.spring.service.CategoriaService;
import ar.gob.run.spring.service.CategoriaTipoIntervencionService;
import ar.gob.run.spring.service.ComisariaService;
import ar.gob.run.spring.service.DepartamentoJudicialService;
import ar.gob.run.spring.service.DerechoAfectadoService;
import ar.gob.run.spring.service.DerivadorService;
import ar.gob.run.spring.service.DestinoEgresoDispositivoService;
import ar.gob.run.spring.service.DimensionService;
import ar.gob.run.spring.service.EstadoCivilService;
import ar.gob.run.spring.service.EstudioCursadoService;
import ar.gob.run.spring.service.ExperienciaAcompService;
import ar.gob.run.spring.service.FamiliaService;
import ar.gob.run.spring.service.FuerzaSeguridadService;
import ar.gob.run.spring.service.GpoBienJuridicoProtegidoService;
import ar.gob.run.spring.service.InstitucionService;
import ar.gob.run.spring.service.JuzgadoService;
import ar.gob.run.spring.service.LegajoService;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.LocalidadService;
import ar.gob.run.spring.service.MotivoAprehensionService;
import ar.gob.run.spring.service.MotivoIntervencionService;
import ar.gob.run.spring.service.MotivoPasoCentroSaludService;
import ar.gob.run.spring.service.MunicipioService;
import ar.gob.run.spring.service.NacionalidadService;
import ar.gob.run.spring.service.ObraSocialService;
import ar.gob.run.spring.service.OperadorPenalJuvenilService;
import ar.gob.run.spring.service.PersonaService;
import ar.gob.run.spring.service.ProcedenciaService;
import ar.gob.run.spring.service.ProvinciaService;
import ar.gob.run.spring.service.SituacionLaboralService;
import ar.gob.run.spring.service.TipoCausaJudicialService;
import ar.gob.run.spring.service.TipoDelitoService;
import ar.gob.run.spring.service.TipoDispositivoService;
import ar.gob.run.spring.service.TipoInstitucionService;
import ar.gob.run.spring.service.TipoIntervencionService;
import ar.gob.run.spring.service.TipoOrganoJudicialService;
import ar.gob.run.spring.service.TipoVinculoFamiliarService;
import ar.gob.run.spring.service.UsuarioService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="commonSelectorsMB")
@SessionScoped
public class CommonSelectors implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{provinciaService}")
    ProvinciaService provinciaService;

	public void setProvinciaService(ProvinciaService provinciaService) {
		this.provinciaService = provinciaService;
	}
	
	public List<Provincia> getProvincias(Provincia elem){
		return provinciaService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{categoriaService}")
	CategoriaService categoriaService;

	public CategoriaService getCategoriaService() {
		return categoriaService;
	}

	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	public List<Categoria> getCategorias(Categoria elem){
		return categoriaService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{DerechoAfectadoService}")
	DerechoAfectadoService derechoAfectadoService;

	public void setDerechoAfectadoService(DerechoAfectadoService derechoAfectadoService) {
		this.derechoAfectadoService = derechoAfectadoService;
	}
	
	public List<DerechoAfectado> getDerechosAfectados(DerechoAfectado elem){
		return derechoAfectadoService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{DerivadorService}")
	DerivadorService derivadorService;

	public void setDerivadorService(DerivadorService derivadorService) {
		this.derivadorService = derivadorService;
	}
	
	public List<Derivador> getDerivadores(Derivador elem){
		return derivadorService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{CategoriaTipoIntervencionService}")
	CategoriaTipoIntervencionService categoriaTipoIntervencionService;

	public void setCategoriaTipoIntervencionService(CategoriaTipoIntervencionService categoriaTipoIntervencionService) {
		this.categoriaTipoIntervencionService = categoriaTipoIntervencionService;
	}
	
	public List<CategoriaTipoIntervencion> getCategoriasTipoIntervencion(CategoriaTipoIntervencion elem){
		return categoriaTipoIntervencionService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{CategoriaMotivoIntervencionService}")
	CategoriaMotivoIntervencionService categoriaMotivoIntervencionService;

	public void setCategoriaMotivoIntervencionService(CategoriaMotivoIntervencionService categoriaMotivoIntervencionService) {
		this.categoriaMotivoIntervencionService = categoriaMotivoIntervencionService;
	}
	
	public List<CategoriaMotivoIntervencion> getCategoriasMotivoIntervencion(CategoriaMotivoIntervencion elem){
		return categoriaMotivoIntervencionService.getAllActivesWithExtra(elem);
	}
	
	public List<CategoriaMotivoIntervencion> getCategoriasMotivoIntervencion(){
		return categoriaMotivoIntervencionService.getAll();
	}
	
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(Zonal zonal){
		return zonalService.getZonales(zonal);
	}
	
	@ManagedProperty(value="#{localService}")
	LocalService localService;

	public void setLocalService(LocalService localService) {
		this.localService = localService;
	}
	
	public List<Local> getLocales(Local ent){
		return localService.getLocales(ent);
	}
	
	public TipoDefensaTecnica[] getValoresTipoDefensaTecnica() {
	    return TipoDefensaTecnica.values();
	  } 
	
	public TipoTiempoComisaria[] getValoresTiempoPermanencia() {
	    return TipoTiempoComisaria.values();
	  } 
	
	//Sexo
	public Sexo[] getValoresSexo() {
	    return Sexo.values();
	  } 
	
	public TipoProblematicaSalud[] getValoresProblematicaSalud() {
	    return TipoProblematicaSalud.values();
	  } 
	
	public TipoVivienda[] getValoresTipoVivienda() {
	    return TipoVivienda.values();
	  } 
	
	public String getYearRange() {
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String yearInString = String.valueOf(year);
	    now.add(Calendar.YEAR, -100);
	    String hundredYearBefore = String.valueOf(now.get(Calendar.YEAR));
	    return hundredYearBefore + ":" + yearInString;
	  } 
	
	public SexoAdmision[] getValoresSexoAdmision() {
	    return SexoAdmision.values();
	} 
	
	public NivelEducacion[] getValoresNivelEducacion() {
	    return NivelEducacion.values();
	} 
	
	public TipoAmbitoCumplimiento[] getAmbitoCumplimiento(){
		return TipoAmbitoCumplimiento.values();
	}
	
	public TipoDomicilioJoven[] getDomicilioJoven(){
		return TipoDomicilioJoven.values();
	}
	
	public TipoDependencia[] getValoresTipoDependencia(){
		return TipoDependencia.values();
	}
	
	public SiNo[] getValoresSiNo(){
		return SiNo.values();
	}
	
	public SiNoSinDato[] getValoresSiNoSinDato(){
		return SiNoSinDato.values();
	}
	
	public EstadoLlamada[] getValoresEstadoLlamada() {
	    return EstadoLlamada.values();
	  } 
	
	public TipoDisponibilidad[] getValoresTipoDisponibilidad() {
	    return TipoDisponibilidad.values();
	  } 
	
	public TipoDisposicion[] getValoresTipoDisposicion() {
	    return TipoDisposicion.values();
	  } 

	@ManagedProperty(value="#{TipoDispositivoService}")
	TipoDispositivoService tipoDispositivoService;
	
	public TipoDispositivoService getTipoDispositivoService() {
		return tipoDispositivoService;
	}

	public void setTipoDispositivoService(TipoDispositivoService tipoDispositivoService) {
		this.tipoDispositivoService = tipoDispositivoService;
	}

	public List<TipoDispositivo> getTiposDispositivos(){
		return this.tipoDispositivoService.getAll();
	}
	
	//Estado del Documento
	public SelectItem[] getValoresEstadoDoc() {
	    SelectItem[] items = new SelectItem[EstadoDocumento.values().length];
	    int i = 0;
	    //items[i++] = new SelectItem(null, "--Seleccione--");
	    for(EstadoDocumento g: EstadoDocumento.values()) {
	      items[i++] = new SelectItem(g, g.getDescripcion());
	    }
	    return items;
	  } 
	
	@ManagedProperty(value="#{TipoVinculoFamiliarService}")
	TipoVinculoFamiliarService tipoVinculoFamiliarService;

	public void setTipoVinculoFamiliarService(TipoVinculoFamiliarService tipoVinculoFamiliarService) {
		this.tipoVinculoFamiliarService = tipoVinculoFamiliarService;
	}
	
	public List<TipoVinculoFamiliar> getTiposVinculosFamiliares(TipoVinculoFamiliar elem){
		return tipoVinculoFamiliarService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{legajoService}")
	LegajoService legajoService;

	public void setLegajoService(LegajoService legajoService) {
		this.legajoService = legajoService;
	}
	
	public List<Legajo> getAutoCompleteLegajo(String query){
		FacesContext context = FacesContext.getCurrentInstance();
	    Legajo o = (Legajo) UIComponent.getCurrentComponent(context).getAttributes().get("filter");
		if (o!=null)
			return legajoService.getLegajosByFilterWithoutExtra(query, o);
		else
			return legajoService.getLegajosByFilter(query);
	}
	
	@ManagedProperty(value="#{UsuarioService}")
	UsuarioService usuarioService;

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public List<Usuario> getAutoCompleteUsuario(String query){
		return usuarioService.getUsuariosByFilter(query, Rol.REFERENTE_PAE);
	}
	
	
	@ManagedProperty(value="#{EstadoCivilService}")
	EstadoCivilService estadoCivilService;

	public void setEstadoCivilService(EstadoCivilService estadoCivilService) {
		this.estadoCivilService = estadoCivilService;
	}
	
	public List<EstadoCivil> getEstadosCiviles(EstadoCivil elem){
		return estadoCivilService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{EstudioCursadoService}")
	EstudioCursadoService estudioCursadoService;

	public void setEstudioCursadoService(EstudioCursadoService estudioCursadoService) {
		this.estudioCursadoService = estudioCursadoService;
	}
	
	public List<EstudioCursado> getEstudiosCursados(EstudioCursado elem){
		return estudioCursadoService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{SituacionLaboralService}")
	SituacionLaboralService situacionLaboralService;

	public void setSituacionLaboralService(SituacionLaboralService situacionLaboralService) {
		this.situacionLaboralService = situacionLaboralService;
	}
	
	public List<SituacionLaboral> getSituacionesLaborales(SituacionLaboral elem){
		return situacionLaboralService.getAllActivesWithExtra(elem);
	}
	
	@ManagedProperty(value="#{PersonaService}")
	PersonaService personaService;

	public void setPersonaService(PersonaService personaService) {
		this.personaService = personaService;
	}
	
	public List<Persona> getAutoCompletePersonas(String elem){
		return personaService.getPersonaByApYNom(elem);
	}
	
	public List<Persona> getAutoCompletePersonasByDNI(String elem){
		return personaService.getPersonaByDNI(elem);
	}
	
	@ManagedProperty(value="#{TipoIntervencionService}")
	TipoIntervencionService tipoIntervencionService;

	public void setTipoIntervencionService(TipoIntervencionService tipoIntervencionService) {
		this.tipoIntervencionService = tipoIntervencionService;
	}
	
	public List<TipoIntervencion> getTiposIntervenciones(TipoIntervencion tpoInt, TipoCircuito tipoCircuito){
		return tipoIntervencionService.getAllActivesWithExtra(tpoInt,tipoCircuito); 
	}
	
	public List<TipoIntervencion> getTiposIntervenciones(){
		return tipoIntervencionService.getAll();
	}
	
	public List<TipoIntervencion> getAutoCompleteTipoIntervencion(String query){
		FacesContext context = FacesContext.getCurrentInstance();
	    TipoIntervencion o = (TipoIntervencion) UIComponent.getCurrentComponent(context).getAttributes().get("filter");
		TipoCircuito tipoCirc = TipoCircuito.fromString((String)UIComponent.getCurrentComponent(context).getAttributes().get("circ"));
		String soloCeseStr = 	(String) UIComponent.getCurrentComponent(context).getAttributes().get("soloCese");
		String soloIniciaStr = (String) UIComponent.getCurrentComponent(context).getAttributes().get("soloInicia");
		String soloIncPAEStr = (String) UIComponent.getCurrentComponent(context).getAttributes().get("soloIncPAE");
		Boolean soloInicia = soloIniciaStr==null?null:Boolean.valueOf(soloIniciaStr);
		Boolean soloCese = soloCeseStr==null?null:Boolean.valueOf(soloCeseStr);
		Boolean soloIncPAE = soloIncPAEStr==null?null:Boolean.valueOf(soloIncPAEStr);
		if (soloInicia!= null && soloInicia) soloCese=null;
	    if (o!=null)
			return tipoIntervencionService.getTipoIntervencionByFilterWithoutExtra(query, o, tipoCirc, soloCese, soloInicia,soloIncPAE);
		else
			return tipoIntervencionService.getTipoIntervencionByFilter(query, tipoCirc, soloCese, soloInicia, soloIncPAE);
	}

	public EstadoDesconocePoseeONo[] getValoresDesconocePoseeONo() {
	    return EstadoDesconocePoseeONo.values();
	  } 
	
	public EstadoDesconoceSIoNO[] getValoresEstadoDesconoceSIoNO() {
	    return EstadoDesconoceSIoNO.values();
	  } 
	
	public EstadoCausaJudicial[] getValoresEstadoCausaJudicial() {
	    return EstadoCausaJudicial.values();
	  } 
	
	public EstadoEscolaridad[] getValoresEstadoEscolaridad() {
	    return EstadoEscolaridad.values();
	  } 
	
	@ManagedProperty(value="#{ObraSocialService}")
	ObraSocialService obraSocialService;

	public void setObraSocialService(ObraSocialService obraSocialService) {
		this.obraSocialService = obraSocialService;
	}
	
	public List<ObraSocial> getObrasSociales(ObraSocial elem){
		return obraSocialService.getAllActivesWithExtra(elem); 
	}
	
	@ManagedProperty(value="#{DepartamentoJudicialService}")
	DepartamentoJudicialService departamentoJudicialService;

	public void setDepartamentoJudicialService(DepartamentoJudicialService departamentoJudicialService) {
		this.departamentoJudicialService = departamentoJudicialService;
	}
	
	public List<DepartamentoJudicial> getDepartamentosJudiciales(DepartamentoJudicial elem){
		return departamentoJudicialService.getAllActivesWithExtra(elem); 
	}
	
	@ManagedProperty(value="#{TipoOrganoJudicialService}")
	TipoOrganoJudicialService tipoOrganoJudicialService;

	public void setTipoOrganoJudicialService(TipoOrganoJudicialService tipoOrganoJudicialService) {
		this.tipoOrganoJudicialService = tipoOrganoJudicialService;
	}
	
	public List<TipoOrganoJudicial> getTipoOrganosJudiciales(TipoOrganoJudicial elem){
		return tipoOrganoJudicialService.getAllActivesWithExtra(elem); 
	}
	
	@ManagedProperty(value="#{TipoInstitucionService}")
	TipoInstitucionService tipoInstitucionService;

	public void setTipoInstitucionService(TipoInstitucionService tipoInstitucionService) {
		this.tipoInstitucionService = tipoInstitucionService;
	}
	
	public List<TipoInstitucion> getTiposInstituciones(TipoInstitucion tpoInt){
		return tipoInstitucionService.getAllActivesWithExtra(tpoInt); 
	}
	
	public List<TipoInstitucion> getTiposInstituciones(){
		return tipoInstitucionService.getAll();
	}

	@ManagedProperty(value="#{MotivoIntervencionService}")
	MotivoIntervencionService motivoIntervencionService;

	public void setMotivoIntervencionService(MotivoIntervencionService motivoIntervencionService) {
		this.motivoIntervencionService = motivoIntervencionService;
	}
	
	public List<MotivoIntervencion> getMotivosIntervenciones(MotivoIntervencion elem){
		return motivoIntervencionService.getAllActivesWithExtra(elem); 
	}
	
	public List<MotivoIntervencion> getMotivosIntervenciones(){
		return motivoIntervencionService.getAll();
	}
	
	List<MotivoIntervencion> motivosIntervencion = new ArrayList<MotivoIntervencion>();
	public List<MotivoIntervencion> getMotivosIntervencionesByCat(CategoriaMotivoIntervencion categoTpo){
		if (motivosIntervencion!=null && motivosIntervencion.size()>0)
			return motivosIntervencion;
		if (categoTpo == null)
			return this.motivoIntervencionService.getAll();
		return motivoIntervencionService.getAllByCatego(categoTpo);
	}
	
	public void getMotivosIntervencionesByCatVC(ValueChangeEvent ve){
		if (ve!=null && ve.getNewValue()!=null)
			motivosIntervencion = motivoIntervencionService.getAllByCatego((CategoriaMotivoIntervencion) ve.getNewValue());
	}


	//Tipo Convivientes
	public TipoConviviente[] getValoresTipoConviviente() {
	    return TipoConviviente.values();
	  } 
	
	public TipoCircuito[] getValoresTipoCircuito() {
	    return TipoCircuito.values();
	  } 
	
	public TipoCircuito[] getValoresTipoCircuitoFiltroRpt() {
		TipoCircuito arr[]= {TipoCircuito.MPI, TipoCircuito.MPE};
	    return arr;
	  } 
	
	
	@ManagedProperty(value="#{JuzgadoService}")
	JuzgadoService juzgadoService;

	public void setJuzgadoService(JuzgadoService juzgadoService) {
		this.juzgadoService = juzgadoService;
	}
	
	public List<Juzgado> getJuzgados(Juzgado tpoInt){
		return juzgadoService.getAllActivesWithExtra(tpoInt); 
	}
	
	public List<Juzgado> getJuzgados(){
		return juzgadoService.getAll();
	}
	
	public List<Juzgado> getJuzgadosIntervinientes(){
		return juzgadoService.getJuzgadosIntervinientes();
	}
	
	public List<Juzgado> getJuzgadosIntervinientesByCausa(CausaJudicial cuas){
		return juzgadoService.getJuzgadosIntervinientes();
	}
	
	public List<Juzgado> getJuzgadosDefensorias(){
		return juzgadoService.getJuzgadosDefensoria();
	}
	
	@ManagedProperty(value="#{OperadorPenalJuvenilService}")
	OperadorPenalJuvenilService operadorPenalJuvenilService;

	public void setOperadorPenalJuvenilService(OperadorPenalJuvenilService operadorPenalJuvenilService) {
		this.operadorPenalJuvenilService = operadorPenalJuvenilService;
	}
	
	public List<OperadorPenalJuvenil> getOperadoresPenalJuvenil(OperadorPenalJuvenil tpoInt){
		return operadorPenalJuvenilService.getAllActivesWithExtra(tpoInt); 
	}
	
	public List<OperadorPenalJuvenil> getOperadoresPenalJuvenilByTD(TipoDispositivo tpoD){
		return operadorPenalJuvenilService.getByTipoDispositivo(tpoD); 
	}
	
	public List<OperadorPenalJuvenil> getOperadoresPenalJuvenil(){
		return operadorPenalJuvenilService.getAll();
	}

	@ManagedProperty(value="#{InstitucionService}")
	InstitucionService institucionService;

	public void setInstitucionService(InstitucionService institucionService) {
		this.institucionService = institucionService;
	}
	
	public List<Institucion> getInstituciones(){
		return institucionService.getAll();
	}
	
	@ManagedProperty(value="#{FamiliaService}")
	FamiliaService familiaService;

	public void setFamiliaService(FamiliaService familiaService) {
		this.familiaService = familiaService;
	}
	
	public List<Familia> getFamilias(){
		return familiaService.getAll();
	}
	
	@ManagedProperty(value="#{ExperienciaAcompService}")
	ExperienciaAcompService experienciaAcompService;

	public void setExperienciaAcompService(ExperienciaAcompService experienciaAcompService) {
		this.experienciaAcompService = experienciaAcompService;
	}
	
	public List<ExperienciaAcomp> getExperienciasAcomp(){
		return experienciaAcompService.getAll();
	}
	
	@ManagedProperty(value="#{DimensionService}")
	DimensionService dimensionService;

	public void setDimensionService(DimensionService dimensionService) {
		this.dimensionService = dimensionService;
	}
	
	public List<Dimension> getDimensiones(){
		return dimensionService.getAll();
	}
	
	@ManagedProperty(value="#{AcompanianteService}")
	AcompanianteService acompanianteService;

	public void setAcompanianteService(AcompanianteService acompanianteService) {
		this.acompanianteService = acompanianteService;
	}
	
	public List<Acompaniante> getAutoCompleteAcompaniante(String query){
		FacesContext context = FacesContext.getCurrentInstance();
	    Acompaniante o = (Acompaniante) UIComponent.getCurrentComponent(context).getAttributes().get("filter");
		if (o!=null)
			return acompanianteService.getAcompaniantesByFilterWithoutExtra(query, o);
		else
			return acompanianteService.getAcompaniantesByFilter(query);
	}
	
	@ManagedProperty(value="#{localidadService}")
	LocalidadService localidadService;

	public void setLocalidadService(LocalidadService localidadService) {
		this.localidadService = localidadService;
	}
	
	public List<Localidad> getLocalidades(){
		return localidadService.getAll();
	}
	
	public List<Localidad> getLocalidades(Provincia prov, Municipio muni){
		return localidadService.getByFilter(prov, muni);
	}

	@ManagedProperty(value="#{municipioService}")
	MunicipioService municipioService;

	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	
	public List<Municipio> getMunicipios(){
		return municipioService.getAll();
	}
	
	public List<Municipio> getMunicipios(Provincia prov){
		return municipioService.getByFilter(prov);
	}
	
	@ManagedProperty(value="#{nacionalidadService}")
	NacionalidadService nacionalidadService;
	private List<Nacionalidad> nacionalidades;

	public void setNacionalidadService(NacionalidadService nacionalidadService) {
		this.nacionalidadService = nacionalidadService;
	}
	
	public List<Nacionalidad> getNacionalidades(){
		if (this.nacionalidades==null)
			this.nacionalidades=nacionalidadService.getAll();
		return this.nacionalidades;
	}
	
	@ManagedProperty(value="#{MotivoPasoCentroSaludService}")
	MotivoPasoCentroSaludService motivoPasoCentroSaludService;
	private List<MotivoPasoCentroSalud> motivosPasoCentroSalud;

	public void setMotivoPasoCentroSaludService(MotivoPasoCentroSaludService motivoPasoCentroSaludService) {
		this.motivoPasoCentroSaludService = motivoPasoCentroSaludService;
	}
	
	public List<MotivoPasoCentroSalud> getMotivosPasoCentroSalud(){
		if (this.motivosPasoCentroSalud==null)
			this.motivosPasoCentroSalud = motivoPasoCentroSaludService.getAll();
		return this.motivosPasoCentroSalud;
	}	
	
	@ManagedProperty(value="#{MotivoAprehensionService}")
	MotivoAprehensionService motivoAprehensionService;
	private List<MotivoAprehension> motivosAprehension;

	public void setMotivoAprehensionService(MotivoAprehensionService motivoAprehensionService) {
		this.motivoAprehensionService = motivoAprehensionService;
	}
	
	public List<MotivoAprehension> getMotivosAprehension(){
		if (this.motivosAprehension==null)
			this.motivosAprehension = motivoAprehensionService.getAll();
		return this.motivosAprehension;
	}	

	@ManagedProperty(value="#{TipoDelitoService}")
	TipoDelitoService tipoDelitoService;
	private List<TipoDelito> tiposDelito;

	public void setTipoDelitoService(TipoDelitoService tipoDelitoService) {
		this.tipoDelitoService = tipoDelitoService;
	}
	
	public List<TipoDelito> getTiposDelito(){
		if (this.tiposDelito == null)
			this.tiposDelito = tipoDelitoService.getAll();
		return this.tiposDelito;
	}	
	
	@ManagedProperty(value="#{ProcedenciaService}")
	ProcedenciaService procedenciaService;
	private List<Procedencia> procedencias;

	public void setProcedenciaService(ProcedenciaService procedenciaService) {
		this.procedenciaService = procedenciaService;
	}
	
	public List<Procedencia> getProcedencias(){
		if (this.procedencias == null)
			this.procedencias = procedenciaService.getAll();
		return this.procedencias;
	}
	
	@ManagedProperty(value="#{FuerzaSeguridadService}")
	FuerzaSeguridadService fuerzaSeguridadService;
	private List<FuerzaSeguridad> fuerzasSeguridad;

	public void setFuerzaSeguridadService(FuerzaSeguridadService fuerzaSeguridadService) {
		this.fuerzaSeguridadService = fuerzaSeguridadService;
	}
	
	public List<FuerzaSeguridad> getFuerzasSeguridad(){
		if (this.fuerzasSeguridad == null)
			this.fuerzasSeguridad = fuerzaSeguridadService.getAll();
		return this.fuerzasSeguridad;
	}	
	
	@ManagedProperty(value="#{ComisariaService}")
	ComisariaService comisariaService;
	private List<Comisaria> comisarias;

	public void setComisariaService(ComisariaService comisariaService) {
		this.comisariaService = comisariaService;
	}
	
	public List<Comisaria> getComisarias(){
		if (this.comisarias == null)
			this.comisarias = comisariaService.getAll();
		return this.comisarias;
	}	

	@ManagedProperty(value="#{DestinoEgresoDispositivoService}")
	DestinoEgresoDispositivoService destinoEgresoDispositivoService;
	private List<DestinoEgresoDispositivo> destinosEgreso;

	public void setDestinoEgresoDispositivoService(DestinoEgresoDispositivoService destinoEgresoDispositivoService) {
		this.destinoEgresoDispositivoService = destinoEgresoDispositivoService;
	}
	
	public List<DestinoEgresoDispositivo> getDestinosEgreso(){
		if (this.destinosEgreso == null)
			this.destinosEgreso = destinoEgresoDispositivoService.getAll();
		return this.destinosEgreso;
	}	

	@ManagedProperty(value="#{TipoCausaJudicialService}")
	TipoCausaJudicialService tipoCausaJudicialService;
	private List<TipoCausaJudicial> tipoCausasJudiciales;

	public void setTipoCausaJudicialService(TipoCausaJudicialService service) {
		this.tipoCausaJudicialService = service;
	}
	
	public List<TipoCausaJudicial> getTipoCausasJudiciales(){
		if (this.tipoCausasJudiciales == null)
			this.tipoCausasJudiciales = tipoCausaJudicialService.getAll();
		return this.tipoCausasJudiciales;
	}	
	
	@ManagedProperty(value="#{GpoBienJuridicoProtegidoService}")
	GpoBienJuridicoProtegidoService gpoBienJuridicoProtegidoService;
	private List<GpoBienJuridicoProtegido> gposBienJuridicoProtegido;

	public void setGpoBienJuridicoProtegidoService(GpoBienJuridicoProtegidoService service) {
		this.gpoBienJuridicoProtegidoService = service;
	}
	
	public List<GpoBienJuridicoProtegido> getGposBienJuridicoProtegido(){
		if (this.gposBienJuridicoProtegido == null)
			this.gposBienJuridicoProtegido = gpoBienJuridicoProtegidoService.getAll();
		return this.gposBienJuridicoProtegido;
	}	
}
