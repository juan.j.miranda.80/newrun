package ar.gob.run.spring.service;

import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.IntervencionPAEDAO;
import ar.gob.run.spring.dao.LegajoDAO;
import ar.gob.run.spring.model.IntervencionPAE;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
		  
@Service("IntervencionPAEService")
@Transactional(readOnly = true)
public class IntervencionPAEService extends GenericService<IntervencionPAE>{

	
    LegajoDAO legajoDAO;
	
	@Autowired
	public void setLegajoDAO(LegajoDAO legajoDAO){
		this.legajoDAO = legajoDAO;
	}

    public LegajoDAO getLegajoDAO() {
        return this.legajoDAO;
    }
	
	
	@Autowired
	public void setIntervencionPAEDAO(IntervencionPAEDAO intervencionDAO){
		this.setDao(intervencionDAO);
	}

    public IntervencionPAEDAO getIntervencionPAEDAO() {
        return (IntervencionPAEDAO) this.getDao();
    }

    public List<IntervencionPAE> getList(Integer legajo){
    	return getIntervencionPAEDAO().getList(legajo);
    }
    
    @Override
    public List<IntervencionPAE> getAllActives(Class type) {
    	return getIntervencionPAEDAO().getList();
    }
    
    public List<IntervencionPAE> getIntervencionesByZonal(Zonal zonal){
    	return getIntervencionPAEDAO().getListByZonal(zonal);
    }
    
	public List<IntervencionPAE> getPendientesSupervisar() {
		return getIntervencionPAEDAO().getPendientesSupervisar();
	}

	@Override
	@Transactional
	public void save(IntervencionPAE dom) {
		getLegajoDAO().update(dom.getLegajo());
		super.save(dom);
	}

	public IntervencionPAE get(Integer id) {
		return super.get(id, IntervencionPAE.class);
	}
}
