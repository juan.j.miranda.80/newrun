package ar.gob.run.spring.model;

public interface Identificable {
	
	public void setId(Integer id);

	public Integer getId();

}
