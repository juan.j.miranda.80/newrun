package ar.gob.run.spring.model;

import java.io.Serializable;
import java.sql.Clob;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.usuario.Usuario;

@Entity
@Table(name="T_INTERV_PROT_INTEGRAL")
public class IntervencionIntegral extends ObjetoABM implements Serializable, Intervencion{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Zonal zonal;
	private Local local;
	private Date fecha;
	private Derivador derivador;
	private TipoIntervencion tipoIntervencion = new TipoIntervencion();
	private MotivoIntervencion motivoIntervencion = new MotivoIntervencion();
	private String informe;
	private Legajo legajo;
	private String archivoAdjunto;
	
	@Override
	public void setId(Integer id) {
		this.id=id;
	}

    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_INTERV_PROT_INTEGRAL")
	@Override
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LOCAL")
	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}
	
	@Column(name="FECHA")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_DERIVADOR")
	public Derivador getDerivador() {
		return derivador;
	}

	public void setDerivador(Derivador derivador) {
		this.derivador = derivador;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_INTERVENCION")
	public TipoIntervencion getTipoIntervencion() {
		return tipoIntervencion;
	}

	public void setTipoIntervencion(TipoIntervencion tipoIntervencion) {
		this.tipoIntervencion = tipoIntervencion;
	}

	@Lob
	@Column(columnDefinition="text", name="INFORME")
	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LEGAJO")
	public Legajo getLegajo() {
		return legajo;
	}

	public void setLegajo(Legajo legajo) {
		this.legajo = legajo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MOTIVO_INTERVENCION")
	public MotivoIntervencion getMotivoIntervencion() {
		return motivoIntervencion;
	}

	public void setMotivoIntervencion(MotivoIntervencion motivoIntervencion) {
		this.motivoIntervencion = motivoIntervencion;
	}

	@Override
	public int compareTo(Intervencion o) {
		if ( this.fecha!= null && o.getFecha()!= null && this.fecha.compareTo(o.getFecha()) != 0)  this.fecha.compareTo(o.getFecha());
		if (this.tipoIntervencion!=null && o.getTipoIntervencion()!=null && this.tipoIntervencion.compareTo(o.getTipoIntervencion()) != 0) return this.tipoIntervencion.compareTo(o.getTipoIntervencion()) ;
		if (this.legajo!=null && o.getLegajo()!=null && this.legajo.getCodigo()!=null && o.getLegajo().getCodigo()!=null &&
				this.legajo.getCodigo().compareTo(o.getLegajo().getCodigo())!=0)
			return this.legajo.getCodigo().compareTo(o.getLegajo().getCodigo());
		return this.getId().compareTo(o.getId());
	}

	@Column(name="ARCHIVO_ADJUNTO")
	public String getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(String archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}

	
	public IntervencionIntegral(){
		super();
	}
	
	public IntervencionIntegral(Integer id, Date fecha, Integer idLegajo,
			String codigo,
			String nombre, String tpoInterv,
			String archivo, String usrAlta){
		super();
		this.fecha = fecha;
		this.id = id;
		this.legajo = new Legajo();
		this.legajo.setCodigo(codigo);
		this.legajo.setApellidoYNombre(nombre);
		this.setTipoIntervencion(new TipoIntervencion());
		this.getTipoIntervencion().setDescripcion(tpoInterv);
		this.archivoAdjunto = archivo;
		this.getDatosAuditoria().setUsuarioAlta(new Usuario());
		this.getDatosAuditoria().getUsuarioAlta().setUserName(usrAlta);
		this.legajo.setId(idLegajo);
		
	}

}
