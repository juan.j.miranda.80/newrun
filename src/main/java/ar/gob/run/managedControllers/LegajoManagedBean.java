package ar.gob.run.managedControllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

import ar.gob.run.spring.model.EstadoDesconocePoseeONo;
import ar.gob.run.spring.model.EstadoDesconoceSIoNO;
import ar.gob.run.spring.model.IntervencionExcepcional;
import ar.gob.run.spring.model.IntervencionIntegral;
import ar.gob.run.spring.model.IntervencionMEP;
import ar.gob.run.spring.model.IntervencionPAE;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Localidad;
import ar.gob.run.spring.model.Municipio;
import ar.gob.run.spring.model.NivelEducacion;
import ar.gob.run.spring.model.Persona;
import ar.gob.run.spring.model.Provincia;
import ar.gob.run.spring.model.Zonal;
import ar.gob.run.spring.model.legajo.EstadoDocumento;
import ar.gob.run.spring.model.legajo.EstadoEscolaridad;
import ar.gob.run.spring.model.legajo.RelacionVincular;
import ar.gob.run.spring.model.legajo.TipoProblematicaSalud;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.security.MySecUser;
import ar.gob.run.spring.service.CommonService;
import ar.gob.run.spring.service.LegajoService;
import ar.gob.run.spring.service.LocalService;
import ar.gob.run.spring.service.LocalidadService;
import ar.gob.run.spring.service.MunicipioService;
import ar.gob.run.spring.service.ProvinciaService;
import ar.gob.run.spring.service.ZonalService;

@ManagedBean(name="legajoMB")
@SessionScoped
public class LegajoManagedBean extends GenericManagedBean<Legajo> {

	private static final long serialVersionUID = 1L;

	private static final String NUEVO_LEGAJO = "nuevoLegajo";
	private static final String EDITOR_LEGAJO = "editorLegajo";
	
	@ManagedProperty(value="#{legajoService}")
    LegajoService legajoService;
	
	RelacionVincular relacionVincular;
	Persona familiarSeleccionado;
	Persona familiarDNISeleccionado;
	
	List<Legajo> listByParam;
	
    @ManagedProperty(value="#{commonService}")
	CommonService commonService;
    
    //logica zonal-local dependiente
	@ManagedProperty(value="#{zonalService}")
	ZonalService zonalService;

	@ManagedProperty(value="#{localService}")
	LocalService localService;
	List<Local> locales;
	
    //	logica provincia-muni-localidad dependiente
  	@ManagedProperty(value="#{provinciaService}")
  	ProvinciaService provinciaService;

  	@ManagedProperty(value="#{municipioService}")
  	MunicipioService municipioService;
  	
  	@ManagedProperty(value="#{localidadService}")
  	LocalidadService localidadService;
  	
  	List<Provincia> provincias;
  	List<Municipio> municipios;
  	List<Localidad> localidades;
    
	Zonal zonalParam;
	String idTipoMPE;
	Boolean porVencer;
	Boolean masDe180;

	Boolean valPuedeEditar = null;

    public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public LegajoService getLegajoService() {
		return legajoService;
	}

	public void setLegajoService(LegajoService service) {
		this.setService(service);
		this.legajoService = service;
	}

	@Override
	public Legajo getInstance() {		
		return super.instance;
	}

	public LegajoManagedBean() {
		super(new Legajo());
	}

	@Override
	public String save() {
		if (getInstance().getFechaNacimiento()!=null &&
				getInstance().getFechaNacimiento().after(new Date())){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					"La fecha de nacimiento no puede ser posterior al día de la fecha", "");
			FacesContext.getCurrentInstance().addMessage(null, message);
			return "";
		}
		if (getInstance().getCodigo() ==null || getInstance().getCodigo().equals("")){
			getInstance().setCodigo(getLegajoService().getNuevoCodigo(getInstance()));
			if (!StringUtils.isEmpty(getInstance().getNroDocumento())){
				getInstance().setEstadoDoc(EstadoDocumento.POSEE_DOCUMENTO);
				if (getLegajoService().existeLegajo(getInstance().getNroDocumento(), getInstance().getCodigo(),getInstance().getId())){
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
							"El numero de documento que intenta ingresar ya existe", "");
					FacesContext.getCurrentInstance().addMessage(null, message);
					return "";
				}
			}
		}else{
			String strMessage="";
			if (StringUtils.isEmpty(getInstance().getTelefono())){
				strMessage = "Telefono del contacto esta vacio";
			}
			if (StringUtils.isEmpty(getInstance().getDireccion()))
				strMessage += (StringUtils.isEmpty(strMessage)?"":",")+ "Dirección del contacto esta vacio";
			if (!StringUtils.isEmpty(getInstance().getNroDocumento()) && getLegajoService().existeLegajo(getInstance().getNroDocumento(), getInstance().getCodigo(),getInstance().getId())){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
						"El numero de documento que intenta ingresar ya existe", "");
				FacesContext.getCurrentInstance().addMessage(null, message);
				return "";
			}
			if (!strMessage.equals("")){
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, strMessage, "");
				FacesContext.getCurrentInstance().addMessage(null, message);
			}			
		}
		return super.save();
	}
	
	@Override
	public String edit(Legajo ent){
		Integer id = null;
		if (ent!=null)
			id = ent.getId();
		else
			id = this.instance.getId();
		
		if (id==null)
			return "/";
		
		this.instance = this.getLegajoService().get(id);
    	//ent = null es un workaround para volver de agregado de interv
		//y que se refresque el listado de interv en la pantalla de leg
		return "/legajo/editorLegajo.xhtml";
    }
    
	@Override
    public String reset() {
    	this.instance = getNewInstance();
    	MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (usr.getUsuario().hasRole(Rol.LOCAL)){
    		if (usr.getUsuario().getZonal()!=null)
    			this.instance.setZonal(usr.getUsuario().getZonal());
    		if (usr.getUsuario().getLocal()!=null)
    			this.instance.setLocal(usr.getUsuario().getLocal());
    	}
        return NUEVO_LEGAJO;
    }
	
	public void actualizarDatosDoc(){
		EstadoDocumento newValue = getInstance().getEstadoDoc();
		if (newValue.equals(EstadoDocumento.POSEE_DOCUMENTO)){
			getInstance().setTipoDocumento("DNI");
		}else{
			getInstance().setTipoDocumento("");
			if (!newValue.equals(EstadoDocumento.POSEE_DOC_EXTRANJERO))
				getInstance().setNroDocumento("");
		}
		
		if (!newValue.equals(EstadoDocumento.NO_POSEE_DOCUMENTO)){
			getInstance().setConstanciaDeParto(false);
			getInstance().setPartidaNacimiento(false);
			getInstance().setSeIntervEnSuGestion(false);
		}
	}


	@Override
	public void setName(String name) {
		this.setName("Legajo");
		
	}

    public String vinculoSeleccionado(ActionEvent event) {
        return null;
    }

    public String getFullName(){
    	if (instance!=null)
    		return instance.getCodigo() + " - " + instance.getApellidoYNombre();
    	return "";
    }
   
	
	public void setZonalService(ZonalService zonalService) {
		this.zonalService = zonalService;
	}
	
	public List<Zonal> getZonales(){
		return zonalService.getZonales(this.instance.getZonal());
	}
	
    public void onZonalChange() {
        if(this.instance.getZonal() !=null && this.instance.getZonal().getId()!=null)
        	locales = localService.getLocales(this.instance.getZonal(),this.instance.getLocal());
        else
        	locales = new ArrayList<Local>();
    }
	
	public void setLocalService(LocalService localService) {
		this.localService = localService;
	}
	
	public List<Local> getLocales(){
		if (locales == null) locales = new ArrayList<Local>();
		return locales;
	}
	
	public void actualizarObraSocial(){
		EstadoDesconocePoseeONo newValue = getInstance().getEstadoObraSocial();
		if (!newValue.equals(EstadoDesconocePoseeONo.POSEE)){
			getInstance().setObraSocial(null);
		}
		
	}
	
	public void actualizarProblSalud(){
		EstadoDesconoceSIoNO newValue = getInstance().getEstadoProblSalud();
		if (!newValue.equals(EstadoDesconoceSIoNO.SI)){
			getInstance().setDetalleProblSalud("");
			getInstance().setProblematicaSalud(TipoProblematicaSalud.SIN_VALOR);
		}
		
	}

	public void actualizarEscolarizado(){
		EstadoEscolaridad newValue = getInstance().getEscolarizado();
		if (!newValue.equals(EstadoEscolaridad.ES)){
			getInstance().setNivelEducacion(NivelEducacion.SIN_VALOR);
			getInstance().setEstablecimientoEscolarizado("");
		}
		
	}
	
	private Boolean editandoRelacionVincular = false;
	public RelacionVincular getRelacionVincular() {
		if (relacionVincular==null)
			this.relacionVincular= new RelacionVincular();
		return relacionVincular;
	}

	public void setRelacionVincular(RelacionVincular relacionVincular) {
		this.relacionVincular = relacionVincular;
	}
	
	public Persona getFamiliarSeleccionado() {
		if (familiarSeleccionado == null)
			familiarSeleccionado = new Persona();
		return familiarSeleccionado;
	}

	public void setFamiliarSeleccionado(Persona familiarSeleccionado) {
		this.familiarSeleccionado = familiarSeleccionado;
	}
	
	public Persona getFamiliarDNISeleccionado() {
		return familiarDNISeleccionado;
	}

	public void setFamiliarDNISeleccionado(Persona familiarDNISeleccionado) {
		this.familiarDNISeleccionado = familiarDNISeleccionado;
	}

	public void deleteRelacionVincular(RelacionVincular ent){
		this.instance.getRelacionesVinculares().remove(ent);
	}
	
	public void deleteMPE(IntervencionExcepcional ent){
		this.instance.getIntervencionesMPE().remove(ent);
	}
	
	public void deleteMPI(IntervencionIntegral ent){
		this.instance.getIntervencionesMPI().remove(ent);
	}

	public void deleteMEP(IntervencionMEP ent){
		this.instance.getIntervencionesMEP().remove(ent);
	}
	
	public void deletePAE(IntervencionPAE ent){
		this.instance.getIntervencionesPAE().remove(ent);
	}
	
	public void relacionVincularSeleccionada(){
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Relacion Vincular Seleccionada", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
        if (!this.relacionVincular.getPoseeLegajo()){
	        if (this.familiarSeleccionado!=null && this.familiarSeleccionado.getId()!=null){
	        	this.relacionVincular.setFamiliar(this.familiarSeleccionado);
	        }else if (this.familiarDNISeleccionado!=null && this.familiarDNISeleccionado.getId()!=null){
	        	this.relacionVincular.setFamiliar(this.familiarDNISeleccionado);
	        } else {
	        	this.relacionVincular.getFamiliar().setApellidoYNombre(this.familiarSeleccionado.getApellidoYNombre());
	        	this.relacionVincular.getFamiliar().setDocumento(this.familiarDNISeleccionado.getDocumento());
	        }
        }else {
        	this.relacionVincular.setFamiliar(null);
        }
		if (!this.instance.getRelacionesVinculares().contains(this.relacionVincular))
			this.instance.getRelacionesVinculares().add(this.relacionVincular);
		this.nuevaRelacionVincular();
	}
	
	public void actualizarDatosRelacionVincular(){
		if (this.relacionVincular.getPoseeLegajo()){
			this.relacionVincular.getFamiliar().setApellidoYNombre(null);
			this.relacionVincular.getFamiliar().setDireccion(null);
			this.relacionVincular.getFamiliar().setDocumento(null);
			this.relacionVincular.getFamiliar().setEstadoCivil(null);
			this.relacionVincular.getFamiliar().setFechaNacimiento(null);
			this.relacionVincular.getFamiliar().setEstudioCursado(null);
			this.relacionVincular.setRepresentanteLegal(null);
			this.relacionVincular.getFamiliar().setSexo(null);
			this.relacionVincular.getFamiliar().setSituacionLaboral(null);
			this.relacionVincular.getFamiliar().setTelefono(null);
		}else{
			this.relacionVincular.setLegajoFamiliar(null);
		}
	}
	
	public void editRelacionVincular(RelacionVincular rel){
		if (rel == null) {
			this.nuevaRelacionVincular();
			rel = this.relacionVincular;
		} else
			this.relacionVincular = rel;
		
		if (rel.getFamiliar()!=null && rel.getFamiliar().getId()!=null)
			this.familiarSeleccionado = rel.getFamiliar();
		if (rel.getFamiliar()!=null && rel.getFamiliar().getId()!=null)
			this.familiarDNISeleccionado = rel.getFamiliar();		
	}
	
	public void nuevaRelacionVincular(){
		this.relacionVincular= new RelacionVincular();
		this.familiarSeleccionado = new Persona();
		this.familiarDNISeleccionado = new Persona();
	}

	public void setParamsBusqueda(ActionEvent event){
		if (event.getComponent().getAttributes().get("zonal")!=null)
			zonalParam = (Zonal)event.getComponent().getAttributes().get("zonal");
		if (event.getComponent().getAttributes().get("tipoMPE")!=null)
			idTipoMPE = String.valueOf(event.getComponent().getAttributes().get("tipoMPE"));
		if (event.getComponent().getAttributes().get("porVencer")!=null)
			porVencer = Boolean.valueOf(event.getComponent().getAttributes().get("porVencer").toString());
		if (event.getComponent().getAttributes().get("masDe180")!=null)
			masDe180 = Boolean.valueOf(event.getComponent().getAttributes().get("masDe180").toString());
//		if (event.getComponent().getAttributes().get("listByParam")!=null)
			
		Object listByParam = event.getComponent().getAttributes().get("listByParam");
		setListByParam(listByParam!=null?
				(List<Legajo>) listByParam:null);
	}
	
	private void clearParams(){
		zonalParam = null;
		idTipoMPE = null;
		porVencer = null;
	}
	
	private List<Legajo> getLegajosAlerta(){
		List<Legajo> result = null;
		if (masDe180!=null && masDe180){
			result = getLegajoService().getLegajosMayor180Dias(zonalParam);
		}else if (porVencer!=null && porVencer)
			result = getLegajoService().getLegajosPorVencer(idTipoMPE, zonalParam);
		else
			result = getLegajoService().getLegajosVencidos(idTipoMPE, zonalParam);
//		clearParams();
		return result;
	}
	
	public String getLegajosFromMenu(){
		clearParams();
		return "/legajo/listado.xhtml";
	}
	
	
	@Override
	public List<Legajo> getList() {
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (idTipoMPE!=null)
			return getLegajosAlerta();
		
		if (getListByParam()!=null)
			return getListByParam();
//		if (usr.getUsuario().hasRole(Rol.ZONAL))
//			return getLegajoService().getLegajosByZonal(usr.getUsuario().getZonal());
		if (usr.getUsuario().hasRole(Rol.LOCAL))
			return getLegajoService().getLegajosByLocal(usr.getUsuario().getLocal());
		
		if (usr.getUsuario().hasRole(Rol.DICTAMEN)|| usr.getUsuario().hasRole(Rol.ADMIN)){
			String verPendientesAprobados = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("verPendientesAprobados");
			if (verPendientesAprobados!=null){
				return legajoService.getPendientesAprobados();
			}
		}
		
		return getLegajoService().getLegajos();
	}
	
	public String puedeEditar(){
		return this.puedeEditar(this.instance);
	}
	
	public String puedeBorrar(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN)) return "true";
		return "false";
	}
	
	public String puedeEditar(Legajo legajo){
		if (legajo==null) return "false";
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				usr.getUsuario().hasRole(Rol.ZONAL) || 
					/** PEDIDO DE CAMBIO EN ERS ETAPA 3
					 * && legajo!=null && legajo.getZonal()!=null 
					&& legajo.getZonal().equals(usr.getUsuario().getZonal())) ||**/
				(usr.getUsuario().hasRole(Rol.LOCAL) && legajo!=null && legajo.getLocal()!=null 
					&& legajo.getLocal().equals(usr.getUsuario().getLocal())) ||
				usr.getUsuario().hasRole(Rol.SUPERVISOR) ||
				usr.getUsuario().hasRole(Rol.DICTAMEN)){
				return "true";
		}else{ 
			return "false";
		}
				
	}
	
	public String puedeAgregarLegajos(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				(usr.getUsuario().hasRole(Rol.ZONAL) ||
						usr.getUsuario().hasRole(Rol.SUPERVISOR) ||
						usr.getUsuario().hasRole(Rol.LOCAL)))
			return "true";
		return "false";
	}
	
	public String puedeAgregarMPE(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				usr.getUsuario().hasRole(Rol.ZONAL) ||
				usr.getUsuario().hasRole(Rol.SUPERVISOR) ||
				usr.getUsuario().hasRole(Rol.DICTAMEN)) //PEDIDO DE CAMBIO ERS3 && this.instance!=null && this.instance.getZonal().equals(usr.getUsuario().getZonal())))
			return "true";
		else
			return "false";
	}
	
	public String puedeAgregarPAE(){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				usr.getUsuario().hasRole(Rol.ZONAL)){ //PEDIDO DE CAMBIO ERS3 && this.instance!=null && this.instance.getZonal().equals(usr.getUsuario().getZonal())))
			List<IntervencionExcepcional> mpes = getInstance().getIntervencionesMPE();
	    	if (mpes!=null && !mpes.isEmpty()){
	    		Collections.sort(mpes, Collections.reverseOrder());
	    		IntervencionExcepcional mpe = mpes.iterator().next();
	    		if (mpe.getTipoIntervencion().getIntervencionCese()!=null &&
	    				mpe.getTipoIntervencion().getIntervencionCese())
	    			return "true";
	    	}    	
		}
		return "false";
	}
	
	public String puedeEditarMP(String userName){
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				(usr.getUsuario().getUserName().equals(userName)) ||
				(usr.getUsuario().hasRole(Rol.ZONAL) && 
						this.instance!=null && this.instance.getZonal().equals(usr.getUsuario().getZonal())))
			return "true";
		else
			return "false";
	}
	
	public String puedeBorrarMP(){ 
		MySecUser usr = (MySecUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (usr.getUsuario().hasRole(Rol.ADMIN) ||
				(usr.getUsuario().hasRole(Rol.ZONAL) && 
						this.instance!=null && this.instance.getZonal().equals(usr.getUsuario().getZonal())))
			return "true";
		else
			return "false";
	}

	public List<Legajo> getListByParam() {
		return listByParam;
	}

	public void setListByParam(List<Legajo> listByParam) {
		this.listByParam = listByParam;
	}
	
	public void handleFileDNI(FileUploadEvent event) {
		this.getInstance().setFileDNI(this.handleFile(event, "LEG", "DNI"));		
    }
	
	public StreamedContent downloadFile(String name) {
		StreamedContent fileDownload = null;
		String path = getCommonService().getFileUploaderPath() + "/LEG/" + name;
		InputStream stream;
		try {
			stream = FileUtils.openInputStream(new File(path));
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			fileDownload = new DefaultStreamedContent(stream, contentType, name);
		} catch (IOException e) {
		}
		return fileDownload;
	}
	
	public void handleFileProblematicaSalud(FileUploadEvent event) {
    	this.getInstance().setFileProblematicaSalud(this.handleFile(event, "PS", "Documentacion problematica de salud"));
    }
	
	private String handleFile(FileUploadEvent event, String folder, String label) {
		String returnFileName = "";
    	try {
            byte[] bytes=null;
     
                if (null!=event.getFile()) {
                    bytes = event.getFile().getContents();
                    String filename = FilenameUtils.getName(event.getFile().getFileName() );
        			File file = new File(getCommonService().getFileUploaderPath() + "/"+folder+"/" + filename);
        			file.getParentFile().mkdirs();	
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
                    stream.write(bytes);
                    stream.close();
                    returnFileName = filename;
                    FacesMessage message = new FacesMessage(label + " " + event.getFile().getFileName() + " fue subido con éxito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    return returnFileName;
                }
    		}catch(IOException e){
    			FacesMessage message = new FacesMessage("Error al subir " + label , event.getFile().getFileName());
                FacesContext.getCurrentInstance().addMessage(null, message);
    		}
    	FacesMessage message = new FacesMessage("Error al intentar subir " + label, event.getFile().getFileName());
        FacesContext.getCurrentInstance().addMessage(null, message);
        return returnFileName;
    }
	
	
	public void handleFilePartidaNacimiento(FileUploadEvent event) {
		this.getInstance().setFilePartidaNacimiento(this.handleFile(event, "LEG", "Partida de nacimiento"));		
    }

	public List<Provincia> getProvincias() {
		if (provincias == null) provincias = getProvinciaService().getAll();
		return provincias;
	}

	public void setProvincias(List<Provincia> provincias) {
		this.provincias = provincias;
	}

	public List<Municipio> getMunicipios() {
		if (municipios==null) municipios = new ArrayList<Municipio>();
		return municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}

	public List<Localidad> getLocalidades() {
		if (localidades==null) localidades = new ArrayList<Localidad>();
		return localidades;
	}

	public void setLocalidades(List<Localidad> localidades) {
		this.localidades = localidades;
	}

	public ProvinciaService getProvinciaService() {
		return provinciaService;
	}

	public MunicipioService getMunicipioService() {
		return municipioService;
	}

	public LocalidadService getLocalidadService() {
		return localidadService;
	}
	
	public void onProvinciaChange(){
		municipios = getMunicipioService().getByFilter(getInstance().getProvincia());
		localidades = getLocalidadService().getByFilter(getInstance().getProvincia(), getInstance().getMunicipio());
	}
	
	public void onMunicipioChange(){
		localidades = getLocalidadService().getByFilter(getInstance().getProvincia(), getInstance().getMunicipio());		
	}

	public void setProvinciaService(ProvinciaService provinciaService) {
		this.provinciaService = provinciaService;
	}

	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}

	public void setLocalidadService(LocalidadService localidadService) {
		this.localidadService = localidadService;
	}

	public void onSelectFamiliar(SelectEvent event){
		if (event.getObject()!=null){
			Persona fliar = (Persona)event.getObject();
			if (fliar.getId()!=null){
				this.getRelacionVincular().setFamiliar(fliar);
				this.familiarDNISeleccionado = fliar;
			}
		}
	}
	
	public void onSelectFamiliarDNI(SelectEvent event){
		if (event.getObject()!=null){
			Persona fliar = (Persona)event.getObject();
			if (fliar.getId()!=null){
				this.getRelacionVincular().setFamiliar(fliar);
				this.familiarSeleccionado = fliar;
			}
		}
	}

}
