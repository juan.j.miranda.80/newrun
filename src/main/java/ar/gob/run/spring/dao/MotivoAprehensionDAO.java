package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.MotivoAprehension;

@Repository
public class MotivoAprehensionDAO extends HibernateDAO<MotivoAprehension,Integer> {
}
