package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.legajo.TipoOrganoJudicial;
import ar.gob.run.spring.service.TipoOrganoJudicialService;

@FacesConverter(value = "tipoOrganoJudicialConverter")
public class TipoOrganoJudicialConverter implements Converter {

	public TipoOrganoJudicialConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), TipoOrganoJudicial.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((TipoOrganoJudicial) value).getId().toString();
	}

	public TipoOrganoJudicialService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{TipoOrganoJudicialService}", TipoOrganoJudicialService.class);

			TipoOrganoJudicialService service = (TipoOrganoJudicialService)vex.getValue(context.getELContext());
			return service;
	}

}
