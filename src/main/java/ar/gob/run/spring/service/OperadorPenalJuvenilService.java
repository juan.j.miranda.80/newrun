package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.OperadorPenalJuvenilDAO;
import ar.gob.run.spring.model.OperadorPenalJuvenil;
import ar.gob.run.spring.model.TipoDispositivo;
import ar.gob.run.spring.model.institucion.TipoInstitucion;
		  
@Service("OperadorPenalJuvenilService")
@Transactional(readOnly = true)
public class OperadorPenalJuvenilService extends GenericService<OperadorPenalJuvenil>{

	
	@Autowired
	public void setOperadorPenalJuvenilDAO(OperadorPenalJuvenilDAO operadorPenalJuvenilDAO){
		this.setDao(operadorPenalJuvenilDAO);
	}

    public OperadorPenalJuvenilDAO getOperadorPenalJuvenilDAO() {
        return (OperadorPenalJuvenilDAO) this.getDao();
    }

	public List<OperadorPenalJuvenil> getAllActivesWithExtra(OperadorPenalJuvenil elem) {
		return super.getAllActivesWithExtra(OperadorPenalJuvenil.class, elem);
	}

	public List<OperadorPenalJuvenil> getAll() {
		return super.getAllActives(OperadorPenalJuvenil.class);
	}

	public List<OperadorPenalJuvenil> getByTipoDispositivo(TipoDispositivo tpoD) {
		if (tpoD==null)
			return this.getAll();
		return getOperadorPenalJuvenilDAO().getByTipoDispositivo(tpoD);
	}
    
}
