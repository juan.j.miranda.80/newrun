package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import ar.gob.run.spring.model.legajo.ReferenteAfectivo;
import ar.gob.run.spring.model.legajo.VinculoFamiliar;

@ManagedBean(name="referenteAfectivoMB")
@SessionScoped
public class ReferenteAfectivoManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String LISTADO = "listado";
    private static final String EDITOR = "editor";

	@ManagedProperty(value="#{legajoMB}")
	LegajoManagedBean legajoMB;

    List<ReferenteAfectivo> list;

    ReferenteAfectivo instance;
    
	public String edit(ReferenteAfectivo ent){
    	this.instance = ent;
    	return EDITOR;
    }
    
//    public void delete(ReferenteAfectivo ent){
//    	getList().remove(ent);
//    }
    
    public String reset() {
    	this.instance = new ReferenteAfectivo();
        return EDITOR;
    }
    
    public String cancel(){
    	this.instance = new ReferenteAfectivo();
    	return LISTADO;
    }

	public ReferenteAfectivo getInstance() {
		if (instance==null)
			instance = new ReferenteAfectivo();
		return instance;
	}

	public void setInstance(ReferenteAfectivo instance) {
		this.instance = instance;
	}

//	public List<ReferenteAfectivo> getList() {
//		return legajoMB.getInstance().getReferentesAfectivos();
//	}

	public void setList(List<ReferenteAfectivo> list) {
		this.list = list;
	}

//    public void referenteSeleccionado() {
//        this.getLegajoMB().getInstance().getReferentesAfectivos().add(getInstance());
//        this.instance=new ReferenteAfectivo();
//    }
    
	public LegajoManagedBean getLegajoMB() {
		return legajoMB;
	}

	public void setLegajoMB(LegajoManagedBean legajoMB) {
		this.legajoMB = legajoMB;
	}

    
}
