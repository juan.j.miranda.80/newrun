package ar.gob.run.spring.model;

import java.io.Serializable;

public enum TipoAlertaMPE implements Serializable{
	
	PER45(45),
	PER90(90),
	PER120(120),
	PER150(150),
	CESE180(180);
	
	private String descripcion;
	private Integer dias;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	private TipoAlertaMPE(Integer dias) {
		this.descripcion = "vencidos " + dias + " días";
		this.dias = dias;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}
	
	
	
}
