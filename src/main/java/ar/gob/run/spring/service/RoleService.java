package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.RoleDAO;
import ar.gob.run.spring.model.usuario.Rol;
		  
@Service("RoleService")
@Transactional(readOnly = true)
public class RoleService extends GenericService<Rol>{

	
	@Autowired
	public void setRoleDAO(RoleDAO roleDAO){
		this.setDao(roleDAO);
	}

    public RoleDAO getRoleDAO() {
        return (RoleDAO) this.getDao();
    }

	public List<Rol> getAllActivesWithExtra(Rol elem) {
		return super.getAllActivesWithExtra(Rol.class,elem);
	}
	
	public List<Rol> getAll() {
		return super.getAllActives(Rol.class);
	}
    
}
