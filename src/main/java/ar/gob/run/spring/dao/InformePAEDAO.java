package ar.gob.run.spring.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.legajo.informePAE.InformePAE;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;

@Repository
public class InformePAEDAO extends HibernateDAO<InformePAE,Integer> {

    public List<InformePAE> getListInformePAE(Integer idLegajo, Usuario usuario) {
        Query query = currentSession().createQuery(
        		"select new InformePAE(c.id, c.legajo.id, "
        		+ "	c.legajo.codigo, c.datosAuditoria.fechaAlta,"
        		+ " c.datosAuditoria.usuarioAlta.userName) from InformePAE c "
        		+ " where c.legajo.id = :idLegajo and c.activo = true "
        		+ " and (true = :hasRoleAdmin or c.datosAuditoria.usuarioAlta = :usuario) "
        		+ " order by c.id desc"); 
    	List<InformePAE> list = query
    			.setBoolean("hasRoleAdmin", usuario.hasRole(Rol.ADMIN))
    			.setParameter("usuario", usuario)
        		.setInteger("idLegajo", idLegajo)
        		.list();
        return list;
    }

}
