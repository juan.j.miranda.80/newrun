package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.DerechoAfectado;
import ar.gob.run.spring.service.DerechoAfectadoService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "derechoAfectadoConverter")
public class DerechoAfectadoConverter extends GenericConverter<DerechoAfectado>{

	public DerechoAfectadoConverter() {
		super(DerechoAfectado.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<DerechoAfectado> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{DerechoAfectadoService}", DerechoAfectadoService.class);

		DerechoAfectadoService derechoAfectadoService = (DerechoAfectadoService)vex.getValue(context.getELContext());
		return derechoAfectadoService;
	}

	


}
