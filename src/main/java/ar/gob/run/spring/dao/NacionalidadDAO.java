package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Nacionalidad;
import ar.gob.run.spring.model.Provincia;

@Repository
public class NacionalidadDAO extends HibernateDAO<Nacionalidad,Integer> {

    public List<Nacionalidad> getNacionalidades() {
        List<Nacionalidad> list = currentSession().createQuery("from Nacionalidad p order by p.descripcion").list();
        return list;
    }
}
