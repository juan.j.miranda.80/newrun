package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.CategoriaDAO;
import ar.gob.run.spring.dao.DerivadorDAO;
import ar.gob.run.spring.model.Categoria;
import ar.gob.run.spring.model.Derivador;

@Service("DerivadorService")
@Transactional(readOnly = true)
public class DerivadorService extends GenericService<Derivador> {

	@Autowired
	public void setDerivadorDAO(DerivadorDAO derivadorDAO){	
		this.setDao(derivadorDAO);
	}
	
	public DerivadorDAO getDerivadorDAO() {
		return (DerivadorDAO) this.getDao();
	}
	
	public List<Derivador> getAll() {
		return super.getAllActives(Derivador.class);
	}
	
	public Derivador get(Integer id){
		return this.get(id, Derivador.class);
	}

	public List<Derivador> getAllActivesWithExtra(Derivador elem) {
		return super.getAllActivesWithExtra(Derivador.class, elem);
	}
}
