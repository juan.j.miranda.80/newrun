/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.gob.run.spring.model.usuario.cidi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;

public class Sesion {

    private String HashCookie;
    private UsuarioCIDI UsuarioCidi;
    private String Mensaje;

    public boolean sesionIniciada(HttpServletRequest request) {

        boolean bEncon;

        bEncon = false;

        for (Cookie unaCookie : request.getCookies()) {
            if (unaCookie.getName().equalsIgnoreCase("CiDi")) {
                this.setHashCookie(unaCookie.getValue());

                if (this.getUsuarioCidi() == null) {
                    setMensaje("No se ha encontrado el usuario en CIDI " + getMensaje());
                    return false;
                }

//                if (!this.getUsuarioCidi().getRespuesta().getResultado().equalsIgnoreCase("OK")) {
//                    setMensaje(this.getUsuarioCidi().getRespuesta().getResultado());
//                    return false;
//                }
                
                bEncon = true;

            } else {
                this.setMensaje("No se han encontrado los datos de CIDI");
            }

        }

        return bEncon;

    }

    public void setUsuarioCidi(String userName, String pwd) {

        Pedido ped;
        CalculadorTokken tokk;
        String okkcv;
        UsuarioCIDI u;


        try {
            tokk = new CalculadorTokken();
			okkcv = tokk.NuevoTokken("CLAVE_APP");

            ped = new Pedido();
            ped.setContrasenia(pwd);
            ped.setHashCookie(this.getHashCookie());
            ped.setIdAplicacion(156);
            ped.setTimeStamp(tokk.getTimeStamp());
            ped.setTokenValue(okkcv);

            u = traerUsuario(ped, "https://cuentacidi.cba.gov.ar/api/Usuario/Obtener_Usuario_Aplicacion");
            this.setUsuarioCidi(u);

        } catch (UnsupportedEncodingException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        }

    }

    private UsuarioCIDI traerUsuario(Pedido ped, String sUrlApi) {

        byte[] contenido;
        DataOutputStream wr;
        BufferedReader in;
        String linea;
        StringBuilder resp;
        HttpsURLConnection con;
        URL dirApi;
        Gson gson;
        UsuarioCIDI u;
        String sPedido;

        u = null;

        try {

            gson = new Gson();
            sPedido = gson.toJson(ped);
            contenido = sPedido.getBytes("UTF-8");
            dirApi = new URL(sUrlApi);
            con = (HttpsURLConnection) dirApi.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "es-AR");

            con.setRequestProperty("Content-type", "application/json");
            con.setRequestProperty("Content-Length", "" + Integer.toString(contenido.length));

            con.setDoOutput(true);
            wr = new DataOutputStream(con.getOutputStream());
            wr.write(contenido);
            wr.flush();
            wr.close();

            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            resp = new StringBuilder();

            while ((linea = in.readLine()) != null) {
                resp.append(linea);
            }
            in.close();

            u = gson.fromJson(resp.toString(), UsuarioCIDI.class);

        } catch (IOException ex) {
            Logger.getLogger(Sesion.class.getName()).log(Level.SEVERE, null, ex);
            setMensaje(ex.getMessage());
        }

        return u;

    }

    /**
     * @return the HashCookie
     */
    public String getHashCookie() {
        return HashCookie;
    }

    /**
     * @param HashCookie the HashCookie to set
     */
    public void setHashCookie(String HashCookie) {
        this.HashCookie = HashCookie;
    }

    /**
     * @return the UsuarioCidi
     */
    public UsuarioCIDI getUsuarioCidi() {
        return UsuarioCidi;
    }

    /**
     * @param UsuarioCidi the UsuarioCidi to set
     */
    public void setUsuarioCidi(UsuarioCIDI UsuarioCidi) {
        this.UsuarioCidi = UsuarioCidi;
    }

    /**
     * @return the Mensaje
     */
    public String getMensaje() {
        return Mensaje;
    }

    /**
     * @param Mensaje the Mensaje to set
     */
    public void setMensaje(String Mensaje) {
        this.Mensaje = Mensaje;
    }

}
