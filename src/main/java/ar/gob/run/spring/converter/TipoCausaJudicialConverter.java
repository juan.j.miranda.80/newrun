package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.TipoCausaJudicial;
import ar.gob.run.spring.model.legajo.CausaJudicial;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.TipoCausaJudicialService;

@FacesConverter(value = "tipoCausaJudicialConverter")
public class TipoCausaJudicialConverter extends GenericConverter<TipoCausaJudicial>{

	public TipoCausaJudicialConverter() {
		super(TipoCausaJudicial.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<TipoCausaJudicial> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{TipoCausaJudicialService}", TipoCausaJudicialService.class);

		TipoCausaJudicialService service = (TipoCausaJudicialService)vex.getValue(context.getELContext());
		return service;
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), TipoCausaJudicial.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((TipoCausaJudicial) value).getId().toString();
	}


}
