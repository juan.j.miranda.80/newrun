package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;

@Repository
public class TipoVinculoFamiliarDAO  extends HibernateDAO<TipoVinculoFamiliar, Integer>{
	    
}