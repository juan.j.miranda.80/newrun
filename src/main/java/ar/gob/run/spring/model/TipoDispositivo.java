package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="T_TIPO_DISPOSITIVO")
public class TipoDispositivo implements Serializable, Identificable{
	
	public static Integer EST_ESP_APREH = 1;
	public static Integer DISP_MED_PEN_TERR = 2;
	public static Integer EST_RESTR_LIB = 3;
	public static Integer EQ_ESP_GUARD_POL = 4;
	public static Integer EST_PRIV_LIB = 5;
	
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String descripcion;
    private Integer order;
   
    @Id
    @Column(name="ID_TIPO_DISPOSITIVO")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", desc : ").append(getDescripcion());
        return strBuff.toString();
    }

    

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Identificable)obj).getId());
	}

	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name="ORDEN")
	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

}
