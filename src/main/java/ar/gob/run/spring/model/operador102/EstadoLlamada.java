package ar.gob.run.spring.model.operador102;

public enum EstadoLlamada {

	RECIBIDA_POR_102("Recibida por 102"), ACEPTADA_POR_ZONAL("Aceptada por Equipo Territorial Provincial");
	
	private String descripcion;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String description) {
		this.descripcion = description;
	}

	private EstadoLlamada(String description) {
		this.descripcion = description;
	}
	
	
	
}
