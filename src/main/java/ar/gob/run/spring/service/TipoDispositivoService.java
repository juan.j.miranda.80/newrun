package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.TipoDispositivoDAO;
import ar.gob.run.spring.model.TipoDispositivo;

@Service("TipoDispositivoService")
@Transactional(readOnly = true)
public class TipoDispositivoService extends GenericService<TipoDispositivo> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setTipoDispositivoDAO(TipoDispositivoDAO dao){	
		this.setDao(dao);
	}
	
	public TipoDispositivoDAO getTipoDispositivoDAO() {
		return (TipoDispositivoDAO) this.getDao();
	}
	
	public List<TipoDispositivo> getAll() {
		return getTipoDispositivoDAO().getTiposDispositivos();
	}
	
	public TipoDispositivo get(Integer id){
		return this.get(id, TipoDispositivo.class);
	}

}
