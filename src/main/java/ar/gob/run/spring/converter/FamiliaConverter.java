package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Familia;
import ar.gob.run.spring.service.FamiliaService;
import ar.gob.run.spring.service.GenericService;

@FacesConverter(value = "familiaConverter")
public class FamiliaConverter extends GenericConverter<Familia>{

	public FamiliaConverter() {
		super(Familia.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Familia> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{FamiliaService}", FamiliaService.class);

		FamiliaService service = (FamiliaService)vex.getValue(context.getELContext());
		return service;
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), Familia.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) return "";
		return ((Familia) value).getId().toString();
	}


}
