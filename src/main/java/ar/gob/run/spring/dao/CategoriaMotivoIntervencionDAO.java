package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.CategoriaMotivoIntervencion;

@Repository
public class CategoriaMotivoIntervencionDAO extends HibernateDAO<CategoriaMotivoIntervencion,Integer> {

    public List<CategoriaMotivoIntervencion> getCategoriasMotivoIntervencion() {
        List<CategoriaMotivoIntervencion> list = currentSession().createQuery("from CategoriaMotivoIntervencion p order by p.descripcion").list();
        return list;
    }
}
