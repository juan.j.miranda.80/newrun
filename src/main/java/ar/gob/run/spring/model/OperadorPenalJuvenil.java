package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.CategoriaTipoIntervencion;
import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;

@Entity
@Table(name="T_OP_PEN_JUV")
public class OperadorPenalJuvenil extends ObjetoABM implements  Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String descripcion;
	
	private TipoDispositivo tipoDispositivo;
	private Municipio departamento;
	private Localidad localidad;
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_OP_PEN_JUV")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="DESCRIPCION")
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_DISPOSITIVO")
	public TipoDispositivo getTipoDispositivo() {
		return tipoDispositivo;
	}
	
	public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
		this.tipoDispositivo = tipoDispositivo;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MUNICIPIO")	
	public Municipio getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(Municipio departamento) {
		this.departamento = departamento;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LOCALIDAD")
	public Localidad getLocalidad() {
		return localidad;
	}
	
	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}
	
	

}
