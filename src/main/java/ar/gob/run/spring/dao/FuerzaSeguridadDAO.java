package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.FuerzaSeguridad;

@Repository
public class FuerzaSeguridadDAO extends HibernateDAO<FuerzaSeguridad,Integer> {

}
