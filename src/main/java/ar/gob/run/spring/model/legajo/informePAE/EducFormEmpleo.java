package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.NivelEducacion;
import ar.gob.run.spring.model.SiNo;
import ar.gob.run.spring.model.legajo.EstudioCursado;

@Entity
@Table(name="T_IPAE_EDUC_FORM_EMPLEO")
public class EducFormEmpleo implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private SiNo 			asisteEstabEducativo = SiNo.N;
	private NivelEducacion 	nivelEducacion = NivelEducacion.SIN_VALOR;
	private EstudioCursado	maximoNivelAlcTrimestre = null;
	private SiNo			asisteFormProfesional = SiNo.N;
	private SiNo			tieneActividadLaboral = SiNo.N;
	private SiNo			esRemunerada = SiNo.N;
	private SiNo			esFormal = SiNo.N;
	private SiNo			trabajaMasDe30H = SiNo.N;
	private SiNo			accedioPorPEgr = SiNo.N;
	private String			principalesDificultades = "";
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_EDUC_FORM_EMPLEO")
	public Integer getId() {
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ASIST_ESTAB_EDUC")
	public SiNo getAsisteEstabEducativo() {
		return asisteEstabEducativo;
	}

	public void setAsisteEstabEducativo(SiNo asisteEstabEducativo) {
		this.asisteEstabEducativo = asisteEstabEducativo;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="NIVEL_EDUCACION")
	public NivelEducacion getNivelEducacion() {
		return nivelEducacion;
	}

	public void setNivelEducacion(NivelEducacion nivelEducacion) {
		this.nivelEducacion = nivelEducacion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MAX_ESTO_CURS_TRIM")
	public EstudioCursado getMaximoNivelAlcTrimestre() {
		return maximoNivelAlcTrimestre;
	}

	public void setMaximoNivelAlcTrimestre(EstudioCursado maximoNivelAlcTrimestre) {
		this.maximoNivelAlcTrimestre = maximoNivelAlcTrimestre;
	}

	
	@Enumerated(EnumType.STRING)
	@Column(name="ASIST_FORM_PROF")
	public SiNo getAsisteFormProfesional() {
		return asisteFormProfesional;
	}

	public void setAsisteFormProfesional(SiNo asisteFormProfesional) {
		this.asisteFormProfesional = asisteFormProfesional;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_ACT_LAB")
	public SiNo getTieneActividadLaboral() {
		return tieneActividadLaboral;
	}

	public void setTieneActividadLaboral(SiNo tieneActividadLaboral) {
		this.tieneActividadLaboral = tieneActividadLaboral;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ES_REMUNERADA")
	public SiNo getEsRemunerada() {
		return esRemunerada;
	}

	public void setEsRemunerada(SiNo esRemunerada) {
		this.esRemunerada = esRemunerada;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ES_FORMAL")
	public SiNo getEsFormal() {
		return esFormal;
	}

	public void setEsFormal(SiNo esFormal) {
		this.esFormal = esFormal;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="TRABAJA_MAS_DE_30H")
	public SiNo getTrabajaMasDe30H() {
		return trabajaMasDe30H;
	}

	public void setTrabajaMasDe30H(SiNo trabajaMasDe30H) {
		this.trabajaMasDe30H = trabajaMasDe30H;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ACC_POR_EGR")
	public SiNo getAccedioPorPEgr() {
		return accedioPorPEgr;
	}

	public void setAccedioPorPEgr(SiNo accedioPorPEgr) {
		this.accedioPorPEgr = accedioPorPEgr;
	}

	@Column(name="PPALES_DIFIC")
	public String getPrincipalesDificultades() {
		return principalesDificultades;
	}

	public void setPrincipalesDificultades(String principalesDificultades) {
		this.principalesDificultades = principalesDificultades;
	}
	
	
}
