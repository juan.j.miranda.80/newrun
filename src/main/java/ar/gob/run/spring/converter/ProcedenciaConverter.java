package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Procedencia;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.ProcedenciaService;

@FacesConverter(value = "procedenciaConverter")
public class ProcedenciaConverter extends GenericConverter<Procedencia>{

	public ProcedenciaConverter() {
		super(Procedencia.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<Procedencia> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{ProcedenciaService}", ProcedenciaService.class);

		ProcedenciaService service = (ProcedenciaService)vex.getValue(context.getELContext());
		return service;
	}

	


}
