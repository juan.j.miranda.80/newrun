package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.legajo.TipoOrganoJudicial;

@Repository
public class TipoOrganoJudicialDAO  extends HibernateDAO<TipoOrganoJudicial, Integer>{
	    
}