package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.ProcedenciaDAO;
import ar.gob.run.spring.model.Procedencia;

@Service("ProcedenciaService")
@Transactional(readOnly = true)
public class ProcedenciaService extends GenericService<Procedencia> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setProcedenciaDAO(ProcedenciaDAO dao){	
		this.setDao(dao);
	}
	
	public ProcedenciaDAO getProcedenciaDAO() {
		return (ProcedenciaDAO) this.getDao();
	}
	
	public List<Procedencia> getAll() {
		return super.getAllActives(Procedencia.class);
	}
	
	public Procedencia get(Integer id){
		return this.get(id, Procedencia.class);
	}

}
