package ar.gob.run.spring.model;

import java.util.Date;

public interface Intervencion extends Comparable<Intervencion> {

//	Integer id;
//	Legajo legajo;
//	Zonal zonal;
//	Local local;
//	Date fecha;
//	Derivador derivador;
//	TipoIntervencion tipo;
//	MotivoIntervencion motivo;
//	
//	public Intervencion(IntervencionIntegral mpi) {
//		this.id = mpi.getId();
//		this.legajo = mpi.getLegajo();
//		this.zonal = mpi.getZonal();
//		this.local = mpi.getLocal();
//		this.fecha = mpi.getFecha();
//		this.derivador = mpi.getDerivador();
//		this.tipo = mpi.getTipoIntervencion();
//		this.motivo = mpi.getMotivoIntervencion();
//	}
//	
//	public Intervencion(IntervencionExcepcional mpe){
//		this.id = mpe.getId();
//		this.legajo = mpe.getLegajo();
//		this.zonal = mpe.getZonal();
//		this.fecha = mpe.getFecha();
//		this.derivador = mpe.getDerivador();
//		this.tipo = mpe.getTipoIntervencion();
//		this.motivo = mpe.getMotivoIntervencion();
//	}
	
	public Integer getId();
	
	public Legajo getLegajo();

	public Zonal getZonal();

	public Local getLocal();
	
	public Date getFecha();
	
	public Derivador getDerivador();
	
	public TipoIntervencion getTipoIntervencion();
	
	public MotivoIntervencion getMotivoIntervencion();
	
}
