package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.MotivoIntervencionDAO;
import ar.gob.run.spring.model.CategoriaMotivoIntervencion;
import ar.gob.run.spring.model.MotivoIntervencion;
		  
@Service("MotivoIntervencionService")
@Transactional(readOnly = true)
public class MotivoIntervencionService extends GenericService<MotivoIntervencion>{

	
	@Autowired
	public void setMotivoIntervencionDAO(MotivoIntervencionDAO motivoIntervencionDAO){
		this.setDao(motivoIntervencionDAO);
	}

    public MotivoIntervencionDAO getMotivoIntervencionDAO() {
        return (MotivoIntervencionDAO) this.getDao();
    }

	public List<MotivoIntervencion> getAllActivesWithExtra(MotivoIntervencion elem) {
		return super.getAllActivesWithExtra(MotivoIntervencion.class, elem);
	}

	public List<MotivoIntervencion> getAll() {
		return super.getAllActives(MotivoIntervencion.class);
	}

	public List<MotivoIntervencion> getAllByCatego(CategoriaMotivoIntervencion categoTpo) {
		return getMotivoIntervencionDAO().getAllByCatego(categoTpo);
	}
}
