package ar.gob.run.spring.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Derivador;
import ar.gob.run.spring.model.EstadoSupervInterv;
import ar.gob.run.spring.model.Intervencion;
import ar.gob.run.spring.model.IntervencionIntegral;
import ar.gob.run.spring.model.IntervencionMEP;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.MotivoIntervencion;
import ar.gob.run.spring.model.Sexo;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;
import ar.gob.run.spring.model.Zonal;

@Repository
public class IntervencionMEPDAO  extends HibernateDAO<IntervencionMEP, Integer>{

	public List<IntervencionMEP> getList(Integer legajo) {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionMEP(e.id, e.fecha, e.fechaIngreso, e.fechaEgreso, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName, e.operadorPenalJuvenil.id, e.operadorPenalJuvenil.descripcion) "
                		+ " from IntervencionMEP e where e.legajo.id=? and e.activo=true order by e.fecha desc")
                .setParameter(0, legajo).list();
        return list;
	}
	
	public List<IntervencionMEP> getList() {
        List list = getSessionFactory().getCurrentSession()
                .createQuery("select new IntervencionMEP(e.id, e.fecha, e.fechaIngreso, e.fechaEgreso, "
                		+ "e.legajo.id, e.legajo.codigo, e.legajo.apellidoYNombre,"
                		+ " e.tipoIntervencion.descripcion, e.archivoAdjunto, "
                		+ " e.datosAuditoria.usuarioAlta.userName, opju.id, opju.descripcion) "
                		+ " from IntervencionMEP e "
                		+ " LEFT OUTER JOIN e.operadorPenalJuvenil AS opju "
                		+ " where e.activo=true order by "
                		+ "	case when (e.fechaIngreso != null) then e.fechaIngreso "
                		+ " when ( e.fecha != null) then e.fecha "
                		+ " when ( e.fechaEgreso != null) then e.fechaEgreso end desc")
                .list();
        return list;
	}

}