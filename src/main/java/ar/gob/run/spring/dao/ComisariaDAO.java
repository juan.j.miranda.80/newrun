package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Comisaria;

@Repository
public class ComisariaDAO extends HibernateDAO<Comisaria,Integer> {

}
