package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.EstadoCivilDAO;
import ar.gob.run.spring.model.legajo.EstadoCivil;
		  
@Service("EstadoCivilService")
@Transactional(readOnly = true)
public class EstadoCivilService extends GenericService<EstadoCivil>{

	
	@Autowired
	public void setEstadoCivilDAO(EstadoCivilDAO estadoCivilDAO){
		this.setDao(estadoCivilDAO);
	}

    public EstadoCivilDAO getEstadoCivilDAO() {
        return (EstadoCivilDAO) this.getDao();
    }

	public List<EstadoCivil> getAllActivesWithExtra(EstadoCivil elem) {
		return super.getAllActivesWithExtra(EstadoCivil.class, elem);
	}

    
}
