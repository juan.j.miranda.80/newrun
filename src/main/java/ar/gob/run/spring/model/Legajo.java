package ar.gob.run.spring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import ar.gob.run.spring.model.legajo.CausaJudicial;
import ar.gob.run.spring.model.legajo.EstadoDocumento;
import ar.gob.run.spring.model.legajo.EstadoEscolaridad;
import ar.gob.run.spring.model.legajo.RelacionVincular;
import ar.gob.run.spring.model.legajo.TipoProblematicaSalud;
import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;



@Entity
@Table(name="T_LEGAJO")
public class Legajo extends ObjetoABM implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private String codigo;
    private String apellidoYNombre;
    private Sexo sexo;
    private Date fechaNacimiento;
    private Zonal zonal;
    private Local local;
    
    private EstadoDocumento estadoDoc;
    private String tipoDocumento;
    private String nroDocumento;
    private Boolean constanciaDeParto = false;
    private Boolean partidaNacimiento = false;
    private Boolean seIntervEnSuGestion = false;
    private Nacionalidad nacionalidad;
    private Nacionalidad paisNacimiento;
    
    private String telefono;
    private String direccion;
    
    private String telefonoLegajo;
    private String calleLegajo;
    private Provincia provincia;
    private Municipio municipio;
    private Localidad localidad;
    private String numeroCalle;
    private String email;
    
    private List<RelacionVincular> relacionesVinculares;
    
    private EstadoDesconocePoseeONo estadoLegajoCausaJudicial;
    private List<CausaJudicial> causasJudiciales;
    
    private EstadoDesconocePoseeONo estadoObraSocial;
    private ObraSocial obraSocial;
    private String observacionObraSocial;
    
    private EstadoDesconoceSIoNO estadoProblSalud;
    private String detalleProblSalud;
    
    private EstadoEscolaridad escolarizado = EstadoEscolaridad.SD;
    private NivelEducacion nivelEducacion = NivelEducacion.SIN_VALOR;
    private String establecimientoEscolarizado;
    
    private String observaciones;
    private String referenciaLlamada102;
    
    private List<IntervencionIntegral> intervencionesMPI;
    private List<IntervencionExcepcional> intervencionesMPE;
    private List<IntervencionMEP> intervencionesMEP;
    private List<IntervencionPAE> intervencionesPAE;
    
    private Acompaniante referentePAE;
    
    private String filePartidaNacimiento;
    private String fileDNI;
    
    private TipoProblematicaSalud problematicaSalud = TipoProblematicaSalud.SIN_VALOR;
    private String fileProblematicaSalud;
    
    private EstadoSupervInterv ultimoEstado;
    
    @Transient
    private TipoVinculoFamiliar tipoVinculoFamiliar;
    
    @Transient
    private Long cantMPI = new Long(0);
    
    @Transient
    private Long cantMPE = new Long(0);
    
    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_LEGAJO")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", apellido : ").append(getApellidoYNombre());
        return strBuff.toString();
    }

    @Column(name="CODIGO")
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name="APELLIDO_Y_NOMBRE")
	public String getApellidoYNombre() {
		if (this.apellidoYNombre!=null) this.setApellidoYNombre(this.apellidoYNombre.toUpperCase());
		return apellidoYNombre;
	}

	public void setApellidoYNombre(String apellidoYNombre) {
		if (apellidoYNombre!=null) apellidoYNombre = apellidoYNombre.toUpperCase();
		this.apellidoYNombre = apellidoYNombre;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="SEXO")
	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	@Column(name="FECHA_NACIMIENTO")	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LOCAL")
	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO_DOCUMENTO")
	public EstadoDocumento getEstadoDoc() {
		if (estadoDoc==null)
			this.estadoDoc = EstadoDocumento.SE_DESCONOCE;
		return estadoDoc;
	}

	public void setEstadoDoc(EstadoDocumento estadoDoc) {
		this.estadoDoc = estadoDoc;
	}

	@Column(name="TIPO_DOCUMENTO")
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Column(name="NRO_DOCUMENTO")
	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_NACIONALIDAD", columnDefinition="integer")
	public Nacionalidad getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Nacionalidad nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PAIS_NAC", columnDefinition="integer")
	public Nacionalidad getPaisNacimiento() {
		return paisNacimiento;
	}

	public void setPaisNacimiento(Nacionalidad paisNacimiento) {
		this.paisNacimiento = paisNacimiento;
	}
	
	@Type(type= "org.hibernate.type.NumericBooleanType")
	@Column(name="CONSTANCIA_PARTO", nullable=true)
	public Boolean getConstanciaDeParto() {
		return constanciaDeParto;
	}

	public void setConstanciaDeParto(Boolean constanciaDeParto) {
		this.constanciaDeParto = constanciaDeParto;
	}

	@Type(type= "org.hibernate.type.NumericBooleanType")
	@Column(name="PARTIDA_NACIMIENTO", nullable=true)
	public Boolean getPartidaNacimiento() {
		return partidaNacimiento;
	}

	public void setPartidaNacimiento(Boolean partidaNacimiento) {
		this.partidaNacimiento = partidaNacimiento;
	}

	@Type(type= "org.hibernate.type.NumericBooleanType")
	@Column(name="SE_INTERV_EN_SU_GESTION", nullable=true)
	public Boolean getSeIntervEnSuGestion() {
		return seIntervEnSuGestion;
	}

	public void setSeIntervEnSuGestion(Boolean seIntervEnSuGestion) {
		this.seIntervEnSuGestion = seIntervEnSuGestion;
	}

	@Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name="DIRECCION")
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_PROVINCIA")
	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MUNICIPIO")
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LOCALIDAD")
	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	@Column(name="NUMERO_CALLE")	
	public String getNumeroCalle() {
		return numeroCalle;
	}

	public void setNumeroCalle(String numeroCalle) {
		this.numeroCalle = numeroCalle;
	}

	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	@Column(name="TELEFONO_LEGAJO")
	public String getTelefonoLegajo() {
		return telefonoLegajo;
	}

	public void setTelefonoLegajo(String telefonoLegajo) {
		this.telefonoLegajo = telefonoLegajo;
	}

	@Column(name="CALLE_LEGAJO")
	public String getCalleLegajo() {
		return calleLegajo;
	}

	public void setCalleLegajo(String calleLegajo) {
		this.calleLegajo = calleLegajo;
	}

	@Column(name="OBSERVACIONES")
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LEGAJO", referencedColumnName="ID_LEGAJO")
	@Fetch(value = FetchMode.SUBSELECT)
	public List<RelacionVincular> getRelacionesVinculares() {
		return relacionesVinculares;
	}

	public void setRelacionesVinculares(List<RelacionVincular> relacionesVinculares) {
		this.relacionesVinculares = relacionesVinculares;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO_CAUSA_JUDICIAL")
	public EstadoDesconocePoseeONo getEstadoLegajoCausaJudicial() {
		if (estadoLegajoCausaJudicial == null)
			this.estadoLegajoCausaJudicial = EstadoDesconocePoseeONo.SE_DESCONOCE;
		return estadoLegajoCausaJudicial;
	}

	public void setEstadoLegajoCausaJudicial(EstadoDesconocePoseeONo estadoLegajoCausaJudicial) {
		this.estadoLegajoCausaJudicial = estadoLegajoCausaJudicial;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LEGAJO", referencedColumnName="ID_LEGAJO")
	@Fetch(value = FetchMode.SUBSELECT)
	public List<CausaJudicial> getCausasJudiciales() {
		return causasJudiciales;
	}

	public void setCausasJudiciales(List<CausaJudicial> causasJudiciales) {
		this.causasJudiciales = causasJudiciales;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO_OBRA_SOCIAL")
	public EstadoDesconocePoseeONo getEstadoObraSocial() {
		if (this.estadoObraSocial==null)
			this.estadoObraSocial = EstadoDesconocePoseeONo.SE_DESCONOCE;
		return estadoObraSocial;
	}

	public void setEstadoObraSocial(EstadoDesconocePoseeONo estadoObraSocial) {
		this.estadoObraSocial = estadoObraSocial;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_OBRA_SOCIAL", columnDefinition="integer")
	public ObraSocial getObraSocial() {
		return obraSocial;
	}

	public void setObraSocial(ObraSocial obraSocial) {
		this.obraSocial = obraSocial;
	}

	@Column(name="OBSERV_OBRA_SOCIAL")	
	public String getObservacionObraSocial() {
		return observacionObraSocial;
	}

	public void setObservacionObraSocial(String observacionObraSocial) {
		this.observacionObraSocial = observacionObraSocial;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO_PROBL_SALUD")
	public EstadoDesconoceSIoNO getEstadoProblSalud() {
		if (this.estadoProblSalud == null) 
			this.estadoProblSalud = EstadoDesconoceSIoNO.SE_DESCONOCE;
		return estadoProblSalud;
	}

	public void setEstadoProblSalud(EstadoDesconoceSIoNO estadoProblSalud) {
		this.estadoProblSalud = estadoProblSalud;
	}

	@Column(name="DETALLE_PROBL_SALUD")
	public String getDetalleProblSalud() {
		return detalleProblSalud;
	}

	public void setDetalleProblSalud(String detalleProblSalud) {
		this.detalleProblSalud = detalleProblSalud;
	}

	@Column(name="REFERENCIA_LLAMADA102")
	public String getReferenciaLlamada102() {
		return referenciaLlamada102;
	}

	public void setReferenciaLlamada102(String referenciaLLamada102) {
		this.referenciaLlamada102 = referenciaLLamada102;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LEGAJO", referencedColumnName="ID_LEGAJO")
	@Fetch(value = FetchMode.SUBSELECT)
	@Where(clause="activo=1")
	@OrderBy(clause="fecha desc")
	public List<IntervencionIntegral> getIntervencionesMPI() {
		return intervencionesMPI;
	}

	public void setIntervencionesMPI(List<IntervencionIntegral> intervencionesMPI) {
		this.intervencionesMPI = intervencionesMPI;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LEGAJO", referencedColumnName="ID_LEGAJO")
	@Fetch(value = FetchMode.SUBSELECT)
	@Where(clause="activo=1")
	@OrderBy(clause="fecha desc")
	public List<IntervencionExcepcional> getIntervencionesMPE() {
		return intervencionesMPE;
	}

	public void setIntervencionesMPE(List<IntervencionExcepcional> intervenionesMPE) {
		this.intervencionesMPE = intervenionesMPE;
	}
	
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LEGAJO", referencedColumnName="ID_LEGAJO")
	@Fetch(value = FetchMode.SUBSELECT)
	@Where(clause="activo=1")
	@OrderBy(clause="fecha desc")
	public List<IntervencionMEP> getIntervencionesMEP() {
		return intervencionesMEP;
	}

	public void setIntervencionesMEP(List<IntervencionMEP> intervenionesMEP) {
		this.intervencionesMEP = intervenionesMEP;
	}
	
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LEGAJO", referencedColumnName="ID_LEGAJO")
	@Fetch(value = FetchMode.SUBSELECT)
	@Where(clause="activo=1")
	@OrderBy(clause="fecha desc")
	public List<IntervencionPAE> getIntervencionesPAE() {
		return intervencionesPAE;
	}

	public void setIntervencionesPAE(List<IntervencionPAE> intervencionesPAE) {
		this.intervencionesPAE = intervencionesPAE;
	}
	
	@Transient
	public EstadoSupervInterv getUltimoEstado(){		
		return this.ultimoEstado;
	}
	
	@Transient
	public TipoVinculoFamiliar getTipoVinculoFamiliar(){		
		return this.tipoVinculoFamiliar;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ACOMPANIANTE")
	public Acompaniante getReferentePAE() {
		return referentePAE;
	}

	public void setReferentePAE(Acompaniante referente) {
		this.referentePAE = referente;
	}

	@Transient
	public String getLblRelVincular(){
		return this.nroDocumento + " - " + this.apellidoYNombre + " - " + this.codigo;
	}

	@Column(name="FILE_PARTIDA_NAC")
	public String getFilePartidaNacimiento() {
		return filePartidaNacimiento;
	}

	public void setFilePartidaNacimiento(String filePartidaNacimiento) {
		this.filePartidaNacimiento = filePartidaNacimiento;
	}

	@Column(name="FILE_DNI")
	public String getFileDNI() {
		return fileDNI;
	}

	public void setFileDNI(String fileDNI) {
		this.fileDNI = fileDNI;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="PROBL_SALUD")
	public TipoProblematicaSalud getProblematicaSalud() {
		return problematicaSalud;
	}

	public void setProblematicaSalud(TipoProblematicaSalud problematicaSalud) {
		this.problematicaSalud = problematicaSalud;
	}

	@Column(name="FILE_PROBLEMATICA_SALUD")
	public String getFileProblematicaSalud() {
		return fileProblematicaSalud;
	}

	public void setFileProblematicaSalud(String fileProblematicaSalud) {
		this.fileProblematicaSalud = fileProblematicaSalud;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="ESCOLARIZADO")
	public EstadoEscolaridad getEscolarizado() {
		if (escolarizado==null) escolarizado = EstadoEscolaridad.SD;
		return escolarizado;
	}

	public void setEscolarizado(EstadoEscolaridad escolarizado) {
		this.escolarizado = escolarizado;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="NIVEL_EDUCACION")	
	public NivelEducacion getNivelEducacion() {
		return nivelEducacion;
	}

	public void setNivelEducacion(NivelEducacion nivelEducacion) {
		this.nivelEducacion = nivelEducacion;
	}

	@Column(name="ESTABLECIMIENTO_ESCOLARIZADO")
	public String getEstablecimientoEscolarizado() {
		return establecimientoEscolarizado;
	}

	public void setEstablecimientoEscolarizado(String establecimientoEscolarizado) {
		this.establecimientoEscolarizado = establecimientoEscolarizado;
	}

	public Legajo(Integer id, Zonal zonal) {
		super();
		this.id = id;
		this.zonal = zonal;
	}

	public Legajo() {
		super();
	}

	public Legajo(Integer id, String codigo, String apYN, String dni, TipoVinculoFamiliar rel){
		this(id,codigo, apYN, dni, "");
		this.tipoVinculoFamiliar = rel; 
	}
	
	public Legajo(Integer id, String codigo, String apYN, String dni, String tel){
		super();
		this.id = id;
		this.codigo = codigo;
		this.apellidoYNombre = apYN;
		this.nroDocumento = dni;
		this.telefono = tel;
	}
	
	public Legajo(Integer id, String codigo, String apYN, String dni, String tel, List<IntervencionExcepcional> intsMPE){
		
		super();
		this.id = id;
		this.codigo = codigo;
		this.apellidoYNombre = apYN;
		this.nroDocumento = dni;
		this.telefono = tel;
		this.intervencionesMPE = intsMPE;
	}
	
	public Legajo(Integer id, String codigo, String apYN, String dni, String tel, EstadoSupervInterv estadoSuperv){
		super();
		this.id = id;
		this.codigo = codigo;
		this.apellidoYNombre = apYN;
		this.nroDocumento = dni;
		this.telefono = tel;
		this.ultimoEstado = estadoSuperv;
	}
	
	//Constructor para rptLegajo
	public Legajo(Integer id, String codig, String apYN, DatosAuditoria datAud, Sexo sexo, 
			Date fechaNac, String tipoDoc, String nroDoc, Zonal zonal, Local local, Long cantMPI,
			Long cantMPE){
		super();
		this.id = id;
		this.codigo = codig;
		this.apellidoYNombre = apYN;
		this.setDatosAuditoria(datAud);
		this.sexo = sexo;
		this.fechaNacimiento = fechaNac;
		this.tipoDocumento = tipoDoc;
		this.nroDocumento = nroDoc;
		this.zonal = zonal;
		this.local = local;
		this.cantMPI = cantMPI;
		this.cantMPE = cantMPE;
	}

	@Transient
	public Long getCantMPI() {
		return cantMPI;
	}

	@Transient
	public Long getCantMPE() {
		return cantMPE;
	}

	public void setCantMPI(Long cantMPI) {
		this.cantMPI = cantMPI;
	}

	public void setCantMPE(Long cantMPE) {
		this.cantMPE = cantMPE;
	}

	
	
}
