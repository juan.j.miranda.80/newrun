package ar.gob.run.spring.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.criterion.Order;



@Entity
@Table(name="T_LOCAL")
public class Local extends ObjetoABM implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String nombre;
    private String codigo;
    private Boolean convenio = false;
    private Boolean servicioConstituido = false;
    private String direccion;
    private String telefono;
    private String celGuardia;
    private String email;
    private String observacion;
    
   
    private Zonal zonal = new Zonal();
    
//    private DatosAuditoria datosAuditoria = new DatosAuditoria();
    
    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_LOCAL")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", userName : ").append(getNombre());
        return strBuff.toString();
    }

    @Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    @Column(name="CODIGO")
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

    @Column(name="DIRECCION")
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

    @Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

    @Column(name="CEL_GUARDIA")
	public String getCelGuardia() {
		return celGuardia;
	}

	public void setCelGuardia(String celGuardia) {
		this.celGuardia = celGuardia;
	}

    @Column(name="EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    @Column(name="OBSERVACION")
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ID_ZONAL", referencedColumnName="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}

	@Column(name="CONVENIO")
	public Boolean getConvenio() {
		return convenio;
	}

	public void setConvenio(Boolean convenio) {
		this.convenio = convenio;
	}

	@Column(name="SERVICIO_CONSTITUIDO")
	public Boolean getServicioConstituido() {
		return servicioConstituido;
	}

	public void setServicioConstituido(Boolean servicioConstituido) {
		this.servicioConstituido = servicioConstituido;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return false;
		return this.getId().equals(((Local)obj).getId());
	}
	
}

