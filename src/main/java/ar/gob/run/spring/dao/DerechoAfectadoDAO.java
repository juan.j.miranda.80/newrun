package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.DerechoAfectado;

@Repository
public class DerechoAfectadoDAO extends HibernateDAO<DerechoAfectado,Integer> {

    public List<DerechoAfectado> getDerechosAfectados() {
        List<DerechoAfectado> list = currentSession().createQuery("from DerechoAfectado p order by p.descripcion").list();
        return list;
    }
}
