package ar.gob.run.spring.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.institucion.Institucion;
import ar.gob.run.spring.model.usuario.Usuario;

@Entity
@Table(name="T_INTERV_PROT_EXCEP")
public class IntervencionExcepcional extends ObjetoABM implements Serializable, Intervencion{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Zonal zonal;
	private Date fecha;
	private Derivador derivador;
	private TipoIntervencion tipoIntervencion;
	private MotivoIntervencion motivoIntervencion;
	private String informe;
	private Legajo legajo;
	private EstadoSupervInterv estadoSuperv;
	private Date fechaUltCambioEstado;
	private String archivoAdjunto;
	
	private Familia familia;
	private Institucion institucion;
	private String txtFamiliaAmpliada;
	private TipoAmbitoCumplimiento ambitoCumplimiento;
	
	private String observacionMedida;
	
	@Override
	public void setId(Integer id) {
		this.id=id;
	}

    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_INTERV_PROT_EXCEP")
	@Override
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}

	@Column(name="FECHA")
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_DERIVADOR")
	public Derivador getDerivador() {
		return derivador;
	}

	public void setDerivador(Derivador derivador) {
		this.derivador = derivador;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_INTERVENCION")
	public TipoIntervencion getTipoIntervencion() {
		return tipoIntervencion;
	}

	public void setTipoIntervencion(TipoIntervencion tipoIntervencion) {
		this.tipoIntervencion = tipoIntervencion;
	}

	@Lob
	@Column(columnDefinition="text", name="INFORME")
	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_LEGAJO")
	public Legajo getLegajo() {
		return legajo;
	}

	public void setLegajo(Legajo legajo) {
		this.legajo = legajo;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_MOTIVO_INTERVENCION")
	public MotivoIntervencion getMotivoIntervencion() {
		return motivoIntervencion;
	}

	public void setMotivoIntervencion(MotivoIntervencion motivoIntervencion) {
		this.motivoIntervencion = motivoIntervencion;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="ESTADO_SUPERV", nullable = true)
	public EstadoSupervInterv getEstadoSuperv() {
		return estadoSuperv;
	}

	public void setEstadoSuperv(EstadoSupervInterv estadoSuperv) {
		this.fechaUltCambioEstado = new Date();
		this.estadoSuperv = estadoSuperv;
	}

	@Override
	public int compareTo(Intervencion o) {
		if ( this.fecha!= null && o.getFecha()!= null && this.fecha.compareTo(o.getFecha()) != 0) return this.fecha.compareTo(o.getFecha());
		if (this.tipoIntervencion!=null && o.getTipoIntervencion()!=null && this.tipoIntervencion.compareTo(o.getTipoIntervencion()) != 0) return this.tipoIntervencion.compareTo(o.getTipoIntervencion()) ;
		if (this.legajo!=null && o.getLegajo()!=null && this.legajo.getCodigo()!=null && o.getLegajo().getCodigo()!=null &&
				this.legajo.getCodigo().compareTo(o.getLegajo().getCodigo())!=0)
			return this.legajo.getCodigo().compareTo(o.getLegajo().getCodigo());
		return this.getId().compareTo(o.getId());
	}

	@Transient
	@Override
	public Local getLocal() {
		return null;
	}

	@Column(name="ARCHIVO_ADJUNTO")
	public String getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(String archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_FAMILIA")
	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_INSTITUCION")
	public Institucion getInstitucion() {
		return institucion;
	}

	public void setInstitucion(Institucion institucion) {
		this.institucion = institucion;
	}

	@Column(name="TXT_FLIA_AMPLIADA")
	public String getTxtFamiliaAmpliada() {
		return txtFamiliaAmpliada;
	}

	public void setTxtFamiliaAmpliada(String txtFamiliaAmpliada) {
		this.txtFamiliaAmpliada = txtFamiliaAmpliada;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="AMBITO_CUMPLIMIENTO", nullable = true)
	public TipoAmbitoCumplimiento getAmbitoCumplimiento() {
		return ambitoCumplimiento;
	}

	public void setAmbitoCumplimiento(TipoAmbitoCumplimiento tipoAmbitoCumplimiento) {
		this.ambitoCumplimiento = tipoAmbitoCumplimiento;
	}

	@Column(name="OBSERVACION_MEDIDA")
	public String getObservacionMedida() {
		return observacionMedida;
	}

	public void setObservacionMedida(String observacionMedida) {
		this.observacionMedida = observacionMedida;
	}

	@Column(name="FECHA_ULT_CMBIO_EST")
	public Date getFechaUltCambioEstado() {
		return fechaUltCambioEstado;
	}

	public void setFechaUltCambioEstado(Date fechaUltCambioEstado) {
		this.fechaUltCambioEstado = fechaUltCambioEstado;
	}

	public IntervencionExcepcional() {
		super();
	}
	
	public IntervencionExcepcional(Integer id, Date fecha, Integer idLegajo,
			String codigo,
			String nombre, String tpoInterv, EstadoSupervInterv estadoSup,
			String archivo, String usrAlta){
		super();
		this.fecha = fecha;
		this.id = id;
		this.legajo = new Legajo();
		this.legajo.setCodigo(codigo);
		this.legajo.setApellidoYNombre(nombre);
		this.setTipoIntervencion(new TipoIntervencion());
		this.getTipoIntervencion().setDescripcion(tpoInterv);
		this.setEstadoSuperv(estadoSup);
		this.archivoAdjunto = archivo;
		this.getDatosAuditoria().setUsuarioAlta(new Usuario());
		this.getDatosAuditoria().getUsuarioAlta().setUserName(usrAlta);
		this.legajo.setId(idLegajo);
		
	}
	
}
