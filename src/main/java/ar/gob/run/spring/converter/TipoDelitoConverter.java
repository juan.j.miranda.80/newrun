package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.TipoDelito;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.TipoDelitoService;

@FacesConverter(value = "tipoDelitoConverter")
public class TipoDelitoConverter extends GenericConverter<TipoDelito>{

	public TipoDelitoConverter() {
		super(TipoDelito.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<TipoDelito> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{TipoDelitoService}", TipoDelitoService.class);

		TipoDelitoService service = (TipoDelitoService)vex.getValue(context.getELContext());
		return service;
	}

	


}
