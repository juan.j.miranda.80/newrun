package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.FuerzaSeguridadDAO;
import ar.gob.run.spring.model.FuerzaSeguridad;

@Service("FuerzaSeguridadService")
@Transactional(readOnly = true)
public class FuerzaSeguridadService extends GenericService<FuerzaSeguridad> {

	private static final long serialVersionUID = 1L;

	@Autowired
	public void setFuerzaSeguridadDAO(FuerzaSeguridadDAO dao){	
		this.setDao(dao);
	}
	
	public FuerzaSeguridadDAO getFuerzaSeguridadDAO() {
		return (FuerzaSeguridadDAO) this.getDao();
	}
	
	public List<FuerzaSeguridad> getAll() {
		return super.getAllActives(FuerzaSeguridad.class);
	}
	
	public FuerzaSeguridad get(Integer id){
		return this.get(id, FuerzaSeguridad.class);
	}

}
