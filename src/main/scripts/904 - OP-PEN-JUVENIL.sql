  CREATE TABLE "T_OP_PEN_JUV" 
   (	"ID_OP_PEN_JUV" NUMBER(10,0) NOT NULL ENABLE, 
	"DESCRIPCION" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
  "FECHA_ALTA" DATE, 
	"ID_USUARIO_ALTA" NUMBER(10,0), 
	"FECHA_MODIFICACION" DATE, 
	"ID_USUARIO_MODIF" NUMBER(10,0), 
	"ACTIVO" NUMBER(1,0), 
	 CONSTRAINT "PK_OP_PEN_JUV" PRIMARY KEY ("ID_OP_PEN_JUV"),
   CONSTRAINT "FK_OP_PEN_JUV_ALTA" FOREIGN KEY ("ID_USUARIO_ALTA")
	  REFERENCES "T_USUARIO" ("ID_USUARIO"), 
	 CONSTRAINT "FK_OP_PEN_JUV_MODIF" FOREIGN KEY ("ID_USUARIO_MODIF")
    REFERENCES "T_USUARIO" ("ID_USUARIO")
   ) ;
