package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.AcompanianteDAO;
import ar.gob.run.spring.model.Acompaniante;

@Service("AcompanianteService")
@Transactional(readOnly = true)
public class AcompanianteService extends GenericService<Acompaniante> {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	public void setAcompanianteDAO(AcompanianteDAO acompanianteDAO){	
		this.setDao(acompanianteDAO);
	}
	
	public AcompanianteDAO getAcompanianteDAO() {
		return (AcompanianteDAO) this.getDao();
	}
	
	public List<Acompaniante> getAll() {
		return super.getAllActives(Acompaniante.class);
	}
	
	public Acompaniante get(Integer id){
		return this.get(id, Acompaniante.class);
	}

	public List<Acompaniante> getAllActivesWithExtra(Acompaniante elem) {
		return super.getAllActivesWithExtra(Acompaniante.class, elem);
	}

	public List<Acompaniante> getAcompaniantesByFilterWithoutExtra(String query, Acompaniante o) {
		return getAcompanianteDAO().getAcompaniantesByFilter(query);
	}

	public List<Acompaniante> getAcompaniantesByFilter(String query) {
		return getAcompanianteDAO().getAcompaniantesByFilter(query);
	}
}
