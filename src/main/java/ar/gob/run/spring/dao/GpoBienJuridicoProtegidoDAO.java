package ar.gob.run.spring.dao;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.GpoBienJuridicoProtegido;

@Repository
public class GpoBienJuridicoProtegidoDAO extends HibernateDAO<GpoBienJuridicoProtegido,Integer> {

}
