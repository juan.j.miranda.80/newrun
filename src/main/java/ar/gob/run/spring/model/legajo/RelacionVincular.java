package ar.gob.run.spring.model.legajo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.Persona;

@Entity
@Table(name="T_RELACION_VINCULAR")
public class RelacionVincular implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private TipoVinculoFamiliar tipoVinculoFamiliar;
	private Boolean poseeLegajo;
	private Boolean representanteLegal = false;
	
	private Persona familiar = new Persona();

	public Legajo legajoFamiliar;

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_RELACION_VINCULAR")
	public Integer getId() {
		return this.id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_VINCULO_FAMILIAR")
	public TipoVinculoFamiliar getTipoVinculoFamiliar() {
		return tipoVinculoFamiliar;
	}

	public void setTipoVinculoFamiliar(TipoVinculoFamiliar tipoVinculoFamiliar) {
		this.tipoVinculoFamiliar = tipoVinculoFamiliar;
	}


	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_LEGAJO_FAMILIAR")
	public Legajo getLegajoFamiliar() {
		return legajoFamiliar;
	}

	public void setLegajoFamiliar(Legajo legajoFamiliar) {
		this.legajoFamiliar = legajoFamiliar;
	}
	
	@Column(name="POSEE_LEGAJO")
	public Boolean getPoseeLegajo() {
		return poseeLegajo;
	}
	public void setPoseeLegajo(Boolean poseeLegajo) {
		this.poseeLegajo = poseeLegajo;
	}
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_PERSONA")
	public Persona getFamiliar() {
		return familiar;
	}

	public void setFamiliar(Persona familiar) {
		this.familiar = familiar;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return super.equals(obj);
		return this.getId().equals(((Identificable)obj).getId());
	}
	
	@Type(type= "org.hibernate.type.NumericBooleanType")
	@Column(name="REPRESENTANTE_LEGAL")
	public Boolean getRepresentanteLegal() {
		return representanteLegal;
	}

	public void setRepresentanteLegal(Boolean representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
}
