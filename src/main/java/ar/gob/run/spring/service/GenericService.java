package ar.gob.run.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.HibernateDAO;
import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.security.MySecUser;

public class GenericService<T> {

	private HibernateDAO<T,Integer> dao;
	static final Logger logger = Logger.getLogger(GenericService.class);
	
	@Transactional
	public void save(T dom) {
		MySecUser usr = (MySecUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer id = ((Identificable)dom).getId();
		logger.info("id: " + 
				(id==null?"-1":id) + 
				" || usuario: " + usr.getUsername() + 
						" || accion: " + (id==null?"creando":"modificando" + " " + dom.toString()) );
		dao.update(dom);
	}
	
	public void delete(T dom){
		if (dom instanceof ObjetoABM){
			((ObjetoABM)dom).setActivo(false);
			this.save(dom);
		}else{
			dao.remove(dom);
		}
	}
	

	public List<T> getAllActivesWithExtra(Class type, T elem) {
		
		List<T> list = dao.list(type); 
		if (elem!= null && 
				elem instanceof ObjetoABM && !((ObjetoABM)elem).getActivo() &&
			((ObjetoABM)elem).getId()!=null)
				if (elem!=null && !list.contains(elem)){
					List<T> newList = new ArrayList<T>();
					newList.add(elem);
					newList.addAll(list);
					list = newList;
				}
		return list;		
	}
	
	public List<T> getAllActives(Class type) {
		return dao.list(type);
	}

	public T get(Integer id, Class type) {
		return (T) dao.find(id, type);
	}

	public HibernateDAO<T, Integer> getDao() {
		return dao;
	}

	public void setDao(HibernateDAO<T, Integer> tDao) {
		this.dao = tDao;
	}

}
