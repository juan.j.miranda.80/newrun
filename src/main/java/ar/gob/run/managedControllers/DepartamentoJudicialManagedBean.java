package ar.gob.run.managedControllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.springframework.dao.DataAccessException;

import ar.gob.run.spring.model.DepartamentoJudicial;
import ar.gob.run.spring.service.DepartamentoJudicialService;

@ManagedBean(name="departamentoJudicialMB")
@ViewScoped
public class DepartamentoJudicialManagedBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value="#{DepartamentoJudicialService}")
    DepartamentoJudicialService service;

    List<DepartamentoJudicial> list;

    DepartamentoJudicial instance;
    
    Boolean editMode = false;
    
    public void save() {
        try {
            getService().save(instance);
            editMode = false;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        editMode = false;
    }

	public void edit(DepartamentoJudicial ent){
    	this.instance = ent;
    	editMode = true;
    }
    
    public void delete(DepartamentoJudicial ent){
    	getService().delete(ent);
    	editMode = false;
    }
    
    public void reset() {
    	this.instance = new DepartamentoJudicial();
        editMode = true;
    }
    
    public void cancel(){
    	this.instance = new DepartamentoJudicial();
    	editMode = false;
    }

	public DepartamentoJudicial getInstance() {
		return instance;
	}

	public void setInstance(DepartamentoJudicial instance) {
		this.instance = instance;
	}

	public DepartamentoJudicialService getService() {
		return service;
	}

	public void setService(DepartamentoJudicialService service) {
		this.service = service;
	}

	public List<DepartamentoJudicial> getList() {
		return getService().getAllActives(DepartamentoJudicial.class);
	}

	public void setList(List<DepartamentoJudicial> list) {
		this.list = list;
	}

	public Boolean getEditMode() {
		return editMode;
	}
	
}
