package ar.gob.run.spring.model.legajo.informePAE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.SiNo;

@Entity
@Table(name="T_IPAE_DDHH_FORM_CIUD")
public class DDHHYFormCiud implements Identificable, Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;

	private SiNo			realizoChDDHHYFormCiud = SiNo.N;
	private SiNo			participaEnOrg = SiNo.N;
	private String			ppalesDificultades = "";

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	@GeneratedValue(generator = "increment")
	@Column(name="ID_IPAE_DDHH_FORM_CIUD")
	public Integer getId() {
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="REALI_CHAR_DDHH_FC")
	public SiNo getRealizoChDDHHYFormCiud() {
		return realizoChDDHHYFormCiud;
	}

	public void setRealizoChDDHHYFormCiud(SiNo realizoChDDHHYFormCiud) {
		this.realizoChDDHHYFormCiud = realizoChDDHHYFormCiud;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="PARTIC_ORG")
	public SiNo getParticipaEnOrg() {
		return participaEnOrg;
	}

	public void setParticipaEnOrg(SiNo participaEnOrg) {
		this.participaEnOrg = participaEnOrg;
	}

	@Column(name="PPALES_DIF")
	public String getPpalesDificultades() {
		return ppalesDificultades;
	}

	public void setPpalesDificultades(String ppalesDificultades) {
		this.ppalesDificultades = ppalesDificultades;
	}
	
	
}
