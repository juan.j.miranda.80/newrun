package ar.gob.run.spring.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Provincia;

@Repository
public class ProvinciaDAO extends HibernateDAO<Provincia,Integer> {

    public List<Provincia> getProvincias() {
        List<Provincia> list = currentSession().createQuery("from Provincia p order by p.nombre").list();
        return list;
    }
}
