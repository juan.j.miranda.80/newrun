package ar.gob.run.spring.model.usuario;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;



@Entity
@Table(name="T_USUARIO")
public class Usuario implements Serializable{
	
    private Integer id;
    private String userName;
    private String password;
    private Boolean activo;
    public Date fechaAlta;
	private Rol rol;
	
	private Zonal zonal;
	
	private Local local;
	
	
    
    public Usuario() {
    	this.zonal=new Zonal();
    	this.local = new Local();
    	this.rol = new Rol();
    	this.activo = true;
	}
    
    public Usuario(Rol rol, String password) {
    	this.zonal=new Zonal();
    	this.local = new Local();
    	this.rol = rol;
    	this.activo = true;
    	this.password = password;
	}

	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_USUARIO")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name="USERNAME")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String name) {
        this.userName = name;
    }
    
    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append("id : ").append(getId());
        strBuff.append(", userName : ").append(getUserName());
        return strBuff.toString();
    }

    @OneToOne(fetch = FetchType.EAGER)  
    @JoinTable(name="T_USUARIO_ROL",  
    joinColumns={@JoinColumn(name="ID_USUARIO", referencedColumnName="ID_USUARIO")},  
    inverseJoinColumns={@JoinColumn(name="ID_ROL", referencedColumnName="ID_ROL")})  
	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
    
	public Boolean hasRole(String role){
		return this.rol!=null && this.rol.getNombre()!=null &&
				this.rol.getNombre().equals(role);
	}
	
	public Boolean hasRole(Integer rol){
		return this.rol!=null && 
				this.rol.getId()!=null && this.rol.getId().equals(rol);
	}

    @Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    @Column(name="ACTIVO")
	public Boolean getActivo() {
    	if (activo==null)this.activo = false;
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_ZONAL")
	public Zonal getZonal() {
		return zonal;
	}

	public void setZonal(Zonal zonal) {
		this.zonal = zonal;
	}

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="ID_LOCAL")
	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	@Column(name="FECHA_ALTA")
	public Date getFechaAlta() {
		return fechaAlta;
	}
	
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
}
