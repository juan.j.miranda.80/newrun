package ar.gob.run.spring.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import ar.gob.run.spring.model.Legajo;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.model.TipoCircuito;
import ar.gob.run.spring.model.TipoIntervencion;

@Repository
public class TipoIntervencionDAO  extends HibernateDAO<TipoIntervencion, Integer>{

	@Autowired
	Environment environment;
	
	@Value("${tipoIntervencion.idSolicitaMPE}")
	private String idSolicitaMPE;
	
	@Value("${tipoIntervencion.idPAEInicial}")
	private String idPAEInicial;
	
	@Value("${tipoIntervencion.idIngresoMEP}")
	private String idIngresoMEP;
	
	public List<TipoIntervencion> getByTipoCircuito(TipoIntervencion elem, TipoCircuito tipoCircuito) {
		List<TipoIntervencion> result = currentSession().createQuery(" from TipoIntervencion t where "
				+ "t.activo=TRUE and "
				+ "t.tipoCircuito = :tipoCircuito ")
			.setParameter("tipoCircuito", tipoCircuito).list();
		
		if (elem!= null && 
				elem instanceof ObjetoABM && !((ObjetoABM)elem).getActivo() &&
			((ObjetoABM)elem).getId()!=null)
				if (elem!=null && !result.contains(elem)){
					List<TipoIntervencion> newList = new ArrayList<TipoIntervencion>();
					newList.add(elem);
					newList.addAll(result);
					result = newList;
				}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TipoIntervencion> getTipoIntervencionByFilter(String strQuery) {
		return getTipoIntervencionByFilter(strQuery, null, null, null, null);
	}

	public List<TipoIntervencion> getTipoIntervencionByFilter(String strQuery, 
			TipoCircuito tipoCirc,
			Boolean soloCese, Boolean soloInicia, Boolean soloIncPAE) {
		Query query = currentSession().createQuery("from TipoIntervencion z where "
				+ "(upper(z.descripcion) like :query) "
				+ "and z.activo = TRUE "
				+ (tipoCirc!=null?"and z.tipoCircuito = :tpoCirc ":"")
				+ (soloCese!=null && soloCese?" and z.intervencionCese = TRUE ":"")
				+ (soloInicia!=null && soloInicia?" and z.id = :idSoloInicia ":" and z.id != :idSoloInicia ")
				+ (soloIncPAE!=null && soloIncPAE?" and z.id = :idSoloIncPAE ":"")
				+ "order by z.descripcion asc").
				setParameter("query", "%"+strQuery.toUpperCase()+"%");
		
		if (tipoCirc!=null)
			query.setParameter("tpoCirc", tipoCirc);
		
//		if (soloInicia!=null && soloInicia)
		query.setParameter("idSoloInicia", Integer.valueOf(idSolicitaMPE));
		
		if (soloIncPAE!=null && soloIncPAE)
			query.setParameter("idSoloIncPAE", Integer.valueOf(idPAEInicial));
		
		List<TipoIntervencion> list = query.list();
		return list;
	}

	public List<TipoIntervencion> getList() {
    	List<TipoIntervencion> list = currentSession().createQuery("from TipoIntervencion z "
    			+ "where z.activo = TRUE "
    			+ "order by z.tipoCircuito, "
    			+ "z.categoriaTipoIntervencion.descripcion,"
    			+ "z.descripcion").list();
        return list;
	}

	public List<TipoIntervencion> getTipoIntervencionMEP(boolean soloInicia) {
    	Query query = currentSession().createQuery("from TipoIntervencion z "
    			+ "where z.activo = TRUE " 
    			+ (soloInicia?" and (z.id = :iniciaMEP ) ": " " )
    			+ " and z.tipoCircuito = :tipoCircuito"
    			+ " order by "
    			+ "z.descripcion");
    	if (soloInicia) query.setInteger("iniciaMEP", Integer.valueOf(idIngresoMEP));
    	query.setParameter("tipoCircuito", TipoCircuito.MEP);
    	
        return query.list();
	}
	
	public List<TipoIntervencion> getTipoIntervencionMEP() {
    	Query query = currentSession().createQuery("from TipoIntervencion z "
    			+ "where z.activo = TRUE and z.tipoCircuito = :tipoCircuito"
    			+ " order by "
    			+ "z.descripcion");
    	query.setParameter("tipoCircuito", TipoCircuito.MEP);
    	
        return query.list();
	}
}