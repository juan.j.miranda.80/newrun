package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.MotivoAprehension;
import ar.gob.run.spring.service.GenericService;
import ar.gob.run.spring.service.MotivoAprehensionService;

@FacesConverter(value = "motivoAprehensionConverter")
public class MotivoAprehensionConverter extends GenericConverter<MotivoAprehension>{

	public MotivoAprehensionConverter() {
		super(MotivoAprehension.class );
	}

	private static final long serialVersionUID = 1L;

	@Override
	protected GenericService<MotivoAprehension> getService(FacesContext context) {
		ValueExpression vex =
				context.getApplication().getExpressionFactory()
		                .createValueExpression(context.getELContext(),
		                        "#{MotivoAprehensionService}", MotivoAprehensionService.class);

		MotivoAprehensionService service = (MotivoAprehensionService)vex.getValue(context.getELContext());
		return service;
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), MotivoAprehension.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null || ((MotivoAprehension) value).getId()==null) return "";
		return ((MotivoAprehension) value).getId().toString();
	}


}
