package ar.gob.run.spring.model.operador102;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.Identificable;
import ar.gob.run.spring.model.ObjetoABM;
import ar.gob.run.spring.model.legajo.TipoVinculoFamiliar;

@Entity
@Table(name="T_LL102_CONVIVIENTE")
public class Conviviente extends ObjetoABM implements Identificable, Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	
	private TipoConviviente tipo;
	private String apellido;
	private String nombre;
	private Integer edad;
	private TipoVinculoFamiliar tipoVinculo;
	private String ocupacion;
	private String institucionEducativa;
	private String institucionSanitaria;
	
//	private Llamada102 llamada102;
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_LL102_CONVIVIENTE")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="TIPO")
	public TipoConviviente getTipo() {
		return tipo;
	}
	public void setTipo(TipoConviviente tipo) {
		this.tipo = tipo;
	}
	
	@Column(name="APELLIDO")
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name="EDAD")
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_TIPO_VINCULO_FAMILIAR")
	public TipoVinculoFamiliar getTipoVinculo() {
		return tipoVinculo;
	}
	public void setTipoVinculo(TipoVinculoFamiliar tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}
	
	@Column(name="OCUPACION")
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	
	@Column(name="INSTITUCION_EDUCATIVA")
	public String getInstitucionEducativa() {
		return institucionEducativa;
	}
	public void setInstitucionEducativa(String institucionEducativa) {
		this.institucionEducativa = institucionEducativa;
	}
	
	@Column(name="INSTITUCION_SANITARIA")
	public String getInstitucionSanitaria() {
		return institucionSanitaria;
	}
	public void setInstitucionSanitaria(String institucionSanitaria) {
		this.institucionSanitaria = institucionSanitaria;
	}

//	@ManyToOne(cascade=CascadeType.ALL)
//	@JoinColumn(name="ID_LLAMADA102", referencedColumnName="ID_LLAMADA102")
//	public Llamada102 getLlamada102() {
//		return llamada102;
//	}
//	public void setLlamada102(Llamada102 llamada102) {
//		this.llamada102 = llamada102;
//	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.getId()==null) return super.equals(obj);
		return this.getId().equals(((Identificable)obj).getId());
	}	
}
