package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.service.LocalService;

@FacesConverter(value = "localConverter")
public class LocalConverter implements Converter{

	public LocalConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), Local.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null || ((Local) value).getId()==null) return "";
		return ((Local) value).getId().toString();
	}

	public LocalService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{localService}", LocalService.class);

			LocalService service = (LocalService)vex.getValue(context.getELContext());
			return service;
	}

}
