package ar.gob.run.spring.model;

public enum NivelEducacion {
	
	SIN_VALOR("--Seleccione--","NN"),
	PRIMARIO_INC("Primaria Incompleta", "PRII"), 
	PRIMARIO("Primaria Completa", "PRIC"),
	SECUNDARIO_INC("Secundaria Incompleta","SECI"), 
	SECUNDARIO("Secundaria Completa","SECC"),
	TERCIARIO_INC("Terciario Incompleto","TERI"), 
	TERCIARIO("Terciario Completo","TERC"),
	UNIVERSITARIO_INC("Universitario Incompleto","UNII"), 
	UNIVERSITARIO("Universitario Completo","UNIC"),
	POSGRADO_INC("Posgrado Incompleto","POSI"),
	POSGRADO("Posgrado Completo","POSC"),
	NO_DECLARA("No declara", "ND");
	
	String descripcion;
	String code;

	private NivelEducacion(String descripcion, String code) {
		this.descripcion = descripcion;
		this.code = code;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public static NivelEducacion fromString(String text) {
		if (text==null) return null;
	    for (NivelEducacion b : NivelEducacion.values()) {
	      if (b.code.equalsIgnoreCase(text)) {
	        return b;
	      }
	    }
	    return null;
	  }

}
