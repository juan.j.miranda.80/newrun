package ar.gob.run.spring.model;

public enum SexoAdmision {
	A("Ambos"), M("Masculino"), F("Femenino");
	
	String descripcion;

	private SexoAdmision(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	

}
