package ar.gob.run.spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ar.gob.run.spring.dao.UsuarioDAO;
import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;
import ar.gob.run.spring.security.MySecUser;

@Service("userDetailsService")
public class UsuarioSecureService implements UserDetailsService {

	@Autowired
    UsuarioDAO usuarioDAO;
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Usuario usr = usuarioDAO.getUsuarioByUserName(userName);
		
		List<GrantedAuthority> authorities =
                buildUserAuthority(usr.getRol());
		return buildUserForAuthentication(usr, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Rol userRole) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		setAuths.add(new SimpleGrantedAuthority(userRole.getCode()));

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}
	
	private User buildUserForAuthentication(Usuario user,
			List<GrantedAuthority> authorities) {
			MySecUser usr =  new MySecUser(user.getUserName(), user.getPassword(),
				user.getActivo(), true, true, true, authorities);
			usr.setUsuario(user);
			return usr;
	}
}
