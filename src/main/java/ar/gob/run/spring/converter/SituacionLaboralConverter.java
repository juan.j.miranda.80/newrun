package ar.gob.run.spring.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ar.gob.run.spring.model.legajo.SituacionLaboral;
import ar.gob.run.spring.service.SituacionLaboralService;

@FacesConverter(value = "situacionLaboralConverter")
public class SituacionLaboralConverter implements Converter {

	public SituacionLaboralConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value==null) return "";
		return getService(context).get(Integer.valueOf(value), SituacionLaboral.class);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null ) return "";
		return ((SituacionLaboral) value).getId().toString();
	}

	public SituacionLaboralService getService(FacesContext context) {
			ValueExpression vex =
					context.getApplication().getExpressionFactory()
			                .createValueExpression(context.getELContext(),
			                        "#{SituacionLaboralService}", SituacionLaboralService.class);

			SituacionLaboralService service = (SituacionLaboralService)vex.getValue(context.getELContext());
			return service;
	}

}
