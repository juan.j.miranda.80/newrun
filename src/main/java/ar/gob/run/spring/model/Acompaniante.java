package ar.gob.run.spring.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import ar.gob.run.spring.model.usuario.Rol;
import ar.gob.run.spring.model.usuario.Usuario;

@Entity
@Table(name="T_ACOMPANIANTE")
public class Acompaniante extends ObjetoABM implements Identificable, Serializable{

	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String apellido;
	private String nombre;
	private String dni;
	private Integer edad;
	private String domicilio;
	private String telefono;
	private String email;
	private String latitud;
	private String longitud;
	private NivelEducacion nivelEducacion;
	private String tituloEducacion;
	private ExperienciaAcomp experiencia;
	private SiNo tieneTrabajo = SiNo.N;
	private String descripcionTrabajoActual;
	private SiNo capacitadoEnEgreso = SiNo.N;
	private SiNo subvencPorProgEgreso = SiNo.N;
	private TipoDisponibilidad disponibilidad = TipoDisponibilidad.A;
	private String descripcionPerfil;
	private String fileInformeAdmision;
	private String fileInformeBaja;
	private Usuario usuario;
	
	@Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    @Column(name="ID_ACOMPANIANTE")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="APELLIDO")
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	@Column(name="NOMBRE")
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name="DNI")
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	@Column(name="EDAD", columnDefinition ="INT(3)")
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	@Column(name="DOMICILIO")
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	@Column(name="TELEFONO")
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="LATITUD")
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	@Column(name="LONGITUD")
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="NIVEL_EDUCACION")
	public NivelEducacion getNivelEducacion() {
		return nivelEducacion;
	}
	public void setNivelEducacion(NivelEducacion nivelEducacion) {
		this.nivelEducacion = nivelEducacion;
	}
	
	@Column(name="TITULO_EDUCACION")
	public String getTituloEducacion() {
		return tituloEducacion;
	}
	public void setTituloEducacion(String tituloEducacion) {
		this.tituloEducacion = tituloEducacion;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_EXPERIENCIA_ACOMP")
	public ExperienciaAcomp getExperiencia() {
		return experiencia;
	}
	public void setExperiencia(ExperienciaAcomp experiencia) {
		this.experiencia = experiencia;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="TIENE_TRABAJO")
	public SiNo getTieneTrabajo() {
		return tieneTrabajo;
	}
	public void setTieneTrabajo(SiNo tieneTrabajo) {
		this.tieneTrabajo = tieneTrabajo;
	}
	
	@Column(name="DESCRIPCION_TRABAJO_ACT")
	public String getDescripcionTrabajoActual() {
		return descripcionTrabajoActual;
	}
	public void setDescripcionTrabajoActual(String descripcionTrabajoActual) {
		this.descripcionTrabajoActual = descripcionTrabajoActual;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="CAPACITADO_EN_EGRESO")
	public SiNo getCapacitadoEnEgreso() {
		return capacitadoEnEgreso;
	}
	public void setCapacitadoEnEgreso(SiNo capacitadoEnEgreso) {
		this.capacitadoEnEgreso = capacitadoEnEgreso;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="SUBVENC_POR_PROG_EG")
	public SiNo getSubvencPorProgEgreso() {
		return subvencPorProgEgreso;
	}
	public void setSubvencPorProgEgreso(SiNo subvencPorProgEgreso) {
		this.subvencPorProgEgreso = subvencPorProgEgreso;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="TIPO_DISPONIBILIDAD")
	public TipoDisponibilidad getDisponibilidad() {
		return disponibilidad;
	}
	public void setDisponibilidad(TipoDisponibilidad disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
	
	@Column(name="DESCRIPCION_PERFIL")
	public String getDescripcionPerfil() {
		return descripcionPerfil;
	}
	public void setDescripcionPerfil(String descripcionPerfil) {
		this.descripcionPerfil = descripcionPerfil;
	}
	
	@Column(name="FILE_INF_ADMISION")
	public String getFileInformeAdmision() {
		return fileInformeAdmision;
	}
	public void setFileInformeAdmision(String fileInformeAdmision) {
		this.fileInformeAdmision = fileInformeAdmision;
	}
	
	@Column(name="FILE_INF_BAJA")
	public String getFileInformeBaja() {
		return fileInformeBaja;
	}
	public void setFileInformeBaja(String fileInformeBaja) {
		this.fileInformeBaja = fileInformeBaja;
	}
	
	@Transient
	public String getApellidoYNombre(){
		return apellido + ", " + nombre;
	}
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_USUARIO")
	public Usuario getUsuario() {
		return usuario;
	}	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
