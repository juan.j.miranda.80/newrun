package ar.gob.run.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.CausaJudicialDAO;
import ar.gob.run.spring.model.legajo.CausaJudicial;
		  
@Service("CausaJudicialService")
@Transactional(readOnly = true)
public class CausaJudicialService extends GenericService<CausaJudicial>{

	
	@Autowired
	public void setCausaJudicialDAO(CausaJudicialDAO causaJudicialDAO){
		this.setDao(causaJudicialDAO);
	}

    public CausaJudicialDAO getCausaJudicialDAO() {
        return (CausaJudicialDAO) this.getDao();
    }

}