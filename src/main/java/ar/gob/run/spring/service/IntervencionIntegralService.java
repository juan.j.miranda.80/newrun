package ar.gob.run.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.gob.run.spring.dao.IntervencionIntegralDAO;
import ar.gob.run.spring.model.IntervencionIntegral;
import ar.gob.run.spring.model.Local;
import ar.gob.run.spring.model.Zonal;
		  
@Service("IntervencionIntegralService")
@Transactional(readOnly = true)
public class IntervencionIntegralService extends GenericService<IntervencionIntegral>{

	
	@Autowired
	public void setIntervencionIntegralDAO(IntervencionIntegralDAO intervencionDAO){
		this.setDao(intervencionDAO);
	}

    public IntervencionIntegralDAO getIntervencionIntegralDAO() {
        return (IntervencionIntegralDAO) this.getDao();
    }

    public List<IntervencionIntegral> getList(Integer legajo){
    	return getIntervencionIntegralDAO().getList(legajo);
    }
    
    @Override
    public List<IntervencionIntegral> getAllActives(Class type) {
    	return getIntervencionIntegralDAO().getList();
    }
    
    public List<IntervencionIntegral> getIntervencionesIntByZonal(Zonal zonal){
    	return getIntervencionIntegralDAO().getListByZonal(zonal);
    }
    
    public List<IntervencionIntegral> getIntervencionesIntByLocal(Local local){
    	return getIntervencionIntegralDAO().getListByLocal(local);
    	
    }

	public IntervencionIntegral get(Integer id) {
		return super.get(id, IntervencionIntegral.class);
	}
    
}
